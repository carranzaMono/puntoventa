<?php
session_set_cookie_params(60*60*24*14);
session_start();
require      "../config.php";
include_once "../assets/funciones/template.php";
include_once "../assets/funciones/searches.php";

sessionstatus();
$usu_tipo   = $_SESSION['usu_tipo'];
$emp_nombre = $_SESSION['usu_empleado'];
$nivel      = 0;
$corte      = dameCorteActivo();
$ccj_activo = $corte['clave'];


?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <title><?= C_PLATFORMNAME; ?> - Caja Dashboard</title>
        <meta content="Admin Dashboard" name="description" />
        <meta content="ThemeDesign" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <link rel="shortcut icon" href="../assets/images/favicon.ico">

        <!--Morris Chart CSS -->
        <link rel="stylesheet" href="../assets/plugins/morris/morris.css">

        <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/icons.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/style.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/myStyle.css" rel="stylesheet">
        <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs5/dt-1.10.25/datatables.min.css"/>
        <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
    </head>


    <body class="fixed-left">

        <!-- Loader -->
        <div id="preloader"><div id="status"><div class="spinner"></div></div></div>

        <!-- Begin page -->
        <div id="wrapper">

            <!-- ========== Left Sidebar Start ========== -->
            <div class="left side-menu">
                <button type="button" class="button-menu-mobile button-menu-mobile-topbar open-left waves-effect">
                    <i class="ion-close"></i>
                </button>

                <!-- LOGO -->
                <div class="topbar-left">
                    <div class="text-center">
                        <!--<a href="index.html" class="logo">Admiry</a>-->
                        <a href="index.html" class="logo">
                          <img src="../assets/images/logopv.png" height="42" alt="logo">
                        </a>
                    </div>
                </div>

                <div class="sidebar-inner slimscrollleft">

                    <div class="user-details">
                        <!--div class="text-center">
                            <img src="../assets/images/users/avatar-1.jpg" alt="" class="rounded-circle">
                        </div-->
                        <div class="user-info">
                          <h4 class="font-16 usuario" data-type="<?= $usu_tipo; ?>">
                            <?= $emp_nombre; ?>
                          </h4>
                          <span class="text-muted user-status"><i class="fa fa-dot-circle-o text-success"></i> Online</span>
                        </div>
                    </div>

                    <?php printNavbar($usu_tipo, $nivel) ?>

                    <div class="clearfix"></div>
                </div> <!-- end sidebarinner -->
            </div>
            <!-- Left Sidebar End -->

            <!-- Start right Content here -->

            <div class="content-page">
                <!-- Start content -->
                <div class="content">

                    <!-- Top Bar Start -->
                    <div class="topbar">

                        <nav class="navbar-custom">

                            <ul class="list-inline float-right mb-0">
                                <?php printOptionAvatar($usu_tipo, $nivel); ?>
                            </ul>

                            <ul class="list-inline menu-left mb-0">
                                <li class="list-inline-item">
                                    <button type="button" class="button-menu-mobile open-left waves-effect">
                                        <i class="ion-navicon"></i>
                                    </button>
                                </li>
                                <li class="hide-phone list-inline-item app-search">
                                    <h3 class="page-title">Dashboard</h3>
                                </li>
                            </ul>

                            <div class="clearfix"></div>

                        </nav>

                    </div>
                    <!-- Top Bar End -->

                    <div class="page-content-wrapper ">
                      <div class="container">
                        <div class="row">
                          <div class="col-12">
                            <div class="card m-b-20">
                              <div class="card-block">
                                <h4 class="mt-0 m-b-15 header-title">
                                  Ventas Recientes
                                  <!--button type="button" name="button" class="btn btn-sm btn-primary btn-agregarNueva-venta">Agregar venta</button-->
                                </h4>

                                <table id="dt-ventas" class="table table-hover m-b-0 table-sm">
                                  <thead>
                                    <tr>
                                      <th>Folio</th>
                                      <th>Fecha</th>
                                      <th>Hora</th>
                                      <th>Cliente</th>
                                      <th>Status</th>
                                      <th>Total</th>
                                      <th></th>
                                    </tr>
                                  </thead>
                                </table>
                              </div>
                            </div>
                          </div>
                        </div><!-- end row -->

                        <div class="row">
                          <div class="col-9">
                            <div class="card m-b-20">
                              <div class="card-block">
                                <h4 class="mt-0 m-b-15 header-title">Cortes de caja</h4>

                                <table id="dt-cortesCaja" class="table table-hover m-b-0 table-sm">
                                  <thead>
                                    <tr>
                                      <th>Folio</th>
                                      <th>Incio</th>
                                      <th>Cierre</th>
                                      <th>Entrego</th>
                                      <th>Status</th>
                                      <th>Venta Total</th>
                                      <th></th>
                                    </tr>
                                  </thead>
                                </table>
                              </div>
                            </div>
                          </div>

                          <div class="col-3">
                            <div class="card bg-gray">
                              <div class="card-block">
                                <div class="row">
                                  <div class="col-12 bg-white mb-2">
                                    <div class="row align-items-center">
                                      <div class="col-7 text-center">
                                        <h6 class="">
                                          <i class="fas fa-shopping-cart fa-2x mb-1"></i>
                                        </h6>
                                      </div>
                                      <div class="col-5 text-left">
                                        <div class="content-text-montos my-2">
                                          <h6 class="title-montos">Vendido</h6>
                                          <?php
                                          $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
                                          if (mysqli_connect_errno()) {
                                            echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
                                          }
                                            $sql    = "SELECT concat('$ ', cast( ifnull(sum(dv_total), 0) as decimal(11, 2)  ) )
                                                            from detalleventas, ventas
                                                              where dv_ccj_clave = $ccj_activo
                                                                and dv_ven_clave = ven_clave
                                                                and (ven_status = 1 or ven_status = 2)";
                                            $result = mysqli_query($con, $sql);
                                            if ($result) {
                                              while ($fila = mysqli_fetch_array($result)) {
                                          ?>

                                          <p class=""> <strong><?= $fila[0]; ?></strong> </p>

                                          <?php
                                              }
                                            }
                                          ?>

                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-12 bg-white mb-2">
                                    <div class="row align-items-center">
                                      <div class="col-7 text-center">
                                        <h6 class="">
                                          <i class="fas fa-hand-holding-usd fa-2x mb-1"></i>
                                        </h6>
                                      </div>
                                      <div class="col-5 text-left">
                                        <div class="content-text-montos my-2">
                                          <h6 class="title-montos">Cobrado</h6>
                                          <?php
                                          $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
                                          if (mysqli_connect_errno()) {
                                            echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
                                          }
                                            $sql    = "SELECT concat('$ ', cast(ifnull(sum(cob_monto), 0) as decimal(11, 2)))
                                                            from ventas, cobros
                                                              where ven_status = 2
                                                                and ven_ccj_clave = $ccj_activo
                                                                and cob_ven_clave = ven_clave";
                                            $result = mysqli_query($con, $sql);
                                            if ($result) {
                                              while ($fila = mysqli_fetch_array($result)) {
                                          ?>

                                          <p class=""> <strong><?= $fila[0]; ?></strong> </p>

                                          <?php
                                              }
                                            }
                                          ?>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-12 bg-white mb-2">
                                    <div class="row align-items-center">
                                      <div class="col-7 text-center">
                                        <h6 class="">
                                          <i class="fab fa-creative-commons-nc fa-2x mb-1"></i>
                                        </h6>
                                      </div>
                                      <div class="col-5 text-left">
                                        <div class="content-text-montos my-2">
                                          <h6 class="title-montos">Cancelado</h6>
                                          <?php
                                          $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
                                          if (mysqli_connect_errno()) {
                                            echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
                                          }
                                            $sql    = "SELECT concat('$ ', cast( ifnull(sum(dv_total), 0) as decimal(11, 2)  ) )
                                                            from detalleventas, ventas
                                                              where dv_ccj_clave = $ccj_activo
                                                                and dv_ven_clave = ven_clave
                                                                and (ven_status = 3)";
                                            $result = mysqli_query($con, $sql);
                                            if ($result) {
                                              while ($fila = mysqli_fetch_array($result)) {
                                          ?>

                                          <p class=""> <strong><?= $fila[0]; ?></strong> </p>

                                          <?php
                                              }
                                            }
                                          ?>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>

                          </div>
                        </div><!-- end row -->

                      </div><!-- container -->

                        <?php  include_once "modal.php"; ?>

                   </div> <!-- Page content Wrapper -->
                 </div> <!-- content -->

                <?= printFooter(); ?>

            </div><!-- End Right content here -->

        </div><!-- END wrapper -->

        <!-- jQuery  -->
        <script src="../assets/js/jquery.min.js"></script>
        <script src="../assets/js/tether.min.js"></script><!-- Tether for Bootstrap -->
        <script src="../assets/js/bootstrap.min.js"></script>
        <script src="../assets/js/modernizr.min.js"></script>
        <script src="../assets/js/detect.js"></script>
        <script src="../assets/js/fastclick.js"></script>
        <script src="../assets/js/jquery.slimscroll.js"></script>
        <script src="../assets/js/jquery.blockUI.js"></script>
        <script src="../assets/js/waves.js"></script>
        <script src="../assets/js/jquery.nicescroll.js"></script>
        <script src="../assets/js/jquery.scrollTo.min.js"></script>

        <!--Morris Chart-->
        <script src="../assets/plugins/raphael/raphael-min.js"></script>
        <script src="../assets/js/messages.js"></script>
        <script src="../assets/js/constantes.js"></script>
        <script src="../assets/js/generales.js"></script>
        <script src="../assets/js/dataTablelanguage.js"></script>
        <script src="../assets/ajax/generales/revisar.js"></script>
        <script src="../assets/ajax/generales/ventas/principal.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/v/bs5/dt-1.10.25/datatables.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.0.1/js/bootstrap.bundle.min.js"></script>
        <!-- App js -->
        <script src="../assets/js/app.js"></script>
        <!-- Alertify js -->
        <script src="../assets/plugins/alertify/js/alertify.js"></script>
        <script type="text/javascript">
        $('.search').select2();
        </script>

    </body>
</html>


<!-- modal para abrir caja ==================================================-->
<div class="modal fade bd-example-modal-lg modal-abrir-corte" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Fondo de caja</h5>
        <button type="button" class="close btn-x-close-crearCorte" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="form-fondobox" class="form-fondobox" method="post">
          <div class="form-group">
            <input type="number" step="any" name="fondo" class="form-control fondo" autofocus>
            <small id="help-fondo" class="form-text text-muted text-danger help-fondo"></small>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-close-crearCorte" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary" form="form-fondobox">Guardar</button>
      </div>
    </div>
  </div>
</div>
<!--=========================================================================-->

<!-- modal para indicar que existe corte abierto ============================-->
<div class="modal fade bd-example-modal-lg modal-existe-caja" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"></h5>
        <button type="button" class="close btn-x-close-modal-existecaja" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center">
        <span class="mdi mdi-alert-circle-outline ico-info info"></span>
        <h3>¡Ops...!</h3>
        <p><strong>Ya tiene un corte de caja abierto</strong></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-close-modal-existecaja" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<!--=========================================================================-->

<!-- modal para indicar que existe corte abierto ============================-->
<div class="modal fade bd-example-modal-lg notexists" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"></h5>
        <button type="button" class="close btn-x-close-modal-sincorte" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center">
        <span class="mdi mdi-alert-circle-outline ico-info info"></span>
        <h3>¡Ops...!</h3>
        <p><strong>Debe de tener un corte de caja abierto.</strong></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-close-modal-sincorte" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<!--=========================================================================-->

<!-- modal para cerrar caja =================================================-->
<div class="modal fade bd-example-modal-lg modal-cierre-caja" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Entrega de caja</h5>
        <button type="button" class="close btn-x-close-modal-entregacaja" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="form-entregabox" class="form-fondobox" method="post">
          <div class="form-group">
            <label>Monto*:</label>
            <input type="number" step="any" name="monto" class="form-control entrega" autofocus>
            <small id="help-entrega" class="form-text text-muted text-danger help-entrega"></small>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-close-modal btn-close-modal-entregacaja" data-dismiss="modal">¡No, cerrar!</button>
        <button type="submit" class="btn btn-primary" form="form-entregabox">¡Si, entregar!</button>
      </div>
    </div>
  </div>
</div>
<!--=========================================================================-->

<!-- modal para indicar que existe corte abierto ============================-->
<div class="modal fade bd-example-modal-lg modal-opensale" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"></h5>
        <button type="button" class="close btn-x-close-modal-foliosAbiertos" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center">
        <span class="mdi mdi-alert-circle-outline ico-info info"></span>
        <h4>¡Ops... tiene folios abiertos!</h4>
        <p style="margin-bottom: 5px;" ><strong>Revise el siguiente folio de venta.</strong></p>
        <p class="fv-abiertos"></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-close-modal-foliosAbiertos" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<!--=========================================================================-->

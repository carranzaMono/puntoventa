<?php
session_set_cookie_params(60*60*24*14);
session_start();
include_once "../config.php";
include_once "../assets/funciones/template.php";
include_once "../assets/funciones/searches.php";

sessionstatus();

if (isset($_GET['folio'])) {

  $ccj_folio = $_GET['folio'];
  $corte     = dameDatosCorte($ccj_folio);
  $ccj_clave = $corte['ccj_clave'];

}
$usu_tipo   = $_SESSION['usu_tipo'];
$emp_nombre = $_SESSION['usu_empleado'];
$nivel      = 0;
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <title> <?= C_PLATFORMNAME; ?> - Detalle de Corte de Caja </title>
        <meta content="Admin Dashboard" name="description" />
        <meta content="ThemeDesign" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <link rel="shortcut icon" href="../assets/images/favicon.ico">

        <!--Morris Chart CSS -->
        <link rel="stylesheet" href="../assets/plugins/morris/morris.css">

        <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/icons.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/style.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/myStyle.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs5/dt-1.10.25/datatables.min.css"/>
    </head>


    <body class="fixed-left">

        <!-- Loader -->
        <div id="preloader"><div id="status"><div class="spinner"></div></div></div>

        <!-- Begin page -->
        <div id="wrapper">

            <!-- ========== Left Sidebar Start ========== -->
            <div class="left side-menu">
                <button type="button" class="button-menu-mobile button-menu-mobile-topbar open-left waves-effect">
                    <i class="ion-close"></i>
                </button>

                <!-- LOGO -->
                <div class="topbar-left">
                    <div class="text-center">
                        <!--<a href="index.html" class="logo">Admiry</a>-->
                        <a href="index.php" class="logo">
                          <img src="../assets/images/logo.png" height="42" alt="logo">
                        </a>
                    </div>
                </div>

                <div class="sidebar-inner slimscrollleft">

                    <div class="user-details">
                        <div class="text-center">
                            <img src="../assets/images/users/avatar-1.jpg" alt="" class="rounded-circle">
                        </div>
                        <div class="user-info">
                            <h4 class="font-16 usuario" data-type="<?= $usu_tipo; ?>">
                              <?= $emp_nombre; ?>
                            </h4>
                            <span class="text-muted user-status"><i class="fa fa-dot-circle-o text-success"></i> Online</span>
                        </div>
                    </div>

                    <?php printNavbar($usu_tipo, $nivel) ?>

                    <div class="clearfix"></div>
                </div> <!-- end sidebarinner -->
            </div>
            <!-- Left Sidebar End -->

            <!-- Start right Content here -->

            <div class="content-page">
                <!-- Start content -->
                <div class="content">

                    <!-- Top Bar Start -->
                    <div class="topbar">

                        <nav class="navbar-custom">

                            <ul class="list-inline float-right mb-0">
                              <?php printOptionAvatar($usu_tipo, $nivel); ?>
                            </ul>

                            <ul class="list-inline menu-left mb-0">
                                <li class="list-inline-item">
                                    <button type="button" class="button-menu-mobile open-left waves-effect">
                                        <i class="ion-navicon"></i>
                                    </button>
                                </li>
                                <li class="hide-phone list-inline-item app-search">
                                    <!--h3 class="page-title">Detalle de caja</h3-->
                                </li>
                            </ul>

                            <div class="clearfix"></div>

                        </nav>

                    </div>
                    <!-- Top Bar End -->

                    <div class="page-content-wrapper ">

                        <div class="container">
                          <div class="d-flex">
                            <div class="ml-auto">
                              <nav aria-label="breadcrumb">
                                <ol class="breadcrumb float-right">
                                  <li class="breadcrumb-item"><a href="../">Dashboard</a></li>
                                  <li class="breadcrumb-item">Caja</li>
                                  <li class="breadcrumb-item active" aria-current="page">Detalle del corte</li>
                                </ol>
                              </nav>
                            </div>
                          </div>
                            <div class="row">

                                <div class="col-12">
                                    <div class="card m-b-20">
                                        <div class="card-block">
                                            <h3 class="mt-0 m-b-5 corte header-title d-flex" data-clave="<?= $corte['ccj_clave']; ?>" data-folio="<?= $ccj_folio; ?>">
                                              Corte de Caja
                                              <span class="ml-auto">Folio: <span class="num-folio"><?= $ccj_folio; ?></span></span>
                                            </h3>
                                            <div class="row justify-content-end">
                                              <div class="col-12 col-lg-2">
                                                <button type="button" name="button" class="btn btn-secondary btn-block"> <i class="ion-printer"></i> Imprimir</button>
                                              </div>
                                            </div>
                                            <hr>
                                            <h4 class="title-data">Datos del corte de caja</h4>
                                            <div class="row">
                                              <div class="col-lg-6 col-12">
                                                <p class="info"><strong>Fecha de inicio: </strong><?= $corte['FechaInicio']; ?></p>
                                                <p class="info"><strong>Hora de inicio: </strong><?= $corte['hrInicio']; ?></p>
                                                <p class="info"><strong>Inicio: </strong><?= $corte['emp_abrio']; ?></p>
                                              </div>
                                              <div class="col-lg-6 col-12">
                                                <p class="info"><strong>Fecha de cierre: </strong><?= $corte['FechaFin']; ?></p>
                                                <p class="info"><strong>Hora de cierre: </strong><?= $corte['hrFin']; ?></p>
                                                <p class="info"><strong>Cerro: </strong><?= $corte['emp_cerro']; ?></p>
                                              </div>
                                            </div>
                                            <div class="row mb-1">
                                              <div class="col-12"><hr></div>
                                              <div class="col-12 col-lg-6 border-left">
                                                <h4 class="title-dtEntradas">Detalle de entradas</h4>
                                                <?php
                                                  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
                                                  if (mysqli_connect_errno()) {
                                                    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
                                                  }

                                                  $sql    = "SELECT fp_nombre,
                                                                    (select concat('$ ', cast(ifnull(sum(mc_monto), 0) as decimal(11, 2))) from movcaja where mc_fp_clave = fp_clave and mc_clasificacion = 1 and mc_tipo = 2) as total
                                                                  from formascobropago
                                                                    where fp_status = 1";
                                                  $result = mysqli_query($con, $sql);
                                                  if ($result) {
                                                    while ($fila = mysqli_fetch_array($result)) {
                                                ?>
                                                    <p class="info"><strong><?= $fila[0]; ?>: </strong><?= $fila[1]; ?></p>
                                                <?php
                                                    }
                                                  }
                                                ?>
                                              </div>
                                              <div class="col-12 col-lg-6">
                                                <p class="info"><strong>Cotizaciones pendientes: </strong><?= $corte['cot_pend_monto']; ?></p>
                                                <p class="info"><strong>Cotizaciones efectuadas: </strong><?= $corte['cot_efec_monto']; ?></p>
                                                <p class="info"><strong>Venta directa: </strong><?= $corte['ventatotal']; ?></p>
                                                <p class="info"><strong>Venta total: </strong><?= $corte['ventatotal']; ?></p>
                                                <p class="info"><strong>Entradas: </strong><?= $corte['entradas']; ?></p>
                                                <p class="info"><strong>Salidas: </strong><?= $corte['salidas']; ?></p>
                                                <p class="info"><strong>Diferencia: </strong><?= $corte['diferencia']; ?></p>
                                              </div>
                                            </div>
                                            <div class="row">
                                              <div class="col-12">
                                                <hr>
                                              </div>
                                              <div class="col-12">
                                                <h4 class="title-relacion">Relación de Movimientos del Corte</h4>
                                                <div class="table-responsive">
                                                  <table class="table table-hover table-sm dt-cortecaja">
                                                    <thead>
                                                      <tr>
                                                        <th>#</th>
                                                        <th>Fecha</th>
                                                        <th>Hora</th>
                                                        <th>Descripción</th>
                                                        <th>Forma del movimiento</th>
                                                        <th>Monto</th>
                                                      </tr>
                                                    </thead>
                                                    <tbody>
                                                      <?php
                                                        $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
                                                        if (mysqli_connect_errno()) {
                                                          echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
                                                        }

                                                        $sql    = "SELECT mc_clave, date_format(mc_fecha, '%Y-%m-%d') as fecha, date_format(mc_fecha, '%r') as hr,
                                                                          mc_observacion, fp_nombre, concat('$ ', cast(ifnull(mc_monto, 0) as decimal(11, 2))) as monto
                                                                      from movcaja, formascobropago
                                                                        where mc_ccj_clave = $ccj_clave
                                                                          and mc_fp_clave = fp_clave
                                                                          order by mc_clave desc";
                                                        $result = mysqli_query($con, $sql);
                                                        if ($result) {
                                                          while ($fila = mysqli_fetch_array($result)) {
                                                      ?>
                                                          <tr>
                                                            <td><?= $fila[0]; ?></td>
                                                            <td><?= $fila[1]; ?></td>
                                                            <td><?= $fila[2]; ?></td>
                                                            <td><?= $fila[3]; ?></td>
                                                            <td><?= $fila[4]; ?></td>
                                                            <td><?= $fila[5]; ?></td>
                                                          </tr>
                                                      <?php
                                                          }
                                                        }
                                                      ?>
                                                    </tbody>
                                                  </table>
                                                </div>
                                              </div>
                                              <div class="col-12">
                                                <hr>
                                                <h4 class="title-relacion">Relación de Cotizaciones en el Corte</h4>
                                                <div class="table-responsive">
                                                  <table class="table table-hover table-sm dt-cortecaja">
                                                    <thead>
                                                      <tr>
                                                        <th>#</th>
                                                        <th>Adquisición</th>
                                                        <th>Fecha</th>
                                                        <th>Hora</th>
                                                        <th>Cotización</th>
                                                        <th>Cliente</th>
                                                        <th>Status</th>
                                                        <th>Monto</th>
                                                      </tr>
                                                    </thead>
                                                    <tbody>
                                                      <?php
                                                        $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
                                                        if (mysqli_connect_errno()) {
                                                          echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
                                                        }

                                                        $sql    = "SELECT cot_clave, cot_tipo, date_format(cot_fecha, '%Y-%m-%d') as fecha, date_format(cot_fecha, '%r') as hr,
                                                                          cot_folio, concat('$ ', cast(ifnull(cot_total, 0) as decimal(11, 2))) as monto,
                                                                          (Select cli_nombre from clientes where cot_cli_clave = cli_clave), cot_status
                                                                      from cotizaciones
                                                                        where cot_ccj_clave = $ccj_clave
                                                                          order by cot_folio desc";
                                                        $result = mysqli_query($con, $sql);
                                                        if ($result) {
                                                          while ($fila = mysqli_fetch_array($result)) {
                                                      ?>
                                                          <tr>
                                                            <td><?= $fila[0]; ?></td>
                                                            <td>
                                                              <?php
                                                              switch ($fila[1]) {
                                                                case 1:
                                                                  echo "Cliente existe";
                                                                  break;
                                                                default:
                                                                  echo "Cliente Nuevo";
                                                                  break;
                                                              }
                                                              ?>
                                                            </td>
                                                            <td><?= $fila[2]; ?></td>
                                                            <td><?= $fila[3]; ?></td>
                                                            <td><?= $fila[4]; ?></td>
                                                            <td><?= $fila[6]; ?></td>
                                                            <td>
                                                              <?php
                                                                switch ($fila[7]) {
                                                                  case 0:
                                                                    echo "Cancelada";
                                                                  break;
                                                                  case 1:
                                                                    echo "";
                                                                  break;
                                                                  default:
                                                                    echo "Paso a venta";
                                                                  break;
                                                              }
                                                              ?>
                                                            </td>
                                                            <td><?= $fila[5]; ?></td>
                                                          </tr>
                                                      <?php
                                                          }
                                                        }
                                                      ?>
                                                    </tbody>
                                                  </table>
                                                </div>
                                              </div>
                                              <div class="col-12">
                                                <hr>
                                                <h4 class="title-relacion">Relación de Ventas en el Corte</h4>
                                                <div class="table-responsive">
                                                  <table class="table table-hover table-sm dt-cortecaja">
                                                    <thead>
                                                      <tr>
                                                        <th>#</th>
                                                        <th>Fecha</th>
                                                        <th>Hora</th>
                                                        <th>Venta</th>
                                                        <th>Cliente</th>
                                                        <th>Status</th>
                                                        <th>Monto</th>
                                                      </tr>
                                                    </thead>
                                                    <tbody>
                                                      <?php
                                                        $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
                                                        if (mysqli_connect_errno()) {
                                                          echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
                                                        }

                                                        $sql    = "SELECT ven_clave, ven_tipo, date_format(ven_fecha, '%Y-%m-%d') as fecha, date_format(ven_fecha, '%r') as hr,
                                                                          ven_folio, concat('$ ', cast(ifnull(ven_total, 0) as decimal(11, 2))) as monto,
                                                                          (Select cli_nombre from clientes where ven_cli_clave = cli_clave), ven_status
                                                                      from ventas
                                                                        where ven_ccj_clave = $ccj_clave
                                                                          order by ven_folio desc";
                                                        $result = mysqli_query($con, $sql);
                                                        if ($result) {
                                                          while ($fila = mysqli_fetch_array($result)) {
                                                      ?>
                                                          <tr>
                                                            <td><?= $fila[0]; ?></td>
                                                            <td><?= $fila[2]; ?></td>
                                                            <td><?= $fila[3]; ?></td>
                                                            <td><?= $fila[4]; ?></td>
                                                            <td><?= $fila[6]; ?></td>
                                                            <td>
                                                              <?php
                                                                switch ($fila[7]) {
                                                                  case 0:
                                                                    echo "Cancelada";
                                                                  break;
                                                                  case 1:
                                                                    echo "";
                                                                  break;
                                                                  default:
                                                                    echo "Cerrado";
                                                                  break;
                                                              }
                                                              ?>
                                                            </td>
                                                            <td><?= $fila[5]; ?></td>
                                                          </tr>
                                                      <?php
                                                          }
                                                        }
                                                      ?>
                                                    </tbody>
                                                  </table>
                                                </div>
                                              </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end row -->

                        </div><!-- container -->

                        <?php  include_once "modal.php"; ?>

                    </div> <!-- Page content Wrapper -->

                </div> <!-- content -->

                <?= printFooter(); ?>

            </div>
            <!-- End Right content here -->

        </div>
        <!-- END wrapper -->


        <!-- jQuery  -->
        <script src="../assets/js/jquery.min.js"></script>
        <script src="../assets/js/tether.min.js"></script><!-- Tether for Bootstrap -->
        <script src="../assets/js/bootstrap.min.js"></script>
        <script src="../assets/js/modernizr.min.js"></script>
        <script src="../assets/js/detect.js"></script>
        <script src="../assets/js/fastclick.js"></script>
        <script src="../assets/js/jquery.slimscroll.js"></script>
        <script src="../assets/js/jquery.blockUI.js"></script>
        <script src="../assets/js/waves.js"></script>
        <script src="../assets/js/jquery.nicescroll.js"></script>
        <script src="../assets/js/jquery.scrollTo.min.js"></script>

        <!-- App js -->
        <script src="../assets/js/app.js"></script>
        <script src="../assets/js/constantes.js"></script>
        <script src="../assets/js/generales.js"></script>
        <script src="../assets/ajax/generales/cortecaja.js"></script>
        <script src="../assets/ajax/generales/revisar.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.0.1/js/bootstrap.bundle.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/v/bs5/dt-1.10.25/datatables.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/2.0.1/js/dataTables.buttons.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/2.0.1/js/buttons.bootstrap4.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/2.0.1/js/buttons.html5.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/2.0.1/js/buttons.print.min.js"></script>
        <script src="../assets/js/dataTablelanguage.js"></script>
        <script src="../assets/js/messages.js"></script>

        <!-- Alertify js -->
        <script src="../assets/plugins/alertify/js/alertify.js"></script>

    </body>
</html>

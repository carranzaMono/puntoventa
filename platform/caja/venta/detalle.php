<?php
session_start();
include_once "../../config.php";
include_once "../../assets/funciones/update.php";
include_once "../../assets/funciones/template.php";
include_once "../../assets/funciones/searches.php";

sessionstatus();
$usu_tipo   = $_SESSION['usu_tipo'];
$emp_nombre = $_SESSION['usu_empleado'];
$nivel      = 1;

if (isset($_GET['folio'])) {

  $ven_folio  = $_GET['folio'];
  $venta      = dameDatoVentaFolio($ven_folio);
  $cliente    = dameDatosCliente($venta['ven_cli_clave']);

  //corregirventa($venta['ven_clave']);
}



?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <title><?= C_PLATFORMNAME; ?> - Caja - Venta <?= $ven_folio; ?></title>
        <meta content="Admin Dashboard" name="description" />
        <meta content="ThemeDesign" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <link rel="shortcut icon" href="../../assets/images/favicon.ico">

        <!--Morris Chart CSS -->
        <link rel="stylesheet" href="../../assets/plugins/morris/morris.css">

        <link href="../../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="../../assets/css/icons.css" rel="stylesheet" type="text/css">
        <link href="../../assets/css/style.css" rel="stylesheet" type="text/css">
        <link href="../../assets/css/myStyle.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs5/dt-1.10.25/datatables.min.css"/>
        <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
    </head>


    <body class="fixed-left">

        <!-- Loader -->
        <div id="preloader"><div id="status"><div class="spinner"></div></div></div>

        <!-- Begin page -->
        <div id="wrapper">

            <!-- ========== Left Sidebar Start ========== -->
            <div class="left side-menu">
                <button type="button" class="button-menu-mobile button-menu-mobile-topbar open-left waves-effect">
                    <i class="ion-close"></i>
                </button>

                <!-- LOGO -->
                <div class="topbar-left">
                    <div class="text-center">
                        <!--<a href="index.html" class="logo">Admiry</a>-->
                        <a href="../" class="logo">
                          <img src="../../assets/images/logo.png" height="42" alt="logo">
                        </a>
                    </div>
                </div>

                <div class="sidebar-inner slimscrollleft">

                    <div class="user-details">
                        <div class="text-center">
                            <img src="../../assets/images/users/avatar-1.jpg" alt="" class="rounded-circle">
                        </div>
                        <div class="user-info">
                            <h4 class="font-16 usuario" data-type="<?= $usu_tipo; ?>"><?= $emp_nombre; ?></h4>
                            <span class="text-muted user-status"><i class="fa fa-dot-circle-o text-success"></i> Online</span>
                        </div>
                    </div>

                    <?php printNavbar($usu_tipo, $nivel) ?>

                    <div class="clearfix"></div>
                </div> <!-- end sidebarinner -->
            </div>
            <!-- Left Sidebar End -->

            <!-- Start right Content here -->

            <div class="content-page">
                <!-- Start content -->
                <div class="content">

                    <!-- Top Bar Start -->
                    <div class="topbar">

                        <nav class="navbar-custom">

                            <ul class="list-inline float-right mb-0">
                              <?php printOptionAvatar($usu_tipo, $nivel); ?>
                            </ul>

                            <ul class="list-inline menu-left mb-0">
                                <li class="list-inline-item">
                                    <button type="button" class="button-menu-mobile open-left waves-effect">
                                        <i class="ion-navicon"></i>
                                    </button>
                                </li>
                                <li class="hide-phone list-inline-item app-search">
                                    <h3 class="page-title">Venta</h3>
                                </li>
                            </ul>

                            <div class="clearfix"></div>

                        </nav>

                    </div>
                    <!-- Top Bar End -->

                    <div class="page-content-wrapper ">

                        <div class="container">
                          <div class="d-flex">
                            <div class="ml-auto">
                              <nav aria-label="breadcrumb">
                                <ol class="breadcrumb float-right">
                                  <li class="breadcrumb-item"><a href="../">Dashboard</a></li>
                                  <li class="breadcrumb-item" aria-current="page">Venta</li>
                                  <li class="breadcrumb-item active" aria-current="page"><?= $ven_folio; ?></li>
                                </ol>
                              </nav>
                            </div>
                          </div>
                            <div class="row">

                                <div class="col-12">
                                  <div class="row">
                                    <div class="col-12 col-lg-12">
                                      <div class="card m-b-20 mb-3">
                                        <div class="card-block">
                                          <h3 class="mt-0 m-b-5 venta header-title d-flex" data-clave="<?= $venta['ven_clave']; ?>" data-folio="<?= $ven_folio; ?>">
                                            <a href="../" class="ml-1" data-toggle="tooltip" data-placement="right" title="regresar a la sección anterior"><i class="fas fa-arrow-circle-left"></i></a>
                                            <span class="ml-auto">Folio: <span class="num-folio"><?= $ven_folio; ?></span></span>
                                          </h3>
                                          <hr style="margin-top: 0px;">
                                          <!---form class="form-addproductCode" action="index.html" method="post">
                                            <div class="row">
                                              <div class="col-12 col-lg-12">
                                                <input type="text" name="codigo" class="form-control form-control-lg text-center" autofocus placeholder="Código">
                                                <small id="help-option" class="help-option form-text text-muted text-danger"></small>
                                              </div>
                                            </div>
                                          </form-->

                                          <div class="row">
                                            <div class="col-12 col-lg-4">
                                              <p class="info"><strong>Fecha: </strong><?=$venta['ven_fecha'];?></p>
                                              <p class="info"><strong>Hora: </strong><?=$venta['ven_hora'];?></p>

                                            </div>
                                            <div class="col-12 col-lg-4">
                                              <p class="info"><strong>Cliente: </strong><?=$cliente['cli_nombre'];?></p>
                                              <p class="info"><strong>Status: </strong><span class="status"></span></p>
                                            </div>
                                            <div class="col-12 -col-lg-4">

                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-12 col-lg-12">
                                      <div class="card m-b-20">
                                        <div class="card-block">
                                          <div class="row">
                                            <div class="col-12">
                                              <ul class="nav nav-tabs" id="myTab" role="tablist">
                                                <li class="nav-item">
                                                  <a class="nav-link active btn-search-code" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Buscar por código</a>
                                                </li>
                                                <li class="nav-item">
                                                  <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Buscar por nombre</a>
                                                </li>
                                              </ul>
                                              <div class="tab-content mb-4" id="myTabContent">
                                                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                                  <div class="card">
                                                    <div class="card-body py-2 px-2">
                                                      <form class="form-addproductCodigo mb-2" action="index.html" method="post">
                                                        <div class="row">
                                                          <div class="col-10">
                                                            <label>Código*:</label>
                                                            <input type="text" class="form-control form-control-sm codigo-barra" name="codigo-barra" value="" autofocus>
                                                          </div>
                                                          <div class="col-2">
                                                            <label></label>
                                                            <button class="btn btn-block btn-primary btn-sm mt-2 btn-addfor-code" type="button" name="button"> <span class="ion-ios7-search-strong"></span> Buscar</button>
                                                          </div>
                                                        </div>
                                                      </form>
                                                    </div>
                                                  </div>
                                                </div>
                                                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                                  <div class="card">
                                                    <div class="card-body py-2 px-2">
                                                      <form class="form-addproductName" action="index.html" method="post">
                                                        <div class="row">
                                                          <div class="col-12 col-lg-8">
                                                            <label>Producto*:</label>
                                                            <select class="form-control form-control-sm nombre-producto search" style="width: 100%;" name="producto">
                                                              <option value="">Selecciona el producto</option>
                                                              <?php
                                                                $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
                                                                if (mysqli_connect_errno()) {
                                                                  echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
                                                                }

                                                                $sql    = "SELECT pro_clave, concat(pro_nombre) as pro_nombre
                                                                              from productos
                                                                                where pro_status = 1";
                                                                $result = mysqli_query($con, $sql);
                                                                if ($result) {
                                                                  while ($fila = mysqli_fetch_array($result)) {
                                                              ?>
                                                              <option value="<?=$fila[0];?>"><?=$fila[1];?></option>
                                                              <?php
                                                                  }
                                                                }
                                                                ?>
                                                            </select>
                                                            <small id="helpform2-producto" class="helpform2-producto form-text text-muted text-danger"></small>
                                                          </div>
                                                          <div class="col-12 col-lg-2">
                                                            <label>Cantidad*:</label>
                                                            <input type="number" name="cantidad" class="form-control form-control-sm cantidad" min="0" step="any">
                                                            <small id="helpform2-cantidad" class="helpform2-cantidad form-text text-muted text-danger"></small>
                                                          </div>
                                                          <div class="col-12 col-lg-2">
                                                            <label></label>
                                                            <button type="submit" name="button" class="btn btn-primary waves-effect waves-light btn-block btn-sm mt-2 btn-addfor-name">Agregar</button>
                                                          </div>
                                                        </div>
                                                      </form>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>

                                            <div class="col-12">
                                              <table id="dt-detalle" class="table table-hover m-b-0 table-sm mt-3">
                                                <thead>
                                                  <tr>
                                                    <th></th>
                                                    <th>Descripción</th>
                                                    <th>P.Unitario</th>
                                                    <th>Catidad</th>
                                                    <th>Subtotal</th>
                                                    <th>I.V.A.</th>
                                                    <th>Total</th>
                                                  </tr>
                                                </thead>
                                              </table>
                                              <hr>
                                              <div class="row justify-content-between">
                                                <div class="col-12 col-lg-3">
                                                  <button type="button" class="btn btn-success waves-effect waves-light btn-sm btn-block btn-iracobrar">
                                                    <i class="fas fa-dollar-sign"></i> Cobrar
                                                  </button>
                                                </div>
                                                <div class="col-12 col-lg-9 py-1">
                                                  <div class="row justify-content-end">
                                                    <div class="col-lg-2 col-6 text-right">
                                                      <p><strong>Total   :</strong></p>
                                                    </div>
                                                    <div class="col-lg-2 col-6 text-left">
                                                      <p><span class="text-cantidades total"></span></p>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                          </div>




                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                            </div>
                            <!-- end row -->

                        </div><!-- container -->

                        <?php  include_once "../assets/modal/caja.php"; ?>

                    </div> <!-- Page content Wrapper -->

                </div> <!-- content -->

                <?= printFooter(); ?>

            </div>
            <!-- End Right content here -->

        </div>
        <!-- END wrapper -->


        <!-- jQuery  -->
        <script src="../../assets/js/jquery.min.js"></script>
        <script src="../../assets/js/tether.min.js"></script><!-- Tether for Bootstrap -->
        <script src="../../assets/js/bootstrap.min.js"></script>
        <script src="../../assets/js/modernizr.min.js"></script>
        <script src="../../assets/js/detect.js"></script>
        <script src="../../assets/js/fastclick.js"></script>
        <script src="../../assets/js/jquery.slimscroll.js"></script>
        <script src="../../assets/js/jquery.blockUI.js"></script>
        <script src="../../assets/js/waves.js"></script>
        <script src="../../assets/js/jquery.nicescroll.js"></script>
        <script src="../../assets/js/jquery.scrollTo.min.js"></script>

        <!-- App js -->
        <script src="../../assets/js/app.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.0.1/js/bootstrap.bundle.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/v/bs5/dt-1.10.25/datatables.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
        <script src="../../assets/js/dataTablelanguage.js"></script>
        <!--script src="../../assets/ajax/admin/caja/revisar.js"></script-->
        <script src="../../assets/js/constantes.js"></script>
        <script src="../../assets/js/generales.js"></script>
        <script src="../../assets/ajax/generales/revisar.js"></script>
        <script src="../../assets/ajax/caja/venta/detalle.js"></script>
        <script src="../../assets/js/messages.js"></script>

        <script type="text/javascript">
        $('.search').select2();
        </script>

        <!-- Alertify js -->
        <script src="../../assets/plugins/alertify/js/alertify.js"></script>

    </body>
</html>

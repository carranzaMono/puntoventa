<?php
session_set_cookie_params(60*60*24*14);
session_start();
include_once "../config.php";
include_once "../assets/funciones/template.php";
include_once "../assets/funciones/searches.php";

sessionstatus();
$usu_tipo   = $_SESSION['usu_tipo'];
$emp_nombre = $_SESSION['usu_empleado'];
$nivel      = 0;
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <title> <?= C_PLATFORMNAME; ?> - Caja Registro de Salidas</title>
        <meta content="Admin Dashboard" name="description" />
        <meta content="ThemeDesign" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <link rel="shortcut icon" href="../assets/images/favicon.ico">

        <!--Morris Chart CSS -->
        <link rel="stylesheet" href="../assets/plugins/morris/morris.css">

        <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/icons.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/style.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/myStyle.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs5/dt-1.10.25/datatables.min.css"/>
    </head>


    <body class="fixed-left">

        <!-- Loader -->
        <div id="preloader"><div id="status"><div class="spinner"></div></div></div>

        <!-- Begin page -->
        <div id="wrapper">

            <!-- ========== Left Sidebar Start ========== -->
            <div class="left side-menu">
                <button type="button" class="button-menu-mobile button-menu-mobile-topbar open-left waves-effect">
                    <i class="ion-close"></i>
                </button>

                <!-- LOGO -->
                <div class="topbar-left">
                    <div class="text-center">
                        <!--<a href="index.html" class="logo">Admiry</a>-->
                        <a href="index.php" class="logo">
                          <img src="../assets/images/logo.png" height="42" alt="logo">
                        </a>
                    </div>
                </div>

                <div class="sidebar-inner slimscrollleft">

                    <div class="user-details">
                        <div class="text-center">
                            <img src="../assets/images/users/avatar-1.jpg" alt="" class="rounded-circle">
                        </div>
                        <div class="user-info">
                            <h4 class="font-16 usuario" data-type="<?= $usu_tipo; ?>">
                              <?= $emp_nombre; ?>
                            </h4>
                            <span class="text-muted user-status"><i class="fa fa-dot-circle-o text-success"></i> Online</span>
                        </div>
                    </div>

                    <?php printNavbar($usu_tipo, $nivel) ?>

                    <div class="clearfix"></div>
                </div> <!-- end sidebarinner -->
            </div>
            <!-- Left Sidebar End -->

            <!-- Start right Content here -->

            <div class="content-page">
                <!-- Start content -->
                <div class="content">

                    <!-- Top Bar Start -->
                    <div class="topbar">

                        <nav class="navbar-custom">

                            <ul class="list-inline float-right mb-0">
                              <?php printOptionAvatar($usu_tipo, $nivel); ?>
                            </ul>

                            <ul class="list-inline menu-left mb-0">
                                <li class="list-inline-item">
                                    <button type="button" class="button-menu-mobile open-left waves-effect">
                                        <i class="ion-navicon"></i>
                                    </button>
                                </li>
                                <li class="hide-phone list-inline-item app-search">
                                    <h3 class="page-title">Salidas</h3>
                                </li>
                            </ul>

                            <div class="clearfix"></div>

                        </nav>

                    </div>
                    <!-- Top Bar End -->

                    <div class="page-content-wrapper ">

                        <div class="container">
                          <div class="d-flex">
                            <div class="ml-auto">
                              <nav aria-label="breadcrumb">
                                <ol class="breadcrumb float-right">
                                  <li class="breadcrumb-item"><a href="../">Dashboard</a></li>
                                  <li class="breadcrumb-item">Caja</li>
                                  <li class="breadcrumb-item active" aria-current="page">Salidas</li>
                                </ol>
                              </nav>
                            </div>
                          </div>
                            <div class="row">

                                <div class="col-12">
                                    <div class="card m-b-20">
                                        <div class="card-block">
                                            <h4 class="mt-0 m-b-15 header-title">
                                              Lista de salidas
                                              <button type="button" name="button" class="btn btn-outline-primary btn-sm" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">Agregar</button>
                                            </h4>

                                            <div class="collapse mb-3" id="collapseExample">
                                              <div class="card card-block">
                                                <form class="form-salidasbox" action="index.html" method="post">
                                                  <div class="row">
                                                    <div class="col-12 col-lg-7 form-group">
                                                      <label>Concepto*:</label>
                                                      <input type="text" name="concepto" class="form-control form-control-sm concepto">
                                                      <small id="help-concepto" class="help-concepto form-text text-muted text-danger"></small>
                                                    </div>
                                                    <div class="col-12 col-lg-3 form-group">
                                                      <label>Monto*:</label>
                                                      <input type="number" name="monto" class="form-control form-control-sm monto">
                                                      <small id="help-monto" class="help-monto form-text text-muted text-danger"></small>
                                                    </div>
                                                    <div class="col-12 col-lg-2">
                                                      <label></label>
                                                      <button type="submit" name="button" class="btn btn-primary btn-sm btn-block mt-2">Guardar</button>
                                                    </div>
                                                  </div>
                                                </form>
                                              </div>
                                            </div>

                                            <table id="dt-salidas" class="table table-hover m-b-0 table-sm">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Salidas</th>
                                                    <th>Monto</th>
                                                </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!-- end row -->

                        </div><!-- container -->

                        <?php  include_once "modal.php"; ?>
                        
                    </div> <!-- Page content Wrapper -->

                </div> <!-- content -->

                <?= printFooter(); ?>

            </div>
            <!-- End Right content here -->

        </div>
        <!-- END wrapper -->


        <!-- jQuery  -->
        <script src="../assets/js/jquery.min.js"></script>
        <script src="../assets/js/tether.min.js"></script><!-- Tether for Bootstrap -->
        <script src="../assets/js/bootstrap.min.js"></script>
        <script src="../assets/js/modernizr.min.js"></script>
        <script src="../assets/js/detect.js"></script>
        <script src="../assets/js/fastclick.js"></script>
        <script src="../assets/js/jquery.slimscroll.js"></script>
        <script src="../assets/js/jquery.blockUI.js"></script>
        <script src="../assets/js/waves.js"></script>
        <script src="../assets/js/jquery.nicescroll.js"></script>
        <script src="../assets/js/jquery.scrollTo.min.js"></script>

        <!-- App js -->
        <script src="../assets/js/app.js"></script>
        <script src="../assets/js/constantes.js"></script>
        <script src="../assets/js/generales.js"></script>
        <script src="../assets/ajax/caja/salidas.js"></script>
        <script src="../assets/ajax/generales/revisar.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.0.1/js/bootstrap.bundle.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/v/bs5/dt-1.10.25/datatables.min.js"></script>
        <script src="../assets/js/dataTablelanguage.js"></script>
        <script src="../assets/js/messages.js"></script>

        <!-- Alertify js -->
        <script src="../assets/plugins/alertify/js/alertify.js"></script>

    </body>
</html>

$(document).ready(function () {
  listbitacora();
});

var getAll = "../assets/php/admin/bitacora.php";

var listbitacora = function () {
  var table = $('#dt-bitacora').DataTable({
    //Muestra o oculta funciones de la tabla
    "lengthChange": false,
    "pageLength"  : 15,
    "responsive"  : true,
    "searching"   : false,
    "autoWidth"   : false,
    "ordering"    : false,
    "paging"      : true,
    "info"        : false,

    //Permite reiniciar la tabla
    "destroy"     :true,

    //Enlista los datos en la tabla
    "ajax":{
      "method": "POST",
      "url"   : getAll
    },
    "columns":[
      {"data": "bit_clave"},
      {"data": "bit_accion"},
      {"data": "bit_modulo"},
      {"data": "bit_empelado"},
      {"data": "bit_descripcion"}
    ],

    //Cambia el idioma
    "language": idioma_es
  });
}

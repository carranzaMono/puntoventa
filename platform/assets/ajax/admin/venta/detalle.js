$(document).ready(function () {
  get_detalle();
  get_montos();
});

var apartado = 2;
var venta    = $('.venta').data('clave');
var folio    = $('.venta').data('folio');

var getAlldetalle   = "assets/php/admin/ventas/detalle.php?venta="+venta;
var url_getDatos    = "assets/php/admin/ventas/getDatos.php?venta="+venta;
var url_agregarFom2 = "assets/php/admin/ventas/add-product-name.php?venta="+venta;
var url_cobrar      = "venta/cobrar.php?folio="+folio;

var url_aum_product = "assets/php/generales/aumentar-cantidad.php";
var url_dis_product = "assets/php/generales/disminuir-cantidad.php";

var get_detalle = function () {

  var raizextra = dameraizextra(nivel);
  var raiz      = dameraiz(nivel);

  var table = $('#dt-ventadetalle').DataTable({
    //Muestra o oculta funciones de la tabla
    "lengthChange": false,
    "pageLength"  : 50,
    "responsive"  : true,
    "searching"   : false,
    "autoWidth"   : false,
    "ordering"    : false,
    "paging"      : true,
    "info"        : false,

    //Permite reiniciar la tabla
    "destroy"     :true,

    //Enlista los datos en la tabla
    "ajax":{
      "method": "POST",
      "url"   : raizextra+getAlldetalle
    },
    "columns":[
      {"defaultContent": "<button name='button' type='button' class='btn btn-outline-light btn-sm btn-decrease mt-1 mr-1' data-toggle='tooltip' data-placement='left' title='Disminuye de uno en uno'><i class='fas fa-minus-circle fa-sm'></i></button>"+
                         "<button name='button' type='button' class='btn btn-outline-light btn-sm btn-increase mt-1' data-toggle='tooltip' data-placement='right' title='Aumenta de uno en uno'><i class='fas fa-plus-circle fa-sm'></i></button>"},
      {"data": "dv_pro_nombre"},
      {"data": "dv_precioUnitario"},
      {"data": "dv_cantidad"},
      {"data": "dv_subtotal"},
      {"data": "dv_iva"},
      {"data": "dv_total"}
      /*{"defaultContent": "<div class='dropdown mo-mb-2'><button class='btn btn-sm btn-secondary dropdown-toggle' type='button' id='dropdownMenuButton' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>Acciones</button><div class='dropdown-menu' aria-labelledby='dropdownMenuButton'><a class='dropdown-item btn-update dropdown-itme-table' href='#'>Modificar</a><a class='dropdown-item btn-delete dropdown-itme-table' href='#'>Eliminar</a></div></div>"}*/
    ],

    //Cambia el idioma
    "language": idioma_es
  });
  btn_increase_product("#dt-ventadetalle tbody", table);
  btn_decrease_product("#dt-ventadetalle tbody", table);
}

/*muestrar los valores de la venta ===========================================*/
var get_montos  = function () {
  var raizextra = dameraizextra(nivel);
  var raiz      = dameraiz(nivel);
  var url       = raizextra+url_getDatos;

  $.ajax({
    type: 'get',
    url : url,
    cache: false,
    dataType: 'Json',
    success: function (response) {
      if (response) {
        response.data.forEach((o) => {
          //console.log(o.subtotal);
          /*$('.subtotal').text(o.subtotal);
          $('.iva').text(o.iva);*/
          $('.total').text(o.total);

        });
      }
    }
  });
}
/*============================================================================*/

/*Agregar productos pór nombre de productos ==================================*/
$('.form-addproductName').on('submit', function (e) {
  e.preventDefault();
  var form     = $(this);
  var data     = form.serialize();
  var type     = form.attr('method');
  var producto = $('.producto2').val();
  var cantidad = $('.cantidad2').val();

  var raizextra = dameraizextra(nivel);
  var raiz      = dameraiz(nivel);

  var url       = raizextra+url_agregarFom2;

  if (producto == vacio) {
    $('.producto2').addClass('input-danger');
    $('.producto2').focus();
    $('#helpform2-producto').text(help_producto).addClass('text-danger');

    $('.producto2').keyup(function () {
      $(this).removeClass('input-danger');
      $('#helpform2-producto').text('');
    });
  }

  if (cantidad == vacio) {
    $('.cantidad2').addClass('input-danger');
    $('.cantidad2').focus();
    $('#helpform2-cantidad').text(help_cantidad).addClass('text-danger');

    $('.cantidad2').keyup(function () {
      $(this).removeClass('input-danger');
      $('#helpform2-cantidad').text('');
    });
  }

  if ([producto, cantidad].includes(vacio)) {

  }else {
    $.ajax({
      type: type,
      data: data,
      url : url,
      cache: false,
      dataType: 'Json',
      success: function (response) {
        switch (response) {
          case 1:
            alertify.success("¡Se agrego correctamente!");
            get_montos();
            get_detalle();
            form[0].reset();
            break;
          default:
          alertify.error("¡Oops... algo salio mal!");
          console.log(response);
        }
      }
    });
  }

});
/*============================================================================*/

/*btn-cobrar manda a la sección para cobrar ==================================*/
$('.btn-iracobrar').on('click', function (e) {
  e.preventDefault();
  $(location).attr('href', url_cobrar);
});
/*============================================================================*/

/*Aumenta de uno en uno la cantidad de un producto ===========================*/
var btn_increase_product = function (tbody, table) {

  $(tbody).on('click', '.btn-increase', function() {
    var data = table.row( $(this).parents("tr") ).data();

    if (data) {

      var dv_clave  = data.dv_clave;

      var raizextra = dameraizextra(nivel);
      var raiz      = dameraiz(nivel);
      var url       = raizextra+url_aum_product;

      $.ajax({
        type: 'post',
        url : url,
        data: {apartado: apartado, dv_clave: dv_clave},
        cache: false,
        dataType: 'Json',
        success: function (response) {
          switch (response) {
            case 1:
              get_detalle();
              get_montos();
              alertify.success("¡Se aumento correctamente!");
              break;
            default:

          }
        }
      });
    }

  });

}
/*============================================================================*/

/*Disminuye de uno en uno la cantidad de un producto hasta eliminarlo ========*/
var btn_decrease_product = function (tbody, table) {

  $(tbody).on('click', '.btn-decrease', function() {
    var data = table.row( $(this).parents("tr") ).data();

    if (data) {

      var dv_clave  = data.dv_clave;

      var raizextra = dameraizextra(nivel);
      var raiz      = dameraiz(nivel);
      var url       = raizextra+url_dis_product;

      $.ajax({
        type: 'post',
        url : url,
        data: {apartado: apartado, dv_clave: dv_clave},
        cache: false,
        dataType: 'Json',
        success: function (response) {
          switch (response) {
            case 1:
              get_detalle();
              get_montos();
              alertify.success("¡Se disminuyo correctamente!");
              break;
            case 0:
              get_detalle();
              get_montos();
              alertify.success("¡Se elimino correctamente!");
              break;
            default:

          }
        }
      });

    }

  });

}
/*============================================================================*/

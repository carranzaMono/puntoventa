$(document).ready(function () {
  getVentas();
});

var url_listventas = "../assets/php/admin/ventas/listventas.php";


var getVentas = function () {
  var table = $('#dt-principal').DataTable({
    //Muestra o oculta funciones de la tabla
    "lengthChange": false,
    "pageLength"  : 6,
    "responsive"  : true,
    "searching"   : true,
    "autoWidth"   : true,
    "ordering"    : false,
    "paging"      : true,
    "info"        : false,

    //Permite reiniciar la tabla
    "destroy"     :true,

    //Enlista los datos en la tabla
    "ajax":{
      "method": "POST",
      "url"   : url_listventas
    },
    "columns":[
      {"data": "ven_folio"},
      {"data": "fecha"},
      {"data": "hora"},
      {"data": "cli_nombre"},
      {"data": "ven_status"},
      {"data": "ven_total"},
      {"defaultContent": "<div class='dropdown mo-mb-2'><button class='btn btn-sm btn-secondary dropdown-toggle' type='button' id='dropdownMenuButton' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>Acciones</button><div class='dropdown-menu' aria-labelledby='dropdownMenuButton'><a class='dropdown-item btn-view dropdown-itme-table' href='#'>Detalle</a><a class='dropdown-item btn-delete dropdown-itme-table' href='#'>Eliminar</a></div></div>"}
    ],

    //Cambia el idioma
    "language": idioma_es
  });
  btn_detalle("#dt-principal tbody", table);
  /*btn_delete_modal("#dt-categorias tbody", table);*/
}

var btn_detalle = function (tbody, table) {

  $(tbody).on('click', '.btn-view', function() {
    var data = table.row( $(this).parents("tr") ).data();

    if (data) {
      $(location).attr('href', ur_venta+data.ven_folio);
    }

  });

}

$(document).ready(function () {
  listdetalle();
});

/*        ====================================================================*/
var clave = $('.proveedor').data('clave');
/*============================================================================*/

var getAlldetalle = "../../../assets/php/admin/catalogos/proveedores/detalle/listar.php?clave="+clave;
var url_create    = "../../../assets/php/admin/catalogos/proveedores/detalle/create.php";
var url_delete    = "../../../assets/php/admin/catalogos/proveedores/detalle/delete.php";


var listdetalle = function () {
  var table = $('#dt-detalle').DataTable({
    //Muestra o oculta funciones de la tabla
    "lengthChange": false,
    "pageLength"  : 15,
    "responsive"  : true,
    "searching"   : true,
    "autoWidth"   : false,
    "ordering"    : false,
    "paging"      : true,
    "info"        : false,

    //Permite reiniciar la tabla
    "destroy"     :true,

    //Enlista los datos en la tabla
    "ajax":{
      "method": "POST",
      "url"   : getAlldetalle
    },
    "columns":[
      {"data": "dp_clave"},
      {"data": "pp_producto"},
      {"data": "pp_marca"},
      {"defaultContent": "<div class='dropdown mo-mb-2'><button class='btn btn-sm btn-secondary dropdown-toggle' type='button' id='dropdownMenuButton' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>Acciones</button><div class='dropdown-menu' aria-labelledby='dropdownMenuButton'><a class='dropdown-item btn-delete dropdown-itme-table' href='#'>Eliminar</a></div></div>"}
    ],

    //Cambia el idioma
    "language": idioma_es
  });
  //btn_delete_modal("#dt-detalle tbody", table);
}

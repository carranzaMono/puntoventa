$(document).ready(function () {
  listdetalle();
});

/*        ====================================================================*/
var clave = $('.producto').data('clave');
/*============================================================================*/

var getAlldetalle = "../../../assets/php/admin/catalogos/productos/detalle/listar.php?clave="+clave;
var url_create    = "../../../assets/php/admin/catalogos/productos/detalle/create.php?clave="+clave;
var url_delete    = "../../../assets/php/admin/catalogos/productos/detalle/delete.php";


var listdetalle = function () {
  var table = $('#dt-detalle').DataTable({
    //Muestra o oculta funciones de la tabla
    "lengthChange": false,
    "pageLength"  : 15,
    "responsive"  : true,
    "searching"   : true,
    "autoWidth"   : false,
    "ordering"    : false,
    "paging"      : true,
    "info"        : false,

    //Permite reiniciar la tabla
    "destroy"     :true,

    //Enlista los datos en la tabla
    "ajax":{
      "method": "POST",
      "url"   : getAlldetalle
    },
    "columns":[
      {"data": "pc_clave"},
      {"data": "cat_nombre"},
      {"defaultContent": "<div class='dropdown mo-mb-2'><button class='btn btn-sm btn-secondary dropdown-toggle' type='button' id='dropdownMenuButton' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>Acciones</button><div class='dropdown-menu' aria-labelledby='dropdownMenuButton'><a class='dropdown-item btn-delete dropdown-itme-table' href='#'>Eliminar</a></div></div>"}
    ],

    //Cambia el idioma
    "language": idioma_es
  });
  btn_delete_modal("#dt-detalle tbody", table);
}

/*Asigana categorias a un producto ===========================================*/
$('.form-agregar').on('submit', function (e) {
  e.preventDefault();
  var form   = $(this);
  var data   = form.serialize();
  var type   = form.attr('method');

  var categoria = $('.categoria').val();

  if (categoria == vacio) {
    $('.categoria').addClass('input-danger');
    $('.categoria').focus();
    $('#help-categoria').text(help_categoria).addClass('text-danger');
  }else {

    $.ajax({
      type: type,
      data: data,
      url : url_create,
      cache: false,
      dataType: 'Json',
      success: function (response) {
        switch (response) {
          case 1:
            listdetalle();
            //form[0].reset();
            alertify.success("¡Se agrego correctamente!");
            break;
          case 2:
            alertify.log("¡Oops.. este registro ya existe!");
            break;
          default:
            alertify.error("¡Oops... algo salio mal!");
            console.log(response);
        }

      }
    });

  }

});
/*============================================================================*/

$('.close-delete').on('click', function (e) {
  e.preventDefault();
  $('#modal-delete').modal('hide');
});

/*Delete =====================================================================*/
var btn_delete_modal = function (tbody, table) {

  $(tbody).on('click', '.btn-delete', function() {
    var data = table.row( $(this).parents("tr") ).data();

    if (data) {

      //console.log(data.CONTipo);
      $('.dclave').val(data.pc_clave);
      $('.dnombre').text(data.cat_nombre);
      /*$(".update_tipouso option[value='"+ data.CONTipo +"']").attr("selected",true);
      $(".update_asignacion option[value='"+ data.CONAsignadoGrua +"']").attr("selected",true);*/
      $('#modal-delete').modal('show');

    }

  });

}
/*form delete*/
$('.form-delete').on('submit', function (e) {
  e.preventDefault();
  var form       = $(this);
  var data       = form.serialize();
  var type       = form.attr('method');
  var asignacion = $('.dclave').val();

  if (asignacion == vacio) {
    alertify.error("¡Oops... algo salio mal!");
  }else {

    $.ajax({
      type: type,
      data: data,
      url : url_delete,
      cache: false,
      dataType: 'Json',
      success: function (response) {
        switch (response) {
          case 1:
            $('#modal-delete').modal('hide');
            alertify.success("¡Se elimino correctamente!");
            listdetalle();
            break;
          default:
          alertify.error("¡Oops... algo salio mal!");
          console.log(response);
        }
      }
    });

  }

});
/*============================================================================*/

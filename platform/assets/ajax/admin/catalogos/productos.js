$(document).ready(function () {
  listproductos();
});

var getAllproductos = "../../assets/php/admin/catalogos/productos/listar.php";
var url_create      = "../../assets/php/admin/catalogos/productos/create.php";
var url_update      = "../../assets/php/admin/catalogos/productos/update.php";
var url_delete      = "../../assets/php/admin/catalogos/productos/delete.php";
var url_detallePro  = "producto/detalle.php?pro=";

var listproductos = function () {
  var table = $('#dt-productos').DataTable({
    //Muestra o oculta funciones de la tabla
    "lengthChange": false,
    "pageLength"  : 15,
    "responsive"  : true,
    "searching"   : true,
    "autoWidth"   : false,
    "ordering"    : false,
    "paging"      : true,
    "info"        : false,

    //Permite reiniciar la tabla
    "destroy"     :true,

    //Enlista los datos en la tabla
    "ajax":{
      "method": "POST",
      "url"   : getAllproductos
    },
    "columns":[
      {"data": "pro_clave"},
      {"data": "pro_nombre"},
      {"data": "pro_resultinventaraible"},
      {"data": "pre_nombre"},
      {"data": "pro_medida"},
      {"data": "su_nombre"},
      {"data": "pro_precioFormat"},
      {"defaultContent": "<div class='dropdown mo-mb-2'><button class='btn btn-sm btn-secondary dropdown-toggle' type='button' id='dropdownMenuButton' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>Acciones</button><div class='dropdown-menu' aria-labelledby='dropdownMenuButton'><a class='dropdown-item btn-detalle dropdown-itme-table' href='#'>Detalle</a><a class='dropdown-item btn-update dropdown-itme-table' href='#'>Modificar</a><a class='dropdown-item btn-delete dropdown-itme-table' href='#'>Eliminar</a></div></div>"}
    ],

    //Cambia el idioma
    "language": idioma_es
  });
  btn_detalle("#dt-productos tbody", table);
  btn_edit_modal("#dt-productos tbody", table);
  btn_delete_modal("#dt-productos tbody", table);
}

/* modal close ===============================================================*/
$('.close-edit').on('click', function (e) {
  e.preventDefault();
  $('#modal-edit').modal('hide');
  $("#help-unombre").text('');
  $("#help-uapPaterno").text('');
  $("#help-uapMaterno").text('');
  $("#help-upuesto").text('');
});

$('.close-delete').on('click', function (e) {
  e.preventDefault();
  $('#modal-delete').modal('hide');
});

/*============================================================================*/

$('.form-agregar').on('submit', function (e) {
  e.preventDefault();
  var form   = $(this);
  var data   = form.serialize();
  var type   = form.attr('method');

  var nombre        = $('.nombre').val();
  var inventariable = $('.inventariable').val();
  var presentacion  = $('.presentacion').val();
  var unidad        = $('.unidad').val();
  var medida        = $('.medida').val();
  var precio        = $('.precio').val();

  if (nombre == vacio) {
    $('.nombre').addClass('input-danger');
    $('.nombre').focus();
    $('#help-nombre').text(help_nombre).addClass('text-danger');

    $('.nombre').keyup(function () {
      $(this).removeClass('input-danger');
      $('#help-nombre').text('');
    });
  }

  if (inventariable == vacio) {
    $('.inventariable').addClass('input-danger');
    $('.inventariable').focus();
    $('#help-inventariable').text(help_inventariable).addClass('text-danger');

    $('.inventariable').keyup(function () {
      $(this).removeClass('input-danger');
      $('#help-inventariable').text('');
    });
  }

  if (unidad == vacio) {
    $('.unidad').addClass('input-danger');
    $('.unidad').focus();
    $('#help-unidad').text(help_unidad).addClass('text-danger');

    $('.unidad').keyup(function () {
      $(this).removeClass('input-danger');
      $('#help-unidad').text('');
    });
  }

  if (precio == vacio) {
    $('.precio').addClass('input-danger');
    $('.precio').focus();
    $('#help-precio').text(help_precio).addClass('text-danger');

    $('.precio').keyup(function () {
      $(this).removeClass('input-danger');
      $('#help-precio').text('');
    });
  }

  if ([nombre, inventariable, unidad, precio].includes(vacio)) {

  }else {

    $.ajax({
      type: type,
      data: data,
      url : url_create,
      cache: false,
      dataType: 'Json',
      success: function (response) {
        if (response > 0) {
          listproductos();
          form[0].reset();
          $(location).attr('href', url_detallePro+response);
        }else if (response == 'ya_existe') {
          alertify.log("¡Oops.. este registro ya existe!");
        }else {
          alertify.error("¡Oops... algo salio mal!");
          console.log(response);
        }
      }
    });

  }

});

/*Btn para ver detalle =======================================================*/
var btn_detalle = function (tbody, table) {

  $(tbody).on('click', '.btn-detalle', function() {
    var data = table.row( $(this).parents("tr") ).data();

    if (data) {

      //console.log(data.prov_clave);
      $(location).attr('href', url_detallePro+data.pro_clave);

    }

  });

}
/*============================================================================*/

$('.close-edit').on('click', function (e) {
  e.preventDefault();
  $('#modal-edit').modal('hide');
  /*$("#help-unombre").text('');
  $("#help-uapPaterno").text('');
  $("#help-uapMaterno").text('');
  $("#help-upuesto").text('');*/
});

/*Update =====================================================================*/
var btn_edit_modal = function (tbody, table) {

  $(tbody).on('click', '.btn-update', function() {
    var data = table.row( $(this).parents("tr") ).data();

    if (data) {

      //console.log(data.emp_nombre);
      $('.uclave').val(data.pro_clave);
      $('.unombre').val(data.pro_nombre);
      $('.uinventariable').val(data.pro_inventariable);
      $('.upresentacion').val(data.pro_presentacion).trigger('change.select2');
      $('.uunidad').val(data.pro_unidad).trigger('change.select2');
      $('.umedida').val(data.pro_medida);
      $('.uprecio').val(data.pro_precio);
      $('#modal-edit').modal('show');

    }

  });

}

/*form update*/
$('.form-update').on('submit', function (e) {
  e.preventDefault();
  var form          = $(this);
  var data          = form.serialize();
  var type          = form.attr('method');
  var nombre        = $('.unombre').val();
  var inventariable = $('.uinventariable').val();
  var unidad        = $('.uunidad').val();
  var precio         = $('.uprecio').val();

  if (nombre == vacio) {
    $('.unombre').addClass('input-danger');
    $('.unombre').focus();
    $('#help-unombre').text(help_nombre).addClass('text-danger');

    $('.unombre').keyup(function () {
      $(this).removeClass('input-danger');
      $('#help-unombre').text('');
    });
  }

  if (inventariable == vacio) {
    $('.uinventariable').addClass('input-danger');
    $('.uinventariable').focus();
    $('#help-uinventariable').text(help_inventariable).addClass('text-danger');

    $('.uinventariable').keyup(function () {
      $(this).removeClass('input-danger');
      $('#help-uinventariable').text('');
    });
  }

  if (unidad == vacio) {
    $('.uunidad').addClass('input-danger');
    $('.uunidad').focus();
    $('#help-uunidad').text(help_unidad).addClass('text-danger');

    $('.uunidad').keyup(function () {
      $(this).removeClass('input-danger');
      $('#help-uunidad').text('');
    });
  }

  if (precio == vacio) {
    $('.uprecio').addClass('input-danger');
    $('.uprecio').focus();
    $('#help-uprecio').text(help_precio).addClass('text-danger');

    $('.uprecio').keyup(function () {
      $(this).removeClass('input-danger');
      $('#help-uprecio').text('');
    });
  }

  if ([nombre, inventariable, unidad, precio].includes(vacio)) {

  }else {

    $.ajax({
      type: type,
      data: data,
      url : url_update,
      cache: false,
      dataType: 'Json',
      success: function (response) {
        if (response > 0) {
          alertify.success("¡Se agrego correctamente!");
          listproductos();
        }else if (response == 'ya_existe') {
          alertify.log("¡Oops.. este registro ya existe!");
        }else {
          alertify.error("¡Oops... algo salio mal!");
          console.log(response);
        }
      }
    });

  }

});
/*============================================================================*/

/*Delete =====================================================================*/
var btn_delete_modal = function (tbody, table) {

  $(tbody).on('click', '.btn-delete', function() {
    var data = table.row( $(this).parents("tr") ).data();

    if (data) {

      var pro_nombre = data.pro_presNombre+' de '+data.pro_medida+' '+data.pro_uniNombre+' - '+data.pro_nombre;
      //console.log(pro_nombre);

      //console.log(data.CONTipo);
      $('.dclave').val(data.pro_clave);
      $('.dnombre').text(pro_nombre);
      /*$(".update_tipouso option[value='"+ data.CONTipo +"']").attr("selected",true);
      $(".update_asignacion option[value='"+ data.CONAsignadoGrua +"']").attr("selected",true);*/
      $('#modal-delete').modal('show');

    }

  });

}

/*form delete*/
$('.form-delete').on('submit', function (e) {
  e.preventDefault();
  var form     = $(this);
  var data     = form.serialize();
  var type     = form.attr('method');
  var producto = $('.dclave').val();

  if (producto == vacio) {
    alertify.error("¡Oops... algo salio mal!");
  }else {

    $.ajax({
      type: type,
      data: data,
      url : url_delete,
      cache: false,
      dataType: 'Json',
      success: function (response) {
        switch (response) {
          case 1:
            $('#modal-delete').modal('hide');
            alertify.success("¡Se elimino correctamente!");
            listproductos();
            break;
          default:
          alertify.error("¡Oops... algo salio mal!");
          console.log(response);
        }
      }
    });

  }

});
/*============================================================================*/

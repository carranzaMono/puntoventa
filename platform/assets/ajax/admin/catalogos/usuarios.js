$(document).ready(function () {
  listusuarios();
});

var getAllusuarios  = "../../assets/php/admin/catalogos/usuarios/listar.php";
var url_create      = "../../assets/php/admin/catalogos/usuarios/create.php";
var url_update      = "../../assets/php/admin/catalogos/usuarios/update.php?user=";
var url_delete      = "../../assets/php/admin/catalogos/usuarios/delete.php";
var url_suspender   = "../../assets/php/admin/catalogos/usuarios/suspender.php";

var listusuarios = function () {
  var table = $('#dt-usuarios').DataTable({
    //Muestra o oculta funciones de la tabla
    "lengthChange": false,
    "pageLength"  : 15,
    "responsive"  : true,
    "searching"   : true,
    "autoWidth"   : false,
    "ordering"    : false,
    "paging"      : true,
    "info"        : false,

    //Permite reiniciar la tabla
    "destroy"     :true,

    //Enlista los datos en la tabla
    "ajax":{
      "method": "POST",
      "url"   : getAllusuarios
    },
    "columns":[
      {"data": "usu_clave"},
      {"data": "usu_nombre"},
      {"data": "tipo_name"},
      {"data": "usu_emp_nombre"},
      {"data": "status_name"},
      {"defaultContent": "<div class='dropdown mo-mb-2'><button class='btn btn-sm btn-secondary dropdown-toggle' type='button' id='dropdownMenuButton' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>Acciones</button><div class='dropdown-menu' aria-labelledby='dropdownMenuButton'><a class='dropdown-item btn-update dropdown-itme-table' href='#'>Modificar</a><a class='dropdown-item btn-delete dropdown-itme-table' href='#'>Eliminar</a><a class='dropdown-item btn-suspender dropdown-itme-table' href='#'>Suspender</a></div></div>"}
    ],

    //Cambia el idioma
    "language": idioma_es
  });
  btn_edit_modal("#dt-usuarios tbody", table);
  btn_delete_modal("#dt-usuarios tbody", table);
  btn_suspender_modal("#dt-usuarios tbody", table);
}


$('.close-edit').on('click', function (e) {
  e.preventDefault();
  $('#modal-edit').modal('hide');
  $("#help-unombre").text('');
});

$('.close-delete').on('click', function (e) {
  e.preventDefault();
  $('#modal-delete').modal('hide');
});

$('.close-suspender').on('click', function (e) {
  e.preventDefault();
  $('#modal-suspender').modal('hide');
});

$('.form-agregar').on('submit', function (e) {
  e.preventDefault();
  var form     = $(this);
  var data     = form.serialize();
  var type     = form.attr('method');
  var username = $('.username').val();
  var password = $('.password').val();
  var empleado = $('.empleado').val();
  var tipoUser = $('.tipo-user').val();

  if (username == vacio) {
    $('.username').addClass('input-danger');
    $('.username').focus();
    $('#help-username').text(help_username).addClass('text-danger');

    $('.username').keyup(function () {
      $(this).removeClass('input-danger');
      $('#help-username').text('');
    });
  }

  if (password == vacio) {
    $('.password').addClass('input-danger');
    $('.password').focus();
    $('#help-password').text(help_password).addClass('text-danger');

    $('.password').keyup(function () {
      $(this).removeClass('input-danger');
      $('#help-password').text('');
    });
  }

  if (empleado == vacio) {
    $('.empleado').addClass('input-danger');
    $('.empleado').focus();
    $('#help-empleado').text(help_empleado).addClass('text-danger');

    $('.empleado').keyup(function () {
      $(this).removeClass('input-danger');
      $('#help-empleado').text('');
    });
  }

  if (tipoUser == vacio) {
    $('.tipo-user').addClass('input-danger');
    $('.tipo-user').focus();
    $('#help-tipouser').text(help_tipoUser).addClass('text-danger');

    $('.tipo-user').keyup(function () {
      $(this).removeClass('input-danger');
      $('#help-tipouser').text('');
    });
  }

  if ([username, password, empleado, tipoUser].includes(vacio)) {

  }else {

    $.ajax({
      type: type,
      data: data,
      url : url_create,
      cache: false,
      dataType: 'Json',
      success: function (response) {
        switch (response) {
          case 1:
            alertify.success("¡Se agrego correctamente!");
            listusuarios();
            form[0].reset();
            break;
          case 2:
            alertify.log("¡Oops.. este registro ya existe!");
            break;
          default:
          alertify.error("¡Oops... algo salio mal!");
          console.log(response);
        }
      }
    });

  }

});

/*Update =====================================================================*/
var btn_edit_modal = function (tbody, table) {

  $(tbody).on('click', '.btn-update', function() {
    var data = table.row( $(this).parents("tr") ).data();

    if (data) {

      //console.log(data.CONTipo);
      $('.uclave').val(data.usu_clave);
      $('.uusername').val(data.usu_nombre);
      $('.utipo-user').val(data.usu_tipo);
      /*$(".update_tipouso option[value='"+ data.CONTipo +"']").attr("selected",true);
      $(".update_asignacion option[value='"+ data.CONAsignadoGrua +"']").attr("selected",true);*/
      $('#modal-edit').modal('show');

    }

  });

}
/*form update*/
$('.form-update-user').on('submit', function (e) {
  e.preventDefault();
  var form     = $(this);
  var data     = form.serialize();
  var type     = form.attr('method');
  var clave    = $('.uclave').val();
  var username = $('.uusername').val();
  var tipoUser = $('.utipo-user').val();

  if (username == vacio) {
    $('.uusername').addClass('input-danger');
    $('.uusername').focus();
    $('#help-uusername').text(help_username).addClass('text-danger');

    $('.uusername').keyup(function () {
      $(this).removeClass('input-danger');
      $('#help-uusername').text('');
    });
  }

  if (tipoUser == vacio) {
    $('.utipo-user').addClass('input-danger');
    $('.utipo-user').focus();
    $('#help-utipouser').text(help_tipoUser).addClass('text-danger');

    $('.utipo-user').keyup(function () {
      $(this).removeClass('input-danger');
      $('#help-utipouser').text('');
    });
  }

  if ([username, tipoUser].includes(vacio)) {

  }else {

    $.ajax({
      type: type,
      data: data,
      url : url_update+clave,
      cache: false,
      dataType: 'Json',
      success: function (response) {
        switch (response) {
          case 1:
            //$('#modal-edit').modal('hide');
            alertify.success("¡Se agrego correctamente!");
            listusuarios();
            break;
          case 2:
            alertify.log("¡Oops.. este registro ya existe!");
            break;
          default:
          alertify.error("¡Oops... algo salio mal!");
          console.log(response);
        }
      }
    });

  }

});

$('.form-update-password').on('submit', function (e) {
  e.preventDefault();
  var form     = $(this);
  var data     = form.serialize();
  var type     = form.attr('method');
  var clave    = $('.uclave').val();
  var password = $('.upassword').val();

  if (password == vacio) {
    $('.upassword').addClass('input-danger');
    $('.upassword').focus();
    $('#help-upassword').text(help_password).addClass('text-danger');

    $('.upassword').keyup(function () {
      $(this).removeClass('input-danger');
      $('#help-upassword').text('');
    });
  }else {

    $.ajax({
      type: type,
      data: data,
      url : url_update+clave,
      cache: false,
      dataType: 'Json',
      success: function (response) {
        switch (response) {
          case 1:
            form[0].reset();
            alertify.success("¡Se agrego correctamente!");
            listusuarios();
            break;
          case 2:
            alertify.log("¡Oops.. este registro ya existe!");
            break;
          default:
          alertify.error("¡Oops... algo salio mal!");
          console.log(response);
        }
      }
    });

  }

});
/*============================================================================*/

/*Delete =====================================================================*/
var btn_delete_modal = function (tbody, table) {

  $(tbody).on('click', '.btn-delete', function() {
    var data = table.row( $(this).parents("tr") ).data();

    if (data) {

      //console.log(data.CONTipo);
      $('.dclave').val(data.usu_clave);
      $('.dnombre').text(data.usu_nombre);
      /*$(".update_tipouso option[value='"+ data.CONTipo +"']").attr("selected",true);
      $(".update_asignacion option[value='"+ data.CONAsignadoGrua +"']").attr("selected",true);*/
      $('#modal-delete').modal('show');

    }

  });

}

/*form delete*/
$('.form-delete').on('submit', function (e) {
  e.preventDefault();
  var form    = $(this);
  var data    = form.serialize();
  var type    = form.attr('method');
  var usuario = $('.dclave').val();

  if (usuario == vacio) {
    alertify.error("¡Oops... algo salio mal!");
  }else {

    $.ajax({
      type: type,
      data: data,
      url : url_delete,
      cache: false,
      dataType: 'Json',
      success: function (response) {
        switch (response) {
          case 1:
            $('#modal-delete').modal('hide');
            alertify.success("¡Se elimino correctamente!");
            listusuarios();
            break;
          default:
          alertify.error("¡Oops... algo salio mal!");
          console.log(response);
        }
      }
    });

  }

});
/*============================================================================*/

/*Suspender ==================================================================*/
var btn_suspender_modal = function (tbody, table) {

  $(tbody).on('click', '.btn-suspender', function() {
    var data = table.row( $(this).parents("tr") ).data();

    if (data) {

      //console.log(data.CONTipo);
      $('.sclave').val(data.usu_clave);
      $('.snombre').text(data.usu_nombre);
      /*$(".update_tipouso option[value='"+ data.CONTipo +"']").attr("selected",true);
      $(".update_asignacion option[value='"+ data.CONAsignadoGrua +"']").attr("selected",true);*/
      $('#modal-suspender').modal('show');

    }

  });

}

/*form suspender*/
$('.form-suspender').on('submit', function (e) {
  e.preventDefault();
  var form   = $(this);
  var data   = form.serialize();
  var type   = form.attr('method');
  var usuario = $('.sclave').val();

  if (usuario == vacio) {
    alertify.error("¡Oops... algo salio mal!");
  }else {

    $.ajax({
      type: type,
      data: data,
      url : url_suspender,
      cache: false,
      dataType: 'Json',
      success: function (response) {
        switch (response) {
          case 1:
            $('#modal-suspender').modal('hide');
            alertify.success("¡Se suspendio correctamente!");
            listusuarios();
            break;
          default:
          alertify.error("¡Oops... algo salio mal!");
          console.log(response);
        }
      }
    });

  }

});
/*============================================================================*/

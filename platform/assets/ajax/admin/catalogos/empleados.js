$(document).ready(function () {
  listempleados();
});

var getAllempleados = "../../assets/php/admin/catalogos/empleados/listar.php";
var url_create      = "../../assets/php/admin/catalogos/empleados/create.php";
var url_update      = "../../assets/php/admin/catalogos/empleados/update.php";
var url_delete      = "../../assets/php/admin/catalogos/empleados/delete.php";


var listempleados = function () {
  var table = $('#dt-empleados').DataTable({
    //Muestra o oculta funciones de la tabla
    "lengthChange": false,
    "pageLength"  : 15,
    "responsive"  : true,
    "searching"   : true,
    "autoWidth"   : false,
    "ordering"    : false,
    "paging"      : true,
    "info"        : false,

    //Permite reiniciar la tabla
    "destroy"     :true,

    //Enlista los datos en la tabla
    "ajax":{
      "method": "POST",
      "url"   : getAllempleados
    },
    "columns":[
      {"data": "emp_clave"},
      {"data": "emp_nombreCompleto"},
      {"data": "emp_telefono"},
      {"data": "emp_correo"},
      {"data": "emp_curp"},
      {"data": "emp_rfc"},
      {"data": "pue_nombre"},
      {"defaultContent": "<div class='dropdown mo-mb-2'><button class='btn btn-sm btn-secondary dropdown-toggle' type='button' id='dropdownMenuButton' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>Acciones</button><div class='dropdown-menu' aria-labelledby='dropdownMenuButton'><a class='dropdown-item btn-update dropdown-itme-table' href='#'>Modificar</a><a class='dropdown-item btn-delete dropdown-itme-table' href='#'>Eliminar</a></div></div>"}
    ],

    //Cambia el idioma
    "language": idioma_es
  });
  btn_edit_modal("#dt-empleados tbody", table);
  btn_delete_modal("#dt-empleados tbody", table);
}

$('.close-edit').on('click', function (e) {
  e.preventDefault();
  $('#modal-edit').modal('hide');
  $("#help-unombre").text('');
  $("#help-uapPaterno").text('');
  $("#help-uapMaterno").text('');
  $("#help-upuesto").text('');
});

$('.close-delete').on('click', function (e) {
  e.preventDefault();
  $('#modal-delete').modal('hide');
});

$('.form-agregar').on('submit', function (e) {
  e.preventDefault();
  var form   = $(this);
  var data   = form.serialize();
  var type   = form.attr('method');

  var nombre    = $('.nombre').val();
  var apPaterno = $('.ap-paterno').val();
  var apMaterno = $('.ap-materno').val();
  var puesto    = $('.puesto').val();
  var telef     = $('.telef').val();
  var correo    = $('.correo').val();
  var curp      = $('.curp').val();
  var rfc       = $('.rfc').val();
  var direccion = $('.direccion').val();

  if (nombre == vacio) {
    $('.nombre').addClass('input-danger');
    $('.nombre').focus();
    $('#help-nombre').text(help_nombre).addClass('text-danger');

    $('.nombre').keyup(function () {
      $(this).removeClass('input-danger');
      $('#help-nombre').text('');
    });
  }

  if (apPaterno == vacio) {
    $('.ap-paterno').addClass('input-danger');
    $('.ap-paterno').focus();
    $('#help-apPaterno').text(help_paterno).addClass('text-danger');

    $('.ap-paterno').keyup(function () {
      $(this).removeClass('input-danger');
      $('#help-apPaterno').text('');
    });
  }

  if (apMaterno == vacio) {
    $('.ap-materno').addClass('input-danger');
    $('.ap-materno').focus();
    $('#help-apMaterno').text(help_materno).addClass('text-danger');

    $('.ap-materno').keyup(function () {
      $(this).removeClass('input-danger');
      $('#help-apMaterno').text('');
    });
  }

  if (puesto == vacio) {
    $('.puesto').addClass('input-danger');
    $('.puesto').focus();
    $('#help-puesto').text(help_puesto).addClass('text-danger');

    $('.puesto').keyup(function () {
      $(this).removeClass('input-danger');
      $('#help-puesto').text('');
    });
  }

  if ([nombre, apPaterno, apMaterno, puesto].includes(vacio)) {

  }else {

    $.ajax({
      type: type,
      data: data,
      url : url_create,
      cache: false,
      dataType: 'Json',
      success: function (response) {
        switch (response) {
          case 1:
            alertify.success("¡Se agrego correctamente!");
            listempleados();
            form[0].reset();
            break;
          case 2:
            alertify.log("¡Oops.. este registro ya existe!");
            break;
          default:
          alertify.error("¡Oops... algo salio mal!");
          console.log(response);
        }
      }
    });

  }

});

/*Update =====================================================================*/
var btn_edit_modal = function (tbody, table) {

  $(tbody).on('click', '.btn-update', function() {
    var data = table.row( $(this).parents("tr") ).data();

    if (data) {

      //console.log(data.emp_nombre);
      $('.uclave').val(data.emp_clave);
      $('.unombre').val(data.emp_nombre);
      $('.uap-paterno').val(data.emp_apPaterno);
      $('.uap-materno').val(data.emp_apMaterno);
      $('.upuesto').val(data.emp_pue_clave);
      $('.udireccion').val(data.emp_direccion);
      $('.urfc').val(data.emp_rfc);
      $('.ucurp').val(data.emp_curp);
      $('.ucorreo').val(data.emp_correo);
      $('.utelef').val(data.emp_telefono);
      /*$(".update_tipouso option[value='"+ data.CONTipo +"']").attr("selected",true);
      $(".update_asignacion option[value='"+ data.CONAsignadoGrua +"']").attr("selected",true);*/
      $('#modal-edit').modal('show');

    }

  });

}
/*form update*/
$('.form-update').on('submit', function (e) {
  e.preventDefault();
  var form      = $(this);
  var data      = form.serialize();
  var type      = form.attr('method');
  var nombre    = $('.unombre').val();
  var apPaterno = $('.uap-paterno').val();
  var apMaterno = $('.uap-materno').val();
  var puesto    = $('.upuesto').val();
  var telef     = $('.utelef').val();
  var correo    = $('.ucorreo').val();
  var curp      = $('.ucurp').val();
  var rfc       = $('.urfc').val();
  var direccion = $('.udireccion').val();


  if (nombre == vacio) {
    $('.unombre').addClass('input-danger');
    $('.unombre').focus();
    $('#help-unombre').text(help_nombre).addClass('text-danger');

    $('.unombre').keyup(function () {
      $(this).removeClass('input-danger');
      $('#help-unombre').text('');
    });
  }

  if (apPaterno == vacio) {
    $('.uap-paterno').addClass('input-danger');
    $('.uap-paterno').focus();
    $('#help-uapPaterno').text(help_paterno).addClass('text-danger');

    $('.uap-paterno').keyup(function () {
      $(this).removeClass('input-danger');
      $('#help-uapPaterno').text('');
    });
  }

  if (apMaterno == vacio) {
    $('.uap-materno').addClass('input-danger');
    $('.uap-materno').focus();
    $('#help-uapMaterno').text(help_materno).addClass('text-danger');

    $('.uap-materno').keyup(function () {
      $(this).removeClass('input-danger');
      $('#help-uapMaterno').text('');
    });
  }

  if (puesto == vacio) {
    $('.upuesto').addClass('input-danger');
    $('.upuesto').focus();
    $('#help-upuesto').text(help_puesto).addClass('text-danger');

    $('.upuesto').keyup(function () {
      $(this).removeClass('input-danger');
      $('#help-upuesto').text('');
    });
  }

  if ([nombre, apPaterno, apMaterno, puesto].includes(vacio)) {

  }else {

    $.ajax({
      type: type,
      data: data,
      url : url_update,
      cache: false,
      dataType: 'Json',
      success: function (response) {
        switch (response) {
          case 1:
            $('#modal-edit').modal('hide');
            alertify.success("¡Se agrego correctamente!");
            listempleados();
            break;
          case 2:
            alertify.log("¡Oops.. este registro ya existe!");
            break;
          default:
          alertify.error("¡Oops... algo salio mal!");
          console.log(response);
        }
      }
    });

  }

});
/*============================================================================*/

/*Delete =====================================================================*/
var btn_delete_modal = function (tbody, table) {

  $(tbody).on('click', '.btn-delete', function() {
    var data = table.row( $(this).parents("tr") ).data();

    if (data) {

      //console.log(data.CONTipo);
      $('.dclave').val(data.emp_clave);
      $('.dnombre').text(data.emp_nombreCompleto);
      /*$(".update_tipouso option[value='"+ data.CONTipo +"']").attr("selected",true);
      $(".update_asignacion option[value='"+ data.CONAsignadoGrua +"']").attr("selected",true);*/
      $('#modal-delete').modal('show');

    }

  });

}

/*form delete*/
$('.form-delete').on('submit', function (e) {
  e.preventDefault();
  var form   = $(this);
  var data   = form.serialize();
  var type   = form.attr('method');
  var puesto = $('.dclave').val();

  if (puesto == vacio) {
    alertify.error("¡Oops... algo salio mal!");
  }else {

    $.ajax({
      type: type,
      data: data,
      url : url_delete,
      cache: false,
      dataType: 'Json',
      success: function (response) {
        switch (response) {
          case 1:
            $('#modal-delete').modal('hide');
            alertify.success("¡Se elimino correctamente!");
            listempleados();
            break;
          default:
          alertify.error("¡Oops... algo salio mal!");
          console.log(response);
        }
      }
    });

  }

});
/*============================================================================*/

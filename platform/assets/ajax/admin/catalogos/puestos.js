$(document).ready(function () {
  listpuestos();
});

var getAllpuestos = "../../assets/php/admin/catalogos/puestos/listar.php";
var url_create    = "../../assets/php/admin/catalogos/puestos/create.php";
var url_update    = "../../assets/php/admin/catalogos/puestos/update.php";
var url_delete    = "../../assets/php/admin/catalogos/puestos/delete.php";


var listpuestos = function () {
  var table = $('#dt-puestos').DataTable({
    //Muestra o oculta funciones de la tabla
    "lengthChange": false,
    "pageLength"  : 15,
    "responsive"  : true,
    "searching"   : true,
    "autoWidth"   : false,
    "ordering"    : false,
    "paging"      : true,
    "info"        : false,

    //Permite reiniciar la tabla
    "destroy"     :true,

    //Enlista los datos en la tabla
    "ajax":{
      "method": "POST",
      "url"   : getAllpuestos
    },
    "columns":[
      {"data": "pue_clave"},
      {"data": "pue_nombre"},
      {"defaultContent": "<div class='dropdown mo-mb-2'><button class='btn btn-sm btn-secondary dropdown-toggle' type='button' id='dropdownMenuButton' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>Acciones</button><div class='dropdown-menu' aria-labelledby='dropdownMenuButton'><a class='dropdown-item btn-update dropdown-itme-table' href='#'>Modificar</a><a class='dropdown-item btn-delete dropdown-itme-table' href='#'>Eliminar</a></div></div>"}
    ],

    //Cambia el idioma
    "language": idioma_es
  });
  btn_edit_modal("#dt-puestos tbody", table);
  btn_delete_modal("#dt-puestos tbody", table);
}

$('.close-edit').on('click', function (e) {
  e.preventDefault();
  $('#modal-edit').modal('hide');
  $("#help-unombre").text('');
});

$('.close-delete').on('click', function (e) {
  e.preventDefault();
  $('#modal-delete').modal('hide');
});

$('.form-agregar').on('submit', function (e) {
  e.preventDefault();
  var form   = $(this);
  var data   = form.serialize();
  var type   = form.attr('method');
  var nombre = $('.nombre').val();

  if (nombre == vacio) {
    $('.nombre').addClass('input-danger');
    $('.nombre').focus();
    $('.msj-help-name').text(help_nombre).addClass('text-danger');

    $('.nombre').keyup(function () {
      $(this).removeClass('input-danger');
      $('.msj-help-name').text('');
    });
  }else {

    $.ajax({
      type: type,
      data: data,
      url : url_create,
      cache: false,
      dataType: 'Json',
      success: function (response) {
        switch (response) {
          case 1:
            alertify.success("¡Se agrego correctamente!");
            listpuestos();
            form[0].reset();
            break;
          case 2:
            alertify.log("¡Oops.. este registro ya existe!");
            break;
          default:
          alertify.error("¡Oops... algo salio mal!");
          console.log(response);
        }
      }
    });

  }

});

/*Update =====================================================================*/
var btn_edit_modal = function (tbody, table) {

  $(tbody).on('click', '.btn-update', function() {
    var data = table.row( $(this).parents("tr") ).data();

    if (data) {

      //console.log(data.CONTipo);
      $('.uclave').val(data.pue_clave);
      $('.unombre').val(data.pue_nombre);
      /*$(".update_tipouso option[value='"+ data.CONTipo +"']").attr("selected",true);
      $(".update_asignacion option[value='"+ data.CONAsignadoGrua +"']").attr("selected",true);*/
      $('#modal-edit').modal('show');

    }

  });

}
/*form update*/
$('.form-update').on('submit', function (e) {
  e.preventDefault();
  var form   = $(this);
  var data   = form.serialize();
  var type   = form.attr('method');
  var nombre = $('.unombre').val();

  if (nombre == vacio) {
    $('.unombre').addClass('input-danger');
    $('.unombre').focus();
    $('.help-unombre').text(help_nombre);
    $('.help-unombre').addClass('text-danger');

    $('.unombre').keyup(function () {
      $(this).removeClass('input-danger');
      $('.help-unombre').text('');
    });
  }else {

    $.ajax({
      type: type,
      data: data,
      url : url_update,
      cache: false,
      dataType: 'Json',
      success: function (response) {
        switch (response) {
          case 1:
            $('#modal-edit').modal('hide');
            alertify.success("¡Se agrego correctamente!");
            listpuestos();
            break;
          case 2:
            alertify.log("¡Oops.. este registro ya existe!");
            break;
          default:
          alertify.error("¡Oops... algo salio mal!");
          console.log(response);
        }
      }
    });

  }

});
/*============================================================================*/

/*Delete =====================================================================*/
var btn_delete_modal = function (tbody, table) {

  $(tbody).on('click', '.btn-delete', function() {
    var data = table.row( $(this).parents("tr") ).data();

    if (data) {

      //console.log(data.CONTipo);
      $('.dclave').val(data.pue_clave);
      $('.dnombre').text(data.pue_nombre);
      /*$(".update_tipouso option[value='"+ data.CONTipo +"']").attr("selected",true);
      $(".update_asignacion option[value='"+ data.CONAsignadoGrua +"']").attr("selected",true);*/
      $('#modal-delete').modal('show');

    }

  });

}

/*form delete*/
$('.form-delete').on('submit', function (e) {
  e.preventDefault();
  var form   = $(this);
  var data   = form.serialize();
  var type   = form.attr('method');
  var puesto = $('.dclave').val();

  if (puesto == vacio) {
    alertify.error("¡Oops... algo salio mal!");
  }else {

    $.ajax({
      type: type,
      data: data,
      url : url_delete,
      cache: false,
      dataType: 'Json',
      success: function (response) {
        switch (response) {
          case 1:
            $('#modal-delete').modal('hide');
            alertify.success("¡Se elimino correctamente!");
            listpuestos();
            break;
          default:
          alertify.error("¡Oops... algo salio mal!");
          console.log(response);
        }
      }
    });

  }

});
/*============================================================================*/

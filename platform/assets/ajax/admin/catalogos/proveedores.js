$(document).ready(function () {
  listproveedores();
});

var getAllproveedores = "../../assets/php/admin/catalogos/proveedores/listar.php";
var url_create        = "../../assets/php/admin/catalogos/proveedores/create.php";
var url_update        = "../../assets/php/admin/catalogos/proveedores/update.php";
var url_delete        = "../../assets/php/admin/catalogos/proveedores/delete.php";
var url_detalleProv   = "proveedor/detalle.php?prov=";


var listproveedores = function () {
  var table = $('#dt-proveedores').DataTable({
    //Muestra o oculta funciones de la tabla
    "lengthChange": false,
    "pageLength"  : 15,
    "responsive"  : true,
    "searching"   : true,
    "autoWidth"   : false,
    "ordering"    : false,
    "paging"      : true,
    "info"        : false,

    //Permite reiniciar la tabla
    "destroy"     :true,

    //Enlista los datos en la tabla
    "ajax":{
      "method": "POST",
      "url"   : getAllproveedores
    },
    "columns":[
      {"data": "prov_clave"},
      {"data": "prov_nombrecompleto"},
      {"data": "prov_telefono"},
      {"data": "prov_correo"},
      {"defaultContent": "<div class='dropdown mo-mb-2'><button class='btn btn-sm btn-secondary dropdown-toggle' type='button' id='dropdownMenuButton' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>Acciones</button><div class='dropdown-menu' aria-labelledby='dropdownMenuButton'><a class='dropdown-item btn-detalle dropdown-itme-table' href='#'>Detalle</a><a class='dropdown-item btn-update dropdown-itme-table' href='#'>Modificar</a><a class='dropdown-item btn-delete dropdown-itme-table' href='#'>Eliminar</a></div></div>"}
    ],

    //Cambia el idioma
    "language": idioma_es
  });
  btn_detalle("#dt-proveedores tbody", table)
  btn_edit_modal("#dt-proveedores tbody", table);
  btn_delete_modal("#dt-proveedores tbody", table);
}

$('.close-edit').on('click', function (e) {
  e.preventDefault();
  $('#modal-edit').modal('hide');
  $("#help-unombre").text('');
  $("#help-uapPaterno").text('');
  $("#help-uapMaterno").text('');
  $("#help-upuesto").text('');
});

$('.close-delete').on('click', function (e) {
  e.preventDefault();
  $('#modal-delete').modal('hide');
});

$('.form-agregar').on('submit', function (e) {
  e.preventDefault();
  var form   = $(this);
  var data   = form.serialize();
  var type   = form.attr('method');

  var nombre    = $('.nombre').val();
  var apPaterno = $('.ap-paterno').val();
  var apMaterno = $('.ap-materno').val();
  //var puesto    = $('.puesto').val();
  var telef     = $('.telef').val();
  var correo    = $('.correo').val();

  if (nombre == vacio) {
    $('.nombre').addClass('input-danger');
    $('.nombre').focus();
    $('#help-nombre').text(help_nombre).addClass('text-danger');

    $('.nombre').keyup(function () {
      $(this).removeClass('input-danger');
      $('#help-nombre').text('');
    });
  }

  if (apPaterno == vacio) {
    $('.ap-paterno').addClass('input-danger');
    $('.ap-paterno').focus();
    $('#help-apPaterno').text(help_paterno).addClass('text-danger');

    $('.ap-paterno').keyup(function () {
      $(this).removeClass('input-danger');
      $('#help-apPaterno').text('');
    });
  }

  if (apMaterno == vacio) {
    $('.ap-materno').addClass('input-danger');
    $('.ap-materno').focus();
    $('#help-apMaterno').text(help_materno).addClass('text-danger');

    $('.ap-materno').keyup(function () {
      $(this).removeClass('input-danger');
      $('#help-apMaterno').text('');
    });
  }


  if ([nombre, apPaterno, apMaterno].includes(vacio)) {

  }else {

    $.ajax({
      type: type,
      data: data,
      url : url_create,
      cache: false,
      dataType: 'Json',
      success: function (response) {
        if (response > 0) {
          listproductos();
          form[0].reset();
          $(location).attr('href', url_detalleProv+response);
        }else if (response == 'ya_existe') {
          alertify.log("¡Oops.. este registro ya existe!");
        }else {
          alertify.error("¡Oops... algo salio mal!");
          console.log(response);
        }
      }
    });

  }

});

/*Btn para ver detalle =======================================================*/
var btn_detalle = function (tbody, table) {

  $(tbody).on('click', '.btn-detalle', function() {
    var data = table.row( $(this).parents("tr") ).data();

    if (data) {

      //console.log(data.prov_clave);
      $(location).attr('href', url_detalleProv+data.prov_clave);

    }

  });

}
/*============================================================================*/

/*Update =====================================================================*/
var btn_edit_modal = function (tbody, table) {

  $(tbody).on('click', '.btn-update', function() {
    var data = table.row( $(this).parents("tr") ).data();

    if (data) {

      //console.log(data.emp_nombre);
      $('.uclave').val(data.prov_clave);
      $('.unombre').val(data.prov_nombre);
      $('.uap-paterno').val(data.prov_appaterno);
      $('.uap-materno').val(data.prov_apmaterno);
      $('.ucorreo').val(data.prov_correo);
      $('.utelef').val(data.prov_telefono);
      $('#modal-edit').modal('show');

    }

  });

}
/*form update*/
$('.form-update').on('submit', function (e) {
  e.preventDefault();
  var form      = $(this);
  var data      = form.serialize();
  var type      = form.attr('method');
  var nombre    = $('.unombre').val();
  var apPaterno = $('.uap-paterno').val();
  var apMaterno = $('.uap-materno').val();
  var telef     = $('.utelef').val();
  var correo    = $('.ucorreo').val();


  if (nombre == vacio) {
    $('.unombre').addClass('input-danger');
    $('.unombre').focus();
    $('#help-unombre').text(help_nombre).addClass('text-danger');

    $('.unombre').keyup(function () {
      $(this).removeClass('input-danger');
      $('#help-unombre').text('');
    });
  }

  if (apPaterno == vacio) {
    $('.uap-paterno').addClass('input-danger');
    $('.uap-paterno').focus();
    $('#help-uapPaterno').text(help_paterno).addClass('text-danger');

    $('.uap-paterno').keyup(function () {
      $(this).removeClass('input-danger');
      $('#help-uapPaterno').text('');
    });
  }

  if (apMaterno == vacio) {
    $('.uap-materno').addClass('input-danger');
    $('.uap-materno').focus();
    $('#help-uapMaterno').text(help_materno).addClass('text-danger');

    $('.uap-materno').keyup(function () {
      $(this).removeClass('input-danger');
      $('#help-uapMaterno').text('');
    });
  }

  if ([nombre, apPaterno, apMaterno].includes(vacio)) {

  }else {

    $.ajax({
      type: type,
      data: data,
      url : url_update,
      cache: false,
      dataType: 'Json',
      success: function (response) {
        switch (response) {
          case 1:
            $('#modal-edit').modal('hide');
            listproveedores();
            break;
          default:
          if (response == 'ya_existe') {
            alertify.log("¡Oops.. este registro ya existe!");
          }else {
            alertify.error("¡Oops... algo salio mal!");
            console.log(response);
          }
        }
      }
    });

  }

});
/*============================================================================*/

/*Delete =====================================================================*/
var btn_delete_modal = function (tbody, table) {

  $(tbody).on('click', '.btn-delete', function() {
    var data = table.row( $(this).parents("tr") ).data();

    if (data) {

      //console.log(data.prov_clave);
      $('.dclave').val(data.prov_clave);
      $('.dnombre').text(data.prov_nombrecompleto);
      /*$(".update_tipouso option[value='"+ data.CONTipo +"']").attr("selected",true);
      $(".update_asignacion option[value='"+ data.CONAsignadoGrua +"']").attr("selected",true);*/
      $('#modal-delete').modal('show');

    }

  });

}

/*form delete*/
$('.form-delete').on('submit', function (e) {
  e.preventDefault();
  var form   = $(this);
  var data   = form.serialize();
  var type   = form.attr('method');
  var proveedor = $('.dclave').val();

  if (proveedor == vacio) {
    alertify.error("¡Oops... algo salio mal!");
  }else {

    $.ajax({
      type: type,
      data: data,
      url : url_delete,
      cache: false,
      dataType: 'Json',
      success: function (response) {
        switch (response) {
          case 1:
            $('#modal-delete').modal('hide');
            alertify.success("¡Se elimino correctamente!");
            listproveedores();
            break;
          default:
          alertify.error("¡Oops... algo salio mal!");
          console.log(response);
        }
      }
    });

  }

});
/*============================================================================*/

$(document).ready(function () {
  listdetalle();
  montosCotizacion();
});

var cotizacion = $('.cotizacion').data('clave');

var apartado                   = 1;

var url_agregar                = "assets/php/cotizacion/addproductos.php?cotizacion="+cotizacion;
var url_getDatos               = "assets/php/cotizacion/getDatos.php?cotizacion="+cotizacion;
var url_cotventa               = "assets/php/cotizacion/cotivent.php?cotizacion="+cotizacion;
var url_pdf                    = "procesos/cotizacion/archivo/pdf.php?cotizacion="+cotizacion;
//var url_add_envio    = "assets/php/generales/agregar-envio.php?clave="+cotizacion;
var url_tipo_cliente           = "assets/php/generales/tipo-cliente.php?clave="+cotizacion;
var url_update_cliente         = "assets/php/cotizacion/updateCliente.php?cotizacion="+cotizacion;
var url_create_cliente         = "assets/php/cotizacion/createCliente.php?cotizacion="+cotizacion;
var url_datos_ucliente         = "assets/php/cotizacion/updateDatosCliente.php";
var url_aum_product            = "assets/php/generales/aumentar-cantidad.php";
var url_dis_product            = "assets/php/generales/disminuir-cantidad.php";
var url_revisar_precios        = "assets/php/cotizacion/revisar-precios.php?cotizacion="+cotizacion;
var url_existe_product         = "assets/php/generales/existe-product.php?clave="+cotizacion;
var url_imprimir_nuevosprecios = "assets/php/cotizacion/imprimir-cambioprecios.php?cotizacion="+cotizacion;
var url_actualizar_y_vender    = "assets/php/cotizacion/actualizaryvender.php?cotizacion="+cotizacion;
var url_venta                  = "venta.php?folio=";

var listdetalle = function () {

  var raizextra = dameraizextra(nivel);
  var raiz      = dameraiz(nivel);

  var table = $('#dt-detalle').DataTable({
    //Muestra o oculta funciones de la tabla
    "lengthChange": false,
    "pageLength"  : 150,
    "responsive"  : true,
    "searching"   : false,
    "autoWidth"   : false,
    "ordering"    : false,
    "paging"      : false,
    "info"        : false,

    //Permite reiniciar la tabla
    "destroy"     :true,

    //Enlista los datos en la tabla
    "ajax":{
      "method": "POST",
      "url"   : raizextra+getAlldetalle
    },
    "columns":[
      //{"data": "dc_clave"},
      {"defaultContent": "<button name='button' type='button' class='btn btn-outline-light btn-sm btn-decrease mt-1 mr-1' data-toggle='tooltip' data-placement='left' title='Disminuye de uno en uno'><i class='fas fa-minus-circle fa-sm'></i></button>"+
                         "<button name='button' type='button' class='btn btn-outline-light btn-sm btn-increase mt-1' data-toggle='tooltip' data-placement='right' title='Aumenta de uno en uno'><i class='fas fa-plus-circle fa-sm'></i></button>"},
      {"data": "dc_pro_nombre"},
      {"data": "dc_precioUnitario"},
      {"data": "dc_cantidad"},
      {"data": "dc_subtotal"},
      {"data": "dc_iva"},
      {"data": "dc_total"}
    ],

    //Cambia el idioma
    "language": idioma_es
  });
  btn_increase_product("#dt-detalle tbody", table);
  btn_decrease_product("#dt-detalle tbody", table);
}

$('.btn-update-cliente').on('click', function (e) {
  e.preventDefault();

  var raizextra  = dameraizextra(nivel);
  var raiz       = dameraiz(nivel);
  var url        = raizextra+url_tipo_cliente;

  if (cotizacion) {
    $.ajax({
      type: 'get',
      data: {apartado: 1},
      url : url,
      cache: false,
      dataType: 'Json',
      success: function (response) {

        switch (response) {
          case "pg":
            $('.modal-select-cliente').modal('show');
            $('.search').append('').trigger('change');
            $('.help-cliente').text(vacio);
            break;
          default:

            if (response) {

              response.data.forEach((o) => {

                $('.u-cli-clave').val(o.cli_clave);
                $('.unombre').val(o.cli_nombre);
                $('.utelefono').val(o.cli_telefono);
                $('.ucorreo').val(o.cli_correo);
                $('.urfc').val(o.cli_rfc);
                $('.udireccion').val(o.cli_direccion);

              });

              $('.modal-datos-uCliente').modal('show');
            }

        }
      }
    });
  }

});

/*============================================================================*/
$('.x-modal-close-selectCliCot').on('click', function (e) {
  e.preventDefault();

    $('.modal-select-cliente').modal('hide');
});

$('.btn-modal-close-selectCliCot').on('click', function (e) {
  e.preventDefault();

  $('.modal-select-cliente').modal('hide');
});

$('.btn-x-modalClose-create-cliente').on('click', function (e) {
  e.preventDefault();

  $('.modal-select-cliente').modal('hide');
});

/*========================================================*/

/*Modal para registrar un cliente ========================*/
$('.btn-registrocliente').on('click', function (e) {
  e.preventDefault();

  $('.modal-select-cliente').modal('hide');
  $('.modal-create-cliente').modal('show');
});

$('.btn-x-modalClose-create-cliente').on('click', function (e) {
  e.preventDefault();

  $('.modal-create-cliente').modal('hide');
});

$('.btn-modalClose-create-cliente').on('click', function (e) {
  e.preventDefault();

  $('.modal-create-cliente').modal('hide');
});

$('.btn-modalClose-datos-ucliente').on('click', function (e) {
  e.preventDefault();

  $('.modal-datos-uCliente').modal('hide');
});

$('.btn-x-modalClose-datos-uCliente').on('click', function (e) {
  e.preventDefault();

  $('.modal-datos-uCliente').modal('hide');
});

$('.btn-close-existeProducto').on('click', function (e) {
  e.preventDefault();

  $('.modal-existeProducto').modal('hide');
});

$('.btn-x-close-modal-existeProducto').on('click', function (e) {
  e.preventDefault();

  $('.modal-existeProducto').modal('hide');
});

$('.btn-x-close-modal-cambioprecio').on('click', function (e) {
  e.preventDefault();

  $('.modal-cambio-precio').modal('hide');
});

$('.btn-close-modal-cambioprecio').on('click', function (e) {
  e.preventDefault();

  $('.modal-cambio-precio').modal('hide');
});
/*============================================================================*/

/*formulario para modificar el cliente =======================================*/
$('#form-update-cliente').on('submit', function (e) {
  e.preventDefault();

  var form       = $(this);
  var data       = form.serialize();
  var type       = form.attr('method');

  var raizextra  = dameraizextra(nivel);
  var raiz       = dameraiz(nivel);
  var url        = raizextra+url_update_cliente;

  var cliente = $('.cliente').val();

  if (cliente == "") {
    $('.cliente').addClass('input-danger');
    $('.cliente').focus();
    $('.help-cliente').text(help_cliente);
    $('.help-cliente').addClass('text-danger');

    /*$('.cliente').keyup(function () {
      $(this).removeClass('input-danger');
      $('.help-cliente').text('');
    });*/
  }else {
    $.ajax({
      type: type,
      data: data,
      url : url,
      cache: false,
      dataType: 'Json',
      success: function (response) {

        switch (response) {
          case "update":

            alertify.success("¡Se modifico correctamente!");
            listdetalle();
            montosCotizacion();
            $('.search').append('').trigger('change');
            $('.help-cliente').text(vacio);

            break;
          default:
          alertify.error("¡Oops... algo salio mal!");

        }

      }
    });
  }

});
/*============================================================================*/

/*formulario para dar de alta un cliente desde btn dentro de la cotizacion ===*/
$('.form-createClienteCot').on('submit', function (e) {
  e.preventDefault();
  var form       = $(this);
  var data       = form.serialize();
  var type       = form.attr('method');

  var raizextra  = dameraizextra(nivel);
  var raiz       = dameraiz(nivel);
  var url        = raizextra+url_create_cliente;

  var rfc       = $('.rfc').val();
  var nombre    = $('.nombre').val();
  var correo    = $('.correo').val();
  var telefono  = $('.telefono').val();
  var direccion = $('.direccion').val();

  if (nombre == vacio) {
    $('.nombre').addClass('input-danger');
    $('.nombre').focus();
    $('.help-nombre').text(help_nombre);
    $('.help-nombre').addClass('text-danger');

    $('.nombre').keyup(function () {
      $(this).removeClass('input-danger');
      $('.help-nombre').text('');
    });
  }

  if ([nombre].includes(vacio)) {

  }else {

    $.ajax({
      type: type,
      data: data,
      url : url,
      cache: false,
      dataType: 'Json',
      success:function (response) {
        switch (response) {
          case "error_save":
            alertify.error("¡Oops... algo salio mal!");
            console.log(response);
            break;
          case "cli_exist":
            alertify.log("¡Oops.. el cliente ya existe!");
            break;

          default:

          $('.modal-create-cliente').modal('hide');
          alertify.success("¡Se modifico correctamente!");
          form[0].reset();
          listdetalle();
          montosCotizacion();

        }
      }
    });

  }

});
/*============================================================================*/

/*formulario para actualizar datos del cliente desde la cotización ===========*/
$('.form-update-datosClienteCot').on('submit', function (e) {
  e.preventDefault();

  var form       = $(this);
  var data       = form.serialize();
  var type       = form.attr('method');

  var raizextra  = dameraizextra(nivel);
  var raiz       = dameraiz(nivel);
  var url        = raizextra+url_datos_ucliente;

  var rfc       = $('.urfc').val();
  var nombre    = $('.unombre').val();
  var correo    = $('.ucorreo').val();
  var telefono  = $('.utelefono').val();
  var direccion = $('.udireccion').val();

  if (nombre == vacio) {
    $('.unombre').addClass('input-danger');
    $('.unombre').focus();
    $('.help-unombre').text(help_nombre);
    $('.help-unombre').addClass('text-danger');

    $('.unombre').keyup(function () {
      $(this).removeClass('input-danger');
      $('.help-unombre').text('');
    });
  }

  if ([nombre].includes(vacio)) {

  }else {

    $.ajax({
      type: type,
      data: data,
      url : url,
      cache: false,
      dataType: 'Json',
      success:function (response) {
        switch (response) {
          case "error_update":
            alertify.error("¡Oops... algo salio mal!");
            console.log(response);
            break;
          case "cli_exist":
            alertify.log("¡Oops.. el cliente ya existe!");
            break;

          default:

          $('.modal-datos-uCliente').modal('hide');
          alertify.success("¡Se modifico correctamente!");
          listdetalle();
          montosCotizacion();

        }
      }
    });

  }

});
/*============================================================================*/

/*$('.btn-zona').on('click', function (e) {
  e.preventDefault();

  var btn       = $(this);
  var zon_clave = btn.data('clave');

  if (zon_clave) {
    $('.modal-add-direccion').modal('show');
  }

});*/

var btn_increase_product = function (tbody, table) {

  $(tbody).on('click', '.btn-increase', function() {
    var data = table.row( $(this).parents("tr") ).data();

    if (data) {

      var dc_clave  = data.dc_clave;

      var raizextra = dameraizextra(nivel);
      var raiz      = dameraiz(nivel);
      var url       = raizextra+url_aum_product;

      $.ajax({
        type: 'post',
        url : url,
        data: {apartado: apartado, dc_clave: dc_clave},
        cache: false,
        dataType: 'Json',
        success: function (response) {
          switch (response) {
            case 1:
              listdetalle();
              montosCotizacion();
              alertify.success("¡Se aumento correctamente!");
              break;
            default:

          }
        }
      });
    }

  });

}

var btn_decrease_product = function (tbody, table) {

  $(tbody).on('click', '.btn-decrease', function() {
    var data = table.row( $(this).parents("tr") ).data();

    if (data) {

      var dc_clave  = data.dc_clave;

      var raizextra = dameraizextra(nivel);
      var raiz      = dameraiz(nivel);
      var url       = raizextra+url_dis_product;

      $.ajax({
        type: 'post',
        url : url,
        data: {apartado: apartado, dc_clave: dc_clave},
        cache: false,
        dataType: 'Json',
        success: function (response) {
          switch (response) {
            case 1:
              listdetalle();
              montosCotizacion();
              alertify.success("¡Se disminuyo correctamente!");
              break;
            case 0:
              listdetalle();
              montosCotizacion();
              alertify.success("¡Se elimino correctamente!");
              break;
            default:

          }
        }
      });

    }

  });

}


var montosCotizacion = function () {
  var raizextra = dameraizextra(nivel);
  var raiz      = dameraiz(nivel);
  var url       = raizextra+url_getDatos;

  $.ajax({
    type: 'get',
    url : url,
    cache: false,
    dataType: 'Json',
    success: function (response) {
      if (response) {
        response.data.forEach((o) => {
          //console.log(o.subtotal);
          //console.log(o.cot_status);
          if (o.cot_status == 2) {
            $('.status').text('Vendido');
            $('.btn-addproducto').prop('disabled', true);
            $('.btn-cotivent').prop('disabled', true);
            $('.btn-generarpdf').prop('disabled', true);
            $('.btn-enviarcorreo').prop('disabled', true);
            $('.cantidad').prop('disabled', true);
            $('.producto').prop('disabled', true);
            $('.btn-decrease').prop('disabled', true);
            $('.btn-increase').prop('disabled', true);
          }

          if (o.cot_status == 1) {
            $('.status').text('Abierto');
          }

          /*$('.subtotal').text(o.subtotal);
          $('.iva').text(o.iva);*/
          $('.cli-nombre').text(o.cli_nombre);
          $('.cli-correo').text(o.cli_correo);
          $('.cli-telf').text(o.cli_telefono);

          if (o.cli_direccion == '' || o.cli_direccion == null) {
            $('.cli_direccion').text("Sin dirección");
          }else {
            $('.cli_direccion').text(o.cli_direccion);
          }

          if (o.cli_rfc == '' || o.cli_rfc == null) {
            $('.cli-rfc').text("Sin RFC");
          }else {
            $('.cli-rfc').text(o.cli_rfc);
          }

          if (o.cot_envio == 0) {
            $('.content-envio').hide();
          }else {
            $('.content-envio').show();
          }

          $('.total').text(o.total);

        });
      }
    }
  });
}

$('#form-add-product').on('submit', function (e) {
  e.preventDefault();
  var form     = $(this);
  var data     = form.serialize();
  var type     = form.attr('method');
  var producto = $('.producto').val();
  var cantidad = $('.cantidad').val();

  var raizextra = dameraizextra(nivel);
  var raiz      = dameraiz(nivel);
  var url       = raizextra+url_agregar;


  if (producto == vacio) {
    $('.producto').addClass('input-danger');
    $('.producto').focus();
    $('#help-producto').text(help_producto).addClass('text-danger');

    $('.producto').keyup(function () {
      $(this).removeClass('input-danger');
      $('#help-producto').text('');
    });
  }

  if (cantidad == vacio) {
    $('.cantidad').addClass('input-danger');
    $('.cantidad').focus();
    $('#help-cantidad').text(help_cantidad).addClass('text-danger');

    $('.cantidad').keyup(function () {
      $(this).removeClass('input-danger');
      $('#help-cantidad').text('');
    });
  }

  if ([producto, cantidad].includes(vacio)) {

  }else {
    $.ajax({
      type: type,
      data: data,
      url : url,
      cache: false,
      dataType: 'Json',
      success: function (response) {
        switch (response) {
          case 1:
            alertify.success("¡Se agrego correctamente!");
            form[0].reset();
            $('.search').val(null).trigger('change');
            listdetalle();
            montosCotizacion();
            break;
          default:
          alertify.error("¡Oops... algo salio mal!");
          console.log(response);
        }
      }
    });
  }

});

$('.btn-cotivent').on('click', function (e) {
  e.preventDefault();

  var raizextra = dameraizextra(nivel);
  var raiz      = dameraiz(nivel);
  var url1      = raizextra+url_revisar_precios;
  var url2      = raizextra+url_imprimir_nuevosprecios;

  if (cotizacion) {

    $.ajax({
      type: 'post',
      url : url1,
      cache: false,
      dataType: 'Json',
      success:function (r1) {

        switch (r1) {
          case "error_search":
            console.log(response);
            alertify.error("¡Oops... algo salio mal!");
            break;
          case "precios-scambios":
            $('.modal-confimarCotiventa').modal('show');
            break;
          case "np":
            $('.modal-existeProducto').modal('show');
            break;
          default:

            $.ajax({
              type: 'post',
              url : url2,
              cache: false,
              dataType: 'html',
              success: function (r2) {
                if (r2) {

                  $('.content-newinfo-price').html(r2);
                  $('.modal-cambio-precio').modal('show');

                }else {
                  console.log(r2);
                }
            }
          });

        }

      }
    });

  }

});


$('.btn-confimacion-cotvent').on('click', function (e) {
  e.preventDefault();

  var raizextra = dameraizextra(nivel);
  var raiz      = dameraiz(nivel);
  var url       = raizextra+url_cotventa;

  $.ajax({
    type: 'post',
    url : url,
    cache: false,
    dataType: 'Json',
    success: function (response) {

      switch (response) {
        case "sin_items":
          alertify.log("¡Oops... la cotización no tiene items!");
          break;
        case "sin_caja":
          alertify.log("¡Oops... no tienes una caja abierta");
          break;
        case "error_insert":
          alertify.error("¡Oops... algo salio mal!");
          break;
        case "not_save":
          alertify.error("¡Oops... no se pudo guardar!");
          break;
        default:
        $(location).attr('href', url_venta+response);
      }

    }
  });

});

$('.btn-generarpdf').on('click', function (e) {
  e.preventDefault();

  var raizextra = dameraizextra(nivel);
  var raiz      = dameraiz(nivel);
  var url       = raizextra+url_existe_product;

  /*
  apartado 1 = cotizacion
           2 = venta
   */
  $.ajax({
    type: 'post',
    url : url,
    data: {apartado: 1},
    cache: false,
    dataType: 'Json',
    success: function (response) {

      switch (response) {
        case "0":
          $('.modal-existeProducto').modal('show');
          break;
        default:
        if (response > 0) {
          window.open(url_pdf, '_blank');
        }else if (response == "error_search") {
          alertify.error("¡Oops... algo salio mal!");
        }
      }

      //console.log(response);
    }
  });
  //$(location).attr('href', url_pdf);


});

$('.btn-close-modal').on('click', function (e) {
  $('.modal-confimarCotiventa').modal('hide');
  /*$('.modal-cotizacion').modal('hide');
  $('.modal-registrocliente').modal('hide');
  $('.modal-cotizacion').modal('hide');
  $('.exists-box').modal('hide');
  $('.open-box').modal('hide');*/
});

$('.btn-confirmacion-cambiar-precios').on('click', function (e) {
  e.preventDefault();

  var raizextra = dameraizextra(nivel);
  var raiz      = dameraiz(nivel);
  var url       = raizextra+url_actualizar_y_vender;

  $.ajax({
    type: 'post',
    url : url,
    cache: false,
    dataType: 'Json',
    success: function (response) {
      if (response) {
        switch (response) {
          case 1:
            listdetalle();
            montosCotizacion();
            alertify.success("¡Se agrego correctamente!");
            $('.modal-cambio-precio').modal('hide');
            break;
          default:
          console.log(response);
          alertify.error("¡Oops... algo salio mal!");
        }
      }
    }
  });

});

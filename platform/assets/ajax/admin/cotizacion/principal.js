$(document).ready(function () {
  getCotizaciones();
});

var url_listcotizaciones = "../assets/php/cotizacion/listcotizaciones.php";
var ur_cotizacion        = "cotizacion.php?folio=";

var getCotizaciones = function () {
  var table = $('#dt-principal').DataTable({
    //Muestra o oculta funciones de la tabla
    "lengthChange": false,
    "pageLength"  : 6,
    "responsive"  : true,
    "searching"   : true,
    "autoWidth"   : true,
    "ordering"    : false,
    "paging"      : true,
    "info"        : false,

    //Permite reiniciar la tabla
    "destroy"     :true,

    //Enlista los datos en la tabla
    "ajax":{
      "method": "POST",
      "url"   : url_listcotizaciones
    },
    "columns":[
      {"data": "cot_folio"},
      {"data": "cot_fecha"},
      {"data": "cot_cliente"},
      {"data": "cot_total"},
      {"defaultContent": "<div class='dropdown mo-mb-2'><button class='btn btn-sm btn-secondary dropdown-toggle' type='button' id='dropdownMenuButton' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>Acciones</button><div class='dropdown-menu' aria-labelledby='dropdownMenuButton'><a class='dropdown-item btn-view dropdown-itme-table' href='#'>Detalle</a><a class='dropdown-item btn-delete dropdown-itme-table' href='#'>Eliminar</a></div></div>"}
    ],

    //Cambia el idioma
    "language": idioma_es
  });
  btn_detalle("#dt-principal tbody", table);
  /*btn_delete_modal("#dt-categorias tbody", table);*/
}

var btn_detalle = function (tbody, table) {

  $(tbody).on('click', '.btn-view', function() {
    var data = table.row( $(this).parents("tr") ).data();

    if (data) {
      $(location).attr('href', ur_cotizacion+data.cot_folio);
    }

  });

}

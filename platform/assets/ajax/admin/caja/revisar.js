var url_openbox     = "assets/php/admin/caja/openbox.php";
var url_exists      = "assets/php/admin/caja/exists.php";
var url_create      = "assets/php/admin/caja/create.php";
var url_gcotizacion = "assets/php/admin/cotizacion/create.php";
var url_entregaCaja = "assets/php/admin/caja/entrega.php";
var url_cotizacion  = "cotizacion.php?folio=";
var url_salida      = "caja/salidas.php";
var url_detalleCaja = "caja/detalle.php?folio=";

/*Abre caja por medio del registro de un fondo de caja =======================*/
$('#btn-openbox').on('click', function (e) {
  e.preventDefault();

  var type = 'post';
  var raiz = dameraizextra(nivel);
  var url  = raiz+url_openbox;

  //console.log(url);
  $.ajax({
    type: type,
    data: {data:'revisar'},
    url : url,
    cache: false,
    dataType: 'Json',
    success: function (response) {
      switch (response) {
        case 0:
          $('.open-box').modal('show');
          break;
        default:
        if (response == 'box_exists') {
          $('.exists-box').modal('show');
        }else {
          alertify.error("¡Oops... algo salio mal!");
          console.log(response);
        }

      }
    }
  });

});
/*============================================================================*/

/*Abre caja para operar ======================================================*/
$('.form-fondobox').on('submit', function (e) {
  e.preventDefault();
  var form   = $(this);
  var data   = form.serialize();
  var type   = form.attr('method');
  var raiz   = dameraizextra(nivel);
  var fondo  = $('.fondo').val();
  var url    = raiz+url_create;

  if (fondo == vacio) {
    $('.fondo').addClass('input-danger');
    $('.fondo').focus();
    $('.help-fondo').text(help_fondo);
    $('.help-fondo').addClass('text-danger');

    $('.fondo').keyup(function () {
      $(this).removeClass('input-danger');
      $('.help-fondo').text('');
    });
  }else {
    $.ajax({
      type: type,
      data: data,
      url : url,
      cache: false,
      dataType: 'Json',
      success: function (response) {
        switch (response) {
          case 1:
            $('.open-box').modal('hide');
            break;
          default:
          alertify.error("¡Oops... algo salio mal!");
          console.log(response);
        }
      }
    });
  }

});
/*============================================================================*/

/*revisa si hay un corte de caja abierto y si lo hay permite hacer la acción =*/
$('.btn-salida').on('click', function (e) {
  e.preventDefault();

  var type      = 'post';
  var raiz      = dameraiz(nivel);
  var raizextra = dameraizextra(nivel);
  var url       = raizextra+url_exists;

  $.ajax({
    type: type,
    data: {data:'revisar'},
    url : url,
    cache: false,
    dataType: 'Json',
    success: function (response) {
      switch (response) {
        case 0:
          $('.notexists').modal('show');
          break;
        case 1:
          $(location).attr('href', url_salida);
          break;
        default:
          alertify.error("¡Oops... algo salio mal!");
          console.log(response);
      }
    }
  });
});
/*============================================================================*/

/*btn que abre un modal para seleccionar un cliente para crear una cotización */
/*$('.btn-gcotizacion').on('click', function (e) {
  e.preventDefault();

  $('.modal-cotizacion').modal('show');

});*/
/*============================================================================*/

/*btn para crear una cotizacion ==============================================*/
$('.btn-gcotizacion').on('click', function (e) {
  e.preventDefault();

  var raiz           = dameraiz(nivel);
  var raizextra      = dameraizextra(nivel);
  var url            = raizextra+url_gcotizacion;
  var cotizacion     = raiz+url_cotizacion;

  //var cliente = $('.cliente').val();
  var cliente = 1;

  if (cliente == vacio) {
    $('.cliente').addClass('input-danger');
    $('.cliente').focus();
    $('.help-cliente').text(help_cliente);
    $('.help-cliente').addClass('text-danger');

    $('.cliente').keyup(function () {
      $(this).removeClass('input-danger');
      $('.help-cliente').text('');
    });
  }else {

    $.ajax({
      type: 'post',
      data: {cliente: cliente},
      url : url,
      cache: false,
      dataType: 'Json',
      success: function (response) {
        switch (response) {
          case "sin_caja":
            $('.notexists').modal('show');
            break;
          case "error_insert":
            alertify.error("¡Oops... algo salio mal!");
            console.log(response);
              break;
          case "not_save":
            alertify.error("¡Oops... algo salio mal!");
            console.log(response);
              break;
          default:
          $(location).attr('href', cotizacion+response);
        }
      }
    });

  }

  //$('.modal-cotizacion').modal('show');

});
/*============================================================================*/

/*revisa si hay un corte de caja abierto y si lo hay permite hacer la acción =*/
/*$('.form-cotizacion').on('submit', function (e) {
  e.preventDefault();

  var form       = $(this);
  var data       = form.serialize();
  var type       = form.attr('method');

  var raiz           = dameraiz(nivel);
  var raizextra      = dameraizextra(nivel);
  var url            = raizextra+url_gcotizacion;
  var cotizacion     = raiz+url_cotizacion;

  var cliente = $('.cliente').val();

  if (cliente == vacio) {
    $('.cliente').addClass('input-danger');
    $('.cliente').focus();
    $('.help-cliente').text(help_cliente);
    $('.help-cliente').addClass('text-danger');

    $('.cliente').keyup(function () {
      $(this).removeClass('input-danger');
      $('.help-cliente').text('');
    });
  }else {

    $.ajax({
      type: type,
      data: data,
      url : url,
      cache: false,
      dataType: 'Json',
      success: function (response) {
        switch (response) {
          case "sin_caja":
            $('.notexists').modal('show');
            break;
          case "error_insert":
            alertify.error("¡Oops... algo salio mal!");
            console.log(response);
              break;
          case "not_save":
            alertify.error("¡Oops... algo salio mal!");
            console.log(response);
              break;
          default:
          $(location).attr('href', cotizacion+response);
        }
      }
    });

  }

});*/
/*============================================================================*/

/*btn que abre un modal para registrar un cliente para crear una cotización ====
$('.btn-registrocliente').on('click', function (e) {
  e.preventDefault();

  $('.modal-cotizacion').modal('hide');
  $('.modal-registrocliente').modal('show');

});
============================================================================*/

/*Revisa si hay caja abierta cuando se le da click al botón para entregar caja*/
$('.btn-entregacaja').on('click', function (e) {
  e.preventDefault();

  var raiz      = dameraiz(nivel);
  var raizextra = dameraizextra(nivel);
  var url       = raizextra+url_exists;

  //console.log(url);

  $.ajax({
    type: 'post',
    data: {data:'revisar'},
    url : url,
    cache: false,
    dataType: 'Json',
    success: function (response) {
      switch (response) {
        case 1:
          $('.modal-close-box').modal('show');
          break;
        case 0:
          alertify.log("¡Oops.. no tienes caja abierta!");
          break;
        default:
        alertify.error("¡Oops... algo salio mal!");
        console.log(response);
      }
    }
  });

});
/*============================================================================*/

/*form para entregar caja ====================================================*/
$('#form-entregabox').on('submit', function (e) {
  e.preventDefault();
  var form       = $(this);
  var data       = form.serialize();
  var type       = form.attr('method');

  var raizextra   = dameraizextra(nivel);
  var raiz        = dameraiz(nivel);
  var url         = raizextra+url_entregaCaja;
  var url_reponse = raiz+url_detalleCaja;
  //var cotizacion = raiz+url_cotizacion;

  var monto    = $('.entrega').val();

  if (monto == vacio) {
    $('.entrega').addClass('input-danger');
    $('.entrega').focus();
    $('.help-entrega').text(help_monto);
    $('.help-entrega').addClass('text-danger');

    $('.entrega').keyup(function () {
      $(this).removeClass('input-danger');
      $('.help-entrega').text('');
    });
  }else {
    $.ajax({
      type: type,
      data: data,
      url : url,
      cache: false,
      dataType: 'Json',
      success:function (response) {
        if (response == "does_not_exist") {

          alertify.log("¡Oops.. no tienes caja abierta!");

        }else if (response == "error_search" || response == "error_insert") {

          alertify.error("¡Oops... algo salio mal!");
          console.log(response);

        }else {

          if (response.data) {
            $('.modal-close-box').modal('hide');
            $('.modal-opensale').modal('show');
            response.data.forEach((o) => {

              $('.fv-abiertos').text(o.folio);

            });
          }else {

            $(location).attr('href', url_reponse+response);

          }

        }
      }
    });
  }



});
/*============================================================================*/

/*$('.btn-close-modal').on('click', function (e) {
  $('.notexists').modal('hide');
  $('.modal-cotizacion').modal('hide');
  $('.modal-registrocliente').modal('hide');
  $('.modal-cotizacion').modal('hide');
  $('.exists-box').modal('hide');
  $('.open-box').modal('hide');
});*/

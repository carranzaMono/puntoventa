$(document).ready(function () {
  listdetalle();
  getDatos();
  listCobros();
});

var venta = $('.venta').data('clave');
var folio = $('.venta').data('folio');

var getAlldetalle = "assets/php/admin/cobrar/detalle.php?venta="+venta;
var url_getDatos  = "assets/php/admin/cobrar/getDatos.php?venta="+venta;
var url_addpago   = "assets/php/admin/cobrar/agregarpago.php?clave="+venta;
var url_getPagos  = "assets/php/admin/cobrar/getPagos.php?venta="+venta;
var url_cambio    = "assets/php/admin/cobrar/cambio.php?venta="+venta;

var listdetalle = function () {

  var raizextra = dameraizextra(nivel);
  var raiz      = dameraiz(nivel);

  var table = $('#dt-ventadetalle').DataTable({
    //Muestra o oculta funciones de la tabla
    "lengthChange": false,
    "pageLength"  : 150,
    "responsive"  : true,
    "searching"   : false,
    "autoWidth"   : false,
    "ordering"    : false,
    "paging"      : false,
    "info"        : false,

    //Permite reiniciar la tabla
    "destroy"     :true,

    //Enlista los datos en la tabla
    "ajax":{
      "method": "POST",
      "url"   : raizextra+getAlldetalle
    },
    "columns":[
      {"data": "dv_clave"},
      {"data": "dv_pro_nombre"},
      {"data": "dv_precioUnitario"},
      {"data": "dv_cantidad"},
      {"data": "dv_total"}
      /*{"defaultContent": "<div class='dropdown mo-mb-2'><button class='btn btn-sm btn-secondary dropdown-toggle' type='button' id='dropdownMenuButton' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>Acciones</button><div class='dropdown-menu' aria-labelledby='dropdownMenuButton'><a class='dropdown-item btn-update dropdown-itme-table' href='#'>Modificar</a><a class='dropdown-item btn-delete dropdown-itme-table' href='#'>Eliminar</a></div></div>"}*/
    ],

    //Cambia el idioma
    "language": idioma_es
  });
  /*btn_edit_modal("#dt-categorias tbody", table);
  btn_delete_modal("#dt-categorias tbody", table);*/
}

/*muestrar los valores de la venta y status  =================================*/
var getDatos = function () {
  var raizextra = dameraizextra(nivel);
  var raiz      = dameraiz(nivel);
  var url       = raizextra+url_getDatos;

  //console.log(nivel);

  $.ajax({
    type: 'get',
    url : url,
    cache: false,
    dataType: 'Json',
    success: function (response) {
      if (response) {
        response.data.forEach((o) => {
          //console.log(o.subtotal);
          $('.subtotal').text(o.subtotal);
          $('.iva').text(o.iva);
          $('.total').text(o.total);
          $('.cobrado').text(o.cobrado);
          $('.resta').text(o.resta);

          switch (o.ven_status) {
            case "1":
              $('.status').text('Pendiente');
            break;
            case "2":
              $('.status').text('Vendido');
              break;
            default:
              $('.status').text('Cancelado');
          }

          if (o.ven_status != 1) {
            $('.forma-pago').prop('disabled', true);
            $('.cobro').prop('disabled', true);
            $('.btn-agregar').prop('disabled', true);
          }

        });
      }
    }
  });
}
/*============================================================================*/

/*btn-cobrar manda a la sección para cobrar ==================================*/
$('.form-addcobro').on('submit', function (e) {
  e.preventDefault();
  var form = $(this);
  var data = form.serialize();
  var type = form.attr('method');

  var raizextra = dameraizextra(nivel);
  var raiz      = dameraiz(nivel);
  var url       = raizextra+url_addpago;

  var forma_pago  = $('.forma-pago').val();
  var cobro       = $('.cobro').val();

  if (forma_pago == vacio) {
    $('.forma-pago').addClass('input-danger');
    $('.forma-pago').focus();
    $('#help-fpago').text(help_fpago).addClass('text-danger');

    $('.forma-cobro').keyup(function () {
      $(this).removeClass('input-danger');
      $('#help-fcobro').text('');
    });
  }

  if (cobro == vacio) {
    $('.cobro').addClass('input-danger');
    $('.cobro').focus();
    $('#help-cobro').text(help_monto).addClass('text-danger');

    $('.cobro').keyup(function () {
      $(this).removeClass('input-danger');
      $('#help-cobro').text('');
    });
  }

  if ([forma_pago, cobro].includes(vacio)) {

  }else {
    $.ajax({
      type: type,
      data: data,
      url : url,
      cache: false,
      dataType: 'Json',
      success: function (response) {
        switch (response) {
          case 1:
            form[0].reset();
            getDatos();
            listCobros();
            dameCambio();
            alertify.success("¡Se agrego correctamente!");
            break;
          default:
          if (response == "monto_excesivo") {
            alertify.log("¡Oops.. no puedes agregar pagos mayores cuando la forma de pago es diferente a efectivo!");
          }else{
            alertify.error("¡Oops... algo salio mal!");
            console.log(response);
          }
        }

      }
    });
  }


});
/*============================================================================*/

/*retorna el cambio si lo hay del ultimo pago agregado =======================*/
var dameCambio = function () {
  var raizextra = dameraizextra(nivel);
  var raiz      = dameraiz(nivel);
  var url       = raizextra+url_cambio;

  $.ajax({
    type: 'get',
    url : url,
    cache: false,
    dataType: 'Json',
    success: function (response) {
      if (response) {
        response.data.forEach((o) => {
          //console.log(o.subtotal);
          $('.cambio').text(o.cambio);

        });
      }
    }
  });
}
/*============================================================================*/

/*lista de pagos realizados del cliente ======================================*/
var listCobros = function () {

  var raizextra = dameraizextra(nivel);
  var raiz      = dameraiz(nivel);

  var table = $('#dt-detalleCobros').DataTable({
    //Muestra o oculta funciones de la tabla
    "lengthChange": false,
    "pageLength"  : 8,
    "responsive"  : true,
    "searching"   : true,
    "autoWidth"   : false,
    "ordering"    : false,
    "paging"      : true,
    "info"        : false,

    //Permite reiniciar la tabla
    "destroy"     :true,

    //Enlista los datos en la tabla
    "ajax":{
      "method": "POST",
      "url"   : raizextra+url_getPagos
    },
    "columns":[
      {"data": "cob_clave"},
      {"data": "cob_fecha"},
      {"data": "fp_nombre"},
      {"data": "cob_monto"},
      {"defaultContent": "<div class='dropdown mo-mb-2'><button class='btn btn-sm btn-secondary dropdown-toggle' type='button' id='dropdownMenuButton' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>Acciones</button><div class='dropdown-menu' aria-labelledby='dropdownMenuButton'><a class='dropdown-item btn-update dropdown-itme-table' href='#'>Modificar</a><a class='dropdown-item btn-delete dropdown-itme-table' href='#'>Eliminar</a></div></div>"}
    ],

    //Cambia el idioma
    "language": idioma_es
  });
  /*btn_edit_modal("#dt-categorias tbody", table);
  btn_delete_modal("#dt-categorias tbody", table);*/
}
/*============================================================================*/

$('.form-login').on('submit', function (e) {
  e.preventDefault();
  var form      = $(this);
  var data      = form.serialize();
  var type      = form.attr('method');

  var username  = $('.user').val();
  var password  = $('.password').val();

  var url_login    = "platform/assets/funciones/login.php";
  var url_admin    = "platform/admin/";
  var url_caja     = "platform/caja/";
  var url_vendedor = "platform/ventas/";
  /*var url_almacen = "platform/almacen/";*/


  if (username == vacio) {
    $('.user').addClass('input-danger');
    $('.user').focus();
    $('#help-user').text(help_usuario).addClass('text-danger');

    $('.user').keyup(function () {
      $(this).removeClass('input-danger');
      $('#help-user').text('');
    });
  }

  if (password == vacio) {
    $('.password').addClass('input-danger');
    $('#help-password').text(help_password).addClass('text-danger');

    $('.password').keyup(function () {
      $(this).removeClass('input-danger');
      $('#help-password').text('');
    });
  }

  if ([username, password].includes(vacio)) {

  }else {
    $.ajax({
      type: type,
      data: data,
      url : url_login,
      cache: false,
      dataType: 'Json',
      success: function (response) {
        switch (response) {
          case '1':
          $(location).attr('href', url_admin);
            break;
          case '3':
          $(location).attr('href', url_caja);
            break;
          default:
            if (response == 'not_exist') {
              $('#help-user').text(usu_not_exist).addClass('text-danger');
            }else if (response == 'incorrect_pass') {
              $('#help-user').text(pass_incorrect).addClass('text-danger');
            }else {
              console.log(response);
            }

        }
      }
    });
  }

});

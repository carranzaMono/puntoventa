
if (usu_tipo == admin) {

  var url_registrode_salidas  = "caja/registro-salidas.php";
  var url_detalleCaja         = "caja/detalle.php?folio=";

}else {

  var url_registrode_salidas  = "registro-salidas.php";
  var url_detalleCaja         = "detalle.php?folio=";
  
}

var url_existe_caja_abierta = "assets/php/generales/caja/existe-caja-abierta.php";
var url_entregaCaja         = "assets/php/generales/caja/entrega.php";
var url_abrir_caja          = "assets/php/generales/caja/abrir-caja.php";

//var url_gcotizacion = "assets/php/admin/cotizacion/create.php";
//var url_cotizacion  = "cotizacion.php?folio=";

/* btns para cerrar ==========================================================*/
$('.btn-x-close-modal-existecaja').on('click', function (e) {
  e.preventDefault();
  $('.modal-existe-caja').modal('hide');
});
$('.btn-close-modal-existecaja').on('click', function (e) {
  e.preventDefault();
  $('.modal-existe-caja').modal('hide');
});
$('.btn-x-close-modal-entregacaja').on('click', function (e) {
  e.preventDefault();
  $('.modal-cierre-caja').modal('hide');
});
$('.btn-close-modal-entregacaja').on('click', function (e) {
  e.preventDefault();
  $('.modal-cierre-caja').modal('hide');
});
$('.btn-x-close-modal-sincorte').on('click', function (e) {
  e.preventDefault();
  $('.notexists').modal('hide');
});
$('.btn-close-modal-sincorte').on('click', function (e) {
  e.preventDefault();
  $('.notexists').modal('hide');
});
$('.btn-close-crearCorte').on('click', function (e) {
  e.preventDefault();
  $('.modal-abrir-corte').modal('hide');
});
$('.btn-x-close-crearCorte').on('click', function (e) {
  e.preventDefault();
  $('.modal-abrir-corte').modal('hide');
});
$('.btn-x-close-modal-foliosAbiertos').on('click', function (e) {
  e.preventDefault();
  $('.modal-opensale').modal('hide');
});
$('.btn-close-modal-foliosAbiertos').on('click', function (e) {
  e.preventDefault();
  $('.modal-opensale').modal('hide');
});


/*============================================================================*/

/*Abre caja por medio del registro de un fondo de caja =======================*/
$('#btn-openbox').on('click', function (e) {
  e.preventDefault();

  var type = 'post';
  var raiz = dameraizextra(nivel);
  var url  = raiz+url_existe_caja_abierta;

  //console.log(url);
  $.ajax({
    type: type,
    data: {data:'revisar'},
    url : url,
    cache: false,
    dataType: 'Json',
    success: function (response) {
      switch (response) {
        case 0:
          $('.modal-abrir-corte').modal('show');
          break;
        default:
        if (response => 1) {
          $('.modal-existe-caja').modal('show');
        }else {
          alertify.error("¡Oops... algo salio mal!");
          console.log(response);
        }

      }
    }
  });

});
/*============================================================================*/

/*Abre caja para operar ======================================================*/
$('.form-fondobox').on('submit', function (e) {
  e.preventDefault();
  var form   = $(this);
  var data   = form.serialize();
  var type   = form.attr('method');
  var raiz   = dameraizextra(nivel);
  var fondo  = $('.fondo').val();
  var url    = raiz+url_abrir_caja;

  if (fondo == vacio) {
    $('.fondo').addClass('input-danger');
    $('.fondo').focus();
    $('.help-fondo').text(help_fondo);
    $('.help-fondo').addClass('text-danger');

    $('.fondo').keyup(function () {
      $(this).removeClass('input-danger');
      $('.help-fondo').text('');
    });
  }else {
    $.ajax({
      type: type,
      data: data,
      url : url,
      cache: false,
      dataType: 'Json',
      success: function (response) {
        switch (response) {
          case 1:
            alertify.success("¡Se agrego correctamente el fondo de caja!");
            $('.modal-abrir-corte').modal('hide');
            break;
          default:
          alertify.error("¡Oops... algo salio mal!");
          console.log(response);
        }
      }
    });
  }

});
/*============================================================================*/

/* redirecciona a la sección del registro de salidas de la caja ==============*/
$('.btn-salida').on('click', function (e) {
  e.preventDefault();

  var type         = 'post';
  var raiz         = dameraiz(nivel);
  var raizextra    = dameraizextra(nivel);
  var url_registro = raiz+url_registrode_salidas;
  var url          = raizextra+url_existe_caja_abierta;

  console.log(url_registro);

  $.ajax({
    type: type,
    data: {data:'revisar'},
    url : url,
    cache: false,
    dataType: 'Json',
    success: function (response) {
      switch (response) {
        case 0:
          $('.notexists').modal('show');
          break;
        case 1:
          $(location).attr('href', url_registro);
          break;
        default:
          alertify.error("¡Oops... algo salio mal!");
          console.log(response);
      }
    }
  });
});
/*============================================================================*/

/*Revisa si hay caja abierta cuando se le da click al botón para entregar caja*/
$('.btn-entregacaja').on('click', function (e) {
  e.preventDefault();

  var raiz      = dameraiz(nivel);
  var raizextra = dameraizextra(nivel);
  var url       = raizextra+url_existe_caja_abierta;

  //console.log(url);

  $.ajax({
    type: 'post',
    data: {data:'revisar'},
    url : url,
    cache: false,
    dataType: 'Json',
    success: function (response) {
      switch (response) {
        case 1:
          $('.modal-cierre-caja').modal('show');
          break;
        case 0:
          $('.notexists').modal('show');
          break;
        default:
        alertify.error("¡Oops... algo salio mal!");
        console.log(response);
      }
    }
  });

});
/*============================================================================*/

/*form para entregar caja ====================================================*/
$('#form-entregabox').on('submit', function (e) {
  e.preventDefault();
  var form       = $(this);
  var data       = form.serialize();
  var type       = form.attr('method');

  var raizextra   = dameraizextra(nivel);
  var raiz        = dameraiz(nivel);
  var url         = raizextra+url_entregaCaja;
  var url_reponse = raiz+url_detalleCaja;
  //var cotizacion = raiz+url_cotizacion;

  var monto    = $('.entrega').val();

  if (monto == vacio) {
    $('.entrega').addClass('input-danger');
    $('.entrega').focus();
    $('.help-entrega').text(help_monto);
    $('.help-entrega').addClass('text-danger');

    $('.entrega').keyup(function () {
      $(this).removeClass('input-danger');
      $('.help-entrega').text('');
    });
  }else {
    $.ajax({
      type: type,
      data: data,
      url : url,
      cache: false,
      dataType: 'Json',
      success:function (response) {
        if (response == "does_not_exist") {

          alertify.log("¡Oops.. no tienes caja abierta!");

        }else if (response == "error_search" || response == "error_insert") {

          alertify.error("¡Oops... algo salio mal!");
          console.log(response);

        }else {

          if (response.data) {
            $('.modal-close-box').modal('hide');
            $('.modal-opensale').modal('show');
            response.data.forEach((o) => {

              $('.fv-abiertos').text(o.folio);

            });
          }else {

            $(location).attr('href', url_reponse+response);

          }

        }
      }
    });
  }



});
/*============================================================================*/

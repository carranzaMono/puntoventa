$(document).ready(function () {
  dataTable();
});

var dataTable = function () {
  var table = $('.dt-cortecaja').DataTable({
    //Muestra o oculta funciones de la tabla
    "lengthChange": false,
    "pageLength"  : 10,
    "responsive"  : true,
    "searching"   : true,
    "autoWidth"   : false,
    "ordering"    : false,
    "paging"      : true,
    "info"        : false,
    dom: 'Bfrtip',
    buttons: [
      'excel'
    ],

    //Permite reiniciar la tabla
    "destroy"     :true,

    //Cambia el idioma
    "language": idioma_es
  });
}

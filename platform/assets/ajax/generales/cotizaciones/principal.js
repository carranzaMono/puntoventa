$(document).ready(function () {
  getCotizaciones();
});

if (usu_tipo == admin) {

  var url_cotizacion        = "cotizacion.php?folio=";

}else {

}


var url_getCotizaciones  = "assets/php/generales/cotizaciones/get-cotizaciones.php";
var url_crear_cotizacion = "assets/php/generales/cotizaciones/crear-cotizaciones.php";
/*============================================================================*/

/*btn para generar una cotización ============================================*/
$('.btn-agregarNueva-cotizacion').on('click', function (e) {
  e.preventDefault();

  var raizextra = dameraizextra(nivel);
  var raiz      = dameraiz(nivel);
  var url       = raizextra+url_crear_cotizacion;

  $.ajax({
    type: 'post',
    url : url,
    cache: false,
    dataType: 'Json',
    success: function (response) {
      switch (response) {
        case 0:
          $('.notexists').modal('show');
          break;
        default:
        if (response == "error_create" || response == "error_insert_bit"){
          alertify.error("¡Oops... algo salio mal!");
          console.log(response);
        }else {
          $(location).attr('href',url_cotizacion+response);
        }

      }

    }
  });

});
/*============================================================================*/

var getCotizaciones = function () {

  var raiz = dameraizextra(nivel);
  var url  = raiz+url_getCotizaciones;

  var table = $('#dt-principal').DataTable({
    //Muestra o oculta funciones de la tabla
    "lengthChange": false,
    "pageLength"  : 6,
    "responsive"  : true,
    "searching"   : true,
    "autoWidth"   : true,
    "ordering"    : false,
    "paging"      : true,
    "info"        : false,

    //Permite reiniciar la tabla
    "destroy"     :true,

    //Enlista los datos en la tabla
    "ajax":{
      "method": "POST",
      "url"   : url
    },
    "columns":[
      {"data": "cot_folio"},
      {"data": "cot_fecha"},
      {"data": "cot_cliente"},
      {"data": "cot_status"},
      {"data": "cot_total"},
      {"defaultContent": "<div class='dropdown mo-mb-2'><button class='btn btn-sm btn-secondary dropdown-toggle' type='button' id='dropdownMenuButton' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>Acciones</button><div class='dropdown-menu' aria-labelledby='dropdownMenuButton'><a class='dropdown-item btn-view dropdown-itme-table' href='#'>Detalle</a><a class='dropdown-item btn-delete dropdown-itme-table' href='#'>Eliminar</a></div></div>"}
    ],

    //Cambia el idioma
    "language": idioma_es
  });
  btn_detalle("#dt-principal tbody", table);
  /*btn_delete_modal("#dt-categorias tbody", table);*/
}

var btn_detalle = function (tbody, table) {

  $(tbody).on('click', '.btn-view', function() {
    var data = table.row( $(this).parents("tr") ).data();

    if (data) {
      $(location).attr('href', url_cotizacion+data.cot_folio);
    }

  });

}

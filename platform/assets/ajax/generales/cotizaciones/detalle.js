$(document).ready(function () {
  get_encabezado();
  get_detalle();
  get_montos();
  //montosCotizacion();
});

var clave = $('.cotizacion').data('clave');

/*btns para cerrar modal =====================================================*/

/*Cerrar modal de confirmación para pasar una cotización a venta ==*/
$('.btn-x-close-modal-confimarCotiventa').on('click', function (e) {
  e.preventDefault();
  //console.log('hola');
  $('.modal-confimarCotiventa').modal('hide');
});

$('.btn-close-modal-confimarCotiventa').on('click', function (e) {
  e.preventDefault();
  $('.modal-confimarCotiventa').modal('hide');
});
/*=================================================================*/

/*Cerrar modal para cambiar cliente ==============================*/
$('.btn-x-close-cambiar-cliente').on('click', function (e) {
  e.preventDefault();
  //console.log('hola');
  $('.modal-cambiar-cliente').modal('hide');
});

$('.btn-modal-close-cambiar-cliente').on('click', function (e) {
  e.preventDefault();
  $('.modal-cambiar-cliente').modal('hide');
});
/*=================================================================*/

/*Cerrar modal para actualizar datos del cliente  =================*/
$('.btn-x-close-actualizar-datos-cliente').on('click', function (e) {
  e.preventDefault();
  //console.log('hola');
  $('.modal-actualizar-datos-cliente').modal('hide');
});

$('.btn-close-actualizar-datos-cliente').on('click', function (e) {
  e.preventDefault();
  $('.modal-actualizar-datos-cliente').modal('hide');
});
/*=================================================================*/
/*Cerrar modal para registrar clientes nuevos =====================*/
$('.btn-x-close-registro-cliente').on('click', function (e) {
  e.preventDefault();
  //console.log('hola');
  $('.modal-registro-cliente').modal('hide');
});

$('.btn-close-registro-cliente').on('click', function (e) {
  e.preventDefault();
  //console.log('hola');
  $('.modal-registro-cliente').modal('hide');
});
/*=================================================================*/
/*Cerrar modal para indicar que la cotización no cuenta con productos ==*/
$('.btn-x-close-modal-no-tiene-productos').on('click', function (e) {
  e.preventDefault();
  //console.log('hola');
  $('.modal-no-tiene-producto').modal('hide');
});

$('.btn-close-modal-no-tiene-productos').on('click', function (e) {
  e.preventDefault();
  //console.log('hola');
  $('.modal-no-tiene-producto').modal('hide');
});
/*======================================================================*/
/*============================================================================*/

if (usu_tipo == admin) {
  var url_venta       = "venta.php?folio=";
  var url_generar_pdf = "cotizacion/archivo/pdf.php?cotizacion="+clave;
}

/*Urls =======================================================================*/
var url_datos_cotizacion         = "assets/php/generales/cotizaciones/datos.php?clave="+clave;
var url_agregar_producto         = "assets/php/generales/cotizaciones/agregar-producto.php?clave="+clave;
var url_getDetalle               = "assets/php/generales/cotizaciones/detalle.php?clave="+clave;
var url_getMontos                = "assets/php/generales/cotizaciones/montos.php?clave="+clave;
var url_cotizacion_a_venta       = "assets/php/generales/cotizaciones/cotizacion-a-venta.php?cotizacion="+clave;

var url_tipo_cliente             = "assets/php/generales/tipo-cliente.php?clave="+clave;
var url_registro_cliente         = "assets/php/generales/cliente/registro-cliente-cotizacion.php?cotizacion="+clave;
var url_cambiar_cliente          = "assets/php/generales/cliente/cambiar-cliente-cotizacion.php?cotizacion="+clave;
var url_actualizar_datos_cliente = "assets/php/generales/cliente/actualizar-datos-cliente-cotizacion.php";

var url_existe_producto          = "assets/php/generales/existe-product.php?clave="+clave;
var url_aumentar_cantidad        = "assets/php/generales/aumentar-cantidad.php";
var url_disminuir_cantidad       = "assets/php/generales/disminuir-cantidad.php";
var url_revisar_precios          = "assets/php/generales/cotizaciones/revisar-precios.php?cotizacion="+clave;
var url_ver_cambios_de_precios   = "assets/php/cotizacion/imprimir-cambioprecios.php?cotizacion="+clave;

/*============================================================================*/

/*Muestar datos del encabezado de la cotización ==============================*/
var get_encabezado = function () {
  var raizextra = dameraizextra(nivel);
  var raiz      = dameraiz(nivel);
  var url       = raizextra+url_datos_cotizacion;

  $.ajax({
    type: 'get',
    url : url,
    cache: false,
    dataType: 'Json',
    success: function (response) {
      if (response) {
        response.data.forEach((o) => {
          //console.log(o.subtotal);
          //console.log(o.cot_status);
          if (o.cot_status == cerrado || o.cot_status == cancelado) {

            if (o.cot_status == cerrado) {
              $('.status').text('Cotización terminda');
            }else {
              $('.status').text('Cotización cancelado');
            }
            $('.btn-addproducto').prop('disabled', true);
            $('.btn-cotivent').prop('disabled', true);
            $('.btn-generarpdf').prop('disabled', true);
            $('.btn-enviarcorreo').prop('disabled', true);
            $('.cantidad').prop('disabled', true);
            $('.producto').prop('disabled', true);
            $('.btn-decrease').prop('disabled', true);
            $('.btn-increase').prop('disabled', true);
          }

          if (o.cot_status == abierto) {
            $('.status').text('Cotización abierto');
            $('.btn-addproducto').prop('disabled', false);
            $('.btn-cotivent').prop('disabled', false);
            $('.btn-generarpdf').prop('disabled', false);
            $('.btn-enviarcorreo').prop('disabled', false);
            $('.cantidad').prop('disabled', false);
            $('.producto').prop('disabled', false);
            $('.btn-decrease').prop('disabled', false);
            $('.btn-increase').prop('disabled', false);
          }

          /*$('.subtotal').text(o.subtotal);
          $('.iva').text(o.iva);*/
          $('.cli-nombre').text(o.cli_nombre);
          $('.cli-correo').text(o.cli_correo);
          $('.cli-telf').text(o.cli_telefono);

          if (o.cli_telefono == '' || o.cli_telefono == null) {
            $('.cli-telf').text("Sin teléfono");
          }else {
            $('.cli-telf').text(o.cli_telefono);
          }

          if (o.cli_correo == '' || o.cli_correo == null) {
            $('.cli-correo').text("Sin correo");
          }else {
            $('.cli-correo').text(o.cli_correo);
          }

          if (o.cli_direccion == '' || o.cli_direccion == null) {
            $('.cli_direccion').text("Sin dirección");
          }else {
            $('.cli_direccion').text(o.cli_direccion);
          }

          if (o.cli_rfc == '' || o.cli_rfc == null) {
            $('.cli-rfc').text("Sin RFC");
          }else {
            $('.cli-rfc').text(o.cli_rfc);
          }

          if (o.cot_envio == 0) {
            $('.content-envio').hide();
          }else {
            $('.content-envio').show();
          }

          //$('.total').text(o.total);

        });
      }
    }
  });
}
/*============================================================================*/

/*btn para abrir modal para actualizar o cambiar cliente  ====================*/
$('.btn-update-cliente').on('click', function (e) {
  e.preventDefault();

  var raizextra  = dameraizextra(nivel);
  var raiz       = dameraiz(nivel);
  var url        = raizextra+url_tipo_cliente;

  if (cotizacion) {
    $.ajax({
      type: 'get',
      data: {apartado: 1},
      url : url,
      cache: false,
      dataType: 'Json',
      success: function (response) {

        switch (response) {
          case "pg"://publico en general
            $('.modal-cambiar-cliente').modal('show');
            $('.search').append('').trigger('change');
            $('.help-cliente').text(vacio);
            break;
          default:

            if (response) {

              response.data.forEach((o) => {

                $('.u-cli-clave').val(o.cli_clave);
                $('.unombre').val(o.cli_nombre);
                $('.utelefono').val(o.cli_telefono);
                $('.ucorreo').val(o.cli_correo);
                $('.urfc').val(o.cli_rfc);
                $('.udireccion').val(o.cli_direccion);

              });

              $('.modal-actualizar-datos-cliente').modal('show');
            }

        }
      }
    });
  }

});
/*============================================================================*/

/*formulario para cambiar el cliente =========================================*/
$('#form-cambiar-cliente').on('submit', function (e) {
  e.preventDefault();

  var form       = $(this);
  var data       = form.serialize();
  var type       = form.attr('method');

  var raizextra  = dameraizextra(nivel);
  var raiz       = dameraiz(nivel);
  var url        = raizextra+url_cambiar_cliente;

  var cliente = $('.cliente').val();

  if (cliente == "") {
    $('.cliente').addClass('input-danger');
    $('.cliente').focus();
    $('.help-cliente').text(help_cliente);
    $('.help-cliente').addClass('text-danger');

    /*$('.cliente').keyup(function () {
      $(this).removeClass('input-danger');
      $('.help-cliente').text('');
    });*/
  }else {
    $.ajax({
      type: type,
      data: data,
      url : url,
      cache: false,
      dataType: 'Json',
      success: function (response) {

        switch (response) {
          case 1:

            $('.modal-cambiar-cliente').modal('hide');
            alertify.success("¡Se modifico correctamente!");
            get_encabezado();
            $('.search').append(null).trigger('change');
            $('.help-cliente').text(vacio);

            break;
          default:
          alertify.error("¡Oops... algo salio mal!");

        }

      }
    });
  }

});
/*============================================================================*/

/*formulario para actualizar datos del cliente ===============================*/
$('#form-actualizar-datos-cliente').on('submit', function (e) {
  e.preventDefault();

  var form       = $(this);
  var data       = form.serialize();
  var type       = form.attr('method');

  var raizextra  = dameraizextra(nivel);
  var raiz       = dameraiz(nivel);
  var url        = raizextra+url_actualizar_datos_cliente;

  var rfc       = $('.urfc').val();
  var nombre    = $('.unombre').val();
  var correo    = $('.ucorreo').val();
  var telefono  = $('.utelefono').val();
  var direccion = $('.udireccion').val();

  if (nombre == vacio) {
    $('.unombre').addClass('input-danger');
    $('.unombre').focus();
    $('.help-unombre').text(help_nombre);
    $('.help-unombre').addClass('text-danger');

    $('.unombre').keyup(function () {
      $(this).removeClass('input-danger');
      $('.help-unombre').text('');
    });
  }

  if ([nombre].includes(vacio)) {

  }else {

    $.ajax({
      type: type,
      data: data,
      url : url,
      cache: false,
      dataType: 'Json',
      success:function (response) {
        switch (response) {
          case "error_update":
            alertify.error("¡Oops... algo salio mal!");
            console.log(response);
            break;
          case "cli_exist":
            alertify.log("¡Oops.. el cliente ya existe!");
            break;

          default:

          $('.modal-datos-uCliente').modal('hide');
          alertify.success("¡Se modifico correctamente!");
          get_encabezado();

        }
      }
    });

  }

});
/*============================================================================*/

$('.btn-registro-cliente').on('click', function (e) {
  e.preventDefault();

  $('.modal-cambiar-cliente').modal('hide');
  $('.modal-actualizar-datos-cliente').modal('hide');
  $('.modal-registro-cliente').modal('show');

});
/*============================================================================*/

/*formulario para dar de alta un cliente =====================================*/
$('.form-registro-cliente').on('submit', function (e) {
  e.preventDefault();
  var form       = $(this);
  var data       = form.serialize();
  var type       = form.attr('method');

  var raizextra  = dameraizextra(nivel);
  var raiz       = dameraiz(nivel);
  var url        = raizextra+url_registro_cliente;

  var rfc       = $('.rfc').val();
  var nombre    = $('.nombre').val();
  var correo    = $('.correo').val();
  var telefono  = $('.telefono').val();
  var direccion = $('.direccion').val();

  if (nombre == vacio) {
    $('.nombre').addClass('input-danger');
    $('.nombre').focus();
    $('.help-nombre').text(help_nombre);
    $('.help-nombre').addClass('text-danger');

    $('.nombre').keyup(function () {
      $(this).removeClass('input-danger');
      $('.help-nombre').text('');
    });
  }

  if ([nombre].includes(vacio)) {

  }else {

    $.ajax({
      type: type,
      data: data,
      url : url,
      cache: false,
      dataType: 'Json',
      success:function (response) {
        switch (response) {
          case "error_save":
            alertify.error("¡Oops... algo salio mal!");
            console.log(response);
            break;
          case "cli_exist":
            alertify.log("¡Oops.. el cliente ya existe!");
            break;

          default:

          $('.modal-registro-cliente').modal('hide');
          alertify.success("¡Se modifico correctamente!");
          form[0].reset();
          get_encabezado();

        }
      }
    });

  }

});
/*============================================================================*/

/*Formulario para agregar productos o servicios ==============================*/
$('#form-add-product').on('submit', function (e) {
  e.preventDefault();
  var form     = $(this);
  var data     = form.serialize();
  var type     = form.attr('method');
  var producto = $('.producto').val();
  var cantidad = $('.cantidad').val();

  var raizextra = dameraizextra(nivel);
  var raiz      = dameraiz(nivel);
  var url       = raizextra+url_agregar_producto;


  if (producto == vacio) {
    $('.producto').addClass('input-danger');
    $('.producto').focus();
    $('#help-producto').text(help_producto).addClass('text-danger');

    $('.producto').keyup(function () {
      $(this).removeClass('input-danger');
      $('#help-producto').text('');
    });
  }

  if (cantidad == vacio) {
    $('.cantidad').addClass('input-danger');
    $('.cantidad').focus();
    $('#help-cantidad').text(help_cantidad).addClass('text-danger');

    $('.cantidad').keyup(function () {
      $(this).removeClass('input-danger');
      $('#help-cantidad').text('');
    });
  }

  if ([producto, cantidad].includes(vacio)) {

  }else {
    $.ajax({
      type: type,
      data: data,
      url : url,
      cache: false,
      dataType: 'Json',
      success: function (response) {
        switch (response) {
          case 1:
            alertify.success("¡Se agrego correctamente!");
            form[0].reset();
            $('.search').val(null).trigger('change');
            get_detalle();
            get_montos();
            break;
          default:
          alertify.error("¡Oops... algo salio mal!");
          console.log(response);
        }
      }
    });
  }

});
/*============================================================================*/

/*Muestra el detalle de la cotización ========================================*/
var get_detalle = function () {

  var raizextra = dameraizextra(nivel);
  var raiz      = dameraiz(nivel);
  var url       = raizextra+url_getDetalle

  var table = $('#dt-detalle').DataTable({
    //Muestra o oculta funciones de la tabla
    "lengthChange": false,
    "pageLength"  : 150,
    "responsive"  : true,
    "searching"   : false,
    "autoWidth"   : false,
    "ordering"    : false,
    "paging"      : false,
    "info"        : false,

    //Permite reiniciar la tabla
    "destroy"     :true,

    //Enlista los datos en la tabla
    "ajax":{
      "method": "POST",
      "url"   : url
    },
    "columns":[
      //{"data": "dc_clave"},
      {"defaultContent": "<button name='button' type='button' class='btn btn-outline-light btn-sm btn-decrease mt-1 mr-1' data-toggle='tooltip' data-placement='left' title='Disminuye de uno en uno'><i class='fas fa-minus-circle fa-sm'></i></button>"+
                         "<button name='button' type='button' class='btn btn-outline-light btn-sm btn-increase mt-1' data-toggle='tooltip' data-placement='right' title='Aumenta de uno en uno'><i class='fas fa-plus-circle fa-sm'></i></button>"},
      {"data": "dc_pro_nombre"},
      {"data": "dc_precioUnitario"},
      {"data": "dc_cantidad"},
      {"data": "dc_subtotal"},
      {"data": "dc_iva"},
      {"data": "dc_total"}
    ],

    //Cambia el idioma
    "language": idioma_es
  });
  btn_aumentar_cantidad("#dt-detalle tbody", table);
  btn_disminuir_cantidad("#dt-detalle tbody", table);
}
/*============================================================================*/

/*devuelve la suma de los totales de los productos o servicios ===============*/
var get_montos = function () {
  var raizextra = dameraizextra(nivel);
  var raiz      = dameraiz(nivel);
  var url       = raizextra+url_getMontos;

  $.ajax({
    type: 'get',
    url : url,
    cache: false,
    dataType: 'Json',
    success: function (response) {
      if (response) {
        response.data.forEach((o) => {

          $('.total').text(o.total);

        });
      }
    }
  });
}
/*============================================================================*/

/*Btn para aumentar cantidad de uno en uno ===================================*/
var btn_aumentar_cantidad = function (tbody, table) {

  $(tbody).on('click', '.btn-increase', function() {
    var data = table.row( $(this).parents("tr") ).data();

    if (data) {

      var dc_clave  = data.dc_clave;

      var raizextra = dameraizextra(nivel);
      var raiz      = dameraiz(nivel);
      var url       = raizextra+url_aumentar_cantidad;

      $.ajax({
        type: 'post',
        url : url,
        data: {apartado: cotizacion, dc_clave: dc_clave},
        cache: false,
        dataType: 'Json',
        success: function (response) {
          switch (response) {
            case 1:
              get_detalle();
              get_montos();
              alertify.success("¡Se aumento correctamente!");
              break;
            default:

          }
        }
      });
    }

  });

}
/*============================================================================*/

/*Btn para disminuir cantidad hasta eliminarlo del detalle ===================*/
var btn_disminuir_cantidad = function (tbody, table) {

  $(tbody).on('click', '.btn-decrease', function() {
    var data = table.row( $(this).parents("tr") ).data();

    if (data) {

      var dc_clave  = data.dc_clave;

      var raizextra = dameraizextra(nivel);
      var raiz      = dameraiz(nivel);
      var url       = raizextra+url_disminuir_cantidad;

      $.ajax({
        type: 'post',
        url : url,
        data: {apartado: cotizacion, dc_clave: dc_clave},
        cache: false,
        dataType: 'Json',
        success: function (response) {
          switch (response) {
            case 1:
              get_detalle();
              get_montos();
              alertify.success("¡Se disminuyo correctamente!");
              break;
            case 0:
              get_detalle();
              get_montos();
              alertify.success("¡Se elimino correctamente!");
              break;
            default:

          }
        }
      });

    }

  });

}
/*============================================================================*/

/*Genera un pdf de la cotización =============================================*/
$('.btn-generarpdf').on('click', function (e) {
  e.preventDefault();

  var raizextra = dameraizextra(nivel);
  var raiz      = dameraiz(nivel);
  var url       = raizextra+url_existe_producto;

  /*
  apartado 1 = cotizacion
           2 = venta
   */
  $.ajax({
    type: 'post',
    url : url,
    data: {apartado: 1},
    cache: false,
    dataType: 'Json',
    success: function (response) {

      switch (response) {
        case "0":
          $('.modal-no-tiene-producto').modal('show');
          break;
        default:
        if (response > 0) {
          window.open(url_generar_pdf, '_blank');
        }else if (response == "error_search") {
          alertify.error("¡Oops... algo salio mal!");
        }
      }

      //console.log(response);
    }
  });
  //$(location).attr('href', url_pdf);


});
/*============================================================================*/

/*Btn para mandar una cotización a venta =====================================*/
$('.btn-cotivent').on('click', function (e) {
  e.preventDefault();

  var raizextra = dameraizextra(nivel);
  var raiz      = dameraiz(nivel);
  var url1      = raizextra+url_revisar_precios;
  var url2      = raizextra+url_ver_cambios_de_precios;

  if (clave) {

    $.ajax({
      type: 'post',
      url : url1,
      cache: false,
      dataType: 'Json',
      success:function (r1) {

        switch (r1) {
          case "error_search":
            console.log(response);
            alertify.error("¡Oops... algo salio mal!");
            break;
          case "precios-scambios":
            $('.modal-confimarCotiventa').modal('show');
            break;
          case "np":
            $('.modal-no-tiene-producto').modal('show');
            break;
          default:

            $.ajax({
              type: 'post',
              url : url2,
              cache: false,
              dataType: 'html',
              success: function (r2) {
                if (r2) {

                  $('.content-newinfo-price').html(r2);
                  $('.modal-cambio-precio').modal('show');

                }else {
                  console.log(r2);
                }
            }
          });

        }

      }
    });

  }

});
/*============================================================================*/

$('.btn-confirmacion-cotvent').on('click', function (e) {
  e.preventDefault();

  var raizextra = dameraizextra(nivel);
  var raiz      = dameraiz(nivel);
  var url       = raizextra+url_cotizacion_a_venta;

  $.ajax({
    type: 'post',
    url : url,
    cache: false,
    dataType: 'Json',
    success: function (response) {

      switch (response) {
        case "sin_items":
          alertify.log("¡Oops... la cotización no tiene items!");
          break;
        case "sin_caja":
          alertify.log("¡Oops... no tienes una caja abierta");
          break;
        case "error_insert":
          alertify.error("¡Oops... algo salio mal!");
          break;
        case "not_save":
          alertify.error("¡Oops... no se pudo guardar!");
          break;
        default:
        $(location).attr('href', url_venta+response);
      }

    }
  });

});

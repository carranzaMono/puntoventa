$(document).ready(function () {
  get_encabezado();
  get_detalle();
  get_montos();
});

/*Variables ==================================================================*/
var venta = $('.venta').data('clave');
var folio = $('.venta').data('folio');
/*============================================================================*/

/*Urls =======================================================================*/
if (usu_tipo == admin) {
  var url_cobrar           = "venta/cobrar.php?folio="+folio;
}else {
  var url_cobrar           = "cobrar.php?folio="+folio;
}
var url_getDatos            = "assets/php/generales/venta/getDatos.php?venta="+venta;
var url_add_product_name    = "assets/php/caja/add-product-name.php?venta="+venta;
var url_getMontos           = "assets/php/generales/venta/getMontos.php?venta="+venta;
var url_detalle             = "assets/php/generales/venta/getDetalle.php?venta="+venta;
var url_saber_status        = "assets/php/generales/venta/saber-status.php?venta="+venta;
var url_terminar_de_agregar = "assets/php/generales/venta/terminar-de-agregar.php?venta="+venta;
/*botones especiales ================*/
var url_aum_product = "assets/php/generales/aumentar-cantidad.php?clave="+venta;
var url_dis_product = "assets/php/generales/disminuir-cantidad.php?clave="+venta;
/*============================================================================*/

/*Muestra datos de la venta ==================================================*/
var get_encabezado = function () {

  var raizextra = dameraizextra(nivel);
  var raiz      = dameraiz(nivel);
  var url       = raizextra+url_getDatos;

  //console.log(nivel);

  $.ajax({
    type: 'post',
    url : url,
    dataType: 'Json',
    cache: false,
    success: function (response) {
      if (response) {
        response.data.forEach((o) => {

          $('.status').text(o.statusString);

          switch (o.ven_status) {
            case "1":
            $('.btn-addfor-code').prop('disabled', false);
            $('.codigo-barra').prop('disabled', false);
            $('.btn-addfor-name').prop('disabled', false);
            $('.nombre-producto').prop('disabled', false);
            $('.cantidad').prop('disabled', false);
              break;
            default:

            $('.btn-addfor-code').prop('disabled', true);
            $('.codigo-barra').prop('disabled', true);
            $('.btn-addfor-name').prop('disabled', true);
            $('.nombre-producto').prop('disabled', true);
            $('.cantidad').prop('disabled', true);

          }

        });
      }
    }
  });
}
/*============================================================================*/

/*Agregar productos pór nombre de productos ==================================*/
$('.form-addproductName').on('submit', function (e) {
  e.preventDefault();
  var form     = $(this);
  var data     = form.serialize();
  var type     = form.attr('method');
  var producto = $('.nombre-producto').val();
  var cantidad = $('.cantidad2').val();

  var raizextra = dameraizextra(nivel);
  var raiz      = dameraiz(nivel);

  var url       = raizextra+url_add_product_name;

  if (producto == vacio) {
    $('.nombre-producto').addClass('input-danger');
    $('.nombre-producto').focus();
    $('#helpform2-producto').text(help_producto).addClass('text-danger');

    $('.nombre-producto').keyup(function () {
      $(this).removeClass('input-danger');
      $('#helpform2-producto').text('');
    });
  }

  if (cantidad == vacio) {
    $('.cantidad').addClass('input-danger');
    $('.cantidad').focus();
    $('#helpform2-cantidad').text(help_cantidad).addClass('text-danger');

    $('.cantidad').keyup(function () {
      $(this).removeClass('input-danger');
      $('#helpform2-cantidad').text('');
    });
  }

  if ([producto, cantidad].includes(vacio)) {

  }else {
    $.ajax({
      type: type,
      data: data,
      url : url,
      cache: false,
      dataType: 'Json',
      success: function (response) {
        switch (response) {
          case 1:
            alertify.success("¡Se agrego correctamente!");
            get_montos();
            get_detalle();
            form[0].reset();
            break;
          default:
          if (response == 2 || response == 3) {
            alertify.log('¡Oops... ya no se puede agregar productos!');
          }else {
            alertify.error("¡Oops... algo salio mal!");
            console.log(response);
          }
        }
      }
    });
  }

});
/*============================================================================*/

/*muestrar los montos de la venta ===========================================*/
var get_montos = function () {

  var raizextra = dameraizextra(nivel);
  var raiz      = dameraiz(nivel);
  var url       = raizextra+url_getMontos;

  $.ajax({
    type: 'get',
    url : url,
    cache: false,
    dataType: 'Json',
    success: function (response) {
      if (response) {
        response.data.forEach((o) => {

          /*console.log(o.subtotal);
          $('.subtotal').text(o.subtotal);
          $('.iva').text(o.iva);*/
          $('.total').text(o.total);

        });
      }
    }
  });
}
/*============================================================================*/

/*DataTable detalle de la venta ==============================================*/
var get_detalle = function () {

  var raizextra = dameraizextra(nivel);
  var raiz      = dameraiz(nivel);
  var url       = raizextra+url_detalle;

  var table = $('#dt-detalle').DataTable({
    //Muestra o oculta funciones de la tabla
    "lengthChange": false,
    "pageLength"  : 50,
    "responsive"  : true,
    "searching"   : false,
    "autoWidth"   : false,
    "ordering"    : false,
    "paging"      : true,
    "info"        : false,

    //Permite reiniciar la tabla
    "destroy"     :true,

    //Enlista los datos en la tabla
    "ajax":{
      "method": "POST",
      "url"   : url
    },
    "columns":[
      {"defaultContent": "<button name='button' type='button' class='btn btn-outline-light btn-sm btn-decrease mt-1 mr-1' data-toggle='tooltip' data-placement='left' title='Disminuye de uno en uno'><i class='fas fa-minus-circle fa-sm'></i></button>"+
                         "<button name='button' type='button' class='btn btn-outline-light btn-sm btn-increase mt-1' data-toggle='tooltip' data-placement='right' title='Aumenta de uno en uno'><i class='fas fa-plus-circle fa-sm'></i></button>"},
      {"data": "dv_pro_nombre"},
      {"data": "dv_precioUnitario"},
      {"data": "dv_cantidad"},
      {"data": "dv_subtotal"},
      {"data": "dv_iva"},
      {"data": "dv_total"}
      /*{"defaultContent": "<div class='dropdown mo-mb-2'><button class='btn btn-sm btn-secondary dropdown-toggle' type='button' id='dropdownMenuButton' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>Acciones</button><div class='dropdown-menu' aria-labelledby='dropdownMenuButton'><a class='dropdown-item btn-update dropdown-itme-table' href='#'>Modificar</a><a class='dropdown-item btn-delete dropdown-itme-table' href='#'>Eliminar</a></div></div>"}*/
    ],

    //Cambia el idioma
    "language": idioma_es
  });
  btn_increase_product("#dt-detalle tbody", table);
  btn_decrease_product("#dt-detalle tbody", table);
}
/*============================================================================*/

/*Aumenta de uno en uno la cantidad de un producto ===========================*/
var btn_increase_product = function (tbody, table) {

  $(tbody).on('click', '.btn-increase', function() {
    var data = table.row( $(this).parents("tr") ).data();

    if (data) {

      var dv_clave  = data.dv_clave;

      var raizextra = dameraizextra(nivel);
      var raiz      = dameraiz(nivel);
      var url       = raizextra+url_aum_product;

      $.ajax({
        type: 'post',
        url : url,
        data: {apartado: venta, dv_clave: dv_clave},
        cache: false,
        dataType: 'Json',
        success: function (response) {
          switch (response) {
            case 1:
              get_montos();
              get_detelle();
              alertify.success("¡Se aumento correctamente!");
              break;
            default:
              if (response == 2 || response == 3) {
                  alertify.log('¡Oops... ya no puede aumentar!');
              }

          }
        }
      });
    }

  });

}
/*============================================================================*/

/*Disminuye de uno en uno la cantidad de un producto hasta eliminarlo ========*/
var btn_decrease_product = function (tbody, table) {

  $(tbody).on('click', '.btn-decrease', function() {
    var data = table.row( $(this).parents("tr") ).data();

    if (data) {

      var dv_clave  = data.dv_clave;

      var raizextra = dameraizextra(nivel);
      var raiz      = dameraiz(nivel);
      var url       = raizextra+url_dis_product;

      $.ajax({
        type: 'post',
        url : url,
        data: {apartado: venta, dv_clave: dv_clave},
        cache: false,
        dataType: 'Json',
        success: function (response) {
          switch (response) {
            case 1:
              get_montos();
              get_detelle();
              alertify.success("¡Se disminuyo correctamente!");
              break;
            case 0:
              get_montos();
              get_detelle();
              alertify.success("¡Se elimino correctamente!");
              break;
            default:
              if (response == 2 || response == 3) {
                alertify.log('¡Oops... ya no puede desminuir!');
              }

          }
        }
      });

    }

  });

}
/*============================================================================*/

/*Botón para mandar a la sección cobrar ======================================*/
$('.btn-iracobrar').on('click', function (e) {
  e.preventDefault();

  var raizextra = dameraizextra(nivel);
  var raiz      = dameraiz(nivel);
  var url1      = raizextra+url_saber_status;
  var url2      = raizextra+url_terminar_de_agregar;

  $.ajax({
    type: 'post',
    url : url1,
    cache: false,
    dataType: 'Json',
    success: function (r1) {
      console.log(r1);
      switch (r1) {
        case "1":
          $.ajax({
            type: 'post',
            url : url,
            cache: false,
            dataType: 'Json',
            success: function (response) {

              switch (response) {
                case 1:
                  if (usu_tipo == admin) {

                    $(location).attr('href', url_cobrar);

                  }else {
                    get_encabezado();
                    alertify.success("¡Se cambio el status correctamente!");
                  }
                break;
                default:

                  alertify.error("¡Oops... algo salio mal!");
                  console.log(response);

              }

            }
          });
        break;
        default:
          $(location).attr('href', url_cobrar);
      }
    }
  });


});
/*============================================================================*/

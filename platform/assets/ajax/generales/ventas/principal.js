$(document).ready(function () {

  switch (usu_tipo) {
    case admin:
      get_ventas();
      break;
    default:
      get_ventas();
      get_cortes();
  }

});

if (usu_tipo == admin) {

  var url_detalle_venta = "venta.php?folio=";

}else {

  var url_detalle_venta = "venta/detalle.php?folio=";
  var url_cobrar_venta  = "venta/cobrar.php?folio=";
  var url_detalle_ccj   = "detalle.php?folio=";

}
var url_crear_venta   = "assets/php/generales/venta/crear-venta.php";
var url_getVentas     = "assets/php/generales/venta/all-ventas.php";
var url_getCortes     = "assets/php/caja/getCortes.php";

var get_ventas = function () {

  var raiz = dameraizextra(nivel);
  var url  = raiz+url_getVentas;

  if (usu_tipo == admin) {

    var table = $('#dt-ventas').DataTable({
      //Muestra o oculta funciones de la tabla
      "lengthChange": false,
      "pageLength"  : 15,
      "responsive"  : true,
      "searching"   : false,
      "autoWidth"   : false,
      "ordering"    : false,
      "paging"      : true,
      "info"        : false,

      //Permite reiniciar la tabla
      "destroy"     :true,

      //Enlista los datos en la tabla
      "ajax":{
        "method": "POST",
        "url"   : url
      },
      "columns":[
        {"data": "ven_folio"},
        {"data": "fecha"},
        {"data": "hora"},
        {"data": "cli_nombre"},
        {"data": "ven_status"},
        {"data": "ven_total"},
        {"defaultContent": "<button type='button' class='btn btn-secondary btn-sm btn-detail waves-effect'>Ver detalle</button>"}
      ],

      //Cambia el idioma
      "language": idioma_es
    });

    btn_view_detail_venta("#dt-ventas tbody", table);

  }else {

    var table = $('#dt-ventas').DataTable({
      //Muestra o oculta funciones de la tabla
      "lengthChange": false,
      "pageLength"  : 15,
      "responsive"  : true,
      "searching"   : false,
      "autoWidth"   : false,
      "ordering"    : false,
      "paging"      : true,
      "info"        : false,

      //Permite reiniciar la tabla
      "destroy"     :true,

      //Enlista los datos en la tabla
      "ajax":{
        "method": "POST",
        "url"   : url
      },
      "columns":[
        {"data": "ven_folio"},
        {"data": "fecha"},
        {"data": "hora"},
        {"data": "cli_nombre"},
        {"data": "ven_status"},
        {"data": "ven_total"},
        {"defaultContent": "<button type='button' class='btn btn-secondary btn-sm btn-cobrar waves-effect'>Cobrar</button>"}
      ],

      //Cambia el idioma
      "language": idioma_es
    });

    btn_cobrar_venta("#dt-ventas tbody", table);

  }


}

/*btn para abrir el detalle de la ventana ====================================*/
var btn_view_detail_venta = function (tbody, table) {

  $(tbody).on('click', '.btn-detail', function() {
    var data = table.row( $(this).parents("tr") ).data();

    if (data) {

      var folio = data.ven_folio;
      var url   = url_detalle_venta+folio;

      //console.log(url);
      $(location).attr('href',url);

    }

  });

}
/*============================================================================*/

/*btn para abrir el detalle de la ventana ====================================*/
var btn_cobrar_venta = function (tbody, table) {

  $(tbody).on('click', '.btn-cobrar', function() {
    var data = table.row( $(this).parents("tr") ).data();

    if (data) {

      var folio = data.ven_folio;
      var url   = url_cobrar_venta+folio;

      //console.log(url);
      $(location).attr('href',url);

    }

  });

}
/*============================================================================*/

var get_cortes = function () {

  var raiz = dameraizextra(nivel);
  var url  = raiz+url_getCortes;

  var table = $('#dt-cortesCaja').DataTable({
    //Muestra o oculta funciones de la tabla
    "lengthChange": false,
    "pageLength"  : 15,
    "responsive"  : true,
    "searching"   : false,
    "autoWidth"   : false,
    "ordering"    : false,
    "paging"      : true,
    "info"        : false,

    //Permite reiniciar la tabla
    "destroy"     :true,

    //Enlista los datos en la tabla
    "ajax":{
      "method": "POST",
      "url"   : url
    },
    "columns":[
      {"data": "ccj_folio"},
      {"data": "ccj_inicio"},
      {"data": "ccj_emp_cerro"},
      {"data": "ccj_fin"},
      {"data": "ccj_status"},
      {"data": "ccj_ventatotal"},
      {"defaultContent": "<button type='button' class='btn btn-secondary btn-sm btn-detail waves-effect'>Ver detalle</button>"}
    ],

    //Cambia el idioma
    "language": idioma_es
  });

  btn_view_detail_caja("#dt-cortesCaja tbody", table);
}


/*btn para abrir el detalle del corte de caja ================================*/
var btn_view_detail_caja = function (tbody, table) {

  $(tbody).on('click', '.btn-detail', function() {
    var data = table.row( $(this).parents("tr") ).data();

    if (data) {

      var folio = data.ccj_folio;
      var url   = url_detalle_ccj+folio;

      $(location).attr('href',url);

    }

  });

}
/*============================================================================*/

/*btn para crear una nueva venta =============================================*/
$('.btn-agregarNueva-venta').on('click', function (e) {
  e.preventDefault();

  var raizextra = dameraizextra(nivel);
  var raiz      = dameraiz(nivel);
  var url       = raizextra+url_crear_venta;

  $.ajax({
    type: 'post',
    url : url,
    cache: false,
    dataType: 'Json',
    success: function (response) {
      switch (response) {
        case 0:
          $('.notexists').modal('show');
          break;
        default:
        if (response == "error_create" || response == "error_insert_bit"){
          alertify.error("¡Oops... algo salio mal!");
          console.log(response);
        }else {
          $(location).attr('href',url_detalle_venta+response);
        }

      }

    }
  });

});
/*============================================================================*/

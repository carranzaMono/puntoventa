$(document).ready(function () {
  //console.log(venta);
  get_datosDeLaVenta();
  get_montosDeLaVenta();
  get_cobros();
});

/*Varibles ===================================================================*/
var venta = $('.venta').data('clave');
var folio = $('.venta').data('folio');
/*============================================================================*/

/*Urls =======================================================================*/
var url_getDatos  = "assets/php/generales/venta/getDatos.php?venta="+venta;
var url_getMontos = "assets/php/generales/cobros/getMontos.php?venta="+venta;
var url_addPago   = "assets/php/generales/cobros/agregarpago.php?clave="+venta;
var url_cambio    = "assets/php/generales/cobros/cambio.php?venta="+venta;
var url_getCobros = "assets/php/generales/cobros/getCobros.php?venta="+venta;
/*============================================================================*/

/*Muestra datos de la venta ==================================================*/
var get_datosDeLaVenta = function () {

  var raizextra = dameraizextra(nivel);
  var raiz      = dameraiz(nivel);
  var url       = raizextra+url_getDatos;

  //console.log(nivel);

  $.ajax({
    type: 'post',
    url : url,
    dataType: 'Json',
    cache: false,
    success: function (response) {
      if (response) {
        response.data.forEach((o) => {

          $('.status').text(o.statusString);

          if (o.ven_status == 1 || o.ven_status == 3) {
            $('.forma-pago').prop('disabled', false);
            $('.cobro').prop('disabled', false);
            $('.btn-agregar').prop('disabled', false);
          }else {
            $('.forma-pago').prop('disabled', true);
            $('.cobro').prop('disabled', true);
            $('.btn-agregar').prop('disabled', true);
          }

        });
      }
    }
  });
}
/*============================================================================*/

/*muestrar los montos de la venta  ===========================================*/
var get_montosDeLaVenta = function () {

  var raizextra = dameraizextra(nivel);
  var raiz      = dameraiz(nivel);
  var url       = raizextra+url_getMontos;

  //console.log(nivel);

  $.ajax({
    type: 'get',
    url : url,
    cache: false,
    dataType: 'Json',
    success: function (response) {
      if (response) {
        response.data.forEach((o) => {
          //console.log(o.subtotal);
          $('.subtotal').text(o.subtotal);
          $('.iva').text(o.iva);
          $('.total').text(o.total);
          $('.cobrado').text(o.cobrado);
          $('.resta').text(o.resta);

          //console.log(o.ven_status);

        });
      }
    }
  });
}
/*============================================================================*/

/*btn-cobrar manda a la sección para cobrar ==================================*/
$('.form-addcobro').on('submit', function (e) {
  e.preventDefault();
  var form = $(this);
  var data = form.serialize();
  var type = form.attr('method');

  var raizextra = dameraizextra(nivel);
  var raiz      = dameraiz(nivel);
  var url       = raizextra+url_addPago;

  var forma_pago  = $('.forma-pago').val();
  var cobro       = $('.cobro').val();

  if (forma_pago == vacio) {
    $('.forma-pago').addClass('input-danger');
    $('.forma-pago').focus();
    $('#help-fpago').text(help_fpago).addClass('text-danger');

    $('.forma-cobro').keyup(function () {
      $(this).removeClass('input-danger');
      $('#help-fcobro').text('');
    });
  }

  if (cobro == vacio) {
    $('.cobro').addClass('input-danger');
    $('.cobro').focus();
    $('#help-cobro').text(help_monto).addClass('text-danger');

    $('.cobro').keyup(function () {
      $(this).removeClass('input-danger');
      $('#help-cobro').text('');
    });
  }

  if ([forma_pago, cobro].includes(vacio)) {

  }else {
    $.ajax({
      type: type,
      data: data,
      url : url,
      cache: false,
      dataType: 'Json',
      success: function (response) {
        switch (response) {
          case 1:

            get_datosDeLaVenta();
            dameCambio();
            get_cobros();
            get_montosDeLaVenta();
            alertify.success("¡Se agrego correctamente!");
            form[0].reset();

            break;
          default:
          if (response == "monto_excesivo") {
            alertify.log("¡Oops.. no puedes agregar pagos mayores cuando la forma de pago es diferente a efectivo!");
          }else{
            alertify.error("¡Oops... algo salio mal!");
            console.log(response);
          }
        }

      }
    });
  }


});
/*============================================================================*/

/*retorna el cambio si lo hay del ultimo pago agregado =======================*/
var dameCambio = function () {
  var raizextra = dameraizextra(nivel);
  var raiz      = dameraiz(nivel);
  var url       = raizextra+url_cambio;

  $.ajax({
    type: 'get',
    url : url,
    cache: false,
    dataType: 'Json',
    success: function (response) {
      if (response) {
        response.data.forEach((o) => {
          //console.log(o.subtotal);
          get_datosDeLaVenta();
          get_montosDeLaVenta()
          $('.text-cambio').text(o.cambio);
          $('.modal-cambio').modal('show');

        });
      }
    }
  });
}
/*============================================================================*/

/*Botón para cerrar modal que muestra el cambio ==============================*/
$('.btn-footer-close-cambio').on('click', function () {

  get_datosDeLaVenta();
  get_montosDeLaVenta();
  $('.text-cambio').text('');
  $('.modal-cambio').modal('hide');

});
/*============================================================================*/

/*lista de pagos realizados por el cliente ===================================*/
var get_cobros = function () {

  var raizextra = dameraizextra(nivel);
  var raiz      = dameraiz(nivel);

  var table = $('#dt-detalleCobros').DataTable({
    //Muestra o oculta funciones de la tabla
    "lengthChange": false,
    "pageLength"  : 8,
    "responsive"  : true,
    "searching"   : true,
    "autoWidth"   : false,
    "ordering"    : false,
    "paging"      : true,
    "info"        : false,

    //Permite reiniciar la tabla
    "destroy"     :true,

    //Enlista los datos en la tabla
    "ajax":{
      "method": "POST",
      "url"   : raizextra+url_getCobros
    },
    "columns":[
      {"data": "cob_clave"},
      {"data": "fecha"},
      {"data": "hora"},
      {"data": "fp_nombre"},
      {"data": "cob_monto"}
    ],

    //Cambia el idioma
    "language": idioma_es
  });
}
/*============================================================================*/

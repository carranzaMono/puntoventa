$(document).ready(function() {
  get_salidas();
});

var url_getSalidas     = "assets/php/generales/caja/getSalidas.php";
var url_create_salidas = "assets/php/generales/caja/agregar-salidas.php";

var get_salidas = function () {

  var raiz = dameraizextra(nivel);
  var url  = raiz+url_getSalidas;

  var table = $('#dt-salidas').DataTable({
    //Muestra o oculta funciones de la tabla
    "lengthChange": false,
    "pageLength"  : 15,
    "responsive"  : true,
    "searching"   : true,
    "autoWidth"   : false,
    "ordering"    : false,
    "paging"      : true,
    "info"        : false,

    //Permite reiniciar la tabla
    "destroy"     :true,

    //Enlista los datos en la tabla
    "ajax":{
      "method": "POST",
      "url"   : url
    },
    "columns":[
      {"data": "mc_clave"},
      {"data": "mc_observacion"},
      {"data": "mc_total"}
    ],

    //Cambia el idioma
    "language": idioma_es
  });
}


/*Registro de salidas ========================================================*/
$('.form-salidasbox').on('submit', function (e) {
  e.preventDefault();
  var form   = $(this);
  var data   = form.serialize();
  var type   = form.attr('method');

  var raizextra = dameraizextra(nivel);
  var raiz      = dameraiz(nivel);
  var url       = raizextra+url_create_salidas;

  var monto     = $('.monto').val();
  var concepto  = $('.concepto').val();

  if (concepto == vacio) {
    $('.concepto').addClass('input-danger');
    $('.concepto').focus();
    $('.help-concepto').text(help_concepto);
    $('.help-concepto').addClass('text-danger');

    $('.concepto').keyup(function () {
      $(this).removeClass('input-danger');
      $('.help-concepto').text('');
    });
  }

  if (monto == vacio) {
    $('.monto').addClass('input-danger');
    $('.monto').focus();
    $('.help-monto').text(help_monto);
    $('.help-monto').addClass('text-danger');

    $('.monto').keyup(function () {
      $(this).removeClass('input-danger');
      $('.help-monto').text('');
    });
  }

  if ([concepto, monto].includes(vacio)) {

  }else {

    $.ajax({
      type: type,
      data: data,
      url : url,
      cache: false,
      dataType: 'Json',
      success: function (response) {
        switch (response) {
          case 1:
            alertify.success("¡Se agrego correctamente!");
            get_salidas();
            form[0].reset();
            break;
          default:
            alertify.error("¡Oops... algo salio mal!");
            console.log(response);
        }
      }
    });

  }

});
/*============================================================================*/

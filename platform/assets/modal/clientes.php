<!-- modal para cambiar cliente =============================================-->
<div class="modal fade bd-example-modal-lg modal-cambiar-cliente" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"></h5>
        <button type="button" class="close btn-x-close-cambiar-cliente" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="form-cambiar-cliente" class="form-update-cliente" method="post">
          <div class="form-group">
            <label>Cliente*:</label>
            <select class="form-control form-control-sm search cliente" name="cliente" style="width: 100%;">
              <option value="">Selecciona algún cliente</option>
              <?php
                $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
                if (mysqli_connect_errno()) {
                  echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
                }

                $sql    = "SELECT cli_clave, cli_nombre from clientes where cli_status = 1 and cli_clave <> 1";
                $result = mysqli_query($con, $sql);
                if ($result) {
                while ($fila = mysqli_fetch_array($result)) {
              ?>
                <option value="<?= $fila[0]; ?>"><?= $fila[1]; ?></option>
              <?php
                  }
                }
              ?>
            </select>
            <div class="row">
              <div class="col-10">
                <small id="help-cliente" class="form-text text-muted text-danger help-cliente"></small>
              </div>
              <div class="col-2 text-right text-secondary">
                <a href="#" class="btn-registro-cliente">Registrar cliente</a>
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-modal-close-cambiar-cliente">¡No, cerrar!</button>
        <button type="submit" class="btn btn-primary" form="form-cambiar-cliente">¡Si, modificar!</button>
      </div>
    </div>
  </div>
</div>
<!--=========================================================================-->

<!-- modal para actualizar datos del cliente ================================-->
<div class="modal fade bd-example-modal-lg modal-actualizar-datos-cliente" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Actualizar datos del cliente</h5>
        <button type="button" class="close btn-x-close-actualizar-datos-cliente" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="form-actualizar-datos-cliente" class="form-actualizar-datos-cliente" method="post">
          <div class="row">
            <div class="col-12">
              <label>Nombre*:</label>
              <input type="number" hidden name="cliente" class="form-control form-control-sm u-cli-clave">
              <input class="form-control form-control-sm unombre" type="text" name="nombre">
              <small id="help-unombre" class="form-text text-muted text-danger help-unombre"></small>
            </div>
          </div>
          <div class="row">
            <div class="col-12 col-lg-4">
              <label>Teléfono:</label>
              <input type="text" class="form-control form-control-sm utelefono" name="telefono">
              <small id="help-utelefono" class="form-text text-muted text-danger help-utelefono"></small>
            </div>
            <div class="col-12 col-lg-4">
              <label>Correo:</label>
              <input type="email" class="form-control form-control-sm ucorreo" name="correo">
              <small id="help-ucorreo" class="form-text text-muted text-danger help-ucorreo"></small>
            </div>
            <div class="col-12 col-lg-4">
              <label>RFC:</label>
              <input type="text" class="form-control form-control-sm urfc" name="rfc">
              <small id="help-urfc" class="form-text text-muted text-danger help-urfc"></small>
            </div>
          </div>
          <div class="row">
            <div class="col-12">
              <label>Dirección:</label>
              <textarea class="form-control form-control-sm udireccion" name="direccion" rows="2"></textarea>
              <small id="help-udireccion" class="form-text text-muted text-danger help-udireccion"></small>
            </div>
          </div>
        </form>
        <div class="row mt-5">
          <div class="col-2 text-right text-secondary">
            <a href="#" class="btn-registro-cliente">Registrar cliente</a>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-close-actualizar-datos-cliente" name="button">¡No, modificar!</button>
        <button type="submit" class="btn btn-primary" form="form-actualizar-datos-cliente">¡Si, modificar!</button>
      </div>
    </div>
  </div>
</div>
<!--=========================================================================-->

<!-- modal para registrar clientes  =========================================-->
<div class="modal fade bd-example-modal-lg modal-registro-cliente" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Registro de cliente</h5>
        <button type="button" class="close btn-x-close-registro-cliente" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="form-registro-cliente" class="form-registro-cliente" method="post">
          <div class="row">
            <div class="col-12">
              <label>Nombre*:</label>
              <input class="form-control form-control-sm nombre" type="text" name="nombre">
              <small id="help-nombre" class="form-text text-muted text-danger help-nombre"></small>
            </div>
          </div>
          <div class="row">
            <div class="col-12 col-lg-4">
              <label>Teléfono:</label>
              <input type="text" class="form-control form-control-sm telefono" name="telefono">
              <small id="help-telefono" class="form-text text-muted text-danger help-telefono"></small>
            </div>
            <div class="col-12 col-lg-4">
              <label>Correo:</label>
              <input type="email" class="form-control form-control-sm correo" name="correo">
              <small id="help-correo" class="form-text text-muted text-danger help-correo"></small>
            </div>
            <div class="col-12 col-lg-4">
              <label>RFC:</label>
              <input type="text" class="form-control form-control-sm rfc" name="rfc">
              <small id="help-rfc" class="form-text text-muted text-danger help-rfc"></small>
            </div>
          </div>
          <div class="row">
            <div class="col-12">
              <label>Dirección:</label>
              <textarea class="form-control form-control-sm direccion" name="direccion" rows="2"></textarea>
              <small id="help-direccion" class="form-text text-muted text-danger help-direccion"></small>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-close-registro-cliente" data-dismiss="modal">¡No, cerrar!</button>
        <button type="submit" class="btn btn-primary" form="form-registro-cliente">¡Si, guardar!</button>
      </div>
    </div>
  </div>
</div>
<!--=========================================================================-->

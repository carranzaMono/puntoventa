<!-- modal para indicar que cambiaron precios en los productos ==============-->
<div class="modal fade bd-example-modal-lg modal-cambio-precio" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"></h5>
        <button type="button" class="close btn-x-close-modal-cambioprecio" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <label><strong>Los siguientes productos cambiaron de precio: </strong></label>
        <ul class="list-group content-newinfo-price">

        </ul>
        <!--p class="text-muted mt-2">
          <small>
            <strong class="text-danger">¡Advertencia!</strong>

          </small>
        </p-->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-close-modal-cambioprecio" data-dismiss="modal">¡No, actualizar!</button>
        <button type="submit" class="btn btn-primary btn-confirmacion-cambiar-precios" form="form-fondobox">¡Si, actualizar!</button>
      </div>
    </div>
  </div>
</div>
<!--=========================================================================-->

<!-- modal para indicar que existe producto en la cotización ================-->
<div class="modal fade bd-example-modal-lg modal-no-tiene-producto" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"></h5>
        <button type="button" class="close btn-x-close-modal-no-tiene-productos" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center">
        <span class="ti-info-alt ico-info info"></span>
        <h6>¡La cotizacion <span class="num-folio"><?= $cot_folio; ?></span> no cuenta con productos!</h6>
        <p><strong></strong></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-close-modal-no-tiene-productos" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<!--=========================================================================-->

<!-- modal para indicar que existe corte abierto ============================-->
<div class="modal fade bd-example-modal-lg modal-confimarCotiventa" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"></h5>
        <button type="button" class="close btn-x-close-modal-confimarCotiventa" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center">
        <span class="ion-ios7-help-outline ico-info info"></span>
        <h6>¿Esta seguro de terminar la cotizacion <span class="num-folio"><?= $cot_folio; ?></span>  y generar una venta?</h6>
        <p><strong></strong></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-close-modal-confimarCotiventa" data-dismiss="modal">¡No, terminar!</button>
        <button type="submit" class="btn btn-primary btn-confirmacion-cotvent">¡Si, terminar!</button>
      </div>
    </div>
  </div>
</div>
<!--=========================================================================-->

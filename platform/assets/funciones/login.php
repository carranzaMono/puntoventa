<?php
include_once "../../config.php";
include_once "searches.php";
include_once "generales.php";

if (isset($_POST['user'])) {

  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
     echo "Error en la conexion de mysql: " . mysqli_connect_error();
  }

  //123
  $username	 = stripslashes(trim($_POST['user']));
  $password  = stripslashes(trim($_POST['password']));
  $mono      = dameDatosMono();
  $valor     = "error";

  if ($username == $mono['username'] && password_verify($password, $mono['password']) == true) {

    session_start();
    $_SESSION['usu_clave'] 		 = 0;
    $_SESSION['usu_name']			 = $mono['username'];
    $_SESSION['usu_emp_clave'] = 0;
    $_SESSION['usu_tipo']			 = "1";
    $_SESSION['usu_empleado']	 = $mono['username'];

    $valor = $_SESSION['usu_tipo'];

  }else {

    $user = searchUserName($username);
    switch ($user['usu_count']) {
      case 0:
        $valor = "not_exist";
        break;
      case 1:
        if (password_verify($password, $user['usu_password']) == true) {

          $empleado = dameDatosEmpleados($user['usu_emp_clave']);
          if ($empleado != 'error_search') {
            session_start();
            $_SESSION['usu_clave'] 		 = $user['usu_clave'];
            $_SESSION['usu_name']			 = $user['usu_nombre'];
            $_SESSION['usu_password']	 = $password;
            $_SESSION['usu_emp_clave'] = $user['usu_emp_clave'];
            $_SESSION['usu_nivel']		 = $user['usu_nivel'];
            $_SESSION['usu_tipo']			 = $user['usu_tipo'];
            $_SESSION['usu_empleado']	 = $empleado['emp_nombre'].' '.$empleado['emp_apPaterno'];

            $modulo			  = "Sesión";
            $accion       = "Incio de sesión";
            $tipo         = 0;
            $descripcion  = $_SESSION['usu_empleado']." inicio sesión";

            $resultado = bitacora($modulo, $accion, $tipo, $_SESSION['usu_emp_clave'], $descripcion);
            if ($resultado) {
              $valor = $_SESSION['usu_tipo'];
            }else {
              $valor = $resultado;
            }

          }else {
            $valor = $empleado;
          }


        }else {
          $valor = "incorrect_pass";
        }
        break;

      default:
        $valor = $user;
        break;
    }


  }

  /*
   usu_tipo 1 = administrador,
            2 = meser@,
            3 = cajer@,
            4 = cocina,
            5 = barra
  */
 //echo $valor;
  echo json_encode($valor);
}

?>

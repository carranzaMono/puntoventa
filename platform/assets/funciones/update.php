<?php
include_once "searches.php";
include_once "maths.php";

/*modifica el status del puesto seleccionado =================================*/
function updateStatusPuesto($pue_clave){
  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $puesto     = dameDatosPuesto($pue_clave);
  $desactivar = 0;
  $activar    = 1;

  switch ($puesto['pue_status']) {
    case 1:
      $sql = "UPDATE puestos set pue_status = $desactivar where pue_clave = $pue_clave";
      break;

    default:
      $sql = "UPDATE puestos set pue_status = $activar where pue_clave = $pue_clave";
      break;
  }
  //echo $sql;
  $result = mysqli_query($con, $sql);
  $valor  = "error_update";
  if ($result) {
    $valor = 1;
  }

  return $valor;
}
/*============================================================================*/

/*Cambia el nombre del puesto seleccionado ===================================*/
function updateNombrePuesto($pue_clave, $pue_nombre){
  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $sql    = "UPDATE puestos set pue_nombre = '$pue_nombre' where pue_clave = $pue_clave";
  $result = mysqli_query($con, $sql);
  $valor  = "error_update";
  if ($result) {
    $valor = 1;
  }

  return $valor;
}
/*============================================================================*/

/*modifica el status del empleado seleccionado ===============================*/
function updateStatusEmpleados($emp_clave){
  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $empleado   = dameDatosEmpleados($emp_clave);
  $desactivar = 0;
  $activar    = 1;

  switch ($empleado['emp_status']) {
    case 1:
      $sql = "UPDATE empleados set emp_status = $desactivar where emp_clave = $emp_clave";
      break;

    default:
      $sql = "UPDATE empleados set emp_status = $activar where emp_clave = $emp_clave";
      break;
  }
  //echo $sql;
  $result = mysqli_query($con, $sql);
  $valor  = "error_update";
  if ($result) {
    $valor = 1;
  }

  return $valor;
}
/*============================================================================*/

/*modifica información del empleado seleccionado =============================*/
function updateEmpleado($emp_nombre, $emp_apPaterno, $emp_apMaterno, $nombreCompleto,
                        $puesto, $emp_direccion, $emp_telefono, $emp_correo, $emp_curp,
                        $emp_rfc, $emp_clave){

  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $sql    = "UPDATE empleados
                set emp_nombre = '$emp_nombre', emp_apPaterno = '$emp_apPaterno',
                    emp_apMaterno = '$emp_apMaterno', emp_direccion = '$emp_direccion',
                    emp_telefono = '$emp_telefono', emp_curp = '$emp_curp', emp_rfc = '$emp_rfc',
                    emp_correo = '$emp_correo', emp_pue_clave = '$puesto', emp_nombreCompleto = '$nombreCompleto'
              where emp_clave = $emp_clave";
  //echo $sql;
  $result = mysqli_query($con, $sql);
  $valor  = "error_update";
  if ($result) {
    $valor = 1;
  }

  return $valor;
}
/*============================================================================*/

/*modifica el status del usuario seleccionado ================================*/
function updateStatusUser($user_clave, $status){

  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $sql    = "UPDATE usuarios set usu_status = $status where usu_clave = $user_clave";
  $result = mysqli_query($con, $sql);
  $valor  = "error_update";
  if ($result) {
    $valor = 1;
  }

  return $valor;
}
/*============================================================================*/

/*modifica información del usuario seleccionado ==============================*/
function updateUser($usu_clave, $user_name, $user_tipo){

  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $sql    = "UPDATE usuarios set usu_nombre = '$user_name', usu_tipo = $user_tipo where usu_clave = $usu_clave";
  $result = mysqli_query($con, $sql);
  $valor  = "error_update";
  if ($result) {
    $valor = 1;
  }

  return $valor;
}

function updateUserPass($usu_clave, $usu_password){

  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $opciones = array('cost' => 5);
  $passHash = password_hash($usu_password, PASSWORD_BCRYPT, $opciones);

  $sql    = "UPDATE usuarios set usu_password = '$passHash' where usu_clave = $usu_clave";
  $result = mysqli_query($con, $sql);
  $valor  = "error_update";
  if ($result) {
    $valor = 1;
  }

  return $valor;
}
/*============================================================================*/

/*modifica el status de una categoria seleccionada ===========================*/
function updateStatusCategoria($cat_clave){
  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $puesto     = dameDatosCategorias($cat_clave);
  $desactivar = 0;
  $activar    = 1;

  switch ($puesto['cat_status']) {
    case 1:
      $sql = "UPDATE categorias set cat_status = $desactivar where cat_clave = $cat_clave";
      break;

    default:
      $sql = "UPDATE categorias set cat_status = $activar where cat_clave = $cat_clave";
      break;
  }
  //echo $sql;
  $result = mysqli_query($con, $sql);
  $valor  = "error_update";
  if ($result) {
    $valor = 1;
  }

  return $valor;
}
/*============================================================================*/

/*Modifica el nombre de la categoria seleccionada ============================*/
function updateNombreCatgoria($cat_clave, $cat_nombre){
  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $sql    = "UPDATE categorias set cat_nombre = '$cat_nombre' where cat_clave = $cat_clave";
  $result = mysqli_query($con, $sql);
  $valor  = "error_update";
  if ($result) {
    $valor = 1;
  }

  return $valor;
}
/*============================================================================*/

/*modifica el status del puesto seleccionado =================================*/
function updateStatusMarca($mar_clave){
  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $marca      = dameDatosMarca($mar_clave);
  $desactivar = 0;
  $activar    = 1;

  switch ($marca['mar_status']) {
    case 1:
      $sql = "UPDATE marcas set mar_status = $desactivar where mar_clave = $mar_clave";
      break;

    default:
      $sql = "UPDATE marcas set mar_status = $activar where mar_clave = $mar_clave";
      break;
  }
  //echo $sql;
  $result = mysqli_query($con, $sql);
  $valor  = "error_update";
  if ($result) {
    $valor = 1;
  }

  return $valor;
}
/*============================================================================*/

/*Modifica el nombre de la marca seleccionada ================================*/
function updateNombreMarca($mar_clave, $mar_nombre){
  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $sql    = "UPDATE marcas set mar_nombre = '$mar_nombre' where mar_clave = $mar_clave";
  $result = mysqli_query($con, $sql);
  $valor  = "error_update";
  if ($result) {
    $valor = 1;
  }

  return $valor;
}
/*============================================================================*/

/*Modifica contenido de la tabla proveedores =================================*/
function updateStatusProveedor($prov_clave){
  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $proveedor  = dameDatosProveedor($prov_clave);
  $desactivar = 0;
  $activar    = 1;

  switch ($proveedor['prov_status']) {
    case 1:
      $sql = "UPDATE proveedores set prov_status = $desactivar where prov_clave = $prov_clave";
      break;

    default:
      $sql = "UPDATE proveedores set prov_status = $activar where prov_clave = $prov_clave";
      break;
  }
  //echo $sql;
  $result = mysqli_query($con, $sql);
  $valor  = "error_update";
  if ($result) {
    $valor = 1;
  }

  return $valor;
}
/*============================================================================*/

function updateProveedor($prov_nombre, $prov_apPaterno, $prov_apMaterno, $nombreCompleto,
                         $prov_telefono, $prov_correo, $prov_clave){

  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $sql    = "UPDATE proveedores
                set prov_nombre = '$prov_nombre', prov_apPaterno = '$prov_apPaterno',
                    prov_apMaterno = '$prov_apMaterno', prov_telefono = '$prov_telefono',
                    prov_correo = '$prov_correo', prov_nombrecompleto = '$nombreCompleto'
              where prov_clave = $prov_clave";

  $result = mysqli_query($con, $sql);
  $valor  = "error_update";
  if ($result) {
   $valor = 1;
  }

  return $valor;
}

/*modifica el status de una categoria asignada a un producto =================*/
function updateStatusCategoriaProducto($pc_clave){
  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $puesto     = dameDatosCategoriaProducto($pc_clave);
  $desactivar = 0;
  $activar    = 1;

  switch ($puesto['pc_status']) {
    case 1:
      $sql = "UPDATE productos_categorias set pc_status = $desactivar where pc_clave = $pc_clave";
      break;

    default:
      $sql = "UPDATE productos_categorias set pc_status = $activar where pc_clave = $pc_clave";
      break;
  }
  //echo $sql;
  $result = mysqli_query($con, $sql);
  $valor  = "error_update";
  if ($result) {
    $valor = 1;
  }

  return $valor;
}
/*============================================================================*/

/*modifica el status del producto seleccionado ===============================*/
function updateStatusproducto($pro_clave){
  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $producto   = dameDatosProducto($pro_clave);
  $desactivar = 0;
  $activar    = 1;

  switch ($producto['pro_status']) {
    case 1:
      $sql = "UPDATE productos set pro_status = $desactivar where pro_clave = $pro_clave";
      break;

    default:
      $sql = "UPDATE productos set pro_status = $activar where pro_clave = $pro_clave";
      break;
  }
  //echo $sql;
  $result = mysqli_query($con, $sql);
  $valor  = "error_update";
  if ($result) {
    $valor = 1;
  }

  return $valor;
}

/*Modifica con el contenido*/
function updateProducto($pro_clave, $nombre, $inventariable, $unidad, $precio, $medida, $presentacion){
  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $sql    = "UPDATE productos
                set pro_nombre = '$nombre', pro_inventariable = $inventariable,
                    pro_presentacion = $presentacion, pro_unidad = $unidad,
                    pro_medida = $medida, pro_precio = $precio
              where pro_clave = $pro_clave";
  $result = mysqli_query($con, $sql);
  $valor  = "error_update";
  if ($result) {
    $valor = 1;
  }

  return $valor;
}
/*============================================================================*/

/*Actualiza el status de un cliente ==========================================*/
function updateStatusCliente($cli_clave){
  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $cliente   = dameDatosCliente($cli_clave);
  $desactivar = 0;
  $activar    = 1;

  switch ($cliente['cli_status']) {
    case 1:
      $sql = "UPDATE clientes set cli_status = $desactivar where cli_clave = $cli_clave";
      break;

    default:
      $sql = "UPDATE clientes set cli_status = $activar where cli_clave = $cli_clave";
      break;
  }
  //echo $sql;
  $result = mysqli_query($con, $sql);
  $valor  = "error_update";
  if ($result) {
    $valor = 1;
  }

  return $valor;
}
/*============================================================================*/

/*
  Indica que el contenido de la cotización seleccionada fue vendida o sera vendida
*/
function updateStatusCotVenta($cotizacion){
  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $vendido = 2;
  $sql     = "UPDATE cotizaciones set cot_status = $vendido where cot_clave = $cotizacion";
  $result  = mysqli_query($con, $sql);
  $valor   = "error_update";
  if ($result) {
    $valor = 1;
  }

  return $valor;
}
/*============================================================================*/

/*Update montos de una venta =================================================*/
function updateMontosVent($venta){
  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $valor    = montoVenta($venta);
  $subtotal = $valor['subtotal'];
  $iva      = $valor['iva'];
  $total    = $valor['total'];

  $sql    = "UPDATE ventas
                set ven_subtotal = $subtotal, ven_iva = $iva, ven_total = $total
              where ven_clave = $venta";
  $result = mysqli_query($con, $sql);
  $valor  = "error_update";
  if ($result) {
    $valor = 1;
  }

  return $valor;
}
/*============================================================================*/

/*Corrigue mosntos de una venta ==============================================*/
function corregirventa($venta){
  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $valor     = montoVenta($venta);
  $subtotal  = $valor['subtotal'];
  $iva       = $valor['iva'];
  $total     = $valor['total'];
  $loCobrado = $valor['loCobrado'];

  $sql    = "UPDATE ventas
                set ven_subtotal = $subtotal, ven_iva = $iva, ven_total = $total, ven_cobrado = $loCobrado
              where ven_clave = $venta";
              //echo $sql;
  $result = mysqli_query($con, $sql);
  $valor  = "error_update";
  if ($result) {

    if ($loCobrado == $total) {

      $sql    = "UPDATE ventas
                    set ven_status = 2
                  where ven_clave = $venta";
      $result = mysqli_query($con, $sql);
      $valor  = "error_update";
      if ($result) {
        $valor = 1;
      }

    }else {
      $valor = 1;
    }

  }

  return $valor;
}
/*============================================================================*/

/*Cierra y asigna montos al corte de caja ====================================*/
function cerrarCaja($caja, $seEntrega, $empleado){

  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $status        = 1;
  $cerrar        = 1;
  $tipo          = 3;//3 = entrega de caja
  $forma_pago    = 1;//1 = efectivo
  $clasificacion = 0;//0 = salidas
  $observacion   = "entrega de caja";
  $fecha         = damedatetime();

  mysqli_autocommit($con, FALSE);
  try {

    mysqli_begin_transaction($con, MYSQLI_TRANS_START_READ_WRITE);
    $sql    = "INSERT into movcaja(mc_fecha, mc_ccj_clave, mc_clasificacion, mc_tipo, mc_fp_clave, mc_monto, mc_observacion, mc_status)
                            values('$fecha', $caja, $clasificacion, $tipo, $forma_pago, $seEntrega, '$observacion', $status)";
    $result = mysqli_query($con, $sql);
    $valor  = "error_insert";
    if ($result) {

      //ccj_entradas, ccj_salidas, ccj_diferiencia

      $sql    = "SELECT cast( (select ifnull(sum(dv_total), 0)  from detalleventas where dv_ccj_clave = ccj_clave and dv_status = 1) as decimal(11, 2)) as ventaTotal,
	                      cast( (select ifnull(sum(mc_monto), 0) from movcaja where mc_ccj_clave = ccj_clave and mc_clasificacion = 1 and mc_tipo = 2) as decimal(11, 2) ) as entradas,
	                      cast( (select ifnull(sum(mc_monto), 0) from movcaja where mc_ccj_clave = ccj_clave and mc_clasificacion = 0 ) as decimal(11, 2) ) as salidas,
                        ( cast( (select ifnull(sum(mc_monto), 0) from movcaja where mc_ccj_clave = ccj_clave and mc_clasificacion = 1 and mc_tipo = 2) as decimal(11, 2) ) - cast( (select ifnull(sum(mc_monto), 0) from movcaja where mc_ccj_clave = ccj_clave and mc_clasificacion = 0 ) as decimal(11, 2) )) as diferiencia
                    from cortecaja
                      where ccj_clave = $caja";
      $result = mysqli_query($con, $sql);
      if ($result) {
        while ($fila = mysqli_fetch_array($result)) {
          $ventaTotal = $fila[0];
          $entradas   = $fila[1];
          $salidas    = $fila[2];
          $diferencia = $fila[3];
        }

        $sql    = "UPDATE cortecaja
                      set ccj_fin = '$fecha', ccj_emp_cerro = $empleado, ccj_ventatotal = $ventaTotal,
                          ccj_entregaron = $seEntrega, ccj_entradas = $entradas, ccj_salidas = $salidas,
                          ccj_diferiencia = $diferencia, ccj_status = $cerrar
                    where ccj_clave = $caja";
        $result = mysqli_query($con, $sql);
        $valor  = "error_insert";
        if ($result) {

          $valor = 1;

        }else {
          mysqli_rollback($con);
        }

      }else {
        mysqli_rollback($con);
      }

    }else {
      mysqli_rollback($con);
    }

    mysqli_commit($con);

  } catch (\Exception $e) {

    $valor = "not_save";
    mysqli_rollback($con);

  }

  return $valor;
}
/*============================================================================*/

/*Modifica el status de una zona seleccionada ================================*/
function updateStatusZona($zon_clave){
  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $zonas     = dameDatosZona($zon_clave);
  $desactivar = 0;
  $activar    = 1;

  switch ($zonas['zon_status']) {
    case 1:
      $sql = "UPDATE zonas set zon_status = $desactivar where zon_clave = $zon_clave";
      break;

    default:
      $sql = "UPDATE zonas set zon_status = $activar where zon_clave = $zon_clave";
      break;
  }
  //echo $sql;
  $result = mysqli_query($con, $sql);
  $valor  = "error_update";
  if ($result) {
    $valor = 1;
  }

  return $valor;
}
/*============================================================================*/

/*Cambia datos de la zona seleccionado =======================================*/
function updateZona($zon_clave, $zon_nombre, $zon_descripcion, $zon_precio){
  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $sql    = "UPDATE zonas
                set zon_nombre = '$zon_nombre',
                    zon_precio = $zon_precio,
                    zon_descripcion = '$zon_descripcion'
              where zon_clave = $zon_clave";
  $result = mysqli_query($con, $sql);
  $valor  = "error_update";
  if ($result) {
    $valor = 1;
  }

  return $valor;
}
/*============================================================================*/
?>

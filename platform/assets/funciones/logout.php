<?php

	include_once "../../config.php";
  include_once "generales.php";

	session_start();

	if (!isset($_SESSION['usu_clave'])) {
		header('location:../index.php');
	}

	// Valores de bitacora
	$modulo			   = "Logout";
	$usuario			 = $_SESSION['usu_clave'];
	$empresa		 	 = 1;
	$descripcion 	 = $_SESSION['usu_name']." cerro sesión";

  $modulo			  = "Sesión";
  $accion       = "Fin de sesión";
  $tipo         = 0;
  $descripcion  = $_SESSION['usu_empleado']." cerro sesión";

	$resultado = bitacora($modulo, $accion, $tipo, $_SESSION['usu_emp_clave'], $descripcion);

	//echo json_encode($_SESSION['usu_name']);
	if ($resultado == 1) {
		session_destroy();
		header('location:../../../');
		//echo json_encode(1);
	}
?>

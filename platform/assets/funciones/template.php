<?php
include_once "searches.php";

function printNavbar($usu_tipo, $nivel){

  $raiz = str_repeat("../", $nivel);
  switch ($usu_tipo) {
    case 1:
        echo '<div id="sidebar-menu" class="menu" data-level="'.$nivel.'">'.
                '<ul>'.
                  '<li>'.
                    '<a href="'.$raiz.'index.php" class="waves-effect">'.
                      '<i class="mdi mdi-view-dashboard"></i>Dashboard'.
                    '</a>'.
                  '</li>'.
                  '<li class="has_sub">'.
                    '<a href="javascript:void(0);" class="waves-effect">'.
                    '<i class="mdi mdi-laptop"></i>'.
                      '<span> Caja </span>'.
                    '</a>'.
                    '<ul class="list-unstyled">'.
                      '<li><a href="#" id="btn-openbox" class="btn-openbox">Abrir caja</a></li>'.
                      '<li><a href="#" id="bt-salida" class="btn-salida">Registro de salidas</a></li>'.
                      '<li><a href="#" id="btn-entregacaja" class="btn-entregacaja">Entregar caja</a></li>'.
                    '</ul>'.
                  '</li>'.
                  '<li>'.
                    '<a href="'.$raiz.'cotizaciones.php" class="waves-effect">'.
                      '<i class="ion-clipboard"></i>Cotizaciones'.
                    '</a>'.
                  '</li>'.
                  '<li>'.
                    '<a href="'.$raiz.'ventas.php" class="waves-effect">'.
                      '<i class="fa fa-shopping-cart"></i>Ventas'.
                    '</a>'.
                  '</li>'.
                  '<li class="has_sub">'.
                    '<a href="javascript:void(0);" class="waves-effect">'.
                      '<i class="mdi mdi-chart-line"></i>'.
                      '<span> Informes </span>'.
                    '</a>'.
                    '<ul class="list-unstyled">'.
                      '<li><a href="charts-morris.html">Invetnario</a></li>'.
                      '<li><a href="charts-chartist.html">Cortes de caja</a></li>'.
                    '</ul>'.
                  '</li>'.
                  '<li class="has_sub">'.
                    '<a href="javascript:void(0);" class="waves-effect">'.
                      '<i class="fa fa-cubes"></i>'.
                      '<span> Almacen </span>'.
                    '</a>'.
                    '<ul class="list-unstyled">'.
                      '<li><a href="advanced-animation.html">Altas de productos</a></li>'.
                      '<li><a href="advanced-highlight.html">Bajas de productos</a></li>'.
                    '</ul>'.
                  '</li>'.
                  '<li class="has_sub">'.
                    '<a href="javascript:void(0);" class="waves-effect">'.
                      '<i class="mdi mdi-buffer"></i>'.
                      '<span> Catálogos </span>'.
                    '</a>'.
                    '<ul class="list-unstyled">'.
                      '<li><a href="'.$raiz.'catalogos/proveedores.php">Proveedores</a></li>'.
                      '<li><a href="'.$raiz.'catalogos/categorias.php">Categorias</a></li>'.
                      '<li><a href="'.$raiz.'catalogos/productos.php">Productos</a></li>'.
                      '<!--li><a href="'.$raiz.'catalogos/zonas.php">Zonas</a></li-->'.
                      '<li><a href="'.$raiz.'catalogos/puestos.php">Puestos</a></li>'.
                      '<li><a href="'.$raiz.'catalogos/empleados.php">Empleados</a></li>'.
                      '<li><a href="'.$raiz.'catalogos/usuarios.php">Usuarios</a></li>'.
                    '</ul>'.
                  '</li>'.
                  '<li>'.
                    '<a href="'.$raiz.'bitacora.php" class="waves-effect">'.
                      '<i class="mdi mdi-format-list-bulleted-type"></i>Bitacora'.
                    '</a>'.
                  '</li>'.
              '</div>';
      break;

    case 3:
    echo '<div id="sidebar-menu" class="menu" data-level="'.$nivel.'">'.
            '<ul>'.
              '<li>'.
                '<a href="'.$raiz.'index.php" class="waves-effect">'.
                  '<i class="mdi mdi-view-dashboard"></i>'.
                  '<span> Dashboard </span>'.
                '</a>'.
              '</li>'.

              '<li class="has_sub">'.
                '<a href="javascript:void(0);" class="waves-effect">'.
                '<i class="mdi mdi-format-list-bulleted"></i>'.
                  '<span> Opciones </span>'.
                '</a>'.
                '<ul class="list-unstyled">'.
                  '<li><a href="#" id="btn-openbox" class="btn-openbox">Abrir caja</a></li>'.
                  '<li><a href="#" id="bt-salida" class="btn-salida">Registro de salidas</a></li>'.
                  '<li><a href="#" id="btn-entregacaja" class="btn-entregacaja">Entregar caja</a></li>'.
                '</ul>'.
              '</li>'.
            '</ul>'.
          '</div>';
      break;

    default:
      // code...
      break;
  }

}

function printOptionAvatar($usu_tipo, $nivel){

  $raizlogout = damelogouturl($nivel);
  $nivelExtra = damenivelExtra($nivel);
  $raiz       = str_repeat("../", $nivelExtra);

  //echo $raiz;

  echo '<li class="list-inline-item dropdown notification-list">'.
          '<a class="nav-link dropdown-toggle arrow-none waves-effect nav-user" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">'.
          '<img src="'.$raiz.'assets/images/users/avatar-1.jpg" alt="user" class="rounded-circle">'.
          '</a>'.
          '<div class="dropdown-menu dropdown-menu-right profile-dropdown ">'.
              '<a class="dropdown-item" href="#"><i class="mdi mdi-account-circle m-r-5 text-muted"></i> Mi perfil</a>'.
              '<a class="dropdown-item" href="'.$raizlogout.'assets/funciones/logout.php"><i class="mdi mdi-logout m-r-5 text-muted"></i> Cerrar sesión</a>'.
          '</div>'.
        '</li>';

}

function printFooter(){
  echo '<footer class="footer">'.
          '© 2021 '.C_PLATFORMNAME.' - <a href="https://monodesigns.com.mx/">By Monodesigns</a> .'.
        '</footer>';
}


?>

<?php

include_once "maths.php";
include_once "update.php";
include_once "searches.php";

/*Registra un puesto =========================================================*/
function insertPuesto($pue_nombre){
  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $status = 1;

  $sql    = "INSERT into puestos(pue_nombre, pue_status)
                          values('$pue_nombre', $status)";
  $result = mysqli_query($con, $sql);
  $valor  = "error_insert";
  if ($result) {
    $valor = 1;
  }

  return $valor;
}
/*============================================================================*/

/*Registra un empleado =======================================================*/
function insertEmpleados($emp_nombre, $emp_apPaterno, $emp_apMaterno, $nombreCompleto,
                         $puesto, $emp_direccion, $emp_telefono, $emp_correo, $emp_curp,
                         $emp_rfc){
  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $status = 1;

  $sql    = "INSERT into empleados(emp_nombre, emp_apPaterno, emp_apMaterno, emp_direccion, emp_telefono, emp_curp, emp_rfc, emp_correo, emp_status, emp_pue_clave, emp_nombreCompleto)
                          values('$emp_nombre', '$emp_apPaterno', '$emp_apMaterno', '$emp_direccion', '$emp_telefono', '$emp_curp', '$emp_rfc', '$emp_correo', $status, $puesto, '$nombreCompleto')";
  $result = mysqli_query($con, $sql);
  $valor  = "error_insert";
  if ($result) {
    $valor = 1;
  }

  return $valor;
}
/*============================================================================*/

/*Registra un usuario ========================================================*/
function insertUsuario($user_name, $user_password, $emp_clave, $user_tipo){
  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $status   = 1;
  $opciones = array('cost' => 5);
  $passHash = password_hash($user_password, PASSWORD_BCRYPT, $opciones);
  $nivel    = 1;

  $sql    = "INSERT into usuarios(usu_nombre, usu_password, usu_tipo, usu_nivel, usu_status, usu_emp_clave)
                           values('$user_name', '$passHash', $user_tipo, $nivel, $status, $emp_clave)";
  $result = mysqli_query($con, $sql);
  $valor  = "error_insert";
  if ($result) {
    $valor = 1;
  }

  return $valor;
}
/*============================================================================*/

/*Registra una categoria =====================================================*/
function insertCategoria($cat_nombre){
  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $status = 1;

  $sql    = "INSERT into categorias(cat_nombre, cat_status)
                          values('$cat_nombre', $status)";
  $result = mysqli_query($con, $sql);
  $valor  = "error_insert";
  if ($result) {
    $valor = 1;
  }

  return $valor;
}
/*============================================================================*/

/*Registra una marca =========================================================*/
function insertMarcas($mar_nombre){
  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $status = 1;

  $sql    = "INSERT into marcas(mar_nombre, mar_status)
                          values('$mar_nombre', $status)";
  $result = mysqli_query($con, $sql);
  $valor  = "error_insert";
  if ($result) {
    $valor = 1;
  }

  return $valor;
}
/*============================================================================*/

/*Registra un proveedor ======================================================*/
function insertProveedor($prov_nombre, $prov_apPaterno, $prov_apMaterno, $nombreCompleto,
                         $prov_telefono, $prov_correo){

  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $status = 1;

  $sql    = "INSERT into proveedores(prov_nombre, prov_appaterno, prov_apmaterno, prov_nombrecompleto, prov_correo, prov_telefono, prov_status)
                              values('$prov_nombre', '$prov_apPaterno', '$prov_apMaterno', '$nombreCompleto', '$prov_correo', '$prov_telefono', $status)";
  $result = mysqli_query($con, $sql);
  $valor  = "error_insert";
  if ($result) {
    $valor = 1;
  }

  return $valor;
}
/*============================================================================*/

/*Resgistro de productos =====================================================*/
function insertproducto($nombre, $inventariable, $unidad, $precio, $medida, $presentacion){
  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $status = 1;
  $sql    = "INSERT into productos(pro_nombre, pro_inventariable, pro_presentacion, pro_unidad,
                                   pro_medida, pro_precio, pro_status)
                            values('$nombre', $inventariable, $presentacion, $unidad, $medida,
                                   $precio, $status)";
  $result = mysqli_query($con, $sql);
  $valor  = "error_insert";
  if ($result) {
    $valor = 1;
  }

  return $valor;
}
/*============================================================================*/

/*Asigna categoría a un producto =============================================*/
function insertcategoriaproducto($producto, $categoria){
  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $status = 1;
  $sql    = "INSERT into productos_categorias(pc_pro_clave, pc_cat_clave, pc_status)
                                       values($producto, $categoria, $status)";
  $result = mysqli_query($con, $sql);
  $valor  = "error_insert";
  if ($result) {
    $valor = 1;
  }

  return $valor;
}
/*============================================================================*/

/*crea corte de caja =========================================================*/
function createcaja($empleado, $monto_de_apertura){
  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $apertura      = 0;
  $status        = 1;
  $tipo          = 0;//0 = Fondo de caja
  $forma_pago    = 1;//1 = efectivo
  $clasificacion = 1;//1 = entrada
  $folio         = damefolioccj();
  $fecha         = damedatetime();
  $observacion   = "fondo de caja";

  mysqli_autocommit($con, FALSE);
  try {
    mysqli_begin_transaction($con, MYSQLI_TRANS_START_READ_WRITE);

    //Inserta corte de caja
    $sql    = "INSERT into cortecaja(ccj_folio, ccj_inicio, ccj_emp_abrio, ccj_status, ccj_fondo)
                              values('$folio', '$fecha', $empleado, $apertura, $monto_de_apertura)";
    $result = mysqli_query($con, $sql);
    $valor  = "error_insert";
    if ($result) {

      //Busca el ultimo registro de una caja
      $sql    = "SELECT ccj_clave, ccj_folio, count(ccj_clave) from cortecaja where ccj_status = 0 order by ccj_clave desc limit 1";
      $result = mysqli_query($con, $sql);
      if ($result) {

        while ($fila = mysqli_fetch_array($result)) {
          $ccj_clave = $fila[0];
          $ccj_folio = $fila[1];
          $ccj_count = $fila[2];
        }

        //Inserta el fondo de caja
        $sql    = "INSERT into movcaja(mc_fecha, mc_ccj_clave, mc_clasificacion, mc_tipo, mc_fp_clave, mc_monto, mc_observacion, mc_status)
                                values('$fecha', $ccj_clave, $clasificacion, $tipo, $forma_pago, $monto_de_apertura, '$observacion', $status)";
        //echo $sql;
        $result = mysqli_query($con, $sql);
        $valor  = "error_insert";
        if ($result) {

          $valor = array('clave' => $ccj_clave, 'folio' => $ccj_folio, 'count' => $ccj_count);

        }else {
          mysqli_rollback($con);
        }

      }

    }else {
      mysqli_rollback($con);
    }

    mysqli_commit($con);

  } catch (\Exception $e) {

    $valor = "not_save";
    mysqli_rollback($con);

  }


  return $valor;
}
/*============================================================================*/


/*Registro de salidas de caja ================================================*/
function insertSalidas($concepto, $monto, $caja, $empleado){
  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $status        = 1;
  $tipo          = 1;//1 = salidas
  $forma_pago    = 1;//1 = efectivo
  $clasificacion = 0;//0 = salidas
  $fecha         = damedatetime();

                                //mc_fecha, mc_ccj_clave, mc_clasificacion, mc_tipo, mc_fp_clave, mc_total, mc_observacion, mc_status
  $sql    = "INSERT into movcaja(mc_fecha, mc_ccj_clave, mc_clasificacion, mc_tipo, mc_fp_clave, mc_monto, mc_observacion, mc_status)
                          values('$fecha', $caja, $clasificacion, $tipo, $forma_pago, $monto, '$concepto', $status)";
  $valro  = "error_insert";
  $result = mysqli_query($con, $sql);
  if ($result) {
    $valor = 1;
  }

  return $valor;
}
/*============================================================================*/

/*Crea encabezado de cotizaciones ============================================*/
function cotizacionNormal($empleado, $cliente){
  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $caja      = boxexists();
  $fecha     = damedatetime();
  $folio     = damefoliocotizacion();
  $ccj_clave = $caja['ccj_clave'];
  $status    = 1;
  $tipo      = 1;

  switch ($caja['ccj_count']) {
    case 0:
      $valor = "sin_caja";
      break;

    default:
      if ($caja['ccj_count'] > 0) {

        mysqli_autocommit($con, FALSE);

        try {

          mysqli_begin_transaction($con, MYSQLI_TRANS_START_READ_WRITE);
          $sql    = "INSERT into cotizaciones(cot_folio, cot_fecha, cot_status, cot_tipo, cot_ccj_clave, cot_cli_clave, cot_envio)
                                       values('$folio', '$fecha', $status, $tipo, $ccj_clave, $cliente, 0)";
          $result = mysqli_query($con, $sql);
          $valor  = "error_insert";
          if ($result) {
            $valor = $folio;
          }
          mysqli_commit($con);

        } catch (\Exception $e) {
          $valor = "not_save";
          mysqli_rollback($con);
        }


      }else {
        $valor = $caja;
      }
      break;
  }

  mysqli_rollback($con);

  return $valor;
}
/*============================================================================*/

/*Resgistro de clientes =====================================================*/
function insertCliente($nombre, $correo, $direccion, $telefono){
  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $status = 1;
  $sql    = "INSERT into clientes(cli_nombre, cli_correo, cli_direccion, cli_telefono, cli_status)
                           values('$nombre', '$correo', '$direccion', '$telefono', $status)";
  $result = mysqli_query($con, $sql);
  $valor  = "error_insert";
  if ($result) {
    $valor = 1;
  }

  return $valor;
}
/*============================================================================*/

/*convierte una cotización a una venta =======================================*/
function cotizacionVenta($cotizacion, $cliente, $subtotal, $iva, $total, $empleado){

  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  //$verificar = verificarCotStock($cotizacion);
  $caja       = boxexists();
  $dc_count   = hasproduct($cotizacion);
  $ccj_clave  = $caja['ccj_clave'];

  $folio      = damefolioventa();
  $fecha      = damedatetime();

  $tipo_de_venta = 2;//proveniten de una cotizacion
  $cot_status    = 2;
  $status        = 1;//Activo

  switch ($dc_count) {
    case 0:
       $valor = "sin_items";
      break;

    default:

      //$valor = "error";
      if ($dc_count > 0) {

        switch ($caja['ccj_count']) {
          case 0:
             $valor = "sin_caja";
            break;

          default:

            if ($caja['ccj_count'] > 0) {

              mysqli_autocommit($con, FALSE);

              try {

                mysqli_begin_transaction($con, MYSQLI_TRANS_START_READ_WRITE);
                $sql    = "INSERT into ventas(ven_cot_clave, ven_ccj_clave, ven_cli_clave, ven_emp_clave,
                                              ven_fecha, ven_tipo, ven_folio, ven_subtotal, ven_iva, ven_total,
                                              ven_status)
                                       values($cotizacion, $ccj_clave, $cliente, $empleado,
                                              '$fecha', $tipo_de_venta, '$folio', $subtotal, $iva, $total,
                                              $status)";
                $result = mysqli_query($con, $sql);
                $valor  = "error_insert";
                if ($result) {

                  $sql    = "SELECT ven_clave, ven_folio from ventas where ven_status = 1 order by ven_clave desc limit 1";
                  $result = mysqli_query($con, $sql);
                  if ($result) {
                    while ($fila = mysqli_fetch_array($result)) {
                      $ven_clave = $fila[0];
                      $ven_folio = $fila[1];
                    }

                    $sql    = "INSERT into detalleventas(dv_fecha, dv_ccj_clave, dv_ven_clave, dv_pro_clave, dv_precioUnitario, dv_cantidad, dv_subtotal, dv_iva, dv_total, dv_tipo, dv_status)
                                                SELECT '$fecha', $ccj_clave,  $ven_clave, dc_pro_clave, dc_precioUnitario, dc_cantidad, dc_subtotal, dc_iva, dc_total, $tipo_de_venta, $status
                  	                               from detallecotizacion
                  		                               where dc_cot_clave = $cotizacion
                                                       and dc_stauts = 1";
                    $result = mysqli_query($con, $sql);
                    $valor  = "error_insert";
                    if ($result) {

                      $sql    = "UPDATE cotizaciones set cot_status = $cot_status where cot_clave = $cotizacion";
                      $result = mysqli_query($con, $sql);
                      $valor  = "error_insert";
                      if ($result) {
                        $valor = $ven_folio;
                      }

                    }

                  }

                }


                mysqli_commit($con);

                //mysqli_autocommit($con, TRUE);
                //var_dump($c);

              } catch (\Exception $e) {
                $valor = "not_save";
                mysqli_rollback($con);
              }


            }

            break;
        }

      }

      break;
  }

  //echo $valor;

  return $valor;
}
/*============================================================================*/

/*Inserta contenido del detalle cotización al  detalle venta =================*/
function insertdetalleCotVenta($cortecaja, $cotizacion, $venta){
  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $fecha  = damedatetime();
  $status = 1;
  $tipo   = 2;//proviene de una cotización

  $sql    = "INSERT into detalleventas(dv_fecha, dv_ccj_clave, dv_ven_clave, dv_pro_clave, dv_precioUnitario, dv_cantidad, dv_iva, dv_total, dv_tipo, dv_status)
                               SELECT '$fecha', $cortecaja,  $venta, dc_pro_clave, dc_precioUnitario, dc_cantidad, dc_iva, dc_total, $tipo, $status
	                               from detallecotizacion
		                               where dc_cot_clave = $cotizacion
                                     and dc_stauts = 1";
  //echo $sql;
  $result = mysqli_query($con, $sql);
  $valor  = "error_insert";
  if ($result) {
    $valor = 1;
  }

  return $valor;
}
/*============================================================================*/

/*Registra cliente y cotización ==============================================*/
function cotizacioncli($nombre, $correo, $telefono, $direccion){
  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $caja          = boxexists();
  $fecha         = damedatetime();
  $folio         = damefoliocotizacion();
  $buscarCliente = searchNameCliente($nombre);
  $ccj_clave     = $caja['ccj_clave'];
  $status        = 1;
  $tipo          = 2;

  switch ($buscarCliente['cli_count']) {
    case 0:

      switch ($caja['ccj_count']) {
        case 1:

          mysqli_autocommit($con, FALSE);

          try {
            mysqli_begin_transaction($con, MYSQLI_TRANS_START_READ_WRITE);
            $sql    = "INSERT into clientes(cli_nombre, cli_telefono, cli_correo, cli_direccion, cli_status)
                                     values('$nombre', '$telefono', '$correo', '$direccion', $status)";

            $result = mysqli_query($con, $sql);
            $valor  = "error_insert";
            if ($result) {

              $sql    = "SELECT cli_clave from clientes where cli_status = 1 order by cli_clave desc limit 1";
              $result = mysqli_query($con, $sql);
              if ($result) {
                while ($fila = mysqli_fetch_array($result)) {
                  $cliente = $fila[0];
                }

                $sql    = "INSERT into cotizaciones(cot_tipo, cot_fecha, cot_folio, cot_ccj_clave, cot_status, cot_cli_clave)
                                             values($tipo, '$fecha', '$folio', $ccj_clave, $status, $cliente)";
                //echo $sql;
                $result = mysqli_query($con, $sql);
                $valor  = "error_insert";
                if ($result) {
                  $valor = $folio;
                }

              }

            }

            mysqli_commit($con);

          } catch (\Exception $e) {
            $valor = "not_save";
            mysqli_rollback($con);
          }

          break;

        default:
          if ($caja['ccj_count'] == 0) {
            $valor = "sin_caja";
          }
          break;
      }

      break;

    default:
      if ($buscarCliente['cli_count']) {
        $valor = "cli_exist";
      }
      break;
  }

  return $valor;
}
/*============================================================================*/

/*agrega pagos realizados por un cliente a una venta =========================*/
function agregarCobro($venta, $formapago, $monto, $cambio, $pagoaroncon, $observacion, $empleado, $cliente){
  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $status        = 1;
  $cerrar_venta  = 2;
  $tipo          = 2;//cobro
  $clasificacion = 1;
  $iva           = 0.16;

  $caja          = boxexists();
  $fecha         = damedatetime();
  $ccj_clave     = $caja['ccj_clave'];
  $retencionIva  = dameretencionIva($monto);

  switch ($caja['ccj_count']) {
    case 0:
      $valor = "sin_caja";
      break;

    default:
      if ($caja['ccj_count'] > 0) {

        mysqli_autocommit($con, FALSE);
        try {

          mysqli_begin_transaction($con, MYSQLI_TRANS_START_READ_WRITE);
          $sql    = "INSERT into cobros(cob_fecha, cob_ven_clave, cob_ccj_clave, cob_fcp_clave, cob_monto,
                                        cob_cambio, cob_pagaroncon, cob_emp_clave, cob_status, cob_cli_clave)
                                 values('$fecha', $venta, $ccj_clave, $formapago, $monto, $cambio, $pagoaroncon,
                                        $empleado, $status, $cliente)";
          //echo $sql;
          $result = mysqli_query($con, $sql);
          $valor  = "error_insert";
          if ($result) {

            $sql    = "SELECT cob_clave from cobros where cob_status = 1 order by cob_clave desc limit 1";
            $result = mysqli_query($con, $sql);
            if ($result) {
              while ($fila = mysqli_fetch_array($result)) {
                $cob_clave = $fila[0];
              }

              $sql    = "INSERT into movcaja(mc_fecha, mc_ccj_clave, mc_clasificacion, mc_tipo, mc_fp_clave, mc_monto,
                                             mc_iva, mc_retiva, mc_ven_clave, mc_cob_clave, mc_observacion, mc_status)
                                      values('$fecha', $ccj_clave, $clasificacion, $tipo, $formapago, $monto,
                                             $iva, $retencionIva, $venta, $cob_clave, '$observacion', $status)";
              $result = mysqli_query($con, $sql);
              $valor  = "error_insert";
              if ($result) {

                $sql = "SELECT cast( (select ifnull(sum(cob_monto), 0) from cobros where cob_ven_clave = ven_clave and cob_status = 1) as decimal(11, 2)) as pagado,
	                             cast( ( select ifnull(sum(dv_total), 0) from detalleventas where dv_ven_clave = ven_clave and dv_status = 1) as decimal(11, 2) ) as porPagar
                            from ventas
                              where ven_clave = $venta";
                //echo $sql;
                $result = mysqli_query($con, $sql);

                if ($result) {

                  while ($fila = mysqli_fetch_array($result)) {
                    $pagosdelcliente = $fila[0];
                    $porPagar        = $fila[1];
                  }


                  if ($pagosdelcliente == $porPagar) {

                    $sql    = "UPDATE ventas
                                  set ven_status = $cerrar_venta, ven_cobrado = $pagosdelcliente
                                where ven_clave = $venta";
                    $result = mysqli_query($con, $sql);
                    if ($result) {
                      /*========================================================
                      ==========================================================

                      -- Aqui es donde se desminuye el stock de los productos --

                      ==========================================================
                      ========================================================*/
                      $valor = 1;

                    }else {
                      mysqli_rollback($con);
                    }

                  }elseif ($pagosdelcliente < $porPagar) {

                    $sql    = "UPDATE ventas
                                  set ven_cobrado = $pagosdelcliente
                                where ven_clave = $venta";
                    $result = mysqli_query($con, $sql);
                    if ($result) {
                      $valor = 1;
                    }else {
                      mysqli_rollback($con);
                    }

                  }else {
                    $valor = "monto_excesivo";
                  }


                }


              }else {
                mysqli_rollback($con);
              }

            }

          }else {
            mysqli_rollback($con);
          }

          mysqli_commit($con);

        } catch (\Exception $e) {

          mysqli_rollback($con);
          $valor = "error_insert";

        }


      }else {
        $valor = $caja;
      }
      break;
  }

  return $valor;
}
/*============================================================================*/

/*Agregar productos al detalle de una venta ==================================*/
function agregardetalleVent($venta, $p_unitario, $cantidad, $subtotal, $iva, $total, $producto){
  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $status    = 1;
  $tipo      = 1;
  $caja      = boxexists();
  $fecha     = damedatetime();
  $ccj_clave = $caja['ccj_clave'];

  $sql    = "INSERT into detalleventas(dv_fecha, dv_ccj_clave, dv_ven_clave, dv_pro_clave, dv_precioUnitario, dv_cantidad, dv_subtotal, dv_iva, dv_total, dv_status, dv_tipo)
                                values('$fecha', $ccj_clave, $venta, $producto, $p_unitario, $cantidad, $subtotal, $iva, $total, $status, $tipo)";
  $result = mysqli_query($con, $sql);
  $valor  = "error_insert";
  if ($result) {
    $valor = updateMontosVent($venta);
  }

  return $valor;

}
/*============================================================================*/

/*Registra una zona ==========================================================*/
function insertZona($zon_nombre, $zon_precio, $zon_descripcion){
  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $status = 1;

  $sql    = "INSERT into zonas(zon_nombre, zon_descripcion, zon_precio, zon_status)
                        values('$zon_nombre', '$zon_descripcion', $zon_precio, $status)";
  $result = mysqli_query($con, $sql);
  $valor  = "error_insert";
  if ($result) {
    $valor = 1;
  }

  return $valor;
}
/*============================================================================*/
?>

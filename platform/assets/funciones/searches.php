<?php
//include_once "../config.php";

/*Busca puesto por nombres ===================================================*/
function searchPuestoNombre($puesto){
  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $sql    = "SELECT count(pue_clave), pue_status, pue_clave
                from puestos
                  where pue_nombre = '$puesto'";
  $result = mysqli_query($con, $sql);
  $valor  = array('error' => "error_search");
  if ($result) {
    while ($fila = mysqli_fetch_array($result)) {
      $valor = array('pue_count' => $fila[0], 'pue_status' => $fila[1], 'pue_clave' => $fila[2]);
    }
  }

  return $valor;
}
/*============================================================================*/

/*Retorna todo el contenido de la tabla puestos ==============================*/
function dameDatosPuesto($puesto){
  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $sql    = "SELECT pue_clave, pue_nombre, pue_status from puestos where pue_clave = $puesto";
  $result = mysqli_query($con, $sql);
  $valor  = "error_search";
  if ($result) {
    while ($fila = mysqli_fetch_array($result)) {
      $valor = array('pue_clave' => $fila[0], 'pue_nombre' => $fila[1], 'pue_status' => $fila[2]);
    }
  }

  return $valor;
}
/*============================================================================*/

/*Retorna todo el contenido de la tabla empleados ============================*/
function dameDatosEmpleados($empleado){
  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $sql    = "SELECT emp_clave, emp_nombre, emp_apPaterno, emp_apMaterno, emp_direccion,
                    emp_telefono, emp_curp, emp_rfc, emp_correo, emp_status, emp_pue_clave,
                    (SELECT pue_nombre from puestos where emp_pue_clave = pue_clave) as emp_pue_nombre,
                    emp_nombreCompleto
                  from empleados
                    where emp_clave = $empleado";
  $result = mysqli_query($con, $sql);
  $valor  = "error_search";
  if ($result) {
    while ($fila = mysqli_fetch_array($result)) {
      $valor = array('emp_clave' => $fila[0], 'emp_nombre' => $fila[1], 'emp_apPaterno' => $fila[2],
                     'emp_apMaterno' => $fila[3], 'emp_direccion' => $fila[4], 'emp_telefono' => $fila[5],
                     'emp_curp' => $fila[6], 'emp_rfc' => $fila[7], 'emp_correo' => $fila[8], 'emp_status' => $fila[9],
                     'emp_pue_clave' => $fila[10], 'emp_pue_nombre' => $fila[11], 'emp_nombreCompleto' => $fila[12]);
    }
  }

  return $valor;
}

function searchNameEmpleado($nombre){

  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $sql    = "SELECT count(emp_clave), emp_clave, emp_status
                from empleados
                  where emp_nombreCompleto = '$nombre'";
  $result = mysqli_query($con, $sql);
  $valor  = "error_search";
  if ($result) {
    while ($fila = mysqli_fetch_array($result)) {
      $valor = array('emp_count' => $fila[0], 'emp_clave' => $fila[1], 'emp_status' => $fila[2]);
    }
  }

  return $valor;
}
/*============================================================================*/

/*Retona el contenido de la tabla usuarios ===================================*/
function searchUserName($username){

  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $sql    = "SELECT count(usu_clave), usu_clave, usu_status, usu_password,
                    usu_tipo, usu_emp_clave, usu_nombre, usu_nivel
                from usuarios
                  where usu_nombre = '$username'";
                //echo $sql;
  $result = mysqli_query($con, $sql);
  $valor  = "error_search";
  if ($result) {
    while ($fila = mysqli_fetch_array($result)) {
      $valor = array('usu_count' => $fila[0], 'usu_clave' => $fila[1],
                     'usu_status' => $fila[2], 'usu_password' => $fila[3],
                     'usu_tipo' => $fila[4], 'usu_emp_clave' => $fila[5],
                     'usu_nombre' => $fila[6], 'usu_nivel' => $fila[7]);
    }
  }

  return $valor;
}

function dameDatosUser($user){
  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $sql    = "SELECT usu_clave, usu_nombre, usu_password, usu_tipo, usu_nivel,
                    usu_status, usu_emp_clave
                  from usuarios
                    where usu_clave = $user";
  $result = mysqli_query($con,$sql);
  $valor = "error_search";
  if ($result) {
    while ($fila = mysqli_fetch_array($result)) {
      $valor = array('usu_clave' => $fila[0], 'usu_nombre' => $fila[1], 'usu_password' => $fila[2],
                     'usu_tipo' => $fila[3], 'usu_nivel' => $fila[4], 'usu_status' => $fila[5],
                     'usu_emp_clave' => $fila[6]);
    }
  }

  return $valor;
}
/*============================================================================*/

/*Retorna contenido de la tabla categorias ===================================*/
function searchCategoriaNombre($categoria){
  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $sql    = "SELECT count(cat_clave), cat_status, cat_clave
                from categorias
                  where cat_nombre = '$categoria'";
  $result = mysqli_query($con, $sql);
  $valor  = array('error' => "error_search");
  if ($result) {
    while ($fila = mysqli_fetch_array($result)) {
      $valor = array('cat_count' => $fila[0], 'cat_status' => $fila[1], 'cat_clave' => $fila[2]);
    }
  }

  return $valor;
}

function dameDatosCategorias($cat_clave){
  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $sql    = "SELECT cat_clave, cat_nombre, cat_status from categorias where cat_clave = $cat_clave";
  $result = mysqli_query($con, $sql);
  $valor  = "error_search";
  if ($result) {
    while ($fila = mysqli_fetch_array($result)) {
      $valor = array('cat_clave' => $fila[0], 'cat_nombre' => $fila[1], 'cat_status' => $fila[2]);
    }
  }

  return $valor;


}
/*============================================================================*/

/*Busca marcas por nombres ===================================================*/
function searchMarcaNombre($marca){
  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $sql    = "SELECT count(mar_clave), mar_status, mar_clave
                from marcas
                  where mar_nombre = '$marca'";
  $result = mysqli_query($con, $sql);
  $valor  = array('error' => "error_search");
  if ($result) {
    while ($fila = mysqli_fetch_array($result)) {
      $valor = array('mar_count' => $fila[0], 'mar_status' => $fila[1], 'mar_clave' => $fila[2]);
    }
  }

  return $valor;
}
/*============================================================================*/

/*Retorna todo el contenido de la tabla marcas ===============================*/
function dameDatosMarca($mar_clave){
  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $sql    = "SELECT mar_clave, mar_nombre, mar_status from marcas where mar_clave = $mar_clave";
  $result = mysqli_query($con, $sql);
  $valor  = "error_search";
  if ($result) {
    while ($fila = mysqli_fetch_array($result)) {
      $valor = array('mar_clave' => $fila[0], 'mar_nombre' => $fila[1], 'mar_status' => $fila[2]);
    }
  }

  return $valor;
}
/*============================================================================*/

/*Retorna todo el contenido de la tabla proveedores ==========================*/
function searchNombreProveedor($proveedor){
  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $sql    = "SELECT count(prov_clave), prov_clave, prov_status
                from proveedores
                  where prov_nombrecompleto = '$proveedor'";
  $result = mysqli_query($con, $sql);
  $valor  = "error_search";
  if ($result) {
    while ($fila = mysqli_fetch_array($result)) {
      $valor = array('prov_count' => $fila[0], 'prov_clave' => $fila[1], 'prov_status' => $fila[2]);
    }
  }

  return $valor;
}

/*Todo el contenido*/
function dameDatosProveedor($proveedor){
  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $sql    = "SELECT prov_clave, prov_nombre, prov_appaterno, prov_apmaterno,
                    prov_nombrecompleto, prov_correo, prov_telefono, prov_status
                 from proveedores
                    where prov_clave = $proveedor";
  $result = mysqli_query($con, $sql);
  $valor  = "error_search";
  if ($result) {
    while ($fila = mysqli_fetch_array($result)) {
      $valor = array('prov_clave' => $fila[0], 'prov_nombre' => $fila[1], 'prov_appaterno' => $fila[2],
                     'prov_apmaterno' => $fila[3], 'prov_nombrecompleto' => $fila[4], 'prov_correo' => $fila[5],
                     'prov_telefono' => $fila[6], 'prov_status' => $fila[7]);
    }
  }

  return $valor;
}
/*============================================================================*/

/*Retorna el ultimo registro en la tabla proveedores =========================*/
function ultimoRegsitroProveedores(){
  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $sql    = "SELECT prov_clave from proveedores order by prov_clave desc limit 1";
  $result = mysqli_query($con, $sql);
  $valor  = "error_search";
  if ($result) {
    while ($fila = mysqli_fetch_array($result)) {
      $valor = $fila[0];
    }
  }

  return $valor;
}
/*============================================================================*/

/*Retorna el contenido de la tabala productos ================================*/
function searchNameproducto($producto){
  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $sql    = "SELECT count(pro_clave), pro_clave, pro_status
                from productos
                  where pro_nombre = '$producto'";
  $result = mysqli_query($con, $sql);
  $valor  = "error_search";
  if ($result) {
    while ($fila = mysqli_fetch_array($result)) {
      $valor = array('pro_count' => $fila[0], 'pro_clave' => $fila[1], 'pro_status' => $fila[2]);
    }
  }

  return $valor;
}

/*Retorna el ultimo registro en la tabla productos ===========================*/
function ultimoRegistroProducto(){
  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $sql    = "SELECT pro_clave from productos order by pro_clave desc limit 1";
  $result = mysqli_query($con, $sql);
  $valor  = "error_search";
  if ($result) {
    while ($fila = mysqli_fetch_array($result)) {
      $valor = $fila[0];
    }
  }

  return $valor;
}
/*============================================================================*/

/*Todo el contendio*/
function dameDatosProducto($producto){
  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $sql    = "SELECT pro_clave, pro_nombre, pro_descripcion, pro_inventariable,
                    pro_presentacion, pro_unidad, pro_medida, pro_cantidadMinima,
                    pro_precioMayoreo, cast(pro_precio as decimal(11,2)) as pro_precio, pro_status,
                    concat('$ ', format(pro_precio, 2)) as pro_formatprecio
                  from productos
                    where pro_clave = $producto";
  $result = mysqli_query($con, $sql);
  $valor  = "error_search";
  if ($result) {
    while ($fila = mysqli_fetch_array($result)) {
      $valor = array('pro_clave' => $fila[0], 'pro_nombre' => $fila[1], 'pro_descripcion' => $fila[2],
                     'pro_inventariable' => $fila[3], 'pro_presentacion' => $fila[4], 'pro_unidad' => $fila[5],
                     'pro_medida' => $fila[6], 'pro_cantidadMinima' => $fila[7], 'pro_precioMayoreo' => $fila[8],
                     'pro_precio' => $fila[9], 'pro_status' => $fila[10], 'pro_formatprecio' => $fila[11]);
    }
  }

  return $valor;
}
/*============================================================================*/

/*Retorna si existe la categoria asignada en un producto =====================*/
function searchCategoriaProducto($producto, $categoria){
  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $sql    = "SELECT count(pc_clave), pc_clave, pc_status
                from productos_categorias
                    where pc_pro_clave = $producto
                      and pc_cat_clave = $categoria";
  $result = mysqli_query($con, $sql);
  $valor  = "error_search";
  if ($result) {
    while ($fila = mysqli_fetch_array($result)) {
      $valor = array('pc_count' => $fila[0], 'pc_clave' => $fila[1], 'pc_status' => $fila[2]);
    }
  }

  return $valor;
}


/*Retorna todo el contenido de la tabla productos_categorias =================*/
function dameDatosCategoriaProducto($pc_clave){
  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $sql    = "SELECT pc_clave, pc_pro_clave, pc_cat_clave, pc_status
                from productos_categorias
                    where pc_clave = $pc_clave";
  $result = mysqli_query($con, $sql);
  $valor  = "error_search";
  if ($result) {
    while ($fila = mysqli_fetch_array($result)) {
      $valor = array('pc_clave' => $fila[0], 'pc_pro_clave' => $fila[1], 'pc_cat_clave' => $fila[2],
                     'pc_status' => $fila[3]);
    }
  }

  return $valor;
}
/*============================================================================*/

/*retorna unidad de medida en string =========================================*/
function dameUnidadMedida($unidadmedida){
  switch ($unidadmedida) {
    case 1:
      $valor = "sin unidad";
      break;
    case 2:
      $valor = "kg";
      break;
    case 3:
      $valor = "g";
      break;
    case 4:
      $valor = "l";
      break;
    case 5:
      $valor = "ml";
      break;
    case 6:
      $valor = "m";
      break;
    case 7:
      $valor = "m2";
      break;
    case 8:
      $valor = "pza.";
      break;
  }

  return $valor;
}
/*============================================================================*/

/*retorna unidad de medida en string =========================================*/
function damePresentacion($presentacion){
  switch ($presentacion) {
    case 1:
      $valor = "Bolsa";
      break;
    case 2:
      $valor = "Botella";
      break;
    case 3:
      $valor = "Caja";
      break;
    case 4:
      $valor = "Frasco";
      break;
    case 5:
      $valor = "Sobre";
      break;
    case 6:
      $valor = "Pieza";
      break;
    case 7:
      $valor = "Paquete";
      break;
    case 8:
      $valor = "Rollo";
      break;
  }

  return $valor;
}
/*============================================================================*/

/*retorna unidad de medida en string =========================================*/
function dameInventariable($presentacion){
  switch ($presentacion) {
    case 1:
      $valor = "Sí";
      break;
    case 2:
      $valor = "No";
      break;
  }

  return $valor;
}
/*============================================================================*/

/*Retorna datos de acceso para equipo mono ===================================*/
function dameDatosMono(){

  $opciones  = array('cost' => 5);
  $password  = password_hash('monodeveloping', PASSWORD_BCRYPT, $opciones);

  $valor = array('username' => 'teamMono', 'password' => $password);

  return $valor;
}
/*============================================================================*/

/**/
function damelogouturl($valor){

  $nivel     = damenivelExtra($valor);
  $resultado = str_repeat("../", $nivel);

  return $resultado;
}

function damenivelExtra($valor){

  $resultado = $valor+1;

  return $resultado;
}
/*============================================================================*/

/*Revisa el status de la sesión ==============================================*/
function sessionstatus(){

  if (!isset($_SESSION['usu_clave'])) {
    header("location: ../../../");
  }

}
/*============================================================================*/

/*Revisa si existe una caja abierta ==========================================*/
function boxexists(){
  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $sql    = "SELECT count(ccj_clave), ccj_clave, ccj_folio
                from cortecaja
                    where ccj_status = 0";
  $result = mysqli_query($con, $sql);
  $valor  = "error_search";
  if ($result) {
    while ($fila = mysqli_fetch_array($result)) {
      $valor = array('ccj_count' => $fila[0], 'ccj_clave' => $fila[1],
                      'ccj_folio' => $fila[2]);
    }
  }

  return $valor;
}
/*============================================================================*/

/*Retorna el ultimo registro en la tabla cortecaja ===========================*/
function ultimoRegistroCaja(){
  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $sql    = "SELECT ccj_clave, ccj_folio
                  from cortecaja
                    where ccj_status = 0
                 order by ccj_clave desc limit 1";
  $result = mysqli_query($con, $sql);
  $valor  = "error_search";
  if ($result) {
    while ($fila = mysqli_fetch_array($result)) {
      $valor = array('ccj_clave' => $fila[0], 'ccj_folio' => $fila[1]);
    }
  }

  return $valor;
}
/*============================================================================*/

/*Dame los datos de un corte en especifico ===================================*/
function dameDatosCorte($folio){
  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $sql    = "SELECT ccj_clave, date_format(ccj_inicio, '%r') as hrInicio, date_format(ccj_inicio, '%d-%m-%Y') as FechaInicio,
	                  (select emp_nombreCompleto from empleados where emp_clave = ccj_emp_abrio) as emp_abrio,
                    date_format(ccj_fin, '%r') as hrFin, date_format(ccj_fin, '%d-%m-%Y') as FechaFin,
                    (select emp_nombreCompleto from empleados where emp_clave = ccj_emp_cerro) as emp_cerro,
                    (Select concat('$ ', Cast(ifnull(sum(dv_total), 0) as decimal(11, 2))) from detalleventas where dv_status = 1 and dv_ccj_clave = ccj_clave) as ccj_ventatotal,
                    Concat('$ ', Cast(ifnull(ccj_entregaron, 0) as decimal(11, 2)) ) as ccj_entregaron,
                    (Select concat('$ ', Cast(ifnull(sum(mc_monto), 0) as decimal(11, 2))) from movcaja where mc_ccj_clave = ccj_clave and mc_clasificacion = 1) as ccj_entradas,
                    (Select concat('$ ', Cast(ifnull(sum(mc_monto), 0) as decimal(11, 2))) from movcaja where mc_ccj_clave = ccj_clave and mc_clasificacion = 0) as ccj_salidas,
                    Concat('$ ', cast(( ( (Select ifnull(sum(mc_monto), 0) from movcaja where mc_ccj_clave = ccj_clave and mc_clasificacion = 1) - (Select ifnull(sum(mc_monto), 0) from movcaja where mc_ccj_clave = ccj_clave and mc_clasificacion = 0) ) ) as decimal(11, 2))) as diferencia,
                    CASE
		                  WHEN ccj_status = 0 THEN 'Abierto'
                      ELSE 'Cerrado'
	                  END as ccj_statusNombre, ccj_status,
                    (Select concat('$ ', cast(ifnull(sum(dc_total), 0) as decimal(11, 2))) from cotizaciones, detallecotizacion where cot_clave = dc_cot_clave and cot_ccj_clave = ccj_clave and cot_status = 1 and dc_stauts = 1) as cot_pend_monto,
                    (Select concat('$ ', cast(ifnull(sum(dc_total), 0) as decimal(11, 2))) from cotizaciones, detallecotizacion where cot_clave = dc_cot_clave and cot_ccj_clave = ccj_clave and cot_status = 2 and dc_stauts = 1) as cot_efec_monto
                  from cortecaja
                    where ccj_folio = '$folio'";
  $result = mysqli_query($con, $sql);
  $valor  = "error_search";
  if ($result) {
    while ($fila = mysqli_fetch_array($result)) {
      $valor = array('ccj_clave' => $fila[0], 'ccj_folio' => $folio, 'hrInicio' => $fila[1],
                     'FechaInicio' => $fila[2], 'emp_abrio' => $fila[3], 'hrFin' => $fila[4],
                     'FechaFin' => $fila[5], 'emp_cerro' => $fila[6], 'ventatotal' => $fila[7],
                     'entregaron' => $fila[8], 'entradas' => $fila[9], 'salidas' => $fila[10],
                     'diferencia' => $fila[11], 'statusNombre' => $fila[12], 'status' => $fila[13],
                     'cot_pend_monto' => $fila[14], 'cot_efec_monto' => $fila[15]);
    }
  }

  return $valor;
}
/*============================================================================*/

/*============================================================================*/
/*retorna folio para corte de caja*/
function damefolioccj() {
  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
     echo "Error en la conexion de mysql: " . mysqli_connect_error();
  }
  $sql    = "SELECT ifnull(cast(max(ccj_clave) as signed), 0) from cortecaja";
  $result = mysqli_query($con,$sql);
  $folio  = "error_search";
  if ($result) {
    while ($fila = mysqli_fetch_array($result)) {
      $valor = $fila[0];
    }

    $indice = $valor + 1;
    $folio  = llenaCeros($indice,4);
  }

  return $folio;
}
/*=================================*/

/*retorna foilo para una cotización*/
function damefoliocotizacion() {
  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
     echo "Error en la conexion de mysql: " . mysqli_connect_error();
  }
  $sql    = "SELECT ifnull(cast(max(cot_clave) as signed), 0) from cotizaciones";
  $result = mysqli_query($con,$sql);
  $folio  = "error_search";
  if ($result) {
    while ($fila = mysqli_fetch_array($result)) {
      $valor = $fila[0];
    }

    $indice = $valor + 1;
    $folio  = llenaCeros($indice,4);
  }

  return $folio;
}
/*=================================*/

/*Retorna folio para una venta*/
function damefolioventa(){
  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
     echo "Error en la conexion de mysql: " . mysqli_connect_error();
  }
  $sql    = "SELECT ifnull(cast(max(ven_clave) as signed), 0) from ventas";
  $result = mysqli_query($con,$sql);
  $folio  = "error_search";
  if ($result) {
    while ($fila = mysqli_fetch_array($result)) {
      $valor = $fila[0];
    }

    $indice = $valor + 1;
    $folio  = llenaCeros($indice,4);
  }

  return $folio;
}
/*===================================*/

function llenaCeros($cadena, $ceros) {
  $resultado = $cadena;
  while (strlen($resultado) < $ceros) {
    $resultado = "0".$resultado;
  }
  return $resultado;
}
/*============================================================================*/

/*Retorna fechas con formato mysql ===========================================*/
function damedatetime(){

  $valor = "error_datetime";
  date_default_timezone_set('America/Mexico_City');
  $valor = date("Y-m-d H:i:s");

  return $valor;
}
/*============================================================================*/

/*Retorna el ultimo registro en la tabla cotización ==========================*/
function ultimoRegistroCotizacion(){
  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $sql    = "SELECT cot_folio
                  from cotizaciones
                    where cot_status = 1
                 order by cot_clave desc limit 1";
  $result = mysqli_query($con, $sql);
  $valor  = "error_search";
  if ($result) {
    while ($fila = mysqli_fetch_array($result)) {
      $valor = $fila[0];
    }
  }

  return $valor;
}
/*============================================================================*/

/*Retorna el contenido de la tabla cotización por medio de folio =============*/
function dameDatosCotFolio($folio){
  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }
                    //cot_clave, cot_tipo, cot_fecha, cot_subtotal, cot_iva, cot_total, cot_ccj_clave, cot_status, cot_cli_clave
  $sql    = "SELECT cot_clave, cot_fecha, cot_status, cot_tipo, cot_ccj_clave, cot_cli_clave,
                    concat('$ ',format(ifnull(cot_subtotal, 0), 2)) as cot_subtotalformat,
                    concat('$ ',format(ifnull(cot_iva, 0), 2)) as cot_ivaformat,
                    concat('$ ',format(ifnull(cot_total, 0), 2)) as cot_totalformat
                from cotizaciones
                  where cot_folio = '$folio'";
  $result = mysqli_query($con, $sql);
  $valor  = "error_search";
  if ($result) {
    while ($fila = mysqli_fetch_array($result)) {
      $valor = array('cot_clave' => $fila[0], 'cot_fecha' => $fila[1], 'cot_status' => $fila[2],
                     'cot_tipo' => $fila[3], 'cot_ccj_clave' => $fila[4], 'cot_cli_clave' => $fila[5],
                     'cot_subtotalformat' => $fila[6], 'cot_ivaformat' => $fila[7],
                     'cot_totalformat' => $fila[8]);
    }
  }

  return $valor;
}
/*============================================================================*/

/*Retorna el contenido de la tabla cotización por medio del id ===============*/
function dameDatosCot($cotizacion){
  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $sql    = "SELECT cot_folio, date_format(cot_fecha, '%d-%m-%Y'), cot_status, cot_tipo, cot_ccj_clave,
                    concat('$ ',cast((ifnull(cot_subtotal, 0)) as decimal(11, 2) )) as cot_subtotalformat,
                    concat('$ ',cast((ifnull(cot_total, 0)) as decimal(11, 2) )) as cot_totalformat,
                    concat('$ ',cast((ifnull(cot_zon_costo, 0)) as decimal(11, 2))) as cot_zon_costoformat,
                    concat('$ ',cast((ifnull(cot_iva, 0)) as decimal(11, 2) )) as cot_ivaformat,
                    ifnull(cot_subtotal, 0) as cot_subtotal, ifnull(cot_total, 0) as cot_total,
                    ifnull(cot_iva, 0) as cot_iva, ifnull(cot_zon_costo, 0) as cot_zon_costo,
                    cot_cli_clave, cot_envio
                from cotizaciones
                  where cot_clave  = $cotizacion";
  $result = mysqli_query($con, $sql);
  //echo $sql;
  $valor  = "error_search";
  if ($result) {
    while ($fila = mysqli_fetch_array($result)) {
      $valor = array('cot_folio' => $fila[0], 'cot_fecha' => $fila[1], 'cot_status' => $fila[2],
                     'cot_tipo' => $fila[3], 'cot_ccj_clave' => $fila[4], 'cot_subtotalformat' => $fila[5],
                     'cot_totalformat' => $fila[6], 'cot_zon_costoformat'=> $fila[7], 'cot_ivaformat' => $fila[8],
                     'cot_subtotal' => $fila[9], 'cot_total' => $fila[10], 'cot_iva' => $fila[11],
                     'cot_zon_costo' => $fila[12], 'cot_cli_clave' => $fila[13], 'cot_envio' => $fila[14]);
    }
  }

  return $valor;
}
/*============================================================================*/

/*Retorna información de un producto agregado al detalle de la cotización o
  venta por medio del id =====================================================*/
function datodelosdetalles($apartado, $clave){
  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  switch ($apartado) {
    case 1:
      $sql = "SELECT dc_precioUnitario, dc_cantidad, dc_subtotal, dc_iva, dc_total,
                      dc_pro_clave, dc_cot_clave
                  from detallecotizacion
                    where dc_clave = $clave";
      break;

    default:
      $sql = "SELECT dv_precioUnitario, dv_cantidad, dv_subtotal, dv_iva, dv_total,
                      dv_pro_clave, dv_ven_clave
                  from detalleventas
                    where dv_clave = $clave";
      break;
  }

  $result = mysqli_query($con, $sql);
  $valor  = "error_search";

  if ($result) {
    while ($fila = mysqli_fetch_array($result)) {
      $valor = array('precioUnitario' => $fila[0], 'cantidad' => $fila[1],
                     'subtotal' => $fila[2], 'iva' => $fila[3],
                     'total' => $fila[4], 'd_pro_clave' => $fila[5],
                     'enc_clave' => $fila[6]);
    }
  }

  return $valor;
}
/*============================================================================*/

/*Busca un cliente por medio del nombre ======================================*/
function searchNameCliente($cliente){
  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $sql    = "SELECT count(cli_clave), cli_clave, cli_status from clientes where cli_nombre = '$cliente'";
  $result = mysqli_query($con, $sql);
  //echo $sql;
  $valor  = "error_search";
  if ($result) {
    while ($fila = mysqli_fetch_array($result)) {
      $valor = array('cli_count' => $fila[0], 'cli_clave' => $fila[1], 'cli_status' => $fila[2]);
    }
  }

  return $valor;
}
/*============================================================================*/

/*Retorna el ultimo registro en la tabla clientes ============================*/
function ultimoRegistroCliente(){
  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $sql    = "SELECT cli_clave
                  from clientes
                    where cli_status = 1
                 order by cli_clave desc limit 1";
  $result = mysqli_query($con, $sql);
  $valor  = "error_search";
  if ($result) {
    while ($fila = mysqli_fetch_array($result)) {
      $valor = $fila[0];
    }
  }

  return $valor;
}
/*============================================================================*/

/*Retorna todos los datos de la tabla clientes ===============================*/
function dameDatosCliente($cliente){
  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $sql    = "SELECT cli_nombre, cli_telefono, cli_correo,
                    cli_direccion, cli_status
                  from clientes
                    where cli_clave = $cliente";
                    //echo $sql;
  $result = mysqli_query($con, $sql);
  $valor  = "error_search";
  if ($result) {
    while ($fila = mysqli_fetch_array($result)) {
      $valor = array('cli_nombre' => $fila[0], 'cli_telefono' => $fila[1], 'cli_correo' => $fila[2],
                     'cli_direccion' => $fila[3], 'cli_status' => $fila[4]);
    }
  }

  return $valor;
}
/*============================================================================*/

/*Retorna el ultimo registro en la tabla ventas ==============================*/
function ultimoRegistroVenta(){
  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $sql    = "SELECT ven_clave, ven_folio
                  from ventas
                    where ven_status = 1
                 order by ven_clave desc limit 1";
  $result = mysqli_query($con, $sql);
  echo $sql;
  $valor  = "error_search";
  if ($result) {
    while ($fila = mysqli_fetch_array($result)) {
      $valor = array('clave' => $fila[0], 'folio' => $fila[1]);
    }
  }

  return $valor;
}
/*============================================================================*/

/*Retorna el contenido de la cabecera de una venta por medio del folio =======*/
function dameDatoVentaFolio($folio){
  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $sql    = "SELECT ven_clave, ven_cot_clave, ven_ccj_clave, ven_cli_clave,
                    ven_emp_clave, date_format(ven_fecha, '%Y-%m-%d'),
                    date_format(ven_fecha, '%r'), ven_tipo, ven_status
                 from ventas
                   where ven_folio = '$folio'";
  //echo $sql;
  $result = mysqli_query($con, $sql);
  $valor  = "error_search";
  if ($result) {
    while ($fila = mysqli_fetch_array($result)) {
      $valor = array('ven_clave' => $fila[0], 'ven_cot_clave' => $fila[1], 'ven_ccj_clave' => $fila[2],
                     'ven_cli_clave' => $fila[3], 'ven_emp_clave' => $fila[4], 'ven_fecha' => $fila[5],
                     'ven_hora' => $fila[6], 'ven_tipo' => $fila[7], 'ven_status' => $fila[8]);
    }
  }

  return $valor;
}
/*============================================================================*/

/*Retorna el contenido de la cabecera de una venta por medio del indentificador */
function dameDatoVenta($venta){
  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $sql    = "SELECT ven_folio, ven_cot_clave, ven_ccj_clave, ven_cli_clave, ven_emp_clave,
                    ven_fecha, ven_tipo, ven_status, ven_iva, ven_total, ven_cobrado
                 from ventas
                   where ven_clave = $venta";
  $result = mysqli_query($con, $sql);
  $valor  = "error_search";
  if ($result) {
    while ($fila = mysqli_fetch_array($result)) {
      $valor = array('ven_folio' => $fila[0], 'ven_cot_clave' => $fila[1], 'ven_ccj_clave' => $fila[2],
                     'ven_cli_clave' => $fila[3], 'ven_emp_clave' => $fila[4], 'ven_fecha' => $fila[5],
                     'ven_tipo' => $fila[6], 'ven_status' => $fila[7], 'ven_iva' => $fila[8],
                     'ven_total' => $fila[9], 'ven_cobrado' => $fila[10]);
      // ven_status
    }
  }

  return $valor;
}
/*============================================================================*/

/*Revisa si una cotización tiene productos ===================================*/
function hasproduct($cotizacion){
  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $valor = "error_search";

  $sql    = "SELECT count(dc_clave) from detallecotizacion where dc_cot_clave = $cotizacion";
  //echo $sql;
  $result = mysqli_query($con, $sql);
  if ($result) {
    while ($fila = mysqli_fetch_array($result)) {
      $valor = $fila[0];
    }
  }

  return $valor;
}
/*============================================================================*/

/*Revisa si existe ventas abiertas en el corte por cerrar ====================*/
function opensale($caja){
  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $sql    = "SELECT count(ven_clave) from ventas where ven_status = 1 and ven_ccj_clave = $caja";
  $result = mysqli_query($con, $sql);

  $valor  = "error_search";
  if ($result) {

    while ($fila = mysqli_fetch_array($result)) {
      $count = $fila[0];
    }

    switch ($count) {
      case 0:
        $valor = "0";
        break;

      default:
        $valor = "error_search";
        if ($count > 0) {

          $sql    = "SELECT ven_folio from ventas where ven_status = 1 and ven_ccj_clave = $caja";
          $result = mysqli_query($con, $sql);
          if ($result) {
            while ($fila = mysqli_fetch_array($result)) {
              $valor = $fila[0];
            }

            //echo $sql;
          }

        }
        break;
    }

  }

  return $valor;
}
/*============================================================================*/

/*Busca zonas por nombres ====================================================*/
function searchZonaNombre($zona){
  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $sql    = "SELECT count(zon_clave), zon_status, zon_clave
                from zonas
                  where zon_nombre = '$zona'";
  $result = mysqli_query($con, $sql);
  $valor  = array('error' => "error_search");
  if ($result) {
    while ($fila = mysqli_fetch_array($result)) {
      $valor = array('zon_count' => $fila[0], 'zon_status' => $fila[1], 'zon_clave' => $fila[2]);
    }
  }

  return $valor;
}
/*============================================================================*/

/*Retorna todo el contenido de la tabla zonas ================================*/
function dameDatosZona($zona){
  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $sql    = "SELECT zon_clave, zon_nombre, zon_descripcion, zon_precio, zon_status
                from zonas
                  where zon_clave = $zona";
  $result = mysqli_query($con, $sql);
  $valor  = "error_search";
  if ($result) {
    while ($fila = mysqli_fetch_array($result)) {
      $valor = array('zon_clave' => $fila[0], 'zon_nombre' => $fila[1],
                     'zon_descripcion' => $fila[2], 'zon_precio' => $fila[3],
                     'zon_status' => $fila[4]);
    }
  }

  return $valor;
}
/*============================================================================*/

/*retorna la clave del detalle si existe productos similares y retorna 0 si no
  existe producto ============================================================*/
function existeProductoEnCotizacion($producto, $cotizacion){

  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

 $valor = "error_search";

 $sql     = "SELECT count(dc_clave), dc_clave, dc_cantidad, dc_precioUnitario
                from detallecotizacion
                  where dc_pro_clave = $producto
                    and dc_cot_clave = $cotizacion";
  $result = mysqli_query($con, $sql);
  if ($result) {
    while ($fila = mysqli_fetch_array($result)) {
      $valor = array('count' => $fila[0], 'clave' => $fila[1], 'cantidad' => $fila[2],
                     'precioUnitario' => $fila[3]);
    }
  }

  return $valor;
}
/*============================================================================*/

/*Retorna clave del corte de caja activo =====================================*/
function dameCorteActivo(){

  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $valor  = "error_search";
  $sql    = "SELECT ccj_clave from cortecaja where ccj_status = 0";
  $result = mysqli_query($con, $sql);
  if ($result) {
    while ($fila = mysqli_fetch_array($result)) {
      $valor = array('clave' => $fila[0]);
    }
  }

  return $valor;
}
/*============================================================================*/

?>

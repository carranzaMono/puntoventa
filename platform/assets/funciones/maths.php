<?php

/*Retornar los montos de subtotal, iva y total de una cotización =============*/
function montosCotizacion($cotizacion){
  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $sql    = "SELECT cast(sum(dc_subtotal) as decimal(11, 3)) as subtotal,
                    cast(sum(dc_iva) as decimal(11, 3)) as iva,
                    cast(sum(dc_total) as decimal(11, 3)) as total
                  from detallecotizacion
                    where dc_cot_clave = $cotizacion
                      and dc_stauts = 1";
  $result = mysqli_query($con, $sql);

  //echo $sql;
  $valor  = "error_search";
  if ($result) {
    while ($fila = mysqli_fetch_array($result)) {
      $valor = array('subtotal' => $fila[0], 'iva' => $fila[1], 'total' => $fila[2]);
    }
  }

  return $valor;
}
/*============================================================================*/

/*Retornar los montos de subtotal, iva y total de una comisión ===============*/
function montoVenta($venta){
  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $sql   = "SELECT (select cast( ifnull(sum(dv_subtotal), 0) as decimal(11,2) ) from detalleventas where dv_ven_clave = ven_clave and dv_status = 1) as subtotal,
                   (select cast( ifnull(sum(dv_iva), 0) as decimal(11,2) ) from detalleventas where dv_ven_clave = ven_clave and dv_status = 1) as iva,
                   (select cast( ifnull(sum(dv_total), 0) as decimal(11,2) ) from detalleventas where dv_ven_clave = ven_clave and dv_status = 1) as total,
                   (select cast( ifnull(sum(cob_monto), 0) as decimal(11, 2) ) from cobros where cob_ven_clave = ven_clave and cob_status = 1) as lopagado
                from ventas
                  where ven_clave = $venta";
  $result = mysqli_query($con, $sql);
  $valor  = "error_search";
  if ($result) {
    while ($fila = mysqli_fetch_array($result)) {
      $valor = array('subtotal' => $fila[0], 'iva' => $fila[1], 'total' => $fila[2], 'loCobrado' => $fila[3]);
    }
  }

  return $valor;
}
/*============================================================================*/

/*Retornar el cambio a devolver de un pago ===================================*/
function dameCambio($venta, $formapago, $monto, $resta){
  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $cambio = $monto - $resta;

  switch ($formapago) {
    case 1:

      if ($cambio <= 0) {

        $valor = array('cambio' => 0, 'pago' => $monto);

      }else {

        $pago  = $monto - $cambio;
        $valor = array('cambio' => $cambio, 'pago' => $pago);

      }

      break;

    default:
        $valor = array('cambio' => 0, 'pago' => $monto);
      break;
  }

  return $valor;
}
/*============================================================================*/

/*calcula la retencion de iva de un pago =====================================*/
function dameretencionIva($monto){

  $valor = 0;
  if ($monto >= 0) {
    $valor  = $monto*0.16;
  }

  return $valor;
}
/*============================================================================*/

/*retorna montos por pagar y lo pagado de una venta ==========================*/
function calcularTotales($clave){

  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $sql    = "SELECT cast( (select ifnull(sum(cob_monto), 0) from cobros where cob_ven_clave = ven_clave and cob_status = 1) as decimal(11, 2)) as pagado,
                    cast( ( select  ifnull(sum(dv_total), 0) from detalleventas where  dv_ven_clave = ven_clave and dv_status = 1) as decimal(11, 2) ) as porPagar,
                    (cast( ( select  ifnull(sum(dv_total), 0) from detalleventas where  dv_ven_clave = ven_clave and dv_status = 1) as decimal(11, 2) ) - cast( (select ifnull(sum(cob_monto), 0) from cobros where cob_ven_clave = ven_clave and cob_status = 1) as decimal(11, 2))) as seresta
                  from ventas
                    where ven_clave = $clave";
  //echo $sql;
  $result = mysqli_query($con, $sql);
  $valor  = "error_search";
  if ($result) {
    while ($fila = mysqli_fetch_array($result)) {
      $valor = array('porPagar' => $fila[1], 'pagado' => $fila[0], 'resta' => $fila[2]);
    }
  }

  return $valor;
}
/*============================================================================*/

?>

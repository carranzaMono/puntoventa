<?php
  include_once "../../../config.php";

  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $sql    = "SELECT ccj_clave, ccj_folio, ccj_inicio, ccj_fin,
                    (select concat('$ ', cast(ifnull(sum(dv_total), 0) as decimal(11, 2))) from detalleventas where dv_ccj_clave = ccj_clave) as ccj_ventatotal,
                    (select concat(emp_nombre,' ',emp_apPaterno) from empleados where emp_clave = ccj_emp_abrio) as ccj_emp_abrio,
                    (select concat(emp_nombre,' ',emp_apPaterno) from empleados where emp_clave = ccj_emp_cerro) as ccj_emp_cerro,
                    CASE
                      WHEN ccj_status = 0 THEN 'Abierto'
                      WHEN ccj_status = 1 THEN 'Cerrado'
                      ELSE 'Cancelado'
                    END as ccj_status
                  from cortecaja
                    order by ccj_clave desc
                    limit 3";
  $result = mysqli_query($con, $sql);

  if (!$result) {
    die("error");
  }else {

    $array['data'] = [];
    while ($data = mysqli_fetch_assoc($result)) {
      $array['data'][] = $data;
    }

    echo json_encode($array);
  }

  mysqli_free_result($result);
  mysqli_close($con);
?>

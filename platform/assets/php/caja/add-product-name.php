<?php
  session_set_cookie_params(60*60*24*14);
  session_start();
  include_once "../../../config.php";
  include_once "../../funciones/create.php";
  include_once "../../funciones/searches.php";
  include_once "../../funciones/generales.php";

  if (isset($_POST['producto'])) {

    $pro_clave = $_POST['producto'];
    $ven_clave = $_GET['venta'];
    $cantidad  = $_POST['cantidad'];

    $venta     = dameDatoVenta($ven_clave);

    if ($venta['ven_status'] == 1) {

      $producto  = dameDatosProducto($pro_clave);

      $p_unitario = $producto['pro_precio'];
      $subtotal   = $p_unitario * $cantidad;
      $iva        = $subtotal * 0.16;
      $total      = $subtotal + $iva;

      $modulo      = "Venta";
      $accion      = "Resgistro de producto";
      $emp_clave   = $_SESSION['usu_emp_clave'];
      $tipo        = 3;//Cotización
      $descripcion = "Se agrego el producto: ".$producto['pro_nombre']." a la venta ".$venta['ven_folio'];

      $detalle    = agregardetalleVent($ven_clave, $p_unitario, $cantidad, $subtotal, $iva, $total, $pro_clave);
      if ($detalle == 1) {
        $resultado = bitacora($modulo, $accion, $tipo, $emp_clave, $descripcion);
      }else {
        $resultado = $detalle;
      }


    }else {
      $resultado = $venta['ven_status'];
    }

    echo json_encode($resultado);

  }


?>

<?php
  include_once "../../../../config.php";

  if ($_GET['venta']) {
    $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
    if (mysqli_connect_errno()) {
      echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
    }

    $venta = $_GET['venta'];

    $sql     = "SELECT (select concat('$ ', cast(ifnull(sum(dv_total) , 0) as decimal(11, 2)) ) from detalleventas where dv_status = 1 and dv_ven_clave = ven_clave) as subtotal,
	                     (select concat('$ ', cast(ifnull(sum(dv_iva), 0) as decimal(11, 2)) ) from detalleventas where dv_status = 1 and dv_ven_clave = ven_clave) as iva,
	                     (select concat('$ ', cast(ifnull(sum(dv_total), 0) as decimal(11, 2)) ) from detalleventas where dv_status = 1 and dv_ven_clave = ven_clave) as total,
                       (select concat('$ ', cast(ifnull(sum(cob_monto), 0) as decimal(11, 2)) ) from cobros where cob_ven_clave = ven_clave and cob_status = 1) as cobrado,
                       ( concat('$ ', cast((select ifnull(sum(dv_total), 0) from detalleventas where dv_status = 1 and dv_ven_clave = ven_clave) as decimal(11,2)) - cast((select ifnull(sum(cob_monto), 0) from cobros where cob_ven_clave = ven_clave and cob_status = 1) as decimal(11, 2))) ) as resta
	                  from ventas
                      where ven_clave = $venta";
    $result = mysqli_query($con, $sql);
    //echo $sql;

    if (!$result) {
      die("error");
    }else {

      $array['data'] = [];
      while ($data = mysqli_fetch_assoc($result)) {
        $array['data'][] = $data;
      }

      echo json_encode($array);
    }

    mysqli_free_result($result);
    mysqli_close($con);
  }
?>

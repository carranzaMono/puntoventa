<?php
  session_set_cookie_params(60*60*24*14);
  session_start();
  include_once "../../../../config.php";
  include_once "../../../funciones/maths.php";
  include_once "../../../funciones/create.php";
  include_once "../../../funciones/searches.php";
  include_once "../../../funciones/generales.php";

  if (isset($_POST['monto'])) {

    $ven_clave = $_GET['clave'];
    $monto     = $_POST['monto'];
    $formapago = $_POST['forma-pago'];
    $emp_clave = $_SESSION['usu_emp_clave'];

    $datosventa    = dameDatoVenta($ven_clave);
    $total         = calcularTotales($ven_clave);
    $cli_clave     = $datosventa['ven_cli_clave'];
    $observacion   = "Cobro de ".$datosventa['ven_folio'];
    $valor         = dameCambio($ven_clave, $formapago, $monto, $total['resta']);

    $cob_monto     = $valor['pago'];
    $cob_cambio    = $valor['cambio'];

    //echo $cob_cambio;

    if ($monto >= 0) {

      if ($total['pagado'] < $total['porPagar'] || $total['pagado'] == 0) {

        switch ($formapago) {
          case 1:
             $sql = agregarCobro($ven_clave, $formapago, $cob_monto, $cob_cambio, $monto, $observacion, $emp_clave, $cli_clave);
            break;

          default:

            if ($monto <= $total['resta']) {
              $sql = agregarCobro($ven_clave, $formapago, $cob_monto, $cob_cambio, $monto, $observacion, $emp_clave, $cli_clave);
            }else {
              $sql = "monto_excesivo";
            }

            break;
        }

        //bitacora

      }

    }

    echo json_encode($sql);
  }

?>

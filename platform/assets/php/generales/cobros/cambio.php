<?php
  include_once "../../../../config.php";

  if (isset($_GET['venta'])) {
    $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
    if (mysqli_connect_errno()) {
      echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
    }

    $venta = $_GET['venta'];

    $sql    = "SELECT concat('$ ', format(ifnull(cob_cambio, 0), 2) ) as cambio
                  from cobros
                    where cob_status = 1
                      and cob_ven_clave = $venta
                      order by cob_clave desc limit 1";
    $result = mysqli_query($con, $sql);
    if (!$result) {
      die("error");
    }else {

      $array['data'] = [];
      while ($data = mysqli_fetch_assoc($result)) {
        $array['data'][] = $data;
      }

      echo json_encode($array);
    }

    mysqli_free_result($result);
    mysqli_close($con);

  }
?>

<?php
  session_set_cookie_params(60*60*24*14);
  session_start();
  include_once "../../../../config.php";
  include_once "../../../funciones/create.php";
  include_once "../../../funciones/searches.php";
  include_once "../../../funciones/generales.php";

  if (isset($_POST['concepto']) && isset($_POST['monto'])) {
    $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
    if (mysqli_connect_errno()) {
      echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
    }

    $concepto = ucfirst(strtolower(trim($_POST['concepto'])));
    $monto    = $_POST['monto'];
    $modulo   = "Caja";
    $accion   = "Salida";
    $tipo     = 2;//Caja

    $caja = boxexists();

    $salida = insertSalidas($concepto, $monto, $caja['ccj_clave'], $_SESSION['usu_emp_clave']);
    if ($salida == 1) {
      $descripcion = "Se registro una salida con monto $monto para la caja".$caja['ccj_folio'];
      $resultado   = bitacora($modulo, $accion, $tipo, $_SESSION['usu_emp_clave'], $descripcion);
    }

    echo json_encode($resultado);

  }
?>

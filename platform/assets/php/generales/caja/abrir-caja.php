<?php
session_set_cookie_params(60*60*24*14);
session_start();
include_once "../../../../config.php";
include_once "../../../funciones/create.php";
include_once "../../../funciones/searches.php";
include_once "../../../funciones/generales.php";

if (isset($_POST['fondo'])) {

  $monto  = $_POST['fondo'];

  $modulo     = "Caja";
  $accion     = "Registro";
  $emp_clave  = $_SESSION['usu_emp_clave'];
  $tipo       = 2;//Caja

  $caja = createcaja($emp_clave, $monto);
  if ($caja['count'] == 1) {

    $descripcion = "Se abrio el corte de caja con folio ".$caja['folio'];
    $bitacora    = bitacora($modulo, $accion, $tipo, $emp_clave, $descripcion);

    if ($bitacora != "error_insert_bit") {
      $resultado = 1;
    }else {
      $resultado = $bitacora;
    }

  }else {
    $resultado = $caja;
  }

  echo json_encode($resultado);
}

?>

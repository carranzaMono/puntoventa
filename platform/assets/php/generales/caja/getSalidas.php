<?php
  include_once "../../../../config.php";

  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $sql    = "SELECT ccj_clave from cortecaja where ccj_status = 0";
  $result = mysqli_query($con, $sql);

  if ($result) {

    while ($fila = mysqli_fetch_array($result)) {
      $ccj_clave = $fila[0];
    }

    $sql    = "SELECT mc_clave, mc_observacion,
                      concat('$ ', format(mc_monto, 2)) as mc_total
                    from movcaja
                        where mc_tipo = 1
                          and mc_ccj_clave = $ccj_clave
                          and mc_clasificacion = 0
                        order by mc_clave desc";
    $result = mysqli_query($con, $sql);

    if (!$result) {
      die("error");
    }else {

      $array['data'] = [];
      while ($data = mysqli_fetch_assoc($result)) {
        $array['data'][] = $data;
      }

      echo json_encode($array);
    }

    mysqli_free_result($result);
    mysqli_close($con);

  }
?>

<?php

session_set_cookie_params(60*60*24*14);
session_start();
include_once "../../../config.php";
include_once "../../funciones/maths.php";
include_once "../../funciones/searches.php";
include_once "../../funciones/generales.php";

if (isset($_POST['apartado'])) {
  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  /*
      apartado 1 = cotización
               2 = venta
  */

  $apartado = $_POST['apartado'];
  $valor    = "error_aldisminuir";
  $borrar   = 0;

  switch ($apartado) {
    case 1:

      $dc_clave    = $_POST['dc_clave'];
      $dc_producto = datodelosdetalles($apartado, $dc_clave);
      $cot_clave   = $dc_producto['enc_clave'];

      mysqli_autocommit($con, FALSE);

      try {

        mysqli_begin_transaction($con, MYSQLI_TRANS_START_READ_WRITE);
        $cantidad = $dc_producto['cantidad']-1;
        $subtotal = $dc_producto['precioUnitario']*$cantidad;
        $iva      = $subtotal * 0.16;
        $total    = $subtotal + $iva;

        if ($cantidad == 0) {
          $sql    = "UPDATE detallecotizacion
                        set dc_stauts = $borrar
                      where dc_clave = $dc_clave";
        }else {

          $sql    = "UPDATE detallecotizacion
                        set dc_cantidad = $cantidad, dc_subtotal = $subtotal,
                            dc_iva = $iva, dc_total = $total
                      where dc_clave = $dc_clave";
        }

        $result = mysqli_query($con, $sql);
        if ($result) {

          $sql    = "SELECT cast(sum(dc_subtotal) as decimal(11, 3)) as subtotal,
                            cast(sum(dc_iva) as decimal(11, 3)) as iva,
                            cast(sum(dc_total) as decimal(11, 3)) as total
                          from detallecotizacion
                            where dc_cot_clave = $cot_clave
                              and dc_stauts = 1";
          $result = mysqli_query($con, $sql);
          if ($result) {
            while ($fila = mysqli_fetch_array($result)) {
              $cot_subtotal = $fila[0];
              $cot_iva      = $fila[1];
              $cot_total    = $fila[2];
            }

            $sql    = "UPDATE cotizaciones
                          set cot_subtotal = $cot_subtotal, cot_iva = $cot_iva,
                              cot_total = $cot_total
                        where cot_clave = $cot_clave";
            //echo $cot_total;
            $result = mysqli_query($con, $sql);
            if ($result) {
              if ($cantidad == 0) {
                $valor = 0;
              }else {
                $valor = 1;
              }
            }else {
              mysqli_rollback($con);
            }
            
          }

        }else {
          mysqli_rollback($con);
        }

        mysqli_commit($con);

      } catch (\Exception $e) {
        mysqli_rollback($con);
      }

      break;

    default:

      $ven_clave   = $_GET['clave'];
      $venta       = dameDatoVenta($ven_clave);

      if ($venta['ven_status'] == 1) {
        $dv_clave    = $_POST['dv_clave'];
        $dc_producto = datodelosdetalles($apartado, $dv_clave);

        mysqli_autocommit($con, FALSE);
        try {

          mysqli_begin_transaction($con, MYSQLI_TRANS_START_READ_WRITE);
          $cantidad = $dc_producto['cantidad']-1;
          $subtotal = $dc_producto['precioUnitario']*$cantidad;
          $iva      = $subtotal * 0.16;
          $total    = $subtotal + $iva;

          if ($cantidad == 0) {
            $sql    = "UPDATE detalleventas
                          set dv_status = $borrar
                        where dv_clave = $dv_clave";
          }else {

            $sql    = "UPDATE detalleventas
                          set dv_cantidad = $cantidad, dv_subtotal = $subtotal,
                              dv_iva = $iva, dv_total = $total
                        where dv_clave = $dv_clave";
          }

          $result = mysqli_query($con, $sql);
          if ($result) {
            if ($cantidad == 0) {
              $valor = 0;
            }else {
              $valor = 1;
            }

          }else {
            mysqli_rollback($con);
          }

          mysqli_commit($con);

        } catch (\Exception $e) {
          mysqli_rollback($con);
        }
      }else {
        $valor = $venta['ven_status'];
      }

      break;
  }

  echo json_encode($valor);
}

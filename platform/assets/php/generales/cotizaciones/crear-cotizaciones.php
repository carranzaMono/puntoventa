<?php
  session_set_cookie_params(60*60*24*14);
  session_start();

  include_once "../../../../config.php";
  include_once "../../../funciones/maths.php";
  include_once "../../../funciones/create.php";
  include_once "../../../funciones/searches.php";
  include_once "../../../funciones/generales.php";

  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $caja          = boxexists();
  $fecha_hr      = damedatetime();
  $cot_tipo      = 1;
  $cot_cli_clave = 1;
  $cot_status    = 1;

  switch ($caja['ccj_count']) {
    case 1:

      $folio_nuevo = damefoliocotizacion();
      $ccj_clave   = $caja['ccj_clave'];

      $modulo      = "Cotización";
      $accion      = "Registro";
      $empleado    = $_SESSION['usu_emp_clave'];
      $tipo        = 1;
      $description = "Se registro la cotización con folio: $folio_nuevo";
      $valor       = "error_create";

      mysqli_autocommit($con, FALSE);
      try {

        mysqli_begin_transaction($con, MYSQLI_TRANS_START_READ_WRITE);
        $sql    = "INSERT into cotizaciones(cot_tipo, cot_fecha, cot_folio, cot_ccj_clave, cot_status, cot_cli_clave, cot_emp_clave)
                               values($cot_tipo, '$fecha_hr', '$folio_nuevo', $ccj_clave, $cot_status, $cot_cli_clave, $empleado)";
        //echo $sql;
        $result = mysqli_query($con, $sql);
        if ($result) {

          $sql     = "SELECT cot_clave from cotizaciones where cot_status = 1 order by cot_clave desc limit 1";
          $result  = mysqli_query($con, $sql);
          if ($result) {

            while ($fila = mysqli_fetch_array($result)) {
              $cot_clave = $fila[0];
            }

            $resultado = bitacora($modulo, $accion, $tipo, $empleado, $description, $cot_clave);
            if ($resultado == 1) {

              $valor = $folio_nuevo;

            }else {
              mysqli_rollback($con);
            }

          }

        }else {
          mysqli_rollback($con);
        }

        mysqli_commit($con);

      } catch (\Exception $e) {
        mysqli_rollback($con);
      }

      break;

    default:
      if ($caja['ccj_count'] == 0) {

        $valor = 0;

      }else {
        $valor = $caja;
      }
      // code...
      break;
  }

  echo json_encode($valor);
?>

<?php
  include_once "../../../../config.php";

  if ($_GET['clave']) {
    $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
    if (mysqli_connect_errno()) {
      echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
    }

    $cot_clave = $_GET['clave'];

    $sql    = "SELECT concat('$ ', cast(ifnull(sum(dc_total), 0) as decimal(11, 2) )) as total
                    from detallecotizacion
                        where dc_cot_clave = $cot_clave
                          and dc_stauts = 1";
    //echo $sql;
    $result = mysqli_query($con, $sql);

    if (!$result) {
      die("error");
    }else {

      $array['data'] = [];
      while ($data = mysqli_fetch_assoc($result)) {
        $array['data'][] = $data;
      }

      echo json_encode($array);
    }

    mysqli_free_result($result);
    mysqli_close($con);
  }
?>

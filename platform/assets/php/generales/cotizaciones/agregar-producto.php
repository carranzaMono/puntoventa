<?php
session_set_cookie_params(60*60*24*14);
session_start();
include_once "../../../../config.php";
include_once "../../../funciones/maths.php";
include_once "../../../funciones/create.php";
include_once "../../../funciones/searches.php";
include_once "../../../funciones/generales.php";

if (isset($_GET['clave'])) {

  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $cot_clave  = $_GET['clave'];
  $pro_clave  = $_POST['producto'];
  $cantidad   = $_POST['cantidad'];
  $cotizacion = dameDatosCot($cot_clave);
  $producto   = dameDatosProducto($pro_clave);

  $valor       = "error_save";
  $modulo      = "Cotización";
  $accion      = "Resgistro de producto";
  $emp_clave   = $_SESSION['usu_emp_clave'];
  $tipo        = 3;//Cotización

  //echo $cotizacion['cot_folio'];
  $descripcion = "Se agrego el producto: ".$producto['pro_nombre']." a la cotización ".$cotizacion['cot_folio'];

  $existProCot = existeProductoEnCotizacion($pro_clave, $cot_clave);

  //echo $existProCot['count'];

  switch ($existProCot['count']) {
    case 0:

      $precioUnitario = $producto['pro_precio'];
      $subtotal       = $precioUnitario * $cantidad;
      $iva            = number_format($subtotal * 0.16, 3);
      $total          = $subtotal + $iva;
      $status         = 1;

      $sql    = "INSERT into detallecotizacion(dc_cot_clave, dc_precioUnitario, dc_cantidad, dc_subtotal, dc_iva, dc_total, dc_pro_clave, dc_stauts)
                                        values($cot_clave, $precioUnitario, $cantidad, $subtotal, $iva, $total, $pro_clave, $status)";
      $result = mysqli_query($con, $sql);
      if ($result) {

        $monto        = montosCotizacion($cot_clave);
        $cot_subtotal = $monto['subtotal'];
        $cot_iva      = $monto['iva'];
        $cot_total    = $monto['total'];

        $sql    = "UPDATE cotizaciones
                      set cot_subtotal = $cot_subtotal, cot_iva = $cot_iva, cot_total = $cot_total
                    where cot_clave = $cot_clave";
                  //echo $sql;
        $result = mysqli_query($con, $sql);
        if ($result) {
          $valor = bitacora($modulo, $accion, $tipo, $emp_clave, $descripcion);
        }
      }

      break;

    case 1:

      $dc_clave    = $existProCot['clave'];
      $dc_cantidad = $existProCot['cantidad'] + $cantidad;
      $subtotal    = $existProCot['precioUnitario'] * $dc_cantidad;
      $iva         = $subtotal * 0.16;
      $total       = $subtotal + $iva;
      mysqli_autocommit($con, FALSE);

      try {

        mysqli_begin_transaction($con, MYSQLI_TRANS_START_READ_WRITE);
        $sql    = "UPDATE detallecotizacion
                      set dc_cantidad = $dc_cantidad, dc_subtotal = $subtotal,
                          dc_iva = $iva, dc_total = $total
                    where dc_clave = $dc_clave";
        $result = mysqli_query($con, $sql);
        if ($result) {

          $monto        = montosCotizacion($cot_clave);
          $cot_subtotal = $monto['subtotal'];
          $cot_iva      = $monto['iva'];
          $cot_total    = $monto['total'];

          $sql    = "UPDATE cotizaciones
                        set cot_subtotal = $cot_subtotal, cot_iva = $cot_iva, cot_total = $cot_total
                      where cot_clave = $cot_clave";
          $result = mysqli_query($con, $sql);
          if ($result) {
             $valor = bitacora($modulo, $accion, $tipo, $emp_clave, $descripcion);
          }else {
            mysqli_rollback($con);
          }

        }else {
          mysqli_rollback($con);
        }

        mysqli_commit($con);

      } catch (\Exception $e) {
        mysqli_rollback($con);
      }

      break;

    default:

    //echo "string";
      break;
  }

  echo json_encode($valor);
}
?>

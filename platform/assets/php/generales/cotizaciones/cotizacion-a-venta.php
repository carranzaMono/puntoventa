<?php
  session_set_cookie_params(60*60*24*14);
  session_start();
  include_once "../../../../config.php";
  include_once "../../../funciones/create.php";
  include_once "../../../funciones/searches.php";
  include_once "../../../funciones/generales.php";

  if ($_GET['cotizacion']) {

    $cotizacion = dameDatosCot($_GET['cotizacion']);

    $modulo     = "Cotización";
    $accion     = "Cotización a Venta";
    $emp_clave  = $_SESSION['usu_emp_clave'];
    $tipo       = 3;//Cotización

    //echo $cotizacion['cot_total'];
    $ordenVenta = cotizacionVenta($_GET['cotizacion'], $cotizacion['cot_cli_clave'] ,$cotizacion['cot_subtotal'], $cotizacion['cot_iva'], $cotizacion['cot_total'], $emp_clave);
    if ($ordenVenta == "error_insert" || $ordenVenta == "sin_caja" || $ordenVenta == "sin_items" || $ordenVenta == "not_save") {
      $resultado = $ordenVenta;
    }else {

      $descripcion = "Se convirtio la cotizacion ".$cotizacion['cot_folio']."a la venta $ordenVenta";
      $bitacora    = bitacora($modulo, $accion, $tipo, $emp_clave, $descripcion);
      if ($bitacora == 1) {
        $resultado = $ordenVenta;
      }else {
        $resultado = $bitacora;
      }

    }

    echo json_encode($resultado);
  }
?>

<?php
  include_once "../../../../config.php";

  if ($_GET['venta']) {
    $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
    if (mysqli_connect_errno()) {
      echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
    }

    $venta = $_GET['venta'];

    $sql    = "SELECT concat('$ ', format(ifnull(sum(dv_total) , 0), 2)) as subtotal,
                      concat('$ ', format(ifnull(sum(dv_iva), 0), 2)) as iva,
                      concat('$ ', format((ifnull(sum(dv_total), 0)), 2)) as total
	                  from detalleventas
		                  where dv_ven_clave = $venta
		                    and dv_status = 1";
    $result = mysqli_query($con, $sql);

    if (!$result) {
      die("error");
    }else {

      $array['data'] = [];
      while ($data = mysqli_fetch_assoc($result)) {
        $array['data'][] = $data;
      }

      echo json_encode($array);
    }

    mysqli_free_result($result);
    mysqli_close($con);
  }
?>

<?php
  session_set_cookie_params(60*60*24*14);
  session_start();

  include_once "../../../../config.php";
  include_once "../../../funciones/searches.php";
  include_once "../../../funciones/generales.php";

  if (isset($_GET['venta'])) {

    $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
    if (mysqli_connect_errno()) {
      echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
    }

    $ven_clave                     = $_GET['venta'];
    $terminar_de_agregar_productos = 3;
    $venta                         = dameDatoVenta($ven_clave);
    $ven_folio                     = $venta['ven_folio'];
    $valor                         = "error_status";

    $modulo      = "Venta";
    $accion      = "Cambio de stattus";
    $empleado    = $_SESSION['usu_emp_clave'];
    $tipo        = 1;
    $description = "Se indico que ya no se pueda agregar productos/servicios a la venta con folio: $ven_folio";
    $valor       = "error_create";

    $sql    = "UPDATE ventas
                  set ven_status = $terminar_de_agregar_productos
                where ven_clave = $ven_clave";
    $result = mysqli_query($con, $sql);
    if ($result) {
      $resultado = bitacora($modulo, $accion, $tipo, $empleado, $description, $ven_clave);
      if ($resultado == 1) {

        $valor = 1;

      }
    }

    echo json_encode($valor);
  }


?>

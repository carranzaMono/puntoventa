<?php
  include_once "../../../../config.php";

  if ($_GET['venta']) {
    $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
    if (mysqli_connect_errno()) {
      echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
    }

    $venta = $_GET['venta'];

    $sql     = "SELECT
                    CASE
                      WHEN ven_status = 0 THEN 'Venta cancelada'
                      WHEN ven_status = 1 THEN 'Venta abierta'
                      WHEN ven_status = 2 THEN 'Venta cobrada'
                      ELSE 'Venta terminada'
                    END as statusString, ven_status
	                  from ventas
                      where ven_clave = $venta";
    $result = mysqli_query($con, $sql);
    //echo $sql;

    if (!$result) {
      die("error");
    }else {

      $array['data'] = [];
      while ($data = mysqli_fetch_assoc($result)) {
        $array['data'][] = $data;
      }

      echo json_encode($array);
    }

    mysqli_free_result($result);
    mysqli_close($con);
  }
?>

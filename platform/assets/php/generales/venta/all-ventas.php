<?php
include_once "../../../../config.php";

$con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
if (mysqli_connect_errno()) {
  echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
}

$sql    = "SELECT ccj_clave from cortecaja where ccj_status =0";
$result = mysqli_query($con, $sql);
if ($result) {
  while ($fila = mysqli_fetch_array($result)) {
    $ccj_clave = $fila[0];
  }

  $sql    = "SELECT ven_folio, cli_nombre,
              date_format(ven_fecha, '%Y-%m-%d') as fecha,
              date_format(ven_fecha, '%r') as hora,
              CASE
                WHEN ven_status = 0 THEN 'Venta cancelada'
                WHEN ven_status = 1 THEN 'Venta abierta'
                WHEN ven_status = 2 THEN 'Venta cobrada'
                ELSE 'Venta terminada'
  	          END as ven_status,
              (select concat('$ ', cast(ifnull(sum(dv_total), 0) as decimal(11, 2))) from detalleventas where dv_ven_clave = ven_clave and dv_status = 1) as ven_total
  	           from ventas, clientes
  		           where ven_cli_clave = cli_clave
                    and ven_ccj_clave = $ccj_clave
                 order by ven_status asc,  ven_folio desc";
  $result = mysqli_query($con, $sql);

  if (!$result) {
    die("error");
  }else {

    $array['data'] = [];
    while ($data = mysqli_fetch_assoc($result)) {
      $array['data'][] = $data;
    }

    echo json_encode($array);
  }

  mysqli_free_result($result);
  mysqli_close($con);
}


?>

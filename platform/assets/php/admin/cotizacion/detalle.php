<?php
  include_once "../../../../config.php";

  if ($_GET['cotizacion']) {
    $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
    if (mysqli_connect_errno()) {
      echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
    }

    $cotizacion = $_GET['cotizacion'];

    $sql    = "SELECT dc_clave,
                      (select concat(pro_nombre) from productos where pro_clave = dc_pro_clave) as dc_pro_nombre,
                      concat('$ ', format(ifnull(dc_precioUnitario, 0), 2)) as dc_precioUnitario,
                      ifnull(dc_cantidad, 0) as dc_cantidad,
                      concat('$ ', format(ifnull(dc_subtotal, 0), 2)) dc_subtotal,
                      concat('$ ', format(ifnull(dc_iva, 0), 2)) as dc_iva,
                      concat('$ ', format(ifnull(dc_total, 0), 2)) as dc_total
	                  from detallecotizacion
		                  where dc_cot_clave = $cotizacion
		                    and dc_stauts = 1";
    $result = mysqli_query($con, $sql);

    if (!$result) {
      die("error");
    }else {

      $array['data'] = [];
      while ($data = mysqli_fetch_assoc($result)) {
        $array['data'][] = $data;
      }

      echo json_encode($array);
    }

    mysqli_free_result($result);
    mysqli_close($con);
  }
?>

<?php
  include_once "../../../../config.php";

  if ($_GET['cotizacion']) {
    $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
    if (mysqli_connect_errno()) {
      echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
    }

    $cotizacion = $_GET['cotizacion'];

    $sql    = "SELECT concat('$ ', format(ifnull(sum(dc_subtotal) , 0), 2)) as subtotal,
                      concat('$ ', format(ifnull(sum(dc_iva), 0), 2)) as iva,
                      ( concat('$ ', cast(ifnull(sum(dc_total) , 0) as decimal(11, 2)) ) ) as total,
                      cot_status, cli_nombre, cli_telefono, cli_correo, cli_rfc, cli_direccion,
                      cot_envio, cot_zon_costo
	                  from cotizaciones, detallecotizacion, clientes
		                  where dc_cot_clave = cot_clave
                        and cot_clave = $cotizacion
                        and cot_cli_clave = cli_clave
		                    and dc_stauts = 1";
    //echo $sql;
    $result = mysqli_query($con, $sql);

    if (!$result) {
      die("error");
    }else {

      $array['data'] = [];
      while ($data = mysqli_fetch_assoc($result)) {
        $array['data'][] = $data;
      }

      echo json_encode($array);
    }

    mysqli_free_result($result);
    mysqli_close($con);
  }
?>

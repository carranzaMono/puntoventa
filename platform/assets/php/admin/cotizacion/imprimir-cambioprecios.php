<?php
  include_once "../../../../config.php";

  if (isset($_GET['cotizacion'])) {

    $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
      if (mysqli_connect_errno()) {
        echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
      }

    $cotizacion = $_GET['cotizacion'];
    $sql   = "SELECT pro_nombre,
                     concat('$ ',cast(pro_precio as decimal(11, 2))),
                     concat('$ ',cast(dc_precioUnitario as decimal(11, 2)))
                  from productos, detallecotizacion
                    where dc_pro_clave = pro_clave
                      and dc_cot_clave = $cotizacion
                      and pro_status = 1
                      and pro_precio <> dc_precioUnitario";

    $result = mysqli_query($con, $sql);
    if ($result) {
      while ($fila = mysqli_fetch_array($result)) {

        echo '<li class="list-group-item d-flex justify-content-between align-items-center">'.$fila[0].
                '<span class="badge text-dark badge-pill"> de: '.$fila[2].' a '.$fila[1].'</span>'.
             '</li>';

      }
    }

  }

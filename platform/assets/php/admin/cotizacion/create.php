<?php
  session_set_cookie_params(60*60*24*14);
  session_start();
  include_once "../../../../config.php";
  include_once "../../../funciones/create.php";
  include_once "../../../funciones/searches.php";
  include_once "../../../funciones/generales.php";

  if (isset($_POST['cliente'])) {

    $cli_clave  = $_POST['cliente'];
    $modulo     = "Cotización";
    $accion     = "Nueva cotización";
    $emp_clave  = $_SESSION['usu_emp_clave'];
    $tipo       = 3;//Cotización

    $cotizacion = cotizacionNormal($cli_clave, $emp_clave);
    if ($cotizacion == "sin_caja" || $cotizacion == "error_insert" || $cotizacion == "not_save") {
      $resultado = $cotización;
    }else {
      $descripcion = "Se creo la cotización ".$cotizacion;
      $bit_result  = bitacora($modulo, $accion, $tipo, $emp_clave, $descripcion);
      if ($bit_result) {
        $resultado = $cotizacion;
      }else {
        $resultado = $bit_result;
      }
    }

    echo json_encode($resultado);
  }
?>

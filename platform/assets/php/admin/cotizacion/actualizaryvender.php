<?php
include_once "../../../../config.php";

if (isset($_GET['cotizacion'])) {

  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $cotizacion = $_GET['cotizacion'];

  $sql    = "SELECT dc_clave, dc_cantidad, dc_pro_clave
                from detallecotizacion
                  where dc_cot_clave = $cotizacion
                    and dc_stauts = 1";
  $result = mysqli_query($con, $sql);
  if ($result) {
    while ($fila = mysqli_fetch_array($result)) {

      $dc_clave     = $fila[0];
      $dc_cantidad  = $fila[1];
      $dc_pro_clave = $fila[2];

      $sql    = "SELECT ifnull(pro_cantidadMinima, 0), pro_precio, pro_precioMayoreo)
                    from productos
                      where pro_clave = $dc_pro_clave";
      $result1 = mysqli_query($con, $sql);
      if ($result1) {
        while ($fila = mysqli_fetch_array($result1)) {

          $pro_precioUnitario = ($fila[0] > 0 && $dc_cantidad >= $fila[0]) ? $fila[2]:$fila[1];
          $dc_subtotal        = $pro_precioUnitario * $dc_cantidad;
          $dc_iva             = number_format($dc_subtotal * 0.16, 3);
          $dc_total           = $dc_subtotal + $dc_iva;

          mysqli_autocommit($con, FALSE);
          try {

            mysqli_begin_transaction($con, MYSQLI_TRANS_START_READ_WRITE);

            $sql    = "UPDATE detallecotizacion
                          set dc_precioUnitario = $pro_precioUnitario, dc_subtotal = $dc_subtotal,
                              dc_iva = $dc_iva, dc_total = $dc_total
                        where dc_clave = $dc_clave";
            $result2 = mysqli_query($con, $sql);
            if ($result2) {

              $sql    = "SELECT sum(dc_subtotal), sum(dc_iva), sum(dc_total)
                            from detallecotizacion
                              where dc_cot_clave = $cotizacion";
              $result3 = mysqli_query($con, $sql);
              if ($result3) {
                while ($fila = mysqli_fetch_array($result3)) {
                  $cot_subtotal = $fila[0];
                  $cot_iva      = $fila[1];
                  $cot_total    = $fila[2];
                }

                $sql     = "UPDATE cotizaciones
                               set cot_subtotal = $cot_subtotal, cot_iva = $cot_iva,
                                   cot_total = $cot_total
                             where cot_clave = $cotizacion";
                $result4 = mysqli_query($con, $sql);
                if ($result4) {
                  $valor = 1;
                }else {
                  mysqli_rollback($con);
                }

              }else {
                mysqli_rollback($con);
              }

            }else {
              mysqli_rollback($con);
            }

            mysqli_commit($con);

          } catch (\Exception $e) {
            mysqli_rollback($con);
          }

        }
      }

    }
  }

  echo json_encode($valor);
}

?>

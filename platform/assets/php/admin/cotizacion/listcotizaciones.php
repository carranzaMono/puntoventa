<?php
include_once "../../../../config.php";

$con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
if (mysqli_connect_errno()) {
  echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
}

$sql    = "SELECT cot_clave, cot_fecha, cot_folio,
                  concat('$ ', format(ifnull(cot_total, 0), 2)) as cot_total,
                  (select cli_nombre from clientes where cli_clave = cot_cli_clave) as cot_cliente
                from cotizaciones
                  where cot_status = 1
               order by cot_folio desc";
$result = mysqli_query($con, $sql);

if (!$result) {
  die("error");
}else {

  $array['data'] = [];
  while ($data = mysqli_fetch_assoc($result)) {
    $array['data'][] = $data;
  }

  echo json_encode($array);
}

mysqli_free_result($result);
mysqli_close($con);
?>

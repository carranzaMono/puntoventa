<?php
session_set_cookie_params(60*60*24*14);
session_start();
include_once "../../../../config.php";
include_once "../../../funciones/searches.php";
include_once "../../../funciones/create.php";
include_once "../../../funciones/update.php";
include_once "../../../funciones/generales.php";

if (isset($_GET['cotizacion'])) {

  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $cli_clave = $_POST['cliente'];
  $cot_clave = $_GET['cotizacion'];

  $cotizacion   = dameDatosCot($cot_clave);
  $cli_anterior = dameDatosCliente($cotizacion['cot_cli_clave']);
  $cli_nuevo    = dameDatosCliente($cli_clave);


  $modulo      = "Cotización";
  $accion      = "Actualización";
  $emp_clave   = $_SESSION['usu_emp_clave'];
  $valor       = "error_update";
  $tipo        = 1;//Catálogo
  $descripcion = "Se cambio el cliente ".$cli_anterior['cli_nombre']." por el cliente ".$cli_nuevo['cli_nombre'].
                 " en la cotización ". $cotizacion['cot_folio'];

  mysqli_autocommit($con, FALSE);
  try {

    mysqli_begin_transaction($con, MYSQLI_TRANS_START_READ_WRITE);
    $sql    = "UPDATE cotizaciones set cot_cli_clave = $cli_clave where cot_clave = $cot_clave";
    $result = mysqli_query($con, $sql);
    if ($result) {

      $bit_result  = bitacora($modulo, $accion, $tipo, $emp_clave, $descripcion);
      if ($bit_result == 1) {
        $valor = "update";
      }else {
        mysqli_rollback($con);
      }

    }else {
      mysqli_rollback($con);
    }

    mysqli_commit($con);

  } catch (\Exception $e) {
    mysqli_rollback($con);
  }

  echo json_encode($valor);
}
?>

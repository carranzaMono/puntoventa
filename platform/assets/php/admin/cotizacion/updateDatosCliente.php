<?php
session_set_cookie_params(60*60*24*14);
session_start();
include_once "../../../../config.php";
include_once "../../../funciones/searches.php";
include_once "../../../funciones/create.php";
include_once "../../../funciones/update.php";
include_once "../../../funciones/generales.php";

if (isset($_POST['cliente'])) {

  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $cli_clave     = $_POST['cliente'];
  $cli_nombre    = trim($_POST['nombre']);
  $cli_rfc       = isset($_POST['rfc'])?$_POST['rfc']:"null";
  $cli_telefono  = isset($_POST['telefono'])?$_POST['telefono']:"null";
  $cli_direccion = isset($_POST['direccion'])?$_POST['direccion']:"null";
  $cli_correo    = isset($_POST['correo'])? trim(strtolower($_POST['correo'])):"null";
  $status        = 1;

  $modulo      = "Catalogos";
  $accion      = "Actualización";
  $emp_clave   = $_SESSION['usu_emp_clave'];
  $valor       = "error_update";
  $tipo        = 1;//Catálogo
  $descripcion = "Se modificaron datos del cliente con registro $cli_clave";

  mysqli_autocommit($con, FALSE);
  try {

    mysqli_begin_transaction($con, MYSQLI_TRANS_START_READ_WRITE);
    $sql    = "UPDATE clientes
                  set cli_nombre = '$cli_nombre', cli_telefono = '$cli_telefono',
                      cli_correo = '$cli_correo', cli_direccion = '$cli_direccion',
                      cli_rfc = '$cli_rfc'
                where cli_clave = $cli_clave";
    $result = mysqli_query($con, $sql);
    if ($result) {

      $bit_result  = bitacora($modulo, $accion, $tipo, $emp_clave, $descripcion);
      if ($bit_result == 1) {
        $valor = "update";
      }else {
        mysqli_rollback($con);
      }

    }else {
      mysqli_rollback($con);
    }

    mysqli_commit($con);

  } catch (\Exception $e) {
    mysqli_rollback($con);
  }

  echo json_encode($valor);
}
?>

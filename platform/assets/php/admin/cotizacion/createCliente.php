<?php
  session_set_cookie_params(60*60*24*14);
  session_start();
  include_once "../../../../config.php";
  include_once "../../../funciones/searches.php";
  include_once "../../../funciones/create.php";
  include_once "../../../funciones/update.php";
  include_once "../../../funciones/generales.php";

  if (isset($_GET['cotizacion'])) {
    $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
    if (mysqli_connect_errno()) {
      echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
    }

    $cot_clave     = $_GET['cotizacion'];
    $cli_nombre    = trim($_POST['nombre']);
    $cli_rfc       = isset($_POST['rfc'])?$_POST['rfc']:"null";
    $cli_telefono  = isset($_POST['telefono'])?$_POST['telefono']:"null";
    $cli_direccion = isset($_POST['direccion'])?$_POST['direccion']:"null";
    $cli_correo    = isset($_POST['correo'])? trim(strtolower($_POST['correo'])):"null";
    $status        = 1;

    $buscarCliente = searchNameCliente($cli_nombre);
    $cotizacion    = dameDatosCot($cot_clave);

    $modulo      = "Cotización";
    $accion      = "Registro de cliente";
    $emp_clave   = $_SESSION['usu_emp_clave'];
    $descripcion = "Se registro el cliente $cli_nombre para la cotización ".$cotizacion['cot_folio'];
    $tipo        = 1;//Catálogo

    switch ($buscarCliente['cli_count']) {
      case 0:

        mysqli_autocommit($con, FALSE);
        try {

          $valor = "error_save";
          mysqli_begin_transaction($con, MYSQLI_TRANS_START_READ_WRITE);
          $sql    = "INSERT into clientes(cli_nombre, cli_telefono, cli_correo, cli_direccion, cli_status, cli_rfc)
                                   values('$cli_nombre', '$cli_telefono', '$cli_correo', '$cli_direccion', $status, '$cli_rfc')";
          $result = mysqli_query($con, $sql);
          if ($result) {

            $sql    = "SELECT cli_clave from clientes where cli_status = 1 order by cli_clave desc limit 1";
            $result = mysqli_query($con, $sql);
            if ($result) {

              while ($fila = mysqli_fetch_array($result)) {
                $cli_clave = $fila[0];
              }

              $sql    = "UPDATE cotizaciones set cot_cli_clave = $cli_clave where cot_clave = $cot_clave";
              $result = mysqli_query($con, $sql);
              if ($result) {

                $bit_result  = bitacora($modulo, $accion, $tipo, $emp_clave, $descripcion);
                if ($bit_result == 1) {
                  $valor = "update";
                }else {
                  mysqli_rollback($con);
                }

              }else {
                mysqli_rollback($con);
              }

            }else {
              mysqli_rollback($con);
            }

          }else {
            mysqli_rollback($con);
          }

          mysqli_commit($con);

        } catch (\Exception $e) {
          mysqli_rollback($con);
        }

        break;

      default:
        if ($buscarCliente['cli_count'] >= 1 && $buscarCliente['cli_status'] == 1) {
          $valor = "cli_exist";
        }
        break;
    }


    echo json_encode($valor);

  }
?>

<?php
  include_once "../../../../../config.php";
  include_once "../../../../funciones/searches.php";
  include_once "../../../../funciones/create.php";
  include_once "../../../../funciones/update.php";
  include_once "../../../../funciones/generales.php";

  if (isset($_POST['nombre'])) {

    $pue_nombre  = ucfirst($_POST['nombre']);
    $modulo      = "Catálogo";
    $accion      = "Registro";
    $empleado    = 1;
    $tipo        = 1;//Catálogo
    $description = "Se registro el puesto $pue_nombre";

    $search    = searchPuestoNombre($pue_nombre);
    $resultado = "error_search";

    switch ($search['pue_count']) {
      case 0:
        $resultado = insertPuesto($pue_nombre);
        break;
      default:
        $resultado = 2;
        if ($search['pue_count'] > 0 && $search['pue_status'] == 0) {
          $resultado = updateStatusPuesto($search['pue_clave']);
        }
        break;
    }

    if ($resultado == 1) {
      $resultado = bitacora($modulo, $accion, $tipo, $empleado, $description);
      //$modulo, $accion, $tipo, $empleado, $description, $clave
    }

    echo json_encode($resultado);
  }
?>

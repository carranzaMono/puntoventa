<?php
include_once "../../../../../config.php";
include_once "../../../../funciones/searches.php";
include_once "../../../../funciones/create.php";
include_once "../../../../funciones/update.php";
include_once "../../../../funciones/generales.php";

if (isset($_POST['clave'])) {

  $pue_clave   = $_POST['clave'];
  $modulo      = "Catálogo";
  $accion      = "Eliminar";
  $empleado    = 1;
  $tipo        = 1;//Catálogo
  $puesto      = dameDatosPuesto($pue_clave);
  $description = "Se elimino el puesto ".$puesto['pue_nombre'];

  $resultado = updateStatusPuesto($pue_clave);
  if ($resultado == 1) {
    $resultado = bitacora($modulo, $accion, $tipo, $empleado, $description);
  }

  echo json_encode($resultado);
}

?>

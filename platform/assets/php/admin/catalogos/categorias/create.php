<?php
  include_once "../../../../../config.php";
  include_once "../../../../funciones/searches.php";
  include_once "../../../../funciones/create.php";
  include_once "../../../../funciones/update.php";
  include_once "../../../../funciones/generales.php";

  if (isset($_POST['nombre'])) {

    $cat_nombre  = ucfirst($_POST['nombre']);
    $modulo      = "Catálogo";
    $accion      = "Registro";
    $empleado    = 1;
    $tipo        = 1;//Catálogo
    $description = "Se registro la categoria $cat_nombre";

    $search    = searchCategoriaNombre($cat_nombre);
    $resultado = "error_search";

    switch ($search['cat_count']) {
      case 0:
        $resultado = insertCategoria($cat_nombre);
        break;
      default:
        $resultado = 2;
        if ($search['cat_count'] > 0 && $search['cat_status'] == 0) {
          $resultado = updateStatusCategoria($search['cat_clave']);
        }
        break;
    }

    if ($resultado == 1) {
      $resultado = bitacora($modulo, $accion, $tipo, $empleado, $description);
      //$modulo, $accion, $tipo, $empleado, $description, $clave
    }

    echo json_encode($resultado);
  }
?>

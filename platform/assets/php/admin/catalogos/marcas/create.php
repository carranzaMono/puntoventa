<?php
  include_once "../../../../../config.php";
  include_once "../../../../funciones/searches.php";
  include_once "../../../../funciones/create.php";
  include_once "../../../../funciones/update.php";
  include_once "../../../../funciones/generales.php";

  if (isset($_POST['nombre'])) {

    $mar_nombre  = ucfirst($_POST['nombre']);
    $modulo      = "Catálogo";
    $accion      = "Registro";
    $empleado    = 1;
    $tipo        = 1;//Catálogo
    $description = "Se registro la marca $mar_nombre";

    $search    = searchMarcaNombre($mar_nombre);
    $resultado = "error_search";

    switch ($search['mar_count']) {
      case 0:
        $resultado = insertMarcas($mar_nombre);
        break;
      default:
        $resultado = 2;
        if ($search['mar_count'] > 0 && $search['mar_status'] == 0) {
          $resultado = updateStatusMarca($search['mar_clave']);
        }
        break;
    }

    if ($resultado == 1) {
      $resultado = bitacora($modulo, $accion, $tipo, $empleado, $description);
      //$modulo, $accion, $tipo, $empleado, $description, $clave
    }

    echo json_encode($resultado);
  }
?>

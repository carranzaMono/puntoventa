<?php
include_once "../../../../../config.php";
include_once "../../../../funciones/searches.php";
include_once "../../../../funciones/create.php";
include_once "../../../../funciones/update.php";
include_once "../../../../funciones/generales.php";

if (isset($_POST['nombre'])) {

  $mar_nombre  = ucfirst($_POST['nombre']);
  $mar_clave   = $_POST['clave'] ;
  $marca       = dameDatosMarca($mar_clave);
  $modulo      = "Catálogo";
  $accion      = "Actualización";
  $empleado    = 1;
  $tipo        = 1;//Catálogo
  $description = "Se modifico la marca ". $marca['mar_nombre']." por $mar_nombre";

  $search    = searchMarcaNombre($mar_nombre);
  $resultado = "error_search";

  switch ($search['mar_count']) {
    case 0:
      $resultado = updateNombreMarca($mar_clave, $mar_nombre);
      break;
    default:
    $resultado = 2;
    if ($search['mar_count'] > 0 && $search['mar_status'] == 0) {
      $resultado = updateStatusMarca($marclave);
    }
      break;
  }

  if ($resultado == 1) {
    $resultado = bitacora($modulo, $accion, $tipo, $empleado, $description);
    //$modulo, $accion, $tipo, $empleado, $description, $clave
  }

  echo json_encode($resultado);
}
?>

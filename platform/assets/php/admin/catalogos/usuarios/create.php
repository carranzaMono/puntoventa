<?php
  include_once "../../../../../config.php";
  include_once "../../../../funciones/searches.php";
  include_once "../../../../funciones/create.php";
  include_once "../../../../funciones/update.php";
  include_once "../../../../funciones/generales.php";

  if (isset($_POST['username'])) {

    $user_name     = strtolower($_POST['username']);
    $user_password = strtolower($_POST['password']);
    $emp_clave     = $_POST['empleado'];
    $user_tipo     = $_POST['tipoUser'];

    $empleado      = dameDatosEmpleados($emp_clave);

    $modulo      = "Catálogo";
    $accion      = "Registro";
    $empleados   = 1;
    $tipo        = 1;//Catálogo
    $description = "Se registro el usuario $user_name para el empleado ".$empleado['emp_nombreCompleto'];

    $search    = searchUserName($user_name);
    $resultado = "error_search";

    //echo $search['usu_count'];

    switch ($search['usu_count']) {
      case 0:
        $resultado = insertUsuario($user_name, $user_password, $emp_clave, $user_tipo);
        break;
      default:
        $resultado = 2;
        if ($search['usu_count'] > 0 && ($search['usu_status'] == 0 || $search['usu_status'] == 2)) {
          $resultado = updateStatusUser($search['usu_clave'], 1);
        }
        break;
    }

    if ($resultado == 1) {
      $resultado = bitacora($modulo, $accion, $tipo, $empleados, $description);
      //$modulo, $accion, $tipo, $empleado, $description, $clave
    }

    echo json_encode($resultado);
  }
?>

<?php
  include_once "../../../../../config.php";

  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $sql    = "SELECT usu_clave, usu_nombre, usu_password, usu_tipo,
                    case
                      when usu_tipo = 1 then 'Administrador'
                      when usu_tipo = 2 then 'Almacenista'
                      when usu_tipo = 3 then 'Cajero'
                      else 'Vendedor'
                    end as tipo_name,
                    case
                      when usu_status = 1 then 'Activo'
                      else 'Suspendido'
                    end as status_name,
                    (SELECT emp_nombreCompleto from empleados where usu_emp_clave = emp_clave) as usu_emp_nombre
                  from usuarios
                      where usu_status = 1
                         or usu_status = 2
                      order by usu_nombre asc";
  $result = mysqli_query($con, $sql);

  if (!$result) {
    die("error");
  }else {

    $array['data'] = [];
    while ($data = mysqli_fetch_assoc($result)) {
      $array['data'][] = $data;
    }

    echo json_encode($array);
  }

  mysqli_free_result($result);
  mysqli_close($con);

?>

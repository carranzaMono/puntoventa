<?php
include_once "../../../../../config.php";
include_once "../../../../funciones/searches.php";
include_once "../../../../funciones/create.php";
include_once "../../../../funciones/update.php";
include_once "../../../../funciones/generales.php";

//echo "string2";
if (isset($_POST['username']) && isset($_GET['user'])) {

  $usu_clave     = $_GET['user'];
  $user_name     = strtolower($_POST['username']);
  $user_tipo     = $_POST['tipoUser'];

  $usuario     = dameDatosUser($usu_clave);
  $modulo      = "Catálogo";
  $accion      = "Actualización";
  $empleado    = 1;
  $tipo        = 1;//Catálogo
  $description = "Se modifico información del usuario ".$usuario['usu_nombre'];

  $resultado = updateUser($usu_clave, $user_name, $user_tipo);

  if ($resultado == 1) {
    $resultado = bitacora($modulo, $accion, $tipo, $empleado, $description);
    //$modulo, $accion, $tipo, $empleado, $description, $clave
  }

  echo json_encode($resultado);
}

if (isset($_POST['password']) && isset($_GET['user'])) {

  $usu_clave     = $_GET['user'];
  $user_password = strtolower($_POST['password']);

  $usuario       = dameDatosUser($usu_clave);
  $modulo        = "Catálogo";
  $accion        = "Actualización";
  $empleado      = 1;
  $tipo          = 1;//Catálogo

  $description   = "Se modifico el password del usuario ".$usuario['usu_nombre'];

  $resultado = updateUserPass($usu_clave, $user_password);

  if ($resultado == 1) {
    $resultado = bitacora($modulo, $accion, $tipo, $empleado, $description);
    //$modulo, $accion, $tipo, $empleado, $description, $clave
  }

  echo json_encode($resultado);
}
?>

<?php
include_once "../../../../../config.php";
include_once "../../../../funciones/searches.php";
include_once "../../../../funciones/create.php";
include_once "../../../../funciones/update.php";
include_once "../../../../funciones/generales.php";

if (isset($_POST['clave'])) {

  $user_clave  = $_POST['clave'];
  $modulo      = "Catálogo";
  $accion      = "Eliminar";
  $empleado    = 1;
  $tipo        = 1;//Catálogo
  $usuario     = dameDatosUser($user_clave);
  $description = "Se elimino al usuario ".$usuario['usu_nombre'];

  $resultado = updateStatusUser($user_clave, 0);
  if ($resultado == 1) {
    $resultado = bitacora($modulo, $accion, $tipo, $empleado, $description);
  }

  echo json_encode($resultado);
}

?>

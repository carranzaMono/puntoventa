<?php
include_once "../../../../../config.php";
include_once "../../../../funciones/searches.php";
include_once "../../../../funciones/create.php";
include_once "../../../../funciones/update.php";
include_once "../../../../funciones/generales.php";

if (isset($_POST['clave'])) {

  $emp_clave   = $_POST['clave'];
  $modulo      = "Catálogo";
  $accion      = "Eliminar";
  $empleados   = 1;
  $tipo        = 1;//Catálogo
  $empleado    = dameDatosEmpleados($emp_clave);
  $description = "Se elimino el empleado ".$empleado['emp_nombreCompleto'];

  $resultado = updateStatusEmpleados($emp_clave);
  if ($resultado == 1) {
    $resultado = bitacora($modulo, $accion, $tipo, $empleados, $description);
  }

  echo json_encode($resultado);
}

?>

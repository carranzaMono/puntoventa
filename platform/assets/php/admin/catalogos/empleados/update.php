<?php
include_once "../../../../../config.php";
include_once "../../../../funciones/searches.php";
include_once "../../../../funciones/create.php";
include_once "../../../../funciones/update.php";
include_once "../../../../funciones/generales.php";

if (isset($_POST['nombre'])) {

  $emp_clave      = $_POST['clave'] ;
  $emp_nombre     = ucfirst($_POST['nombre']);
  $emp_apPaterno  = ucfirst($_POST['ap-paterno']);
  $emp_apMaterno  = ucfirst($_POST['ap-materno']);
  $nombreCompleto = trim($emp_nombre.' '.$emp_apPaterno.' '.$emp_apMaterno);
  $puesto         = $_POST['puesto'];
  $emp_telefono   = $_POST['telef']?$_POST['telef']:"null";
  $emp_correo     = $_POST['correo']?$_POST['correo']:"null";
  $emp_curp       = $_POST['curp']?$_POST['curp']:"null";
  $emp_rfc        = $_POST['rfc']?$_POST['rfc']:"null";
  $emp_direccion  = $_POST['direccion']?$_POST['direccion']:"null";

  $empleado    = dameDatosEmpleados($emp_clave);
  $modulo      = "Catálogo";
  $accion      = "Actualización";
  $empleados   = 1;
  $tipo        = 1;//Catálogo
  $description = "Se modifico información del empleado ". $empleado['emp_nombreCompleto'];

  //echo $emp_telefono;
  $resultado = "error_search";
  $resultado = updateEmpleado($emp_nombre, $emp_apPaterno, $emp_apMaterno, $nombreCompleto,
                               $puesto, $emp_direccion, $emp_telefono, $emp_correo, $emp_curp,
                               $emp_rfc, $emp_clave);

  if ($resultado == 1) {
    $resultado = bitacora($modulo, $accion, $tipo, $empleados, $description);
    //$modulo, $accion, $tipo, $empleado, $description, $clave
  }

  echo json_encode($resultado);
}
?>

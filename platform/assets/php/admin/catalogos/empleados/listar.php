<?php
  include_once "../../../../../config.php";

  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $sql    = "SELECT emp_clave, emp_nombre, emp_apPaterno, emp_apMaterno, emp_direccion,
                    emp_telefono, emp_curp, emp_rfc, emp_correo, emp_pue_clave,
                    (select pue_nombre from puestos where emp_pue_clave = pue_clave) as pue_nombre,
                    emp_nombreCompleto
                  from empleados
                      where emp_status = 1
                      order by emp_nombreCompleto asc";
  $result = mysqli_query($con, $sql);

  if (!$result) {
    die("error");
  }else {

    $array['data'] = [];
    while ($data = mysqli_fetch_assoc($result)) {
      $array['data'][] = $data;
    }

    echo json_encode($array);
  }

  mysqli_free_result($result);
  mysqli_close($con);

?>

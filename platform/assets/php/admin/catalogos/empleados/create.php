<?php
  session_set_cookie_params(60*60*24*14);
  session_start();
  include_once "../../../../../config.php";
  include_once "../../../../funciones/searches.php";
  include_once "../../../../funciones/create.php";
  include_once "../../../../funciones/update.php";
  include_once "../../../../funciones/generales.php";

  if (isset($_POST['nombre'])) {

    $emp_nombre     = ucfirst($_POST['nombre']);
    $emp_apPaterno  = ucfirst($_POST['ap-paterno']);
    $emp_apMaterno  = ucfirst($_POST['ap-materno']);
    $nombreCompleto = trim($emp_nombre.' '.$emp_apPaterno.' '.$emp_apMaterno);
    $puesto         = $_POST['puesto'];
    $emp_telefono   = is_null($_POST['telef'])?$_POST['telef']:"null";
    $emp_correo     = is_null($_POST['correo'])?$_POST['correo']:"null";
    $emp_curp       = is_null($_POST['curp'])?$_POST['curp']:"null";
    $emp_rfc        = is_null($_POST['rfc'])?$_POST['rfc']:"null";
    $emp_direccion  = is_null($_POST['direccion'])?$_POST['direccion']:"null";


    $modulo      = "Catálogo";
    $accion      = "Registro";
    $empleado    = $_SESSION['usu_emp_clave'];
    $tipo        = 1;//Catálogo
    $description = "Se registro el empleado $nombreCompleto";

    $search    = searchNameEmpleado($nombreCompleto);
    $resultado = "error_search";

    switch ($search['emp_count']) {
      case 0:
        $resultado = insertEmpleados($emp_nombre, $emp_apPaterno, $emp_apMaterno, $nombreCompleto,
                                     $puesto, $emp_direccion, $emp_telefono, $emp_correo, $emp_curp,
                                     $emp_rfc);
        break;
      default:
        $resultado = 2;
        if ($search['emp_count'] > 0 && $search['emp_status'] == 0) {
          $resultado = updateStatusEmpleados($search['emp_clave']);
        }
        break;
    }

    if ($resultado == 1) {
      $resultado = bitacora($modulo, $accion, $tipo, $empleado, $description);
      //$modulo, $accion, $tipo, $empleado, $description, $clave
    }

    echo json_encode($resultado);
  }
?>

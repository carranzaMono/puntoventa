<?php
include_once "../../../../../config.php";
include_once "../../../../funciones/searches.php";
include_once "../../../../funciones/create.php";
include_once "../../../../funciones/update.php";
include_once "../../../../funciones/generales.php";

if (isset($_POST['nombre'])) {

  $pro_clave      = $_POST['clave'];
  $nombre         = ucfirst($_POST['nombre']);
  $inventariable  = $_POST['inventariable'];
  $unidad         = $_POST['unidad'];
  $precio         = $_POST['precio'];
  $medida         = isset($_POST['medida'])?$_POST['medida']:"null";
  $presentacion   = isset($_POST['presentacion'])?$_POST['presentacion']:"null";

  $producto    = dameDatosProducto($pro_clave);
  $modulo      = "Catálogo";
  $accion      = "Actualización";
  $empleados   = 1;
  $tipo        = 1;//Catálogo
  $description = "Se modifico información del producto ". $producto['pro_nombre'];

  $resultado = updateProducto($pro_clave, $nombre, $inventariable, $unidad, $precio,
                              $medida, $presentacion);

  if ($resultado == 1) {
    $resultado = bitacora($modulo, $accion, $tipo, $empleados, $description);
    //$modulo, $accion, $tipo, $empleado, $description, $clave
  }

  echo json_encode($resultado);
}
?>

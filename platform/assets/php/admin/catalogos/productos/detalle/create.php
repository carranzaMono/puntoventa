<?php
  include_once "../../../../../../config.php";
  include_once "../../../../../funciones/searches.php";
  include_once "../../../../../funciones/create.php";
  include_once "../../../../../funciones/update.php";
  include_once "../../../../../funciones/generales.php";

  if (isset($_POST['categoria'])) {

    $cat_clave     = $_POST['categoria'];
    $pro_clave     = $_GET['clave'];
    $categoria     = dameDatosCategorias($cat_clave);
    $producto      = dameDatosProducto($pro_clave);
    $unidadmedida  = dameUnidadMedida($producto['pro_unidad']);
    $presentacion  = damePresentacion($producto['pro_presentacion']);
    $pro_nombre    = trim("$presentacion ".$producto['pro_medida']." $unidadmedida - ".$producto['pro_nombre']);

    $modulo      = "Catálogo";
    $accion      = "Registro";
    $empleado    = 1;
    $tipo        = 1;//Catálogo
    $description = "Se asigno la categoria ".$categoria['cat_nombre']." al producto $pro_nombre";

    $search     = searchCategoriaProducto($pro_clave, $cat_clave);
    $resultado  = "error_search";

    switch ($search['pc_count']) {
      case 0:
        $sql = insertcategoriaproducto($pro_clave, $cat_clave);
        break;
      default:
        $resultado = 2;
        if ($search['pc_count'] > 0 && $search['pc_status'] == 0) {
          $sql = updateStatusCategoriaProducto($search['pc_clave']);
        }
        break;
    }

    if ($sql == 1) {
      $sqlbit = bitacora($modulo, $accion, $tipo, $empleado, $description);
      if ($sqlbit == 1) {
        $resultado = $sqlbit;
      }
    }

    echo json_encode($resultado);
  }
?>

<?php
  include_once "../../../../../../config.php";

if (isset($_GET['clave'])) {
  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $producto = $_GET['clave'];

  $sql    = "SELECT pc_clave, cat_nombre
                from productos_categorias, categorias
                  where pc_cat_clave = cat_clave
                    and pc_pro_clave = $producto
                    and pc_status = 1 order by cat_nombre asc";
  $result = mysqli_query($con, $sql);

  if (!$result) {
    die("error");
  }else {

    $array['data'] = [];
    while ($data = mysqli_fetch_assoc($result)) {
      $array['data'][] = $data;
    }

    echo json_encode($array);
  }

  mysqli_free_result($result);
  mysqli_close($con);
}

?>

<?php
  include_once "../../../../../config.php";

  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $sql    = "SELECT pro_clave, pro_nombre, pro_descripcion, pro_inventariable, pro_presentacion,
                    pro_unidad, pro_medida, pro_cantidadMinima, pro_precioMayoreo, pro_precio,
                    concat('$ ',format(pro_precio,2)) as pro_precioFormat, su_nombre, pre_nombre,
                    case
                      when pro_inventariable = 1 then 'Si'
                      else 'No'
                    end pro_resultinventaraible
                  from productos, sistemaunidades, presentaciones
                      where pro_status = 1
                        and pro_presentacion = pre_clave
                        and pro_unidad = su_clave
                      order by pro_nombre asc";

  $result = mysqli_query($con, $sql);

  if (!$result) {
    die("error");
  }else {

    $array['data'] = [];
    while ($data = mysqli_fetch_assoc($result)) {
      $array['data'][] = $data;
    }

    echo json_encode($array);
  }

  mysqli_free_result($result);
  mysqli_close($con);
?>

<?php
include_once "../../../../../config.php";
include_once "../../../../funciones/searches.php";
include_once "../../../../funciones/create.php";
include_once "../../../../funciones/update.php";
include_once "../../../../funciones/generales.php";

//echo "string";
if (isset($_POST['clave'])) {

  $pro_clave        = $_POST['clave'];
  $modulo           = "Catálogo";
  $accion           = "Eliminar";
  $empleado         = 1;
  $tipo             = 1;//Catálogo
  $producto         = dameDatosProducto($pro_clave);
  $unidadmedida     = dameUnidadMedida($producto['pro_unidad']);
  $presentacion     = damePresentacion($producto['pro_presentacion']);

  $pro_nombre       = trim("$presentacion ".$producto['pro_medida']." $unidadmedida - ".$producto['pro_nombre']);
  $description      = "Se elimino el producto $pro_nombre";

  $resultado = updateStatusproducto($pro_clave);
  if ($resultado == 1) {
    $resultado = bitacora($modulo, $accion, $tipo, $empleado, $description);
  }

  echo json_encode($resultado);
}

?>

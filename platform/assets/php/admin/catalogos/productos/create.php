<?php
  include_once "../../../../../config.php";
  include_once "../../../../funciones/searches.php";
  include_once "../../../../funciones/create.php";
  include_once "../../../../funciones/update.php";
  include_once "../../../../funciones/generales.php";

  if (isset($_POST['nombre'])) {

    $nombre         = ucfirst($_POST['nombre']);
    $inventariable  = $_POST['inventariable'];
    $unidad         = $_POST['unidad'];
    $precio         = $_POST['precio'];
    $medida         = isset($_POST['medida'])?$_POST['medida']:"null";
    $presentacion   = isset($_POST['presentacion'])?$_POST['presentacion']:"null";

    $modulo      = "Catálogo";
    $accion      = "Registro";
    $empleado    = 1;
    $tipo        = 1;//Catálogo
    $description = "Se registro el producto $nombre";

    $search     = searchNameproducto($nombre);
    $resultado  = "error_search";

    switch ($search['pro_count']) {
      case 0:
        $sql          = insertproducto($nombre, $inventariable, $unidad, $precio, $medida, $presentacion);
        $searchUltimo = ultimoRegistroProducto();
        break;
      default:
        $resultado = 'ya_existe';
        if ($search['pro_count'] > 0 && $search['pro_status'] == 0) {
          $sql = updateStatusproducto($search['pro_clave']);
          $searchUltimo = $search['pro_clave'];
        }
        break;
    }

    if ($sql == 1) {
      $sqlbit = bitacora($modulo, $accion, $tipo, $empleado, $description);
      if ($sqlbit == 1) {
        $resultado = $searchUltimo;
      }
    }

    echo json_encode($resultado);
  }
?>

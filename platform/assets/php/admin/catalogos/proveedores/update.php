<?php
include_once "../../../../../config.php";
include_once "../../../../funciones/searches.php";
include_once "../../../../funciones/create.php";
include_once "../../../../funciones/update.php";
include_once "../../../../funciones/generales.php";

if (isset($_POST['nombre'])) {

  $prov_clave     = $_POST['clave'] ;
  $prov_nombre    = ucfirst($_POST['nombre']);
  $prov_apPaterno = ucfirst($_POST['ap-paterno']);
  $prov_apMaterno = ucfirst($_POST['ap-materno']);
  $nombreCompleto = trim($prov_nombre.' '.$prov_apPaterno.' '.$prov_apMaterno);
  $prov_telefono  = isset($_POST['telef'])?$_POST['telef']:"null";
  $prov_correo    = isset($_POST['correo'])?$_POST['correo']:"null";

  $proveedor   = dameDatosProveedor($prov_clave);
  $modulo      = "Catálogo";
  $accion      = "Actualización";
  $empleados   = 1;
  $tipo        = 1;//Catálogo
  $description = "Se modifico información del proveedor ". $proveedor['prov_nombrecompleto'];

  //echo $emp_telefono;
  $resultado = "error_search";
  $resultado = updateProveedor($prov_nombre, $prov_apPaterno, $prov_apMaterno, $nombreCompleto,
                               $prov_telefono, $prov_correo, $prov_clave);

  if ($resultado == 1) {
    $resultado = bitacora($modulo, $accion, $tipo, $empleados, $description);
    //$modulo, $accion, $tipo, $empleado, $description, $clave
  }

  echo json_encode($resultado);
}
?>

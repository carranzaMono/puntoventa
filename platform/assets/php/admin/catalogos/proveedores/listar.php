<?php
  include_once "../../../../../config.php";

  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $sql    = "SELECT prov_clave, prov_nombre, prov_appaterno, prov_apmaterno, prov_nombrecompleto,
                    prov_correo, prov_telefono, prov_status
                  from proveedores
                      where prov_status = 1
                      order by prov_nombrecompleto asc";
  $result = mysqli_query($con, $sql);

  if (!$result) {
    die("error");
  }else {

    $array['data'] = [];
    while ($data = mysqli_fetch_assoc($result)) {
      $array['data'][] = $data;
    }

    echo json_encode($array);
  }

  mysqli_free_result($result);
  mysqli_close($con);

?>

<?php
include_once "../../../../../config.php";
include_once "../../../../funciones/searches.php";
include_once "../../../../funciones/update.php";
include_once "../../../../funciones/generales.php";

if (isset($_POST['clave'])) {

  $prov_clave  = $_POST['clave'];
  $modulo      = "Catálogo";
  $accion      = "Eliminar";
  $empleado    = 1;
  $tipo        = 1;//Catálogo
  $proveedor   = dameDatosProveedor($prov_clave);
  $description = "Se elimino el proveedor ".$proveedor['prov_nombrecompleto'];

  $resultado = updateStatusProveedor($prov_clave);
  if ($resultado == 1) {
    $resultado = bitacora($modulo, $accion, $tipo, $empleado, $description);
  }

  echo json_encode($resultado);
}

?>

<?php
  include_once "../../../../../../config.php";

if (isset($_GET['clave'])) {
  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $proveedor = $_GET['clave'];

  $sql    = "SELECT pp_clave, pro_nombre
                from proveedores_productos, productos
                  where pp_status = 1
                    and pp_prov_clave = $proveedor
                    and pp_pro_clave = pro_clave order by pro_nombre asc";
  $result = mysqli_query($con, $sql);

  if (!$result) {
    die("error");
  }else {

    $array['data'] = [];
    while ($data = mysqli_fetch_assoc($result)) {
      $array['data'][] = $data;
    }

    echo json_encode($array);
  }

  mysqli_free_result($result);
  mysqli_close($con);
}

?>

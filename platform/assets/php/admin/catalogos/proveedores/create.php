<?php
  include_once "../../../../../config.php";
  include_once "../../../../funciones/searches.php";
  include_once "../../../../funciones/create.php";
  include_once "../../../../funciones/update.php";
  include_once "../../../../funciones/generales.php";

  if (isset($_POST['nombre'])) {

    $prov_nombre     = ucfirst($_POST['nombre']);
    $prov_apPaterno  = ucfirst($_POST['ap-paterno']);
    $prov_apMaterno  = ucfirst($_POST['ap-materno']);
    $nombreCompleto  = trim($prov_nombre.' '.$prov_apPaterno.' '.$prov_apMaterno);
    $prov_telefono   = isset($_POST['telef'])?$_POST['telef']:"null";
    $prov_correo     = isset($_POST['correo'])?$_POST['correo']:"null";


    $modulo      = "Catálogo";
    $accion      = "Registro";
    $empleado    = 1;
    $tipo        = 1;//Catálogo
    $description = "Se registro el proveedor $nombreCompleto";

    $search       = searchNombreProveedor($nombreCompleto);
    $searchUltimo = "error_search";

    switch ($search['prov_count']) {
      case 0:
        $sql          = insertProveedor($prov_nombre, $prov_apPaterno, $prov_apMaterno, $nombreCompleto,
                                        $prov_telefono, $prov_correo);
        $searchUltimo = ultimoRegsitroProveedores();
        break;
      default:
        $resultado = 'ya_existe';
        if ($search['prov_count'] > 0 && $search['prov_status'] == 0) {
          $sql          = updateStatusProveedor($search['prov_clave']);
          $searchUltimo = $search['prov_clave'];
        }
        break;
    }

    if ($sql == 1) {
      $sqlbit = bitacora($modulo, $accion, $tipo, $empleado, $description);
      if ($sqlbit == 1) {
        $resultado = $searchUltimo;
      }
    }

    echo json_encode($resultado);
  }
?>

<?php
  session_set_cookie_params(60*60*24*14);
  session_start();
  include_once "../../../../../config.php";
  include_once "../../../../funciones/searches.php";
  include_once "../../../../funciones/create.php";
  include_once "../../../../funciones/update.php";
  include_once "../../../../funciones/generales.php";

  if (isset($_POST['nombre'])) {

    $nombre    = trim($_POST['nombre']);
    $correo    = trim(strtolower($_POST['correo']));
    $direccion = isset($_POST['direccion'])?$_POST['direccion']:"null";
    $telefono  = isset($_POST['telefono'])?$_POST['telefono']:"null";

    $modulo      = "Cotización";
    $accion      = "Nueva cotización";
    $emp_clave   = $_SESSION['usu_emp_clave'];
    $tipo        = 1;//Catálogo

    $cotizacion = cotizacioncli($nombre, $correo, $telefono, $direccion);

    if ($cotizacion == "error_insert" || $cotizacion == "not_save" ||
        $cotizacion == "sin_caja" || $cotizacion == "cli_exist") {

          $resultado = $cotizacion;

    }else {

      $descripcion = "Se creo cotización con folio ".$cotizacion." para el cliente $nombre";
      $bit_result  = bitacora($modulo, $accion, $tipo, $emp_clave, $descripcion);
      if ($bit_result == 1) {
        $resultado = $cotizacion;
      }else {
        $resultado = $bit_result;
      }

    }

    echo json_encode($resultado);

  }
?>

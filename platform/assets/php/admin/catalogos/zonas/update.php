<?php
include_once "../../../../../config.php";
include_once "../../../../funciones/searches.php";
include_once "../../../../funciones/create.php";
include_once "../../../../funciones/update.php";
include_once "../../../../funciones/generales.php";

if (isset($_POST['nombre'])) {

  $zon_nombre  = ucfirst($_POST['nombre']);
  if (isset($_POST['descripcion'])) { $zon_descripcion = $_POST['descripcion'];}else {$zon_descripcion = '';}
  $zon_precio  = $_POST['precio'];
  $zon_clave   = $_POST['clave'];
  $zona        = dameDatosZona($zon_clave);
  $modulo      = "Catálogo";
  $accion      = "Actualización";
  $empleado    = 1;
  $tipo        = 1;//Catálogo
  $description = "Se modifico la zona ". $zona['zon_nombre']." por $zon_nombre";

  $search    = searchZonaNombre($zon_nombre);
  $resultado = "error_search";

  switch ($search['zon_count']) {
    case 0:
      $resultado = updateZona($zon_clave, $zon_nombre, $zon_descripcion, $zon_precio);
      break;
    default:
    $resultado = 2;
    if ($search['zon_count'] > 0 && $search['zon_status'] == 0) {
      $resultado = updateStatusPuesto($zon_clave);
    }
      break;
  }

  if ($resultado == 1) {
    $resultado = bitacora($modulo, $accion, $tipo, $empleado, $description);
    //$modulo, $accion, $tipo, $empleado, $description, $clave
  }

  echo json_encode($resultado);
}
?>

<?php
  include_once "../../../../../config.php";
  include_once "../../../../funciones/searches.php";
  include_once "../../../../funciones/create.php";
  include_once "../../../../funciones/update.php";
  include_once "../../../../funciones/generales.php";

  if (isset($_POST['nombre'])) {

    $zon_nombre  = ucfirst($_POST['nombre']);
    if (isset($_POST['descripcion'])) { $zon_descripcion = $_POST['descripcion'];}else {$zon_descripcion = '';}
    $zon_precio  = $_POST['precio'];
    $modulo      = "Catálogo";
    $accion      = "Registro";
    $empleado    = 1;
    $tipo        = 1;//Catálogo
    $description = "Se registro la zona $zon_nombre";

    $search    = searchZonaNombre($zon_nombre);
    $resultado = "error_search";

    switch ($search['zon_count']) {
      case 0:
        $resultado = insertZona($zon_nombre, $zon_precio, $zon_descripcion);
        break;
      default:
        $resultado = 2;
        if ($search['zon_count'] > 0 && $search['zon_status'] == 0) {
          $resultado = updateStatusZona($search['zon_clave']);
        }
        break;
    }

    if ($resultado == 1) {
      $resultado = bitacora($modulo, $accion, $tipo, $empleado, $description);
      //$modulo, $accion, $tipo, $empleado, $description, $clave
    }

    echo json_encode($resultado);
  }
?>

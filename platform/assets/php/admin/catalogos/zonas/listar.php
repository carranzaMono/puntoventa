<?php
  include_once "../../../../../config.php";

  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $sql    = "SELECT zon_clave, zon_nombre, zon_descripcion, zon_precio,
                    concat('$ ', cast(zon_precio as decimal(11, 2))) as zon_precio_format
                  from zonas
                      where zon_status = 1
                      order by zon_nombre asc";
  $result = mysqli_query($con, $sql);

  if (!$result) {
    die("error");
  }else {

    $array['data'] = [];
    while ($data = mysqli_fetch_assoc($result)) {
      $array['data'][] = $data;
    }

    echo json_encode($array);
  }

  mysqli_free_result($result);
  mysqli_close($con);

?>

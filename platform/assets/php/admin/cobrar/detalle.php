<?php
  include_once "../../../../config.php";

  if ($_GET['venta']) {
    $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
    if (mysqli_connect_errno()) {
      echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
    }

    $venta = $_GET['venta'];

    $sql    = "SELECT dv_clave,
                      (select concat(pro_nombre) from productos where pro_clave = dv_pro_clave) as dv_pro_nombre,
                      concat('$ ', format(ifnull(dv_precioUnitario, 0), 2)) as dv_precioUnitario,
                      ifnull(dv_cantidad, 0) as dv_cantidad,
                      concat('$ ', format(ifnull(dv_iva, 0), 2)) as dv_iva,
                      concat('$ ', format(ifnull(dv_total, 0), 2)) as dv_total
	                  from detalleventas
		                  where dv_ven_clave = $venta
		                    and dv_status = 1";
    $result = mysqli_query($con, $sql);
    //echo $sql;
    if (!$result) {
      die("error");
    }else {

      $array['data'] = [];
      while ($data = mysqli_fetch_assoc($result)) {
        $array['data'][] = $data;
      }

      echo json_encode($array);
    }

    mysqli_free_result($result);
    mysqli_close($con);
  }
?>

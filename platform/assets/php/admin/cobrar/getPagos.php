<?php
  include_once "../../../../config.php";

  if (isset($_GET['venta'])) {
    $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
    if (mysqli_connect_errno()) {
      echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
    }

    $venta = $_GET['venta'];

    $sql    = "SELECT cob_clave, cob_fecha, fp_nombre,
                      concat('$ ', cast(ifnull(cob_monto ,0) as decimal(11, 2))) as cob_monto
                  from cobros, formascobropago
                    where cob_status = 1
                      and cob_ven_clave = $venta
                      and cob_fcp_clave = fp_clave";
    $result = mysqli_query($con, $sql);
    if (!$result) {
      die("error");
    }else {

      $array['data'] = [];
      while ($data = mysqli_fetch_assoc($result)) {
        $array['data'][] = $data;
      }

      echo json_encode($array);
    }

    mysqli_free_result($result);
    mysqli_close($con);

  }
?>

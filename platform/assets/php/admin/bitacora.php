<?php
  include_once "../../../config.php";

  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
  if (mysqli_connect_errno()) {
    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
  }

  $sql    = "SELECT bit_clave, bit_modulo, bit_accion, bit_tipo,
                    bit_emp_clave as bit_empelado,
                    bit_descripcion, bit_claves
                  from bitacora
                      order by bit_clave desc";
  $result = mysqli_query($con, $sql);

  if (!$result) {
    die("error");
  }else {

    $array['data'] = [];
    while ($data = mysqli_fetch_assoc($result)) {
      $array['data'][] = $data;
    }

    echo json_encode($array);
  }

  mysqli_free_result($result);
  mysqli_close($con);

?>

<?php
session_start();
include_once "../../config.php";
include_once "../../assets/funciones/update.php";
include_once "../../assets/funciones/template.php";
include_once "../../assets/funciones/searches.php";

sessionstatus();
$usu_tipo   = $_SESSION['usu_tipo'];
$emp_nombre = $_SESSION['usu_empleado'];
$nivel      = 1;

if (isset($_GET['folio'])) {

  $ven_folio  = $_GET['folio'];
  $venta      = dameDatoVentaFolio($ven_folio);
  $cliente    = dameDatosCliente($venta['ven_cli_clave']);
  $ven_clave  = $venta['ven_clave'];

  //corregirventa($venta['ven_clave']);
}

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <title><?= C_PLATFORMNAME; ?> - Admin venta <?= $ven_folio; ?></title>
        <meta content="Admin Dashboard" name="description" />
        <meta content="ThemeDesign" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <link rel="shortcut icon" href="../../assets/images/favicon.ico">

        <!--Morris Chart CSS -->
        <link rel="stylesheet" href="../../assets/plugins/morris/morris.css">

        <link href="../../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="../../assets/css/icons.css" rel="stylesheet" type="text/css">
        <link href="../../assets/css/style.css" rel="stylesheet" type="text/css">
        <link href="../../assets/css/myStyle.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs5/dt-1.10.25/datatables.min.css"/>
        <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
    </head>


    <body class="fixed-left">

        <!-- Loader -->
        <div id="preloader"><div id="status"><div class="spinner"></div></div></div>

        <!-- Begin page -->
        <div id="wrapper">

            <!-- ========== Left Sidebar Start ========== -->
            <div class="left side-menu">
                <button type="button" class="button-menu-mobile button-menu-mobile-topbar open-left waves-effect">
                    <i class="ion-close"></i>
                </button>

                <!-- LOGO -->
                <div class="topbar-left">
                    <div class="text-center">
                        <!--<a href="index.html" class="logo">Admiry</a>-->
                        <a href="../" class="logo">
                          <img src="../../assets/images/logo.png" height="42" alt="logo">
                        </a>
                    </div>
                </div>

                <div class="sidebar-inner slimscrollleft">

                    <div class="user-details">
                        <div class="text-center">
                            <img src="../../assets/images/users/avatar-1.jpg" alt="" class="rounded-circle">
                        </div>
                        <div class="user-info">
                            <h4 class="font-16 usuario" data-type="<?= $usu_tipo; ?>"><?= $emp_nombre; ?></h4>
                            <span class="text-muted user-status"><i class="fa fa-dot-circle-o text-success"></i> Online</span>
                        </div>
                    </div>

                    <?php printNavbar($usu_tipo, $nivel) ?>

                    <div class="clearfix"></div>
                </div> <!-- end sidebarinner -->
            </div>
            <!-- Left Sidebar End -->

            <!-- Start right Content here -->

            <div class="content-page">
                <!-- Start content -->
                <div class="content">

                    <!-- Top Bar Start -->
                    <div class="topbar">

                        <nav class="navbar-custom">

                            <ul class="list-inline float-right mb-0">
                              <?php printOptionAvatar($usu_tipo, $nivel); ?>
                            </ul>

                            <ul class="list-inline menu-left mb-0">
                                <li class="list-inline-item">
                                    <button type="button" class="button-menu-mobile open-left waves-effect">
                                        <i class="ion-navicon"></i>
                                    </button>
                                </li>
                                <li class="hide-phone list-inline-item app-search">
                                    <h3 class="page-title">Cobrar</h3>
                                </li>
                            </ul>

                            <div class="clearfix"></div>

                        </nav>

                    </div>
                    <!-- Top Bar End -->

                    <div class="page-content-wrapper ">

                        <div class="container">
                          <div class="d-flex">
                            <div class="ml-auto">
                              <nav aria-label="breadvrumb">
                                <ol class="breadcrumb float-right">
                                  <li class="breadcrumb-item"><a href="../">Dashboard</a></li>
                                  <li class="breadcrumb-item" aria-current="page">Venta</li>
                                  <li class="breadcrumb-item" aria-current="page">Cobrar</li>
                                  <li class="breadcrumb-item active" aria-current="page"><?= $ven_folio; ?></li>
                                </ol>
                              </nav>
                            </div>
                          </div>
                            <div class="row">

                                <div class="col-12">
                                  <div class="row">
                                    <div class="col-12 col-lg-12">
                                      <div class="card m-b-20 mb-2">
                                        <div class="card-block">
                                          <h3 class="mt-0 m-b-5 venta header-title d-flex" data-clave="<?= $venta['ven_clave']; ?>" data-folio="<?= $ven_folio; ?>">
                                            Agregar pago <a href="../venta.php?folio=<?= $ven_folio; ?>" class="ml-1" data-toggle="tooltip" data-placement="right" title="regresar a la sección anterior"><i class="fas fa-arrow-circle-left"></i></a>
                                            <span class="ml-auto">Folio: <span class="num-folio"><?= $ven_folio; ?></span></span>
                                          </h3>
                                          <hr style="margin-top: 0px;">
                                          <form class="form-addcobro" action="index.html" method="post">
                                            <div class="row">
                                              <div class="col-12 col-lg-5 mb-1">
                                                <label>Forma de pago*:</label>
                                                <select class="form-control form-control-sm forma-pago" name="forma-pago">
                                                  <option value="">Selecciona una forma de pago</option>
                                                  <option value="1" selected>Efectivo</option>
                                                  <option value="2">Tarjeta de credito</option>
                                                  <option value="3">Tarjeta de debito</option>
                                                  <option value="4">Vales de Despensa</option>
                                                  <option value="5">Cheque</option>
                                                  <option value="6">Otros</option>
                                                </select>
                                                <small id="help-fpago" class="help-fpago form-text text-muted text-danger"></small>
                                              </div>
                                              <div class="col-12 col-lg-5 mb-1">
                                                <label>Monto*:</label>
                                                <input type="number" step="any" min="0" name="monto" class="form-control form-control-sm cobro" autofocus placeholder="">
                                                <small id="help-cobro" class="help-cobro form-text text-muted text-danger"></small>
                                              </div>
                                              <div class="col-12 col-lg-2">
                                                <label></label>
                                                <button type="submit" class="btn btn-block btn-primary waves-effect waves-light btn-agregar btn-sm mt-2" name="button">Agregar</button>
                                              </div>
                                            </div>
                                          </form>
                                          <div class="row">
                                            <div class="col-12">

                                              <div class="modal fade bs-example-modal-sm modal-cambio" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                                                <div class="modal-dialog modal-sm">
                                                  <div class="modal-content">
                                                    <div class="modal-header bg-primary">
                                                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    </div>
                                                    <div class="modal-body text-center content-cambio">
                                                      <h6>Cambio de:</h6>
                                                      <span class="text-cambio"></span>
                                                    </div>
                                                    <div class="modal-footer text-center">
                                                      <button type="button" class="btn btn-secondary btn-block btn-footer-close-cambio">Ok</button>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>


                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-12 col-lg-12">
                                      <div class="card m-b-20 mb-3">
                                        <div class="card-block">
                                          <div class="row mb-3">
                                            <div class="col-12 col-lg-4">
                                              <p class="info"><strong>Fecha: </strong><?=$venta['ven_fecha'];?></p>
                                              <p class="info"><strong>Hora: </strong><?=$venta['ven_hora'];?></p>
                                            </div>
                                            <div class="col-12 col-lg-4">
                                              <p class="info"><strong>Cliente: </strong><?=$cliente['cli_nombre'];?></p>
                                              <p class="info"><strong>Status: </strong><strong class="status"></strong></p>
                                            </div>
                                            <div class="col-12 col-lg-4">
                                              <button type="button" name="button" class="btn btn-secondary waves-effect float-right"><i class="ion-printer"></i> Imprimir ticket</button>
                                            </div>
                                          </div>

                                          <div class="table-rep-plugin">
                                            <div class="table-responsive">
                                              <table class="table table-hover m-b-0 table-sm">
                                                <thead>
                                                  <tr>
                                                    <th>Descripción</th>
                                                    <th>P.Unitario</th>
                                                    <th>Catidad</th>
                                                    <th>Subtotal</th>
                                                    <th>I.V.A.</th>
                                                    <th>Total</th>
                                                  </tr>
                                                </thead>
                                                <tbody>
                                                  <?php
                                                    $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
                                                    if (mysqli_connect_errno()) {
                                                      echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
                                                    }

                                                    $sql    = "SELECT (select pro_nombre from productos where pro_clave = dv_pro_clave) as dv_producto,
                                                                      concat('$ ', format(ifnull(dv_precioUnitario, 0), 2)) as dv_precioUnitario,
                                                                      ifnull(dv_cantidad, 0) as dv_cantidad,
                                                                      concat('$ ', format(ifnull(dv_subtotal, 0), 2)) as dv_subtotal,
                                                                      concat('$ ', format(ifnull(dv_iva, 0), 2)) as dv_iva,
                                                                      concat('$ ', format(ifnull(dv_total, 0), 2)) as dv_total
                                                                    from detalleventas
                                                                      where dv_ven_clave = $ven_clave
                                                                        and dv_status = 1";
                                                    $result = mysqli_query($con ,$sql);
                                                    if ($result) {
                                                      while ($fila = mysqli_fetch_array($result)) {
                                                  ?>
                                                  <tr>
                                                    <td><?= $fila[0]; ?></td>
                                                    <td><?= $fila[1]; ?></td>
                                                    <td class="text-center"><?= $fila[2]; ?></td>
                                                    <td><?= $fila[3]; ?></td>
                                                    <td><?= $fila[4]; ?></td>
                                                    <td><?= $fila[5]; ?></td>
                                                  </tr>
                                                  <?php
                                                      }
                                                    }
                                                  ?>
                                                </tbody>
                                              </table>
                                            </div>
                                          </div>


                                          <hr style="margin-top: 0px;">
                                          <div class="row justify-content-end">
                                            <div class="col-lg-2 col-6 text-right">
                                              <p class="info"><strong>Total   :</strong></p>
                                              <p class="info"><strong>Pagado  :</strong></p>
                                              <hr style="margin-top: 8px; margin-bottom: 5px;" class="">
                                              <p class="info"><strong>Resta   :</strong></p>
                                            </div>
                                            <div class="col-lg-2 col-6 text-left">
                                              <p class="info"><span class="total"></span></p>
                                              <p class="info"><span class="cobrado"></span></p>
                                              <hr style="margin-top: 8px; margin-bottom: 5px;" class="">
                                              <p class="info"><strong><span class="resta"></span></strong></p>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="col-12 col-lg-12">
                                  <div class="card m-b-20">
                                    <div class="card-block">
                                      <h3 class="mt-0 m-b-5 header-title">
                                        Lista pagos
                                      </h3>
                                      <div class="table-rep-plugin">
                                        <table id="dt-detalleCobros" class="table table-hover m-b-0 table-sm">
                                          <thead>
                                            <tr>
                                              <th>#</th>
                                              <th>Fecha</th>
                                              <th>Hora</th>
                                              <th>Forma de pago</th>
                                              <th>Monto</th>
                                            </tr>
                                          </thead>
                                        </table>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                            </div>
                            <!-- end row -->

                        </div><!-- container -->

                        <?php  include_once "../../assets/modal/caja.php"; ?>
                        <?php/*include_once "../cotizacion/modal.php";*/?>

                    </div> <!-- Page content Wrapper -->

                </div> <!-- content -->

                <?= printFooter(); ?>

            </div>
            <!-- End Right content here -->

        </div>
        <!-- END wrapper -->


        <!-- jQuery  -->
        <script src="../../assets/js/jquery.min.js"></script>
        <script src="../../assets/js/tether.min.js"></script><!-- Tether for Bootstrap -->
        <script src="../../assets/js/bootstrap.min.js"></script>
        <script src="../../assets/js/modernizr.min.js"></script>
        <script src="../../assets/js/detect.js"></script>
        <script src="../../assets/js/fastclick.js"></script>
        <script src="../../assets/js/jquery.slimscroll.js"></script>
        <script src="../../assets/js/jquery.blockUI.js"></script>
        <script src="../../assets/js/waves.js"></script>
        <script src="../../assets/js/jquery.nicescroll.js"></script>
        <script src="../../assets/js/jquery.scrollTo.min.js"></script>

        <!-- App js -->
        <script src="../../assets/js/app.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.0.1/js/bootstrap.bundle.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/v/bs5/dt-1.10.25/datatables.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
        <script src="../../assets/js/dataTablelanguage.js"></script>
        <script src="../../assets/js/constantes.js"></script>
        <script src="../../assets/js/generales.js"></script>
        <script src="../../assets/ajax/generales/revisar.js"></script>
        <script src="../../assets/ajax/generales/ventas/cobrar.js"></script>
        <script src="../../assets/js/messages.js"></script>

        <script type="text/javascript">
        $('.search').select2();
        </script>

        <!-- Alertify js -->
        <script src="../../assets/plugins/alertify/js/alertify.js"></script>

    </body>
</html>

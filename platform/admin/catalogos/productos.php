<?php
session_set_cookie_params(60*60*24*14);
session_start();
include_once "../../config.php";
include_once "../../assets/funciones/template.php";
include_once "../../assets/funciones/searches.php";

sessionstatus();
$usu_tipo   = $_SESSION['usu_tipo'];
$emp_nombre = $_SESSION['usu_empleado'];
$nivel      = 1;

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <title><?= C_PLATFORMNAME; ?> - Admin Catálogos</title>
        <meta content="Admin Dashboard" name="description" />
        <meta content="ThemeDesign" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <link rel="shortcut icon" href="../../assets/images/favicon.ico">

        <!--Morris Chart CSS -->
        <link rel="stylesheet" href="../../assets/plugins/morris/morris.css">

        <link href="../../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="../../assets/css/icons.css" rel="stylesheet" type="text/css">
        <link href="../../assets/css/style.css" rel="stylesheet" type="text/css">
        <link href="../../assets/css/myStyle.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs5/dt-1.10.25/datatables.min.css"/>
        <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    </head>


    <body class="fixed-left">

        <!-- Loader -->
        <div id="preloader"><div id="status"><div class="spinner"></div></div></div>

        <!-- Begin page -->
        <div id="wrapper">

            <!-- ========== Left Sidebar Start ========== -->
            <div class="left side-menu">
                <button type="button" class="button-menu-mobile button-menu-mobile-topbar open-left waves-effect">
                    <i class="ion-close"></i>
                </button>

                <!-- LOGO -->
                <div class="topbar-left">
                    <div class="text-center">
                        <!--<a href="index.html" class="logo">Admiry</a>-->
                        <a href="../" class="logo">
                          <img src="../../assets/images/logo.png" height="42" alt="logo">
                        </a>
                    </div>
                </div>

                <div class="sidebar-inner slimscrollleft">

                    <div class="user-details">
                        <div class="text-center">
                            <img src="../../assets/images/users/avatar-1.jpg" alt="" class="rounded-circle">
                        </div>
                        <div class="user-info">
                            <h4 class="font-16"><?= $emp_nombre; ?></h4>
                            <span class="text-muted user-status"><i class="fa fa-dot-circle-o text-success"></i> Online</span>
                        </div>
                    </div>

                    <?php printNavbar($usu_tipo, $nivel) ?>

                    <div class="clearfix"></div>
                </div> <!-- end sidebarinner -->
            </div>
            <!-- Left Sidebar End -->

            <!-- Start right Content here -->

            <div class="content-page">
                <!-- Start content -->
                <div class="content">

                    <!-- Top Bar Start -->
                    <div class="topbar">

                        <nav class="navbar-custom">

                            <ul class="list-inline float-right mb-0">
                              <?php printOptionAvatar($usu_tipo, $nivel); ?>
                            </ul>

                            <ul class="list-inline menu-left mb-0">
                                <li class="list-inline-item">
                                    <button type="button" class="button-menu-mobile open-left waves-effect">
                                        <i class="ion-navicon"></i>
                                    </button>
                                </li>
                                <li class="hide-phone list-inline-item app-search">
                                    <h3 class="page-title">Productos</h3>
                                </li>
                            </ul>

                            <div class="clearfix"></div>

                        </nav>

                    </div>
                    <!-- Top Bar End -->

                    <div class="page-content-wrapper ">

                        <div class="container">
                          <div class="d-flex">
                            <div class="ml-auto">
                              <nav aria-label="breadcrumb">
                                <ol class="breadcrumb float-right">
                                  <li class="breadcrumb-item"><a href="../">Dashboard</a></li>
                                  <li class="breadcrumb-item">Catálogo</li>
                                  <li class="breadcrumb-item active" aria-current="page">Productos</li>
                                </ol>
                              </nav>
                            </div>
                          </div>
                            <div class="row">

                                <div class="col-12">
                                    <div class="card m-b-20">
                                        <div class="card-block">
                                            <h4 class="mt-0 m-b-15 header-title">
                                              Lista de productos
                                              <button type="button" name="button" class="btn btn-outline-primary btn-sm" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">Agregar</button>
                                            </h4>

                                            <div class="collapse mb-3" id="collapseExample">
                                              <div class="card card-block">
                                                <form class="form-agregar" action="index.html" method="post">
                                                  <div class="row">
                                                    <div class="col-12 col-lg-9 form-group">
                                                      <label>Nombre*:</label>
                                                      <input type="text" name="nombre" class="form-control form-control-sm nombre">
                                                      <small id="help-nombre" class="form-text text-muted text-danger"></small>
                                                    </div>
                                                    <div class="col-12 col-lg-3 form-group">
                                                      <label>Inventariable*:</label>
                                                      <select class="form-control form-control-sm inventariable" name="inventariable">
                                                        <option value="">Selecciona una opción</option>
                                                        <option value="0">No</option>
                                                        <option value="1">Si</option>
                                                      </select>
                                                      <small id="help-inventariable" class="form-text text-muted text-danger"></small>
                                                    </div>
                                                  </div>
                                                  <div class="row">
                                                    <div class="col-12 col-lg-3 form-group">
                                                      <label>Presentación:</label>
                                                      <select class="form-control form-control-sm presentacion search" style="width: 100%;" name="presentacion">
                                                        <option value="">Selecciona una opción</option>
                                                        <option value="1">Bolsa</option>
                                                        <option value="2">Botella</option>
                                                        <option value="3">Caja</option>
                                                        <option value="4">Frasco</option>
                                                        <option value="5">Sobre</option>
                                                        <option value="6">Pieza</option>
                                                        <option value="7">Paquete</option>
                                                        <option value="8">Rollo</option>
                                                      </select>
                                                      <small id="help-presentacion" class="form-text text-muted text-danger"></small>
                                                    </div>
                                                    <div class="col-12 col-lg-3 form-group">
                                                      <label>Unidad*:</label>
                                                      <select class="form-control form-control-sm unidad search" style="width: 100%;" name="unidad">
                                                        <option value="">Selecciona una opción</option>
                                                        <option value="1">sin unidad</option>
                                                        <option value="2">kg</option>
                                                        <option value="3">g</option>
                                                        <option value="4">l</option>
                                                        <option value="5">ml</option>
                                                        <option value="6">m</option>
                                                        <option value="7">m2</option>
                                                        <option value="8">pza.</option>
                                                      </select>
                                                      <small id="help-unidad" class="form-text text-muted text-danger"></small>
                                                    </div>
                                                    <div class="col-12 col-lg-3 form-group">
                                                      <label>Cantidad:</label>
                                                      <input type="number" step="any" min="0" name="medida" class="form-control form-control-sm medida">
                                                      <small id="help-medida" class="form-text text-muted text-danger"></small>
                                                    </div>
                                                    <div class="col-12 col-lg-3 form-group">
                                                      <label>Precio*:</label>
                                                      <input type="number" step="any" min="0" name="precio" class="form-control form-control-sm precio">
                                                      <small id="help-precio" class="form-text text-muted text-danger"></small>
                                                    </div>
                                                  </div>
                                                  <div class="row justify-content-end">
                                                    <div class="col-3">
                                                      <button type="submit" name="button" class="btn btn-primary btn-sm btn-block">Guardar</button>
                                                    </div>
                                                  </div>
                                                </form>
                                              </div>
                                            </div>
                                            <table id="dt-productos" class="table table-hover m-b-0 table-sm">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Producto</th>
                                                    <th>Inventariable</th>
                                                    <th>Presentación</th>
                                                    <th>Cantidad</th>
                                                    <th>Unidad</th>
                                                    <th>Precio</th>
                                                    <th></th>
                                                </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <!-- Modal para actualizar -->
                                <div id="modal-edit" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                  <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <h6 class="modal-title"></h6>
                                        <button type="button" class="close-edit" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                      </div>
                                      <div class="modal-body">
                                        <form id="form-update" class="form-update" action="index.html" method="post">
                                          <input type="text" name="clave" hidden class="form-control form-control-sm uclave">
                                          <div class="row">
                                            <div class="col-12 col-lg-6 form-group">
                                              <label>Nombre*:</label>
                                              <input type="text" name="nombre" class="form-control form-control-sm unombre">
                                              <small id="help-unombre" class="form-text text-muted text-danger"></small>
                                            </div>
                                            <div class="col-12 col-lg-6 form-group">
                                              <label>Inventariable*:</label>
                                              <select class="form-control form-control-sm uinventariable" name="inventariable">
                                                <option value="">Selecciona una opción</option>
                                                <option value="0">No</option>
                                                <option value="1">Si</option>
                                              </select>
                                              <small id="help-uinventariable" class="form-text text-muted text-danger"></small>
                                            </div>
                                          </div>
                                          <div class="row">
                                            <div class="col-12 col-lg-6 form-group">
                                              <label>Presentación:</label>
                                              <select class="form-control form-control-sm upresentacion search" style="width: 100%;" name="presentacion">
                                                <option value="">Selecciona una opción</option>
                                                <option value="1">Bolsa</option>
                                                <option value="2">Botella</option>
                                                <option value="3">Caja</option>
                                                <option value="4">Frasco</option>
                                                <option value="5">Sobre</option>
                                                <option value="6">Pieza</option>
                                                <option value="7">Paquete</option>
                                                <option value="8">Rollo</option>
                                              </select>
                                              <small id="help-upresentacion" class="form-text text-muted text-danger"></small>
                                            </div>
                                            <div class="col-12 col-lg-6 form-group">
                                              <label>Unidad*:</label>
                                              <select class="form-control form-control-sm uunidad search" style="width: 100%;" name="unidad">
                                                <option value="">Selecciona una opción</option>
                                                <option value="1">sin unidad</option>
                                                <option value="2">kg</option>
                                                <option value="3">g</option>
                                                <option value="4">l</option>
                                                <option value="5">ml</option>
                                                <option value="6">m</option>
                                                <option value="7">m2</option>
                                                <option value="8">pza.</option>
                                              </select>
                                              <small id="help-uunidad" class="form-text text-muted text-danger"></small>
                                            </div>
                                          </div>
                                          <div class="row">
                                            <div class="col-12 col-lg-6 form-group">
                                              <label>Cantidad:</label>
                                              <input type="number" step="any" min="0" name="medida" class="form-control form-control-sm umedida">
                                              <small id="help-umedida" class="form-text text-muted text-danger"></small>
                                            </div>
                                            <div class="col-12 col-lg-6 form-group">
                                              <label>Precio*:</label>
                                              <input type="number" step="any" min="0" name="precio" class="form-control form-control-sm uprecio">
                                              <small id="help-uprecio" class="form-text text-muted text-danger"></small>
                                            </div>
                                          </div>
                                        </form>
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary close-edit">Cerrar</button>
                                        <button type="submit" form="form-update" class="btn btn-primary">Actualizar</button>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <!-- Modal para actualizar -->
                                <div id="modal-delete" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                  <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <h6 class="modal-title"></h6>
                                        <button type="button" class="close-delete" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                      </div>
                                      <div class="modal-body text-center">
                                        <form id="form-delete" class="form-delete" action="index.html" method="post">
                                          <div class="my-4">
                                            <i class="ti-help-alt"></i>
                                            <input type="text" hidden name="clave" class="form-control form-control-sm dclave">
                                            <h6>¿Esta seguro de eliminar el producto <span class="dnombre"></span>?</h6>
                                          </div>
                                        </form>
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary close-delete">¡No, eliminar!</button>
                                        <button type="submit" form="form-delete" class="btn btn-primary">¡Si, eliminar!</button>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                            </div>
                            <!-- end row -->

                        </div><!-- container -->

                        <?php  include_once "../../assets/modal/caja.php"; ?>

                    </div> <!-- Page content Wrapper -->

                </div> <!-- content -->

                <?= printFooter(); ?>

            </div>
            <!-- End Right content here -->

        </div>
        <!-- END wrapper -->


        <!-- jQuery  -->
        <script src="../../assets/js/jquery.min.js"></script>
        <script src="../../assets/js/tether.min.js"></script><!-- Tether for Bootstrap -->
        <script src="../../assets/js/bootstrap.min.js"></script>
        <script src="../../assets/js/modernizr.min.js"></script>
        <script src="../../assets/js/detect.js"></script>
        <script src="../../assets/js/fastclick.js"></script>
        <script src="../../assets/js/jquery.slimscroll.js"></script>
        <script src="../../assets/js/jquery.blockUI.js"></script>
        <script src="../../assets/js/waves.js"></script>
        <script src="../../assets/js/jquery.nicescroll.js"></script>
        <script src="../../assets/js/jquery.scrollTo.min.js"></script>

        <!-- App js -->
        <script src="../../assets/js/app.js"></script>
        <script src="../../assets/js/constantes.js"></script>
        <script src="../../assets/js/generales.js"></script>
        <script src="../../assets/ajax/generales/revisar.js"></script>
        <script src="../../assets/ajax/admin/catalogos/productos.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.0.1/js/bootstrap.bundle.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/v/bs5/dt-1.10.25/datatables.min.js"></script>
        <script src="../../assets/js/dataTablelanguage.js"></script>
        <script src="../../assets/js/messages.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

        <!-- Alertify js -->
        <script src="../../assets/plugins/alertify/js/alertify.js"></script>
        <script type="text/javascript">
        $('.search').select2();
        </script>
    </body>
</html>

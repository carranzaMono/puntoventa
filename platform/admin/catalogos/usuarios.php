<?php
session_set_cookie_params(60*60*24*14);
session_start();
include_once "../../config.php";
include_once "../../assets/funciones/template.php";
include_once "../../assets/funciones/searches.php";

sessionstatus();
$usu_tipo   = $_SESSION['usu_tipo'];
$emp_nombre = $_SESSION['usu_empleado'];
$nivel      = 1;

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <title>Admiry - Responsive Flat Admin Dashboard</title>
        <meta content="Admin Dashboard" name="description" />
        <meta content="ThemeDesign" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <link rel="shortcut icon" href="../../assets/images/favicon.ico">

        <!--Morris Chart CSS -->
        <link rel="stylesheet" href="../../assets/plugins/morris/morris.css">

        <link href="../../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="../../assets/css/icons.css" rel="stylesheet" type="text/css">
        <link href="../../assets/css/style.css" rel="stylesheet" type="text/css">
        <link href="../../assets/css/myStyle.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs5/dt-1.10.25/datatables.min.css"/>
        <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    </head>


    <body class="fixed-left">

        <!-- Loader -->
        <div id="preloader"><div id="status"><div class="spinner"></div></div></div>

        <!-- Begin page -->
        <div id="wrapper">

            <!-- ========== Left Sidebar Start ========== -->
            <div class="left side-menu">
                <button type="button" class="button-menu-mobile button-menu-mobile-topbar open-left waves-effect">
                    <i class="ion-close"></i>
                </button>

                <!-- LOGO -->
                <div class="topbar-left">
                    <div class="text-center">
                        <!--<a href="index.html" class="logo">Admiry</a>-->
                        <a href="../" class="logo">
                          <img src="../../assets/images/logo.png" height="42" alt="logo">
                        </a>
                    </div>
                </div>

                <div class="sidebar-inner slimscrollleft">

                    <div class="user-details">
                        <div class="text-center">
                            <img src="../../assets/images/users/avatar-1.jpg" alt="" class="rounded-circle">
                        </div>
                        <div class="user-info">
                            <h4 class="font-16 usuario" data-type="<?= $usu_tipo; ?>"><?= $emp_nombre; ?></h4>
                            <span class="text-muted user-status"><i class="fa fa-dot-circle-o text-success"></i> Online</span>
                        </div>
                    </div>

                    <?php printNavbar($usu_tipo, $nivel) ?>

                    <div class="clearfix"></div>
                </div> <!-- end sidebarinner -->
            </div>
            <!-- Left Sidebar End -->

            <!-- Start right Content here -->

            <div class="content-page">
                <!-- Start content -->
                <div class="content">

                    <!-- Top Bar Start -->
                    <div class="topbar">

                        <nav class="navbar-custom">

                            <ul class="list-inline float-right mb-0">
                              <?php printOptionAvatar($usu_tipo, $nivel); ?>
                            </ul>

                            <ul class="list-inline menu-left mb-0">
                                <li class="list-inline-item">
                                    <button type="button" class="button-menu-mobile open-left waves-effect">
                                        <i class="ion-navicon"></i>
                                    </button>
                                </li>
                                <li class="hide-phone list-inline-item app-search">
                                    <h3 class="page-title">Usuarios</h3>
                                </li>
                            </ul>

                            <div class="clearfix"></div>

                        </nav>

                    </div>
                    <!-- Top Bar End -->

                    <div class="page-content-wrapper ">

                        <div class="container">
                          <div class="d-flex">
                            <div class="ml-auto">
                              <nav aria-label="breadcrumb">
                                <ol class="breadcrumb float-right">
                                  <li class="breadcrumb-item"><a href="../">Dashboard</a></li>
                                  <li class="breadcrumb-item">Catálogo</li>
                                  <li class="breadcrumb-item active" aria-current="page">Usuarios</li>
                                </ol>
                              </nav>
                            </div>
                          </div>
                            <div class="row">

                                <div class="col-12">
                                    <div class="card m-b-20">
                                        <div class="card-block">
                                            <h4 class="mt-0 m-b-15 header-title">
                                              Lista de usuarios
                                              <button type="button" name="button" class="btn btn-outline-primary btn-sm" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">Agregar</button>
                                            </h4>

                                            <div class="collapse mb-3" id="collapseExample">
                                              <div class="card card-block">
                                                <form class="form-agregar" action="index.html" method="post">
                                                  <div class="row">
                                                    <div class="col-12 col-lg-6 form-group">
                                                      <label>Username*:</label>
                                                      <input type="text" name="username" class="form-control form-control-sm username">
                                                      <small id="help-username" class="form-text text-muted text-danger"></small>
                                                    </div>
                                                    <div class="col-12 col-lg-6 form-group">
                                                      <label>Password*:</label>
                                                      <input type="password" name="password" class="form-control form-control-sm password">
                                                      <small id="help-password" class="form-text text-muted text-danger"></small>
                                                    </div>
                                                  </div>
                                                  <div class="row">
                                                    <div class="col-lg-6 col-12 form-group">
                                                      <label>Empleado*:</label>
                                                      <select id="empleado" class="form-control form-control-sm empleado search" name="empleado" style="width: 100%;">
                                                        <option value="">Seleccione un empleado</option>
                                                        <?php
                                                          $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
                                                          if (mysqli_connect_errno()) {
                                                            echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
                                                          }

                                                          $sql    = "SELECT emp_clave, emp_nombreCompleto
                                                                        from empleados
                                                                          where Not EXISTS (SELECT * from usuarios where usu_emp_clave = emp_clave)
                                                                            and emp_status = 1";
                                                          $result = mysqli_query($con, $sql);
                                                          if ($result) {
                                                          while ($fila = mysqli_fetch_array($result)) {
                                                        ?>
                                                        <option value="<?= $fila[0]; ?>"><?= $fila[1]; ?></option>
                                                        <?php
                                                            }
                                                          }
                                                        ?>
                                                      </select>
                                                      <small id="help-empleado" class="form-text text-muted text-danger"></small>
                                                    </div>
                                                    <div class="col-lg-6 col-12">
                                                      <label>Tipo*:</label>
                                                      <select class="form-control form-control-sm tipo-user" name="tipoUser">
                                                        <option value="">Seleccione un tipo de usuario</option>
                                                        <option value="1">Administrador</option>
                                                        <option value="2">Almacenista</option>
                                                        <option value="3">Cajero</option>
                                                        <option value="4">Vendedor</option>
                                                      </select>
                                                      <small id="help-tipouser" class="form-text text-muted text-danger"></small>
                                                    </div>
                                                  </div>
                                                  <div class="row justify-content-end">
                                                    <div class="col-3">
                                                      <button type="submit" name="button" class="btn btn-primary btn-sm btn-block mt-2">Guardar</button>
                                                    </div>
                                                  </div>
                                                </form>
                                              </div>
                                            </div>

                                            <table id="dt-usuarios" class="table table-hover m-b-0 table-sm">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Username</th>
                                                    <th>Tipo</th>
                                                    <th>Empleado</th>
                                                    <th>Status</th>
                                                    <th></th>
                                                </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <!-- Modal para actualizar -->
                                <div id="modal-edit" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                  <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <h6 class="modal-title"></h6>
                                        <button type="button" class="close-edit" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                      </div>
                                      <div class="modal-body">
                                        <input type="text" name="clave" hidden class="form-control form-control-sm uclave">
                                        <form id="form-update-user" class="form-update-user" action="index.html" method="post">
                                          <div class="row">
                                            <div class="col-12 col-lg-4 form-group">
                                              <label>Username*:</label>
                                              <input type="text" name="username" class="form-control form-control-sm uusername">
                                              <small id="help-uusername" class="form-text text-muted text-danger"></small>
                                            </div>
                                            <div class="col-12 col-lg-4 form-group">
                                              <label>Tipo*:</label>
                                              <select class="form-control form-control-sm utipo-user" name="tipoUser">
                                                <option value="">Seleccione un tipo de usuario</option>
                                                <option value="1">Administrador</option>
                                                <option value="3">Almacenista</option>
                                                <option value="2">Cajero</option>
                                                <option value="4">Vendedor</option>
                                              </select>
                                              <small id="help-utipouser" class="form-text text-muted text-danger"></small>
                                            </div>
                                            <div class="col-12 col-lg-4 form-group">
                                              <label></label>
                                              <button type="submit" form="form-update-user" class="btn btn-primary btn-block btn-sm mt-2">Actualizar</button>
                                            </div>
                                          </div>
                                        </form>
                                        <hr>
                                        <form id="form-update-password" class="form-update-password" action="index.html" method="post">
                                          <div class="row">
                                            <div class="col-lg-8 col-12">
                                              <label>Password*:</label>
                                              <input type="password" name="password" class="form-control form-control-sm upassword" placeholder="Escribe una nueva contraseña">
                                              <small id="help-upassword" class="form-text text-muted text-danger"></small>
                                            </div>
                                            <div class="col-lg-4 col-12">
                                              <label></label>
                                              <button type="submit" form="form-update-password" class="btn btn-primary btn-block btn-sm mt-2">Actualizar</button>
                                            </div>
                                          </div>
                                        </form>
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary close-edit">Cerrar</button>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <!-- Modal para eliminar -->
                                <div id="modal-delete" class="modal fade bd-example-modal-lg modal-delete" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                  <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <h6 class="modal-title"></h6>
                                        <button type="button" class="close-delete" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                      </div>
                                      <div class="modal-body text-center">
                                        <form id="form-delete" class="form-delete" action="index.html" method="post">
                                          <div class="my-4">
                                            <i class="ti-help-alt"></i>
                                            <input type="text" hidden name="clave" class="form-control form-control-sm dclave">
                                            <h6>¿Esta seguro de eliminar al usuario <span class="dnombre"></span>?</h6>
                                          </div>
                                        </form>
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary close-delete">¡No, eliminar!</button>
                                        <button type="submit" form="form-delete" class="btn btn-primary">¡Si, eliminar!</button>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <!-- Modal para suspender -->
                                <div id="modal-suspender" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                  <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <h6 class="modal-title"></h6>
                                        <button type="button" class="close-delete" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                      </div>
                                      <div class="modal-body text-center">
                                        <form id="form-suspender" class="form-suspender" action="index.html" method="post">
                                          <div class="my-4">
                                            <i class="ti-help-alt"></i>
                                            <input type="text" name="clave" hidden class="form-control form-control-sm sclave">
                                            <h6>¿Esta seguro de suspender al usuario <span class="snombre"></span>?</h6>
                                          </div>
                                        </form>
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary close-suspender">¡No, suspender!</button>
                                        <button type="submit" form="form-suspender" class="btn btn-primary">¡Si, suspender!</button>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                            </div>
                            <!-- end row -->

                        </div><!-- container -->

                        <?php  include_once "../../assets/modal/caja.php"; ?>

                    </div> <!-- Page content Wrapper -->

                </div> <!-- content -->

                <?= printFooter(); ?>

            </div>
            <!-- End Right content here -->

        </div>
        <!-- END wrapper -->


        <!-- jQuery  -->
        <script src="../../assets/js/jquery.min.js"></script>
        <script src="../../assets/js/tether.min.js"></script><!-- Tether for Bootstrap -->
        <script src="../../assets/js/bootstrap.min.js"></script>
        <script src="../../assets/js/modernizr.min.js"></script>
        <script src="../../assets/js/detect.js"></script>
        <script src="../../assets/js/fastclick.js"></script>
        <script src="../../assets/js/jquery.slimscroll.js"></script>
        <script src="../../assets/js/jquery.blockUI.js"></script>
        <script src="../../assets/js/waves.js"></script>
        <script src="../../assets/js/jquery.nicescroll.js"></script>
        <script src="../../assets/js/jquery.scrollTo.min.js"></script>

        <!-- App js -->
        <script src="../../assets/js/app.js"></script>
        <script src="../../assets/js/constantes.js"></script>
        <script src="../../assets/js/generales.js"></script>
        <script src="../../assets/ajax/generales/revisar.js"></script>
        <script src="../../assets/ajax/admin/catalogos/usuarios.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.0.1/js/bootstrap.bundle.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/v/bs5/dt-1.10.25/datatables.min.js"></script>
        <script src="../../assets/js/dataTablelanguage.js"></script>
        <script src="../../assets/js/messages.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

        <!-- Alertify js -->
        <script src="../../assets/plugins/alertify/js/alertify.js"></script>
        <script type="text/javascript">
        $('.search').select2();
        </script>

    </body>
</html>

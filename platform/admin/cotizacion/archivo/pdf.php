
<?php
  require '../../../config.php';
  include_once  '../../../assets/funciones/searches.php';
  require_once  '../../../assets/dompdf/autoload.inc.php';
  use Dompdf\Dompdf;
  use Dompdf\Options;

  if (isset($_GET['cotizacion'])) {

    $options    = new Options();
    $options->set('isRemoteEnabled', TRUE);

    $pdf        = new Dompdf($options);
    $ccj_clave  = $_GET['cotizacion'];
    $cotizacion = dameDatosCot($ccj_clave);
    $doc_nombre = "Cotización - ".$cotizacion['cot_folio'];
    $cliente    = dameDatosCliente($cotizacion['cot_cli_clave']);

    ob_start();
?>
  <!--
  Html =========================================================================
  ==============================================================================
  -->
  <html lang="en">
    <head>
      <meta charset="utf-8">
      <title>Cotización - <?= $cotizacion['cot_folio']; ?></title>
    </head>
    <body>

      <!--
      Style ====================================================================
      ==========================================================================
      -->
      <style media="screen">
        .clearfix:after {
          content: "";
          display: table;
          clear: both;
        }

        a {
          color: #5D6975;
          text-decoration: underline;
        }

        body {
          position: relative;
          width: 100%!important;
          margin: 0 auto;
          color: #001028;
          background: #FFFFFF;
          font-family: -apple-system, system-ui, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif;
          font-size: 12px;
        }

        header {
          padding: 5px 0;
          margin-bottom: 30px;
        }

        #logo {
          text-align: center;
          margin-bottom: 10px;
        }

        #logo img {
          width: 85px;
        }

        h1, h3 {
          border-top: 1px solid  #5D6975;
          border-bottom: 1px solid  #5D6975;
          color: #5D6975;
          font-size: 2em;
          line-height: 1.4em;
          font-weight: normal;
          text-align: center;
          margin: 0 0 15px 0;
          background: url("http://localhost/puntoventa/platform/assets/images/patron1.png");
        }

        #company, #project{
          display: inline-block;
        }

        #company{
          float: left;
          text-align: left;
        }

        #project{
          float: right;
        }


        #project span {
          color: #5D6975;
          text-align: right;
          width: 52px;
          margin-right: 10px;
          display: inline-block;
          font-size: 0.8em;
        }

        #project label{
          color: #EF3939;
        }

        #project div,
        #company div {
          white-space: nowrap;
        }

        table {
          width: 100%;
          border-collapse: collapse;
          border-spacing: 0;
          margin-bottom: 15px;
        }

        table tr:nth-child(2n-1) td {
          background: #F5F5F5;
        }

        table th,
        table td {
          text-align: center;
        }

        table th {
          padding: 5px 20px;
          color: #5D6975;
          border-bottom: 1px solid #C1CED9;
          white-space: nowrap;
          font-weight: normal;
        }

        table .service,
        table .desc {
          text-align: left;

        }

        table td {
          padding: 10px;
          text-align: right;
        }

        table td.service,
        table td.desc {
          vertical-align: top;
          width: 40%!important;
        }

        .td-one, .td-two, .td-three{
          width: 33%!important;
        }

        .td-two, .td-three{
          font-size: 1.17em!important;
        }

        table td.qty, table td.unit{
          text-align: center;
        }

        table td.unit,
        table td.qty,
        table td.total {
          font-size: 1em;
        }

        table td.grand {
          border-top: 1px solid #5D6975;;
        }

        #notices .notice {
          color: #5D6975;
          font-size: 1em;
        }

        footer {
          color: #5D6975;
          width: 100%;
          height: 20px;
          position: absolute;
          bottom: 0;
          border-top: 1px solid #C1CED9;
          padding: 8px 0;
          text-align: center;
        }
      </style>
      <!--
      ==========================================================================
      ==========================================================================
      -->

      <!--
      Contenido ================================================================
      ==========================================================================
      -->
      <header class="clearfix">
        <div id="logo">
          <img src="http://localhost/puntoventa/platform/assets/images/logo-generico.png" alt="">
        </div>
        <h3>Cotización</h3>
        <div id="company" class="clearfix">
          <div>Nombre de la empresa</div>
          <div>Dirección<br /> </div>
          <div>(602) 519-0450</div>
          <div>empresa@example.com</div>
        </div>
        <div id="project">
          <div><span>Folio:</span> <label><?= $cotizacion['cot_folio']; ?></label></div>
          <div><span>Cliente:</span> <?=$cliente['cli_nombre'];?></div>
          <div><span>Dirección:</span> <?=$cliente['cli_direccion'];?></div>
          <div><span>Email:</span> <a href="mailto:<?=$cliente['cli_correo'];?>"><?=$cliente['cli_correo'];?></a></div>
          <div><span>Fecha:</span> <?= $cotizacion['cot_fecha']; ?></div>
        </div>
      </header>
      <main>
        <table>
          <thead>
            <tr>
              <!--th class="service">SERVICE</th-->
              <th class="desc">Descripción</th>
              <th>P.Unitario</th>
              <th>Cantidad</th>
              <th>Subtotal</th>
              <th>I.V.A.</th>
              <th>Total</th>
            </tr>
          </thead>
          <tbody>
            <?php
              $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
              if (mysqli_connect_errno()) {
                echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
              }

              $sql    = "SELECT (select pro_nombre from productos where pro_clave = dc_pro_clave) as dc_pro_nombre,
                                concat('$ ', format(ifnull(dc_precioUnitario, 0), 2)) as dc_precioUnitario,
                                ifnull(dc_cantidad, 0) as dc_cantidad,
                                concat('$ ', format(ifnull(dc_subtotal, 0), 2)) as dc_subtotal,
                                concat('$ ', format(ifnull(dc_iva, 0), 2)) as dc_iva,
                                concat('$ ', format(ifnull(dc_total, 0), 2)) as dc_total
          	                  from detallecotizacion
          		                  where dc_cot_clave = $ccj_clave
          		                    and dc_stauts = 1";
              $result = mysqli_query($con ,$sql);
              if ($result) {
                while ($fila = mysqli_fetch_array($result)) {
            ?>
            <tr>
              <!--td class="service">Design</td-->
              <td class="desc"><?= $fila[0]; ?></td>
              <td class="unit"><?= $fila[1]; ?></td>
              <td class="qty"><?= $fila[2]; ?></td>
              <td class="unit"><?= $fila[3]; ?></td>
              <td class="unit"><?= $fila[4]; ?></td>
              <td class="unit"><?= $fila[5]; ?></td>
            </tr>
            <?php
                }
              }
            ?>
            <?php

            $sql = "SELECT concat('$ ', format(ifnull(sum(dc_total) , 0), 2)) as subtotal,
                              concat('$ ', format(ifnull(sum(dc_iva), 0), 2)) as iva,
                              concat('$ ', format(ifnull(sum(dc_total), 0), 2)) as total,
                              cot_status
        	                  from cotizaciones, detallecotizacion
        		                  where dc_cot_clave = $ccj_clave
                                and cot_clave = dc_cot_clave
        		                    and dc_stauts = 1";
            $result = mysqli_query($con, $sql);
            if ($result) {
              while ($fila = mysqli_fetch_array($result)) {
            ?>
            <!--tr>
              <td class=""></td>
              <td colspan="4">Subtotal</td>
              <td class="total"></td>
            </tr-->
            <!--tr>
              <td class=""></td>
              <td colspan="4">I.V.A.</td>
              <td class="total"></td>
            </tr-->
            <tr>
              <td class="grand td-one"></td>
              <td colspan="4" class="grand td-two">TOTAL</td>
              <td class="grand td-three"><?= $fila[2]; ?></td>
            </tr>
            <?php
              }
            }
            ?>
          </tbody>
        </table>
        <div id="notices">
          <div>Condiciones Comerciales:</div>
          <div class="notice">
            <ul>
              <li>Los precios expresados son en moneda nacional.</li>
              <li>La forma de pago es de contado contra entrega.</li>
              <li>La vigencia de esta cotización sera de 3 dias.</li>
            </ul>
          </div>
        </div>
      </main>
      <footer>
        La factura fue creada en una computadora y es válida sin la firma y el sello.
      </footer>
    </body>
    <!--
    ============================================================================
    ============================================================================
    -->
  </html>
  <!--
  ==============================================================================
  ==============================================================================
  -->
<?php

    $contenido = ob_get_clean();

    $pdf->loadHtml($contenido);
    $pdf->setPaper('A4');
    $pdf->render();
    $pdf->stream($doc_nombre, array("Attachment"=>0));
  }


?>

<?php
session_start();
include_once "../config.php";
include_once "../assets/funciones/template.php";
include_once "../assets/funciones/searches.php";

sessionstatus();
$usu_tipo   = $_SESSION['usu_tipo'];
$emp_nombre = $_SESSION['usu_empleado'];
$nivel      = 0;

if (isset($_GET['folio'])) {

  $cot_folio  = $_GET['folio'];
  $cotizacion = dameDatosCotFolio($cot_folio);
  //$cliente    = dameDatosCliente($cotizacion['cot_cli_clave']);

}

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <title><?= C_PLATFORMNAME; ?> - Admin Cotización <?=$cot_folio;?></title>
        <meta content="Admin Dashboard" name="description" />
        <meta content="ThemeDesign" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <link rel="shortcut icon" href="../assets/images/favicon.ico">

        <!--Morris Chart CSS -->
        <link rel="stylesheet" href="../assets/plugins/morris/morris.css">

        <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/icons.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/style.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/myStyle.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs5/dt-1.10.25/datatables.min.css"/>
        <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
    </head>


    <body class="fixed-left">

        <!-- Loader -->
        <div id="preloader"><div id="status"><div class="spinner"></div></div></div>

        <!-- Begin page -->
        <div id="wrapper">

            <!-- ========== Left Sidebar Start ========== -->
            <div class="left side-menu">
                <button type="button" class="button-menu-mobile button-menu-mobile-topbar open-left waves-effect">
                    <i class="ion-close"></i>
                </button>

                <!-- LOGO -->
                <div class="topbar-left">
                    <div class="text-center">
                        <!--<a href="index.html" class="logo">Admiry</a>-->
                        <a href="../" class="logo">
                          <img src="../assets/images/logo.png" height="42" alt="logo">
                        </a>
                    </div>
                </div>

                <div class="sidebar-inner slimscrollleft">

                    <div class="user-details">
                        <div class="text-center">
                            <img src="../assets/images/users/avatar-1.jpg" alt="" class="rounded-circle">
                        </div>
                        <div class="user-info">
                            <h4 class="font-16 usuario" data-type="<?= $usu_tipo; ?>"><?= $emp_nombre; ?></h4>
                            <span class="text-muted user-status"><i class="fa fa-dot-circle-o text-success"></i> Online</span>
                        </div>
                    </div>

                    <?php printNavbar($usu_tipo, $nivel) ?>

                    <div class="clearfix"></div>
                </div> <!-- end sidebarinner -->
            </div>
            <!-- Left Sidebar End -->

            <!-- Start right Content here -->

            <div class="content-page">
                <!-- Start content -->
                <div class="content">

                    <!-- Top Bar Start -->
                    <div class="topbar">

                        <nav class="navbar-custom">

                            <ul class="list-inline float-right mb-0">
                              <?php printOptionAvatar($usu_tipo, $nivel); ?>
                            </ul>

                            <ul class="list-inline menu-left mb-0">
                                <li class="list-inline-item">
                                    <button type="button" class="button-menu-mobile open-left waves-effect">
                                        <i class="ion-navicon"></i>
                                    </button>
                                </li>
                                <li class="hide-phone list-inline-item app-search">
                                    <h3 class="page-title"></h3>
                                </li>
                            </ul>

                            <div class="clearfix"></div>

                        </nav>

                    </div>
                    <!-- Top Bar End -->

                    <div class="page-content-wrapper ">

                        <div class="container">
                          <div class="d-flex">
                            <div class="ml-auto">
                              <nav aria-label="breadcrumb">
                                <ol class="breadcrumb float-right">
                                  <li class="breadcrumb-item"><a href="../admin">Dashboard</a></li>
                                  <li class="breadcrumb-item" aria-current="page">Cotización</li>
                                  <li class="breadcrumb-item active" aria-current="page"><?= $cot_folio; ?></li>
                                </ol>
                              </nav>
                            </div>
                          </div>
                            <div class="row">

                                <div class="col-12">
                                  <div class="d-block">
                                    <div class="card m-b-20 mb-3">
                                      <div class="card-block">
                                        <h3 class="mt-0 m-b-5 cotizacion header-title d-flex" data-clave="<?= $cotizacion['cot_clave']; ?>">
                                          Cotización
                                          <span class="ml-auto">Folio: <span class="num-folio"><?= $cot_folio; ?></span></span>
                                        </h3>
                                        <div class="row">
                                          <div class="col-12">
                                            <button type="button" class="btn btn-secondary btn-sm btn-update-cliente" name="button"><i class="fas fa-user-edit"></i> Modificar datos</button>
                                          </div>
                                          <hr>
                                          <div class="col-12 col-lg-4">
                                            <p class="info"><strong>Fecha: </strong><?=$cotizacion['cot_fecha']?></p>
                                            <p class="info"><strong>Cliente: </strong><span class="cli-nombre"></span> </p>
                                          </div>
                                          <div class="col-12 col-lg-4">
                                            <p class="info"><strong>Correo: </strong> <span class="cli-correo"></span></p>
                                            <p class="info"><strong>Teléfono: </strong> <span class="cli-telf"></span></p>
                                          </div>
                                          <div class="col-12 col-lg-4">
                                            <p class="info"><strong>RFC: </strong><span class="cli-rfc"></span></p>
                                            <p class="info"><strong>Status: </strong><span class="status"></span></p>
                                          </div>
                                          <div class="col-12">
                                            <p class="info"><strong>Dirección: </strong><span class="cli_direccion"></span></p>
                                          </div>
                                        </div>
                                        <hr>
                                        <form id="form-add-product" class="" action="index.html" method="post">
                                          <div class="row">
                                            <div class="col-lg-8 col-12">
                                              <label>Producto*:</label>
                                              <select style="width: 100%;" class="form-control form-control-sm search producto" name="producto">
                                                <option value="">Selecciona los productos</option>
                                                <?php
                                                  $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
                                                  if (mysqli_connect_errno()) {
                                                    echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
                                                  }

                                                  $sql    = "SELECT pro_clave, pro_nombre
                                                                from productos
                                                                  where pro_status = 1
                                                                  order by pro_nombre asc";
                                                  $result = mysqli_query($con, $sql);
                                                  if ($result) {
                                                    while ($fila = mysqli_fetch_array($result)) {
                                                ?>
                                                <option value="<?=$fila[0];?>"><?=$fila[1];?></option>
                                                <?php
                                                    }
                                                  }
                                                  ?>
                                              </select>
                                              <small id="help-producto" class="help-producto form-text text-muted text-danger"></small>
                                            </div>
                                            <div class="col-lg-2">
                                              <label>Cantidad*:</label>
                                              <input type="number" step="any" min="0" class="form-control form-control-sm cantidad" name="cantidad">
                                              <small id="help-cantidad" class="help-cantidad form-text text-muted text-danger"></small>
                                            </div>
                                            <div class="col-lg-2">
                                              <label></label>
                                              <button type="submit" name="button" class="btn btn-primary btn-sm btn-block mt-2 waves-effect waves-light btn-addproducto">
                                                Agregar
                                              </button>
                                            </div>
                                          </div>
                                        </form>
                                      </div>
                                    </div>
                                  </div>

                                  <div class="d-block">
                                    <div class="card m-b-20">
                                      <div class="card-block">
                                        <div class="row">
                                          <div class="col-12">
                                            <!--button type="button" class="btn btn-secondary btn-sm waves-effect btn-enviarcorreo"><i class="mdi mdi-email-outline"></i> Enviar por correo</button-->
                                            <!--button type="button" class="btn btn-secondary btn-sm waves-effect" data-toggle="collapse" data-target="#collapseAddEnvio" aria-expanded="false" aria-controls="collapseExample"><i class="fas fa-shipping-fast"></i> Seleccionar zona para envio</button-->
                                            <button type="button" class="btn btn-secondary btn-sm waves-effect btn-generarpdf"><i class="mdi mdi-file-pdf-box"></i> Generar pdf</button>
                                            <button type="button" class="btn btn-secondary btn-sm waves-effect btn-cotivent"><i class="mdi mdi-cart-outline"></i> Terminar de cotizar </button>

                                            <div class="collapse mt-2" id="collapseAddEnvio">
                                              <div class="card card-body py-1 px-1">
                                                <div class="row">
                                                  <?php
                                                    $con = mysqli_connect(C_HOST, C_USERNAME,C_PASSWORD, C_DBNAME);
                                                    if (mysqli_connect_errno()) {
                                                      echo json_encode("Error en la conexion de mysql: " . mysqli_connect_error());
                                                    }

                                                    $sql    = "SELECT zon_clave, zon_nombre, zon_descripcion
                                                                    from zonas
                                                                      where zon_status = 1
                                                                      order by zon_nombre asc";
                                                    $result = mysqli_query($con, $sql);
                                                    if ($result) {
                                                      while ($fila = mysqli_fetch_array($result)) {
                                                  ?>
                                                  <div class="col-3">
                                                    <button type="button" class="btn btn-secondary btn-block btn-sm btn-zona" data-clave="<?= $fila[0]; ?>" name="button">
                                                      <div class="row">
                                                        <div class="col-3 text-center">
                                                          <h6><?= $fila[1]; ?></h6>
                                                        </div>
                                                        <div class="col-9">
                                                          <div class="row">
                                                            <div class="col-12 text-left">
                                                              <small class="d-block">
                                                                <strong>Lugares pertecientes a la zona:</strong>
                                                              </small>
                                                              <small class="d-block">
                                                                <?= $fila[2]; ?>
                                                              </small>
                                                            </div>
                                                          </div>
                                                        </div>
                                                      </div>
                                                    </button>
                                                  </div>
                                                  <?php
                                                      }
                                                    }
                                                  ?>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                          <div class="col-12 mt-1">
                                            <div class="table-responsive">
                                              <table id="dt-detalle" class="table table-hover m-b-0 table-sm">
                                                <thead>
                                                  <tr>
                                                    <th></th>
                                                    <th>Descripción</th>
                                                    <th>P.Unitario</th>
                                                    <th>Catidad</th>
                                                    <th>Subtotal</th>
                                                    <th>I.V.A.</th>
                                                    <th>Total</th>
                                                  </tr>
                                                </thead>
                                              </table>
                                            </div>
                                          </div>

                                          <div class="col-lg-12">
                                            <hr>
                                            <!--div class="row justify-content-end content-envio">
                                              <div class="col-12 col-lg-4 text-right">
                                                <div class="row">
                                                  <div class="col-6">
                                                    <p><strong>Envio  :</strong></p>
                                                  </div>
                                                  <div class="col-4">
                                                    <div class="row">
                                                      <div class="col-12">
                                                        <p class="text-cantidades"><span class="envio"></span></p>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                            </div-->
                                            <div class="row justify-content-end">
                                              <div class="col-12 col-lg-4 text-right">
                                                <div class="row">
                                                  <div class="col-6">
                                                    <p><strong>Total  :</strong></p>
                                                  </div>
                                                  <div class="col-4">
                                                    <div class="row">
                                                      <div class="col-12">
                                                        <p class="text-cantidades"><span class="total"></span></p>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                            </div>
                            <!-- end row -->

                        </div><!-- container -->

                        <?php
                          include_once "../assets/modal/caja.php";
                          include_once "../assets/modal/clientes.php";
                          include_once "../assets/modal/cotizaciones.php";
                        ?>
                        <?php  ?>

                    </div> <!-- Page content Wrapper -->

                </div> <!-- content -->

                <?= printFooter(); ?>

            </div>
            <!-- End Right content here -->

        </div>
        <!-- END wrapper -->


        <!-- jQuery  -->
        <script src="../assets/js/jquery.min.js"></script>
        <script src="../assets/js/tether.min.js"></script><!-- Tether for Bootstrap -->
        <script src="../assets/js/bootstrap.min.js"></script>
        <script src="../assets/js/modernizr.min.js"></script>
        <script src="../assets/js/detect.js"></script>
        <script src="../assets/js/fastclick.js"></script>
        <script src="../assets/js/jquery.slimscroll.js"></script>
        <script src="../assets/js/jquery.blockUI.js"></script>
        <script src="../assets/js/waves.js"></script>
        <script src="../assets/js/jquery.nicescroll.js"></script>
        <script src="../assets/js/jquery.scrollTo.min.js"></script>

        <!-- App js -->
        <script src="../assets/js/app.js"></script>
        <script src="../assets/js/constantes.js"></script>
        <script src="../assets/js/generales.js"></script>
        <script src="../assets/ajax/generales/revisar.js"></script>
        <script src="../assets/ajax/generales/cotizaciones/detalle.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.0.1/js/bootstrap.bundle.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/v/bs5/dt-1.10.25/datatables.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
        <script src="../assets/js/dataTablelanguage.js"></script>
        <script src="../assets/js/messages.js"></script>

        <script type="text/javascript">
          $('.search').select2();
        </script>

        <!-- Alertify js -->
        <script src="../assets/plugins/alertify/js/alertify.js"></script>

    </body>
</html>

-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 06-10-2021 a las 16:18:21
-- Versión del servidor: 10.4.17-MariaDB
-- Versión de PHP: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `puntoventa`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bitacora`
--

CREATE TABLE `bitacora` (
  `bit_clave` int(11) NOT NULL,
  `bit_modulo` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `bit_accion` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `bit_tipo` int(11) DEFAULT NULL,
  `bit_emp_clave` int(11) DEFAULT NULL,
  `bit_descripcion` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `bit_claves` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `bitacora`
--

INSERT INTO `bitacora` (`bit_clave`, `bit_modulo`, `bit_accion`, `bit_tipo`, `bit_emp_clave`, `bit_descripcion`, `bit_claves`) VALUES
(1, 'Catálogo', 'Actualización', 1, 1, 'Se modifico el puesto Almacenista por AlmacenistaX', 0),
(2, 'Catálogo', 'Actualización', 1, 1, 'Se modifico el puesto AlmacenistaX por Almacenista', 0),
(3, 'Catálogo', 'Eliminar', 1, 1, 'Se elimino el puesto Almacenista', 0),
(4, 'Catálogo', 'Eliminar', 1, 1, 'Se elimino el puesto Almacenista', 0),
(5, 'Catálogo', 'Eliminar', 1, 1, 'Se elimino el puesto Almacenista', 0),
(6, 'Catálogo', 'Eliminar', 1, 1, 'Se elimino el puesto Almacenista', 0),
(7, 'Catálogo', 'Registro', 1, 1, 'Se registro el puesto Almacenista', 0),
(8, 'Catálogo', 'Registro', 1, 1, 'Se registro el empleado Ismael Carranza Tronco', 0),
(9, 'Catálogo', 'Eliminar', 1, 1, 'Se elimino el empleado Ismael Carranza Tronco', 0),
(10, 'Catálogo', 'Eliminar', 1, 1, 'Se elimino el empleado Ismael Carranza Tronco', 0),
(11, 'Catálogo', 'Registro', 1, 1, 'Se registro el empleado Ismael Carranza Tronco', 0),
(12, 'Catálogo', 'Actualización', 1, 1, 'Se modifico información del empleado Ismael Carranza Tronco', 0),
(13, 'Catálogo', 'Actualización', 1, 1, 'Se modifico información del empleado Ismael Carranza Tronco', 0),
(14, 'Catálogo', 'Actualización', 1, 1, 'Se modifico información del empleado Ismael Carranza Tronco', 0),
(15, 'Catálogo', 'Actualización', 1, 1, 'Se modifico información del empleado Ismael Carranza Tronco', 0),
(16, 'Catálogo', 'Actualización', 1, 1, 'Se modifico información del empleado Ismael Carranza Tronco', 0),
(17, 'Catálogo', 'Actualización', 1, 1, 'Se modifico información del empleado Ismael Carranza Tronco', 0),
(18, 'Catálogo', 'Actualización', 1, 1, 'Se modifico información del empleado Ismael Carranza Tronco', 0),
(19, 'Catálogo', 'Actualización', 1, 1, 'Se modifico información del empleado Ismael Carranza Tronco', 0),
(20, 'Catálogo', 'Eliminar', 1, 1, 'Se elimino al usuario ', 0),
(21, 'Catálogo', 'Eliminar', 1, 1, 'Se suspendio al usuario 1', 0),
(22, 'Catálogo', 'Actualización', 1, 1, 'Se modifico el password del usuario 1', 0),
(23, 'Catálogo', 'Actualización', 1, 1, 'Se modifico información del usuario 1', 0),
(24, 'Catálogo', 'Actualización', 1, 1, 'Se modifico el password del usuario 1', 0),
(25, 'Catálogo', 'Actualización', 1, 1, 'Se modifico el password del usuario admin', 0),
(26, 'Catálogo', 'Actualización', 1, 1, 'Se modifico el password del usuario admin', 0),
(27, 'Catálogo', 'Actualización', 1, 1, 'Se modifico el password del usuario admin', 0),
(28, 'Catálogo', 'Registro', 1, 1, 'Se registro la categoria Dulces', 0),
(29, 'Catálogo', 'Registro', 1, 1, 'Se registro la categoria Galletas', 0),
(30, 'Catálogo', 'Eliminar', 1, 1, 'Se elimino la categoria Galletas', 0),
(31, 'Catálogo', 'Actualización', 1, 1, 'Se modifico la categoria  por Cervezas', 0),
(32, 'Catálogo', 'Actualización', 1, 1, 'Se modifico la categoria  por Refrescos', 0),
(33, 'Catálogo', 'Actualización', 1, 1, 'Se modifico la categoria Refrescos por Dulces', 0),
(34, 'Catálogo', 'Registro', 1, 1, 'Se registro la categoria Galletas', 0),
(35, 'Catálogo', 'Registro', 1, 1, 'Se registro la marca Coca cola', 0),
(36, 'Catálogo', 'Registro', 1, 1, 'Se registro la marca Coca cola', 0),
(37, 'Catálogo', 'Registro', 1, 1, 'Se registro la marca Coca cola', 0),
(38, 'Catálogo', 'Registro', 1, 1, 'Se registro la marca Coca cola', 0),
(39, 'Catálogo', 'Registro', 1, 1, 'Se registro la marca Coca cola', 0),
(40, 'Catálogo', 'Registro', 1, 1, 'Se registro la marca Coca cola', 0),
(41, 'Catálogo', 'Registro', 1, 1, 'Se registro la marca Pepsi', 0),
(42, 'Catálogo', 'Eliminar', 1, 1, 'Se elimino al usuario ', 0),
(43, 'Catálogo', 'Eliminar', 1, 1, 'Se elimino al usuario e', 0),
(44, 'Catálogo', 'Eliminar', 1, 1, 'Se elimino al usuario Pepsi', 0),
(45, 'Catálogo', 'Registro', 1, 1, 'Se registro la marca Pepsi', 0),
(46, 'Catálogo', 'Actualización', 1, 1, 'Se modifico la marca Pepsi por Pepsi x', 0),
(47, 'Catálogo', 'Actualización', 1, 1, 'Se modifico la marca Pepsi x por Pepsi', 0),
(48, 'Catálogo', 'Registro', 1, 1, 'Se registro el proveedor Ismael Carranza Tronco', 0),
(49, 'Catálogo', 'Registro', 1, 1, 'Se registro el proveedor Pruebas Pruebas Pruebas', 0),
(50, 'Catálogo', 'Registro', 1, 1, 'Se registro el proveedor Daniel Nsiau Jdbcibna', 0),
(51, 'Catálogo', 'Registro', 1, 1, 'Se registro el proveedor Adjsak Ksdnaovñ Lkzdfkmval', 0),
(52, 'Catálogo', 'Eliminar', 1, 1, 'Se elimino el proveedor Pruebas Pruebas Pruebas', 0),
(53, 'Catálogo', 'Eliminar', 1, 1, 'Se elimino el proveedor Pruebas Pruebas Pruebas', 0),
(54, 'Catálogo', 'Registro', 1, 1, 'Se registro la marca Lala', 0),
(55, 'Catálogo', 'Actualización', 1, 1, 'Se modifico información del proveedor ', 0),
(56, 'Catálogo', 'Actualización', 1, 1, 'Se modifico información del proveedor ', 0),
(57, 'Catálogo', 'Actualización', 1, 1, 'Se modifico información del proveedor A Ksdnaovñ Lkzdfkmval', 0),
(58, 'Catálogo', 'Actualización', 1, 1, 'Se modifico información del proveedor A Ksdnaovñ Lkzdfkmval', 0),
(59, 'Catálogo', 'Registro', 1, 1, 'Se registro el producto Agua Oxigenada - Balmen', 0),
(60, 'Catálogo', 'Registro', 1, 1, 'Se registro el producto Agua Oxigenada - Balmen', 0),
(61, 'Catálogo', 'Registro', 1, 1, 'Se registro el producto Agua Oxigenada - Balmen', 0),
(62, 'Catálogo', 'Registro', 1, 1, 'Se registro la categoria Antisépticos', 0),
(63, 'Catálogo', 'Registro', 1, 1, 'Se asigno la categoria Antisépticos al producto Botella 112 ml - Agua Oxigenada - Balmen', 0),
(64, 'Catálogo', 'Eliminar', 1, 1, 'Se elimino la categoria Antisépticos del producto Botella 112 ml - Agua Oxigenada - Balmen', 0),
(65, 'Catálogo', 'Eliminar', 1, 1, 'Se elimino el producto Botella 112 ml - Agua Oxigenada - Balmen', 0),
(66, 'Catálogo', 'Eliminar', 1, 1, 'Se elimino el producto Botella 112 ml - Agua Oxigenada - Balmen', 0),
(67, 'Catálogo', 'Eliminar', 1, 1, 'Se elimino el producto Botella 112 ml - Agua Oxigenada - Balmen', 0),
(68, 'Catálogo', 'Actualización', 1, 1, 'Se modifico información del producto Agua Oxigenada - Balmen', 0),
(69, 'Catálogo', 'Actualización', 1, 1, 'Se modifico información del producto Agua Oxigenada - Balmen', 0),
(70, 'Catálogo', 'Eliminar', 1, 1, 'Se elimino al usuario admin', 0),
(71, 'Catálogo', 'Actualización', 1, 1, 'Se modifico el password del usuario admin', 0),
(72, 'Sesión', 'Incio de sesión', 0, 1, 'Ismael Carranza inicio sesión', 0),
(73, 'Sesión', 'Incio de sesión', 0, 1, 'Ismael Carranza inicio sesión', 0),
(74, 'Sesión', 'Incio de sesión', 0, 1, 'Ismael Carranza inicio sesión', 0),
(75, 'Sesión', 'Incio de sesión', 0, 1, 'Ismael Carranza inicio sesión', 0),
(76, 'Sesión', 'Incio de sesión', 0, 1, 'Ismael Carranza inicio sesión', 0),
(77, 'Sesión', 'Incio de sesión', 0, 1, 'Ismael Carranza inicio sesión', 0),
(78, 'Sesión', 'Fin de sesión', 0, 1, 'Ismael Carranza cerro sesión', 0),
(79, 'Sesión', 'Fin de sesión', 0, 1, 'Ismael Carranza cerro sesión', 0),
(80, 'Sesión', 'Incio de sesión', 0, 1, 'Ismael Carranza inicio sesión', 0),
(81, 'Sesión', 'Fin de sesión', 0, 1, 'Ismael Carranza cerro sesión', 0),
(82, 'Sesión', 'Incio de sesión', 0, 1, 'Ismael Carranza inicio sesión', 0),
(83, 'Sesión', 'Fin de sesión', 0, 1, 'Ismael Carranza cerro sesión', 0),
(84, 'Sesión', 'Fin de sesión', 0, 0, 'team cerro sesión', 0),
(85, 'Sesión', 'Fin de sesión', 0, 0, 'teamMono cerro sesión', 0),
(86, 'Sesión', 'Fin de sesión', 0, 0, 'teamMono cerro sesión', 0),
(87, 'Sesión', 'Incio de sesión', 0, 1, 'Ismael Carranza inicio sesión', 0),
(88, 'Sesión', 'Incio de sesión', 0, 1, 'Ismael Carranza inicio sesión', 0),
(89, 'Sesión', 'Fin de sesión', 0, 1, 'Ismael Carranza cerro sesión', 0),
(90, 'Sesión', 'Incio de sesión', 0, 1, 'Ismael Carranza inicio sesión', 0),
(91, 'Sesión', 'Incio de sesión', 0, 1, 'Ismael Carranza inicio sesión', 0),
(92, 'Sesión', 'Incio de sesión', 0, 1, 'Ismael Carranza inicio sesión', 0),
(93, 'Sesión', 'Incio de sesión', 0, 1, 'Ismael Carranza inicio sesión', 0),
(94, 'Sesión', 'Incio de sesión', 0, 1, 'Ismael Carranza inicio sesión', 0),
(95, 'Sesión', 'Incio de sesión', 0, 1, 'Ismael Carranza inicio sesión', 0),
(96, 'Sesión', 'Fin de sesión', 0, 131, ' cerro sesión', 0),
(97, 'Sesión', 'Incio de sesión', 0, 1, 'Ismael Carranza inicio sesión', 0),
(98, 'Sesión', 'Fin de sesión', 0, 131, ' cerro sesión', 0),
(99, 'Sesión', 'Incio de sesión', 0, 1, 'Ismael Carranza inicio sesión', 0),
(100, 'Caja', 'Registro', 2, 1, 'Se abrio el corte de caja con folio 0001', 0),
(101, 'Caja', 'Registro', 2, 1, 'Se abrio el corte de caja con folio 0001', 0),
(102, 'Sesión', 'Incio de sesión', 0, 1, 'Ismael Carranza inicio sesión', 0),
(103, 'Sesión', 'Incio de sesión', 0, 1, 'Ismael Carranza inicio sesión', 0),
(104, 'Caja', 'Salida', 2, 1, 'Se registro una salida con monto 100 para la caja0001', 0),
(105, 'Caja', 'Salida', 2, 1, 'Se registro una salida con monto 5 para la caja0001', 0),
(106, 'Caja', 'Salida', 2, 1, 'Se registro una salida con monto 50 para la caja0001', 0),
(107, 'Sesión', 'Incio de sesión', 0, 1, 'Ismael Carranza inicio sesión', 0),
(108, 'Cotización', 'Resgistro de producto', 3, 1, 'Se agrego el producto: Agua Oxigenada - Balmen a la cotización 0010', 0),
(109, 'Cotización', 'Resgistro de producto', 3, 1, 'Se agrego el producto: Agua Oxigenada - Balmen a la cotización 0010', 0),
(110, 'Catálogo', 'Registro', 1, 1, 'Se registro el cliente P??blico en general', 0),
(111, 'Cotización', 'Nueva cotización', 1, 1, 'Se creo cotización con folio 0001 para el cliente Público en general', 0),
(112, 'Caja', 'Registro', 2, 1, 'Se abrio el corte de caja con folio 0001', 0),
(113, 'Cotización', 'Nueva cotización', 1, 1, 'Se creo cotización con folio 0001 para el cliente Público en general', 0),
(114, 'Cotización', 'Nueva cotización', 1, 1, 'Se creo cotización con folio 0001 para el cliente Público en general', 0),
(115, 'Cotización', 'Nueva cotización', 1, 1, 'Se creo cotización con folio 0001 para el cliente Público en general', 0),
(116, 'Cotización', 'Resgistro de producto', 3, 1, 'Se agrego el producto: Agua Oxigenada - Balmen a la cotización 0001', 0),
(117, 'Cotización', 'Resgistro de producto', 3, 1, 'Se agrego el producto: Agua Oxigenada - Balmen a la cotización 0001', 0),
(118, 'Catálogo', 'Registro', 1, 1, 'Se registro el producto 3D white - Oral B', 0),
(119, 'Catálogo', 'Registro', 1, 1, 'Se registro la categoria Pasta dental', 0),
(120, 'Catálogo', 'Registro', 1, 1, 'Se asigno la categoria Pasta dental al producto Paquete 180 g - 3D white - Oral B', 0),
(121, 'Catálogo', 'Registro', 1, 1, 'Se registro el producto Alcohol Etílico - Atlanta', 0),
(122, 'Catálogo', 'Registro', 1, 1, 'Se asigno la categoria Antisépticos al producto Botella 250 ml - Alcohol Etílico - Atlanta', 0),
(123, 'Catálogo', 'Registro', 1, 1, 'Se registro la categoria Suspensión', 0),
(124, 'Catálogo', 'Registro', 1, 1, 'Se registro el producto Dexametasona - Alin Depot', 0),
(125, 'Catálogo', 'Registro', 1, 1, 'Se asigno la categoria Suspensión al producto Caja 2 ml - Dexametasona - Alin Depot', 0),
(126, 'Cotización', 'Cotización a Venta', 3, 1, 'Se convirtio la cotizacion 0001a la venta ', 0),
(127, 'Cotización', 'Cotización a Venta', 3, 1, 'Se convirtio la cotizacion 0001a la venta 0001', 0),
(128, 'Catálogo', 'Actualización', 1, 1, 'Se modifico información del producto 3D white - Oral B', 0),
(129, 'Catálogo', 'Eliminar', 1, 1, 'Se elimino el producto Paquete 2 pza. - 3D white - Oral B', 0),
(130, 'Catálogo', 'Eliminar', 1, 1, 'Se elimino el producto Paquete 2 pza. - 3D white - Oral B', 0),
(131, 'Catálogo', 'Eliminar', 1, 1, 'Se elimino el producto Paquete 2 pza. - 3D white - Oral B', 0),
(132, 'Catálogo', 'Eliminar', 1, 1, 'Se elimino el producto Paquete 2 pza. - 3D white - Oral B', 0),
(133, 'Catálogo', 'Actualización', 1, 1, 'Se modifico información del producto 3D white - Oral B', 0),
(134, 'Catálogo', 'Registro', 1, 1, 'Se registro la categoria  Antibióticos', 0),
(135, 'Catálogo', 'Registro', 1, 1, 'Se asigno la categoria  Antibióticos al producto Caja 20 pza. - Treda 129mg/280mg/30mg', 0),
(136, 'Catálogo', 'Actualización', 1, 1, 'Se modifico información del producto Treda 129mg/280mg/30mg', 0),
(137, 'Catálogo', 'Actualización', 1, 1, 'Se modifico información del producto Treda 129mg/280mg/30mg', 0),
(138, 'Catálogo', 'Registro', 1, 1, 'Se asigno la categoria  Antibióticos al producto Botella 75 ml - Treda Suspensión', 0),
(139, 'Catálogo', 'Registro', 1, 1, 'Se asigno la categoria  Antibióticos al producto Caja 20 pza. - Treda 129mg/280mg/30mg Tabletas', 0),
(140, 'Catálogo', 'Registro', 1, 1, 'Se asigno la categoria  Antibióticos al producto Botella 75 ml - Treda Suspensión', 0),
(141, 'Catálogo', 'Registro', 1, 1, 'Se registro la categoria Accesorios Médicos', 0),
(142, 'Catálogo', 'Actualización', 1, 1, 'Se modifico información del producto Treda 129mg/280mg/30mg', 0),
(143, 'Catálogo', 'Registro', 1, 1, 'Se asigno la categoria Accesorios Médicos al producto Caja 1 pza. - Collarín Le Roy Cervical Grande', 0),
(144, 'Catálogo', 'Registro', 1, 1, 'Se asigno la categoria Accesorios Médicos al producto Caja 20 pza. - Treda 129mg/280mg/30mg', 0),
(145, 'Catálogo', 'Actualización', 1, 1, 'Se modifico información del producto Treda 129mg/280mg/30mg', 0),
(146, 'Catálogo', 'Actualización', 1, 1, 'Se modifico información del producto Collarín Le Roy Cervical Grande', 0),
(147, 'Catálogo', 'Actualización', 1, 1, 'Se modifico información del producto Termómetro Citizen Digital CTA 303', 0),
(148, 'Catálogo', 'Actualización', 1, 1, 'Se modifico información del producto Treda 129mg/280mg/30mg Tabletas', 0),
(149, 'Catálogo', 'Actualización', 1, 1, 'Se modifico información del producto Treda 129mg/280mg/30mg Tabletas', 0),
(150, 'Catálogo', 'Actualización', 1, 1, 'Se modifico información del producto Treda Suspensión', 0),
(151, 'Catálogo', 'Registro', 1, 1, 'Se registro el producto Termómetro Digital de Frente Omron MC-720', 0),
(152, 'Catálogo', 'Registro', 1, 1, 'Se asigno la categoria Accesorios Médicos al producto Pieza 1 pza. - Termómetro Digital de Frente Omron MC-720', 0),
(153, 'Catálogo', 'Registro', 1, 1, 'Se registro el producto Termómetro Digital Sunshine con Punta Flexible', 0),
(154, 'Catálogo', 'Registro', 1, 1, 'Se asigno la categoria Accesorios Médicos al producto Pieza 1 pza. - Termómetro Digital Sunshine con Punta Flexible', 0),
(155, 'Catálogo', 'Registro', 1, 1, 'Se registro el producto Oxímetro Digital de Pulso LMT-01', 0),
(156, 'Catálogo', 'Registro', 1, 1, 'Se asigno la categoria Accesorios Médicos al producto Pieza 1 pza. - Oxímetro Digital de Pulso LMT-01', 0),
(157, 'Catálogo', 'Registro', 1, 1, 'Se registro el producto Termómetro Digital de Pluma', 0),
(158, 'Catálogo', 'Registro', 1, 1, 'Se asigno la categoria Accesorios Médicos al producto Pieza 1 pza. - Termómetro Digital de Pluma', 0),
(159, 'Catálogo', 'Registro', 1, 1, 'Se registro el producto Termómetro Infrarrojo Hi8us HG01 de Frente', 0),
(160, 'Catálogo', 'Registro', 1, 1, 'Se asigno la categoria Accesorios Médicos al producto Pieza 1 pza. - Termómetro Infrarrojo Hi8us HG01 de Frente', 0),
(161, 'Catálogo', 'Registro', 1, 1, 'Se registro el producto Oxímetro Digital de Pulso Xignal para Adulto', 0),
(162, 'Catálogo', 'Registro', 1, 1, 'Se asigno la categoria Accesorios Médicos al producto Pieza 1 pza. - Oxímetro Digital de Pulso Xignal para Adulto', 0),
(163, 'Catálogo', 'Registro', 1, 1, 'Se registro el producto Oxímetro de Pulso Levy and Lulu Digital', 0),
(164, 'Catálogo', 'Registro', 1, 1, 'Se asigno la categoria Accesorios Médicos al producto Pieza 1 pza. - Oxímetro de Pulso Levy and Lulu Digital', 0),
(165, 'Catálogo', 'Registro', 1, 1, 'Se registro el producto Monitor Omron Presión Arterial Automático Brazo', 0),
(166, 'Catálogo', 'Registro', 1, 1, 'Se asigno la categoria Accesorios Médicos al producto Pieza 1 pza. - Monitor Omron Presión Arterial Automático Brazo', 0),
(167, 'Catálogo', 'Registro', 1, 1, 'Se registro el producto Nebulizador de Compresor Omron Ne-C801LA', 0),
(168, 'Catálogo', 'Registro', 1, 1, 'Se asigno la categoria Accesorios Médicos al producto Pieza 1 pza. - Nebulizador de Compresor Omron Ne-C801LA', 0),
(169, 'Catálogo', 'Registro', 1, 1, 'Se registro el producto Nebulizador Omron de Compresor Mod NE-C101', 0),
(170, 'Catálogo', 'Registro', 1, 1, 'Se asigno la categoria Accesorios Médicos al producto Pieza 1 pza. - Nebulizador Omron de Compresor Mod NE-C101', 0),
(171, 'Catálogo', 'Registro', 1, 1, 'Se registro el producto Baumanómetro Citizen Ch-617', 0),
(172, 'Catálogo', 'Registro', 1, 1, 'Se asigno la categoria Accesorios Médicos al producto Pieza 1 pza. - Baumanómetro Citizen Ch-617', 0),
(173, 'Catálogo', 'Registro', 1, 1, 'Se registro el producto Nebulizador Nebucor P-103', 0),
(174, 'Catálogo', 'Registro', 1, 1, 'Se asigno la categoria Accesorios Médicos al producto Pieza 1 pza. - Nebulizador Nebucor P-103', 0),
(175, 'Catálogo', 'Registro', 1, 1, 'Se registro el producto Baumanómetro Citizen Ch-452 Brazo', 0),
(176, 'Catálogo', 'Registro', 1, 1, 'Se asigno la categoria Accesorios Médicos al producto Pieza 1 pza. - Baumanómetro Citizen Ch-452 Brazo', 0),
(177, 'Catálogo', 'Registro', 1, 1, 'Se registro el producto Nebulizador Nebucor De Compresor P102', 0),
(178, 'Catálogo', 'Registro', 1, 1, 'Se asigno la categoria Accesorios Médicos al producto Pieza 1 pza. - Nebulizador Nebucor De Compresor P102', 0),
(179, 'Catálogo', 'Registro', 1, 1, 'Se registro el producto Termómetro Infrarrojo Levy and Lulu a Distancia', 0),
(180, 'Catálogo', 'Registro', 1, 1, 'Se asigno la categoria Accesorios Médicos al producto Pieza 1 pza. - Termómetro Infrarrojo Levy and Lulu a Distancia', 0),
(181, 'Catálogo', 'Registro', 1, 1, 'Se registro el producto Kit Para Nebulizador Sun Shine 6pzas', 0),
(182, 'Catálogo', 'Registro', 1, 1, 'Se asigno la categoria Accesorios Médicos al producto Bolsa 1 pza. - Kit Para Nebulizador Sun Shine 6pzas', 0),
(183, 'Catálogo', 'Registro', 1, 1, 'Se registro el producto Monitor Neutek de Presión Sanguínea Brazo Digital', 0),
(184, 'Catálogo', 'Registro', 1, 1, 'Se asigno la categoria Accesorios Médicos al producto Pieza 1 pza. - Monitor Neutek de Presión Sanguínea Brazo Digital', 0),
(185, 'Catálogo', 'Registro', 1, 1, 'Se registro el producto Termómetro Omron Digital MC-246', 0),
(186, 'Catálogo', 'Registro', 1, 1, 'Se asigno la categoria Accesorios Médicos al producto Pieza 1 pza. - Termómetro Omron Digital MC-246', 0),
(187, 'Catálogo', 'Registro', 1, 1, 'Se registro el producto Monitor Omron de Presión Arterial Automático Muñeca con 30 Memorias', 0),
(188, 'Catálogo', 'Registro', 1, 1, 'Se asigno la categoria Accesorios Médicos al producto Pieza 1 pza. - Monitor Omron de Presión Arterial Automático Muñeca con 30 Memorias', 0),
(189, 'Catálogo', 'Registro', 1, 1, 'Se registro el producto Termómetro Digital Citizen de Pluma', 0),
(190, 'Catálogo', 'Registro', 1, 1, 'Se asigno la categoria Accesorios Médicos al producto Pieza 1 pza. - Termómetro Digital Citizen de Pluma', 0),
(191, 'Catálogo', 'Registro', 1, 1, 'Se registro el producto Termómetro Digital Contra el Agua MT 201C', 0),
(192, 'Catálogo', 'Registro', 1, 1, 'Se asigno la categoria Accesorios Médicos al producto Pieza 1 pza. - Termómetro Digital Contra el Agua MT 201C', 0),
(193, 'Catálogo', 'Registro', 1, 1, 'Se registro el producto Baumanómetro de Muñeca Nebucor HL168F', 0),
(194, 'Catálogo', 'Registro', 1, 1, 'Se asigno la categoria Accesorios Médicos al producto Pieza 1 pza. - Baumanómetro de Muñeca Nebucor HL168F', 0),
(195, 'Catálogo', 'Registro', 1, 1, 'Se registro el producto Férula Flents Insty Splint Para Dedos 2 Piezas Mediano/Grande', 0),
(196, 'Catálogo', 'Registro', 1, 1, 'Se asigno la categoria Accesorios Médicos al producto Paquete 1 pza. - Férula Flents Insty Splint Para Dedos 2 Piezas Mediano/Grande', 0),
(197, 'Catálogo', 'Registro', 1, 1, 'Se registro el producto Monitor Neutek De Presión Sanguínea Para Muñeca Automático', 0),
(198, 'Catálogo', 'Registro', 1, 1, 'Se asigno la categoria Accesorios Médicos al producto Pieza 1 pza. - Monitor Neutek De Presión Sanguínea Para Muñeca Automático', 0),
(199, 'Catálogo', 'Registro', 1, 1, 'Se registro el producto Monitor Omron de Presión Arterial Automático Muñeca', 0),
(200, 'Catálogo', 'Registro', 1, 1, 'Se asigno la categoria Accesorios Médicos al producto Pieza 1 pza. - Monitor Omron de Presión Arterial Automático Muñeca', 0),
(201, 'Catálogo', 'Registro', 1, 1, 'Se registro el producto Baumanómetro Nebucor HL888JA Brazo', 0),
(202, 'Catálogo', 'Registro', 1, 1, 'Se asigno la categoria Accesorios Médicos al producto Pieza 1 pza. - Baumanómetro Nebucor HL888JA Brazo', 0),
(203, 'Catálogo', 'Registro', 1, 1, 'Se registro el producto Termómetro Infrarrojo HG01', 0),
(204, 'Catálogo', 'Registro', 1, 1, 'Se asigno la categoria Accesorios Médicos al producto Pieza 1 pza. - Termómetro Infrarrojo HG01', 0),
(205, 'Catálogo', 'Registro', 1, 1, 'Se registro el producto Termómetro Omron Digital Flex MC 343F', 0),
(206, 'Catálogo', 'Registro', 1, 1, 'Se asigno la categoria Accesorios Médicos al producto Pieza 1 pza. - Termómetro Omron Digital Flex MC 343F', 0),
(207, 'Catálogo', 'Registro', 1, 1, 'Se registro el producto Nebulizador Sun Shine Flujo Continuo', 0),
(208, 'Catálogo', 'Registro', 1, 1, 'Se asigno la categoria Accesorios Médicos al producto Pieza 1 pza. - Nebulizador Sun Shine Flujo Continuo', 0),
(209, 'Catálogo', 'Registro', 1, 1, 'Se registro el producto Nebulizador Neutek Hi-Neb de Pistón', 0),
(210, 'Catálogo', 'Registro', 1, 1, 'Se asigno la categoria Accesorios Médicos al producto Pieza 1 pza. - Nebulizador Neutek Hi-Neb de Pistón', 0),
(211, 'Catálogo', 'Registro', 1, 1, 'Se registro el producto Monitor Omron de Presión Arterial de Muñeca Automático Mod HEM-6230', 0),
(212, 'Catálogo', 'Registro', 1, 1, 'Se asigno la categoria Accesorios Médicos al producto Pieza 1 pza. - Monitor Omron de Presión Arterial de Muñeca Automático Mod HEM-6230', 0),
(213, 'Catálogo', 'Registro', 1, 1, 'Se registro el producto Muñequera Para Pulgar Barrere Mediano', 0),
(214, 'Catálogo', 'Registro', 1, 1, 'Se asigno la categoria Accesorios Médicos al producto Pieza 1 pza. - Muñequera Para Pulgar Barrere Mediano', 0),
(215, 'Catálogo', 'Registro', 1, 1, 'Se registro el producto Nebulizador Sun Shine con Pistón', 0),
(216, 'Catálogo', 'Registro', 1, 1, 'Se asigno la categoria Accesorios Médicos al producto Pieza 1 pza. - Nebulizador Sun Shine con Pistón', 0),
(217, 'Catálogo', 'Registro', 1, 1, 'Se registro el producto Rodillera Elástica Rekord Le Roy Mediana', 0),
(218, 'Catálogo', 'Registro', 1, 1, 'Se asigno la categoria Accesorios Médicos al producto Pieza 1 pza. - Rodillera Elástica Rekord Le Roy Mediana', 0),
(219, 'Catálogo', 'Registro', 1, 1, 'Se registro el producto Tobillera Elástica Rekord Le Roy Mediana', 0),
(220, 'Catálogo', 'Registro', 1, 1, 'Se asigno la categoria Accesorios Médicos al producto Pieza 1 pza. - Tobillera Elástica Rekord Le Roy Mediana', 0),
(221, 'Catálogo', 'Registro', 1, 1, 'Se registro el producto Rodillera Elástica Rekord Le Roy Grande', 0),
(222, 'Catálogo', 'Registro', 1, 1, 'Se asigno la categoria Accesorios Médicos al producto Pieza 1 pza. - Rodillera Elástica Rekord Le Roy Grande', 0),
(223, 'Catálogo', 'Registro', 1, 1, 'Se registro el producto Collarín Barrere Blando Beige Grande', 0),
(224, 'Catálogo', 'Registro', 1, 1, 'Se asigno la categoria Accesorios Médicos al producto Pieza 1 pza. - Collarín Barrere Blando Beige Grande', 0),
(225, 'Sesión', 'Incio de sesión', 0, 1, 'Ismael Carranza inicio sesión', 0),
(226, 'Sesión', 'Incio de sesión', 0, 1, 'Ismael Carranza inicio sesión', 0),
(227, 'Cotización', 'Nueva cotización', 3, 1, 'Se creo la cotización 0001 para el cliente Público en general', 0),
(228, 'Cotización', 'Cotización a Venta', 3, 1, 'Se convirtio la cotizacion 0004a la venta 0001', 0),
(229, 'Cotización', 'Resgistro de producto', 3, 1, 'Se agrego el producto: Baumanómetro Citizen Ch-452 Brazo a la cotización 0003', 0),
(230, 'Cotización', 'Cotización a Venta', 3, 1, 'Se convirtio la cotizacion 0003a la venta error', 0),
(231, 'Cotización', 'Cotización a Venta', 3, 1, 'Se convirtio la cotizacion 0003a la venta error', 0),
(232, 'Cotización', 'Cotización a Venta', 3, 1, 'Se convirtio la cotizacion 0003a la venta ', 0),
(233, 'Cotización', 'Cotización a Venta', 3, 1, 'Se convirtio la cotizacion 0003a la venta ', 0),
(234, 'Cotización', 'Cotización a Venta', 3, 1, 'Se convirtio la cotizacion 0003a la venta ', 0),
(235, 'Cotización', 'Cotización a Venta', 3, 1, 'Se convirtio la cotizacion 0003a la venta ', 0),
(236, 'Cotización', 'Cotización a Venta', 3, 1, 'Se convirtio la cotizacion 0003a la venta ', 0),
(237, 'Cotización', 'Cotización a Venta', 3, 1, 'Se convirtio la cotizacion 0003a la venta ', 0),
(238, 'Cotización', 'Cotización a Venta', 3, 1, 'Se convirtio la cotizacion 0003a la venta ', 0),
(239, 'Cotización', 'Cotización a Venta', 3, 1, 'Se convirtio la cotizacion 0003a la venta 1', 0),
(240, 'Cotización', 'Cotización a Venta', 3, 1, 'Se convirtio la cotizacion 0003a la venta ', 0),
(241, 'Cotización', 'Cotización a Venta', 3, 1, 'Se convirtio la cotizacion 0003a la venta ', 0),
(242, 'Cotización', 'Cotización a Venta', 3, 1, 'Se convirtio la cotizacion 0003a la venta 0001', 0),
(243, 'Cotización', 'Cotización a Venta', 3, 1, 'Se convirtio la cotizacion 0003a la venta 0001', 0),
(244, 'Sesión', 'Incio de sesión', 0, 1, 'Ismael Carranza inicio sesión', 0),
(245, 'Cotización', 'Nueva cotización', 1, 1, 'Se creo cotización con folio  para el cliente Juan López', 0),
(246, 'Cotización', 'Nueva cotización', 1, 1, 'Se creo cotización con folio  para el cliente Juan López', 0),
(247, 'Cotización', 'Nueva cotización', 1, 1, 'Se creo cotización con folio  para el cliente Juan López', 0),
(248, 'Cotización', 'Nueva cotización', 1, 1, 'Se creo cotización con folio  para el cliente Juan López', 0),
(249, 'Cotización', 'Nueva cotización', 1, 1, 'Se creo cotización con folio 0001 para el cliente Juan López', 0),
(250, 'Cotización', 'Nueva cotización', 1, 1, 'Se creo cotización con folio 0001 para el cliente Público en general', 0),
(251, 'Cotización', 'Nueva cotización', 1, 1, 'Se creo cotización con folio 0001 para el cliente Público en general', 0),
(252, 'Cotización', 'Resgistro de producto', 3, 1, 'Se agrego el producto: Baumanómetro Citizen Ch-452 Brazo a la cotización 0002', 0),
(253, 'Cotización', 'Resgistro de producto', 3, 1, 'Se agrego el producto: Rodillera Elástica Rekord Le Roy Mediana a la cotización 0002', 0),
(254, 'Cotización', 'Cotización a Venta', 3, 1, 'Se convirtio la cotizacion 0002a la venta 0001', 0),
(255, 'Cotización', 'Nueva cotización', 3, 1, 'Se creo la cotización 0003', 0),
(256, 'Sesión', 'Incio de sesión', 0, 1, 'Ismael Carranza inicio sesión', 0),
(257, 'Sesión', 'Fin de sesión', 0, 131, 'Ismael Carranza cerro sesión', 0),
(258, 'Sesión', 'Incio de sesión', 0, 1, 'Ismael Carranza inicio sesión', 0),
(259, 'Sesión', 'Incio de sesión', 0, 1, 'Ismael Carranza inicio sesión', 0),
(260, 'Sesión', 'Incio de sesión', 0, 1, 'Ismael Carranza inicio sesión', 0),
(261, 'Cotización', 'Resgistro de producto', 3, 1, 'Se agrego el producto: Baumanómetro Citizen Ch-452 Brazo a la cotización 0003', 0),
(262, 'Cotización', 'Resgistro de producto', 3, 1, 'Se agrego el producto: Treda 129mg/280mg/30mg 20 Tabletas a la cotización 0003', 0),
(263, 'Cotización', 'Resgistro de producto', 3, 1, 'Se agrego el producto: Rodillera Elástica Rekord Le Roy Mediana a la cotización 0003', 0),
(264, 'Cotización', 'Nueva cotización', 3, 1, 'Se creo la cotización 0001', 0),
(265, 'Cotización', 'Resgistro de producto', 3, 1, 'Se agrego el producto: Kit Para Nebulizador Sun Shine 6pzas a la cotización 0001', 0),
(266, 'Cotización', 'Resgistro de producto', 3, 1, 'Se agrego el producto: Oxímetro de Pulso Levy and Lulu Digital a la cotización 0001', 0),
(267, 'Cotización', 'Resgistro de producto', 3, 1, 'Se agrego el producto: Treda 129mg/280mg/30mg 20 Tabletas a la cotización 0001', 0),
(268, 'Cotización', 'Resgistro de producto', 3, 1, 'Se agrego el producto: Collarín Le Roy Cervical Grande a la cotización 0001', 0),
(269, 'Cotización', 'Cotización a Venta', 3, 1, 'Se convirtio la cotizacion 0001a la venta 0001', 0),
(270, 'Cotización', 'Nueva cotización', 3, 1, 'Se creo la cotización 0002', 0),
(271, 'Venta', 'Resgistro de producto', 3, 1, 'Se agrego el producto: Monitor Omron Presión Arterial Automático Brazo a la venta 0001', 0),
(272, 'Venta', 'Resgistro de producto', 3, 1, 'Se agrego el producto: Collarín Barrere Blando Beige Grande a la venta 0001', 0),
(273, 'Sesión', 'Incio de sesión', 0, 1, 'Ismael Carranza inicio sesión', 0),
(274, 'Sesión', 'Incio de sesión', 0, 1, 'Ismael Carranza inicio sesión', 0),
(275, 'Sesión', 'Fin de sesión', 0, 134, 'Ismael Carranza cerro sesión', 0),
(276, 'Sesión', 'Incio de sesión', 0, 1, 'Ismael Carranza inicio sesión', 0),
(277, 'Cotización', 'Cotización a Venta', 3, 1, 'Se convirtio la cotizacion 0001a la venta 0002', 0),
(278, 'Sesión', 'Incio de sesión', 0, 1, 'Ismael Carranza inicio sesión', 0),
(279, 'Cotización', 'Resgistro de producto', 3, 1, 'Se agrego el producto: Termómetro Omron Digital MC-246 a la cotización 0002', 0),
(280, 'Cotización', 'Resgistro de producto', 3, 1, 'Se agrego el producto: Monitor Omron de Presión Arterial de Muñeca Automático Mod HEM-6230 a la cotización 0002', 0),
(281, 'Cotización', 'Cotización a Venta', 3, 1, 'Se convirtio la cotizacion 0002a la venta 0003', 0),
(282, 'Sesión', 'Incio de sesión', 0, 1, 'Ismael Carranza inicio sesión', 0),
(283, 'Caja', 'Registro', 2, 1, 'Se abrio el corte de caja con folio ', 0),
(284, 'Cotización', 'Nueva cotización', 3, 1, 'Se creo la cotización 0001', 0),
(285, 'Cotización', 'Resgistro de producto', 3, 1, 'Se agrego el producto: Termómetro Digital de Pluma a la cotización 0001', 0),
(286, 'Cotización', 'Resgistro de producto', 3, 1, 'Se agrego el producto: Kit Para Nebulizador Sun Shine 6pzas a la cotización 0001', 0),
(287, 'Cotización', 'Cotización a Venta', 3, 1, 'Se convirtio la cotizacion 0001a la venta 0001', 0),
(288, 'Cotización', 'Nueva cotización', 3, 1, 'Se creo la cotización 0002', 0),
(289, 'Cotización', 'Resgistro de producto', 3, 1, 'Se agrego el producto: Monitor Omron Presión Arterial Automático Brazo a la cotización 0002', 0),
(290, 'Cotización', 'Resgistro de producto', 3, 1, 'Se agrego el producto: Nebulizador Nebucor P-103 a la cotización 0002', 0),
(291, 'Cotización', 'Cotización a Venta', 3, 1, 'Se convirtio la cotizacion 0002a la venta 0002', 0),
(292, 'Cotización', 'Nueva cotización', 3, 1, 'Se creo la cotización 0003', 0),
(293, 'Cotización', 'Resgistro de producto', 3, 1, 'Se agrego el producto: Férula Flents Insty Splint Para Dedos 2 Piezas Mediano/Grande a la cotización 0003', 0),
(294, 'Cotización', 'Resgistro de producto', 3, 1, 'Se agrego el producto: Collarín Le Roy Cervical Grande a la cotización 0003', 0),
(295, 'Cotización', 'Cotización a Venta', 3, 1, 'Se convirtio la cotizacion 0003a la venta 0003', 0),
(296, 'Venta', 'Resgistro de producto', 3, 1, 'Se agrego el producto: Nebulizador Sun Shine con Pistón a la venta 0003', 0),
(297, 'Sesión', 'Fin de sesión', 0, 115, 'Ismael Carranza cerro sesión', 0),
(298, 'Sesión', 'Incio de sesión', 0, 1, 'Ismael Carranza inicio sesión', 0),
(299, 'Caja', 'Salida', 2, 1, 'Se registro una salida con monto 15 para la caja0001', 0),
(300, 'Sesión', 'Fin de sesión', 0, 131, 'Ismael Carranza cerro sesión', 0),
(301, 'Sesión', 'Incio de sesión', 0, 1, 'Ismael Carranza inicio sesión', 0),
(302, 'Sesión', 'Fin de sesión', 0, 134, ' cerro sesión', 0),
(303, 'Sesión', 'Incio de sesión', 0, 1, 'Ismael Carranza inicio sesión', 0),
(304, 'Caja', 'Registro', 2, 1, 'Se abrio el corte de caja con folio ', 0),
(305, 'Cotización', 'Nueva cotización', 1, 1, 'Se creo cotización con folio 0004 para el cliente prueba', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE `categorias` (
  `cat_clave` int(11) NOT NULL,
  `cat_nombre` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `cat_status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`cat_clave`, `cat_nombre`, `cat_status`) VALUES
(1, ' Antibióticos', 1),
(2, 'Accesorios Médicos', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `cli_clave` int(11) NOT NULL,
  `cli_nombre` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `cli_telefono` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `cli_correo` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `cli_direccion` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `cli_status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`cli_clave`, `cli_nombre`, `cli_telefono`, `cli_correo`, `cli_direccion`, `cli_status`) VALUES
(1, 'Público en general', 'Sin teléfono', 'sincorreo@correo.com', 'Sin dirección', 1),
(2, 'prueba', '', 'prueba@gmail.com', '', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cobros`
--

CREATE TABLE `cobros` (
  `cob_clave` int(11) NOT NULL,
  `cob_fecha` datetime DEFAULT NULL,
  `cob_fcp_clave` int(11) NOT NULL,
  `cob_ven_clave` int(11) NOT NULL,
  `cob_ccj_clave` int(11) NOT NULL,
  `cob_pagaroncon` decimal(11,3) DEFAULT NULL,
  `cob_cambio` decimal(11,3) DEFAULT NULL,
  `cob_monto` decimal(11,3) DEFAULT NULL,
  `cob_descuento` decimal(11,3) DEFAULT NULL,
  `cob_emp_clave` int(11) NOT NULL,
  `cob_status` int(11) DEFAULT NULL,
  `cob_cli_clave` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `cobros`
--

INSERT INTO `cobros` (`cob_clave`, `cob_fecha`, `cob_fcp_clave`, `cob_ven_clave`, `cob_ccj_clave`, `cob_pagaroncon`, `cob_cambio`, `cob_monto`, `cob_descuento`, `cob_emp_clave`, `cob_status`, `cob_cli_clave`) VALUES
(1, '2021-09-30 06:51:38', 1, 1, 1, '200.000', '40.850', '159.150', NULL, 1, 1, 1),
(2, '2021-09-30 06:53:20', 1, 2, 1, '500.000', '0.000', '500.000', NULL, 1, 1, 1),
(3, '2021-09-30 06:55:18', 1, 2, 1, '471.200', '0.000', '471.200', NULL, 1, 1, 1),
(4, '2021-09-30 06:55:36', 2, 2, 1, '1000.000', '0.000', '1000.000', NULL, 1, 1, 1),
(5, '2021-09-30 16:41:57', 2, 3, 1, '1249.630', '0.000', '1249.630', NULL, 1, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cortecaja`
--

CREATE TABLE `cortecaja` (
  `ccj_clave` int(11) NOT NULL,
  `ccj_folio` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `ccj_inicio` datetime DEFAULT NULL,
  `ccj_emp_abrio` int(11) DEFAULT NULL,
  `ccj_fondo` decimal(11,3) DEFAULT NULL,
  `ccj_fin` datetime DEFAULT NULL,
  `ccj_emp_cerro` int(11) DEFAULT NULL,
  `ccj_ventatotal` decimal(11,3) DEFAULT NULL,
  `ccj_entregaron` decimal(11,3) DEFAULT NULL,
  `ccj_entradas` decimal(11,3) DEFAULT NULL,
  `ccj_salidas` decimal(11,3) DEFAULT NULL,
  `ccj_diferiencia` decimal(11,3) DEFAULT NULL,
  `ccj_status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `cortecaja`
--

INSERT INTO `cortecaja` (`ccj_clave`, `ccj_folio`, `ccj_inicio`, `ccj_emp_abrio`, `ccj_fondo`, `ccj_fin`, `ccj_emp_cerro`, `ccj_ventatotal`, `ccj_entregaron`, `ccj_entradas`, `ccj_salidas`, `ccj_diferiencia`, `ccj_status`) VALUES
(1, '0001', '2021-09-30 06:50:03', 1, '500.000', '2021-10-01 05:04:13', 1, '3379.990', '3864.980', '3379.980', '3879.980', '-500.000', 1),
(2, '0002', '2021-10-04 12:30:02', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cotizaciones`
--

CREATE TABLE `cotizaciones` (
  `cot_clave` int(11) NOT NULL,
  `cot_tipo` int(11) DEFAULT NULL,
  `cot_fecha` datetime DEFAULT NULL,
  `cot_folio` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `cot_subtotal` decimal(11,3) DEFAULT NULL,
  `cot_iva` decimal(11,3) DEFAULT NULL,
  `cot_total` decimal(11,3) DEFAULT NULL,
  `cot_ccj_clave` int(11) DEFAULT NULL,
  `cot_status` int(11) DEFAULT NULL,
  `cot_cli_clave` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `cotizaciones`
--

INSERT INTO `cotizaciones` (`cot_clave`, `cot_tipo`, `cot_fecha`, `cot_folio`, `cot_subtotal`, `cot_iva`, `cot_total`, `cot_ccj_clave`, `cot_status`, `cot_cli_clave`) VALUES
(1, 1, '2021-09-30 06:50:40', '0001', '137.200', '21.952', '159.152', 1, 2, 1),
(2, 1, '2021-09-30 06:52:28', '0002', '1699.310', '271.890', '1971.200', 1, 2, 1),
(3, 1, '2021-09-30 07:31:47', '0003', '274.470', '43.915', '318.385', 1, 2, 1),
(4, 2, '2021-10-04 12:30:19', '0004', NULL, NULL, NULL, 2, 1, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detallecotizacion`
--

CREATE TABLE `detallecotizacion` (
  `dc_clave` int(11) NOT NULL,
  `dc_cot_clave` int(11) NOT NULL,
  `dc_precioUnitario` decimal(11,3) DEFAULT NULL,
  `dc_cantidad` float DEFAULT NULL,
  `dc_subtotal` decimal(11,2) DEFAULT NULL,
  `dc_iva` decimal(11,3) DEFAULT NULL,
  `dc_total` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `dc_stauts` int(11) DEFAULT NULL,
  `dc_pro_clave` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `detallecotizacion`
--

INSERT INTO `detallecotizacion` (`dc_clave`, `dc_cot_clave`, `dc_precioUnitario`, `dc_cantidad`, `dc_subtotal`, `dc_iva`, `dc_total`, `dc_stauts`, `dc_pro_clave`) VALUES
(1, 1, '77.400', 1, '77.40', '12.384', '89.784', 1, 10),
(2, 1, '59.800', 1, '59.80', '9.568', '69.368', 1, 22),
(3, 2, '965.860', 1, '965.86', '154.538', '1120.3976', 1, 14),
(4, 2, '733.450', 1, '733.45', '117.352', '850.802', 1, 18),
(5, 3, '109.050', 1, '109.05', '17.448', '126.498', 1, 29),
(6, 3, '165.420', 1, '165.42', '26.467', '191.8872', 1, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalleinventarios`
--

CREATE TABLE `detalleinventarios` (
  `di_clave` int(11) NOT NULL,
  `di_inv_clave` int(11) DEFAULT NULL,
  `di_pro_clave` int(11) DEFAULT NULL,
  `di_cantidad` float DEFAULT NULL,
  `di_tipomov` int(11) DEFAULT NULL,
  `di_totalCompra` decimal(11,3) DEFAULT NULL,
  `di_totalSalida` decimal(11,3) DEFAULT NULL,
  `di_status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalleventas`
--

CREATE TABLE `detalleventas` (
  `dv_clave` int(11) NOT NULL,
  `dv_fecha` datetime DEFAULT NULL,
  `dv_ccj_clave` int(11) DEFAULT NULL,
  `dv_ven_clave` int(11) NOT NULL,
  `dv_pro_clave` int(11) DEFAULT NULL,
  `dv_precioUnitario` decimal(11,3) DEFAULT NULL,
  `dv_cantidad` float DEFAULT NULL,
  `dv_subtotal` decimal(11,3) DEFAULT NULL,
  `dv_iva` decimal(11,3) DEFAULT NULL,
  `dv_total` decimal(11,3) DEFAULT NULL,
  `dv_status` int(11) DEFAULT NULL,
  `dv_tipo` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `detalleventas`
--

INSERT INTO `detalleventas` (`dv_clave`, `dv_fecha`, `dv_ccj_clave`, `dv_ven_clave`, `dv_pro_clave`, `dv_precioUnitario`, `dv_cantidad`, `dv_subtotal`, `dv_iva`, `dv_total`, `dv_status`, `dv_tipo`) VALUES
(1, '2021-09-30 06:51:15', 1, 1, 10, '77.400', 1, '77.400', '12.384', '89.784', 1, 2),
(2, '2021-09-30 06:51:15', 1, 1, 22, '59.800', 1, '59.800', '9.568', '69.368', 1, 2),
(4, '2021-09-30 06:52:56', 1, 2, 14, '965.860', 1, '965.860', '154.538', '1120.398', 1, 2),
(5, '2021-09-30 06:52:56', 1, 2, 18, '733.450', 1, '733.450', '117.352', '850.802', 1, 2),
(6, '2021-09-30 07:32:50', 1, 3, 29, '109.050', 1, '109.050', '17.448', '126.498', 1, 2),
(7, '2021-09-30 07:32:50', 1, 3, 5, '165.420', 1, '165.420', '26.467', '191.887', 1, 2),
(9, '2021-09-30 07:33:10', 1, 3, 39, '802.800', 1, '802.800', '128.448', '931.248', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleados`
--

CREATE TABLE `empleados` (
  `emp_clave` int(11) NOT NULL,
  `emp_nombre` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `emp_apPaterno` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `emp_apMaterno` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `emp_direccion` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `emp_telefono` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `emp_curp` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `emp_rfc` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `emp_correo` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `emp_status` int(11) DEFAULT NULL,
  `emp_pue_clave` int(11) NOT NULL,
  `emp_nombreCompleto` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `empleados`
--

INSERT INTO `empleados` (`emp_clave`, `emp_nombre`, `emp_apPaterno`, `emp_apMaterno`, `emp_direccion`, `emp_telefono`, `emp_curp`, `emp_rfc`, `emp_correo`, `emp_status`, `emp_pue_clave`, `emp_nombreCompleto`) VALUES
(1, 'Ismael', 'Carranza', 'Tronco', '546546fzkvaom', 'kmfzolmzÑ', '54646464', '564646', 'carranzatronco@gmail.com', 1, 3, 'Ismael Carranza Tronco');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresas`
--

CREATE TABLE `empresas` (
  `empr_clave` int(11) NOT NULL,
  `empr_nombre` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `empr_direccion` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `empr_logo` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `empr_status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `formascobropago`
--

CREATE TABLE `formascobropago` (
  `fp_clave` int(11) NOT NULL,
  `fp_nombre` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `fp_tipo` int(11) DEFAULT NULL,
  `fp_status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `formascobropago`
--

INSERT INTO `formascobropago` (`fp_clave`, `fp_nombre`, `fp_tipo`, `fp_status`) VALUES
(1, 'Efectivo', 1, 1),
(2, 'Tarjeta de credito', 0, 1),
(3, 'Tarjeta de debito', 0, 1),
(4, 'Vale de despensas', 0, 1),
(5, 'Cheque', 0, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inventarios`
--

CREATE TABLE `inventarios` (
  `inv_clave` int(11) NOT NULL,
  `inv_fechaInicio` datetime DEFAULT NULL,
  `inv_folio` int(11) DEFAULT NULL,
  `inv_empInicio` int(11) DEFAULT NULL,
  `inv_totalEntrada` decimal(11,3) DEFAULT NULL,
  `inv_totoalSalidas` decimal(11,3) DEFAULT NULL,
  `inv_fechaCierre` datetime DEFAULT NULL,
  `inv_emplCerro` int(11) DEFAULT NULL,
  `inv_productos` int(11) DEFAULT NULL,
  `inv_totalCompra` decimal(11,3) DEFAULT NULL,
  `inv_status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `movcaja`
--

CREATE TABLE `movcaja` (
  `mc_clave` int(11) NOT NULL,
  `mc_fecha` datetime DEFAULT NULL,
  `mc_ccj_clave` int(11) DEFAULT NULL,
  `mc_clasificacion` int(11) DEFAULT NULL,
  `mc_tipo` int(11) DEFAULT NULL,
  `mc_fp_clave` int(11) DEFAULT NULL,
  `mc_monto` decimal(11,3) DEFAULT NULL,
  `mc_iva` float DEFAULT NULL,
  `mc_retiva` decimal(11,3) DEFAULT NULL,
  `mc_ven_clave` int(11) DEFAULT NULL,
  `mc_cob_clave` int(11) DEFAULT NULL,
  `mc_observacion` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `mc_status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `movcaja`
--

INSERT INTO `movcaja` (`mc_clave`, `mc_fecha`, `mc_ccj_clave`, `mc_clasificacion`, `mc_tipo`, `mc_fp_clave`, `mc_monto`, `mc_iva`, `mc_retiva`, `mc_ven_clave`, `mc_cob_clave`, `mc_observacion`, `mc_status`) VALUES
(1, '2021-09-30 06:50:03', 1, 1, 0, 1, '500.000', NULL, NULL, NULL, NULL, 'fondo de caja', 1),
(2, '2021-09-30 06:51:38', 1, 1, 2, 1, '159.150', 0.16, '25.464', 1, 1, 'Cobro de 0001', 1),
(3, '2021-09-30 06:53:20', 1, 1, 2, 1, '500.000', 0.16, '80.000', 2, 2, 'Cobro de 0002', 1),
(4, '2021-09-30 06:55:18', 1, 1, 2, 1, '471.200', 0.16, '75.392', 2, 3, 'Cobro de 0002', 1),
(5, '2021-09-30 06:55:36', 1, 1, 2, 2, '1000.000', 0.16, '160.000', 2, 4, 'Cobro de 0002', 1),
(6, '2021-09-30 08:19:22', 1, 0, 1, 1, '15.000', NULL, NULL, NULL, NULL, 'Compra de garrafón de agua', 1),
(7, '2021-09-30 16:41:57', 1, 1, 2, 2, '1249.630', 0.16, '199.941', 3, 5, 'Cobro de 0003', 1),
(9, '2021-10-01 05:04:13', 1, 0, 3, 1, '3864.980', NULL, NULL, NULL, NULL, 'entrega de caja', 1),
(10, '2021-10-04 12:30:02', 2, 1, 0, 1, '0.000', NULL, NULL, NULL, NULL, 'fondo de caja', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `presentaciones`
--

CREATE TABLE `presentaciones` (
  `pre_clave` int(11) NOT NULL,
  `pre_nombre` varchar(255) DEFAULT NULL,
  `pre_status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `presentaciones`
--

INSERT INTO `presentaciones` (`pre_clave`, `pre_nombre`, `pre_status`) VALUES
(1, 'Bolsa', 1),
(2, 'Botella', 1),
(3, 'Caja', 1),
(4, 'Frasco', 1),
(5, 'Sobre', 1),
(6, 'Pieza', 1),
(7, 'Paquete', 1),
(8, 'Rollo', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `pro_clave` int(11) NOT NULL,
  `pro_nombre` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `pro_descripcion` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `pro_inventariable` int(11) DEFAULT NULL,
  `pro_presentacion` int(11) DEFAULT NULL,
  `pro_unidad` int(11) DEFAULT NULL,
  `pro_medida` float DEFAULT NULL,
  `pro_cantidadMinima` int(11) DEFAULT NULL,
  `pro_precioMayoreo` decimal(11,3) DEFAULT NULL,
  `pro_precio` decimal(11,3) DEFAULT NULL,
  `pro_status` int(11) DEFAULT NULL,
  `pro_tienecodigos` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`pro_clave`, `pro_nombre`, `pro_descripcion`, `pro_inventariable`, `pro_presentacion`, `pro_unidad`, `pro_medida`, `pro_cantidadMinima`, `pro_precioMayoreo`, `pro_precio`, `pro_status`, `pro_tienecodigos`) VALUES
(1, 'Treda 129mg/280mg/30mg 20 Tabletas', NULL, 1, 3, 8, 1, NULL, NULL, '117.500', 1, 1),
(4, 'Treda Suspensión 75 ml', NULL, 1, 2, 8, 1, NULL, NULL, '122.510', 1, 1),
(5, 'Collarín Le Roy Cervical Grande', NULL, 1, 6, 8, 1, NULL, NULL, '165.420', 1, 1),
(6, 'Termómetro Citizen Digital CTA 303', NULL, 1, 6, 8, 1, NULL, NULL, '87.900', 1, 1),
(7, 'Termómetro Digital de Frente Omron MC-720', NULL, 1, 6, 8, 1, NULL, NULL, '810.760', 1, NULL),
(8, 'Termómetro Digital Sunshine con Punta Flexible', NULL, 1, 6, 8, 1, NULL, NULL, '49.000', 1, NULL),
(9, 'Oxímetro Digital de Pulso LMT-01', NULL, 1, 6, 8, 1, NULL, NULL, '699.900', 1, NULL),
(10, 'Termómetro Digital de Pluma', NULL, 1, 6, 8, 1, NULL, NULL, '77.400', 1, NULL),
(11, 'Termómetro Infrarrojo Hi8us HG01 de Frente', NULL, 1, 6, 8, 1, NULL, NULL, '812.000', 1, NULL),
(12, 'Oxímetro Digital de Pulso Xignal para Adulto', NULL, 1, 6, 8, 1, NULL, NULL, '437.490', 1, NULL),
(13, 'Oxímetro de Pulso Levy and Lulu Digital', NULL, 1, 6, 8, 1, NULL, NULL, '438.890', 1, NULL),
(14, 'Monitor Omron Presión Arterial Automático Brazo', NULL, 1, 6, 8, 1, NULL, NULL, '965.860', 1, NULL),
(15, 'Nebulizador de Compresor Omron Ne-C801LA', NULL, 1, 6, 8, 1, NULL, NULL, '1420.970', 1, NULL),
(16, 'Nebulizador Omron de Compresor Mod NE-C101', NULL, 1, 6, 8, 1, NULL, NULL, '1217.030', 1, NULL),
(17, 'Baumanómetro Citizen Ch-617', NULL, 1, 6, 8, 1, NULL, NULL, '539.180', 1, NULL),
(18, 'Nebulizador Nebucor P-103', NULL, 1, 6, 8, 1, NULL, NULL, '733.450', 1, NULL),
(19, 'Baumanómetro Citizen Ch-452 Brazo', NULL, 1, 6, 8, 1, NULL, NULL, '665.140', 1, NULL),
(20, 'Nebulizador Nebucor De Compresor P102', NULL, 1, 6, 8, 1, NULL, NULL, '762.550', 1, NULL),
(21, 'Termómetro Infrarrojo Levy and Lulu a Distancia', NULL, 1, 6, 8, 1, NULL, NULL, '760.700', 1, NULL),
(22, 'Kit Para Nebulizador Sun Shine 6pzas', NULL, 1, 1, 8, 1, NULL, NULL, '59.800', 1, NULL),
(23, 'Monitor Neutek de Presión Sanguínea Brazo Digital', NULL, 1, 6, 8, 1, NULL, NULL, '512.920', 1, NULL),
(24, 'Termómetro Omron Digital MC-246', NULL, 1, 6, 8, 1, NULL, NULL, '101.020', 1, NULL),
(25, 'Monitor Omron de Presión Arterial Automático Muñeca con 30 Memorias', NULL, 1, 6, 8, 1, NULL, NULL, '908.740', 1, NULL),
(26, 'Termómetro Digital Citizen de Pluma', NULL, 1, 6, 8, 1, NULL, NULL, '80.600', 1, NULL),
(27, 'Termómetro Digital Contra el Agua MT 201C', NULL, 1, 6, 8, 1, NULL, NULL, '55.100', 1, NULL),
(28, 'Baumanómetro de Muñeca Nebucor HL168F', NULL, 1, 6, 8, 1, NULL, NULL, '489.510', 1, NULL),
(29, 'Férula Flents Insty Splint Para Dedos 2 Piezas Mediano/Grande', NULL, 1, 7, 8, 1, NULL, NULL, '109.050', 1, NULL),
(30, 'Monitor Neutek De Presión Sanguínea Para Muñeca Automático', NULL, 1, 6, 8, 1, NULL, NULL, '424.150', 1, NULL),
(31, 'Monitor Omron de Presión Arterial Automático Muñeca', NULL, 1, 6, 8, 1, NULL, NULL, '796.470', 1, NULL),
(32, 'Baumanómetro Nebucor HL888JA Brazo', NULL, 1, 6, 8, 1, NULL, NULL, '613.510', 1, NULL),
(33, 'Termómetro Infrarrojo HG01', NULL, 1, 6, 8, 1, NULL, NULL, '661.300', 1, NULL),
(34, 'Termómetro Omron Digital Flex MC 343F', NULL, 1, 6, 8, 1, NULL, NULL, '126.290', 1, NULL),
(35, 'Nebulizador Sun Shine Flujo Continuo', NULL, 1, 6, 8, 1, NULL, NULL, '703.700', 1, NULL),
(36, 'Nebulizador Neutek Hi-Neb de Pistón', NULL, 1, 6, 8, 1, NULL, NULL, '665.140', 1, NULL),
(37, 'Monitor Omron de Presión Arterial de Muñeca Automático Mod HEM-6230', NULL, 1, 6, 8, 1, NULL, NULL, '1265.710', 1, NULL),
(38, 'Muñequera Para Pulgar Barrere Mediano', NULL, 1, 6, 8, 1, NULL, NULL, '84.600', 1, NULL),
(39, 'Nebulizador Sun Shine con Pistón', NULL, 1, 6, 8, 1, NULL, NULL, '802.800', 1, NULL),
(40, 'Rodillera Elástica Rekord Le Roy Mediana', NULL, 1, 6, 8, 1, NULL, NULL, '64.930', 1, NULL),
(41, 'Tobillera Elástica Rekord Le Roy Mediana', NULL, 1, 6, 8, 1, NULL, NULL, '69.810', 1, NULL),
(42, 'Rodillera Elástica Rekord Le Roy Grande', NULL, 1, 6, 8, 1, NULL, NULL, '64.930', 1, NULL),
(43, 'Collarín Barrere Blando Beige Grande', NULL, 1, 6, 8, 1, NULL, NULL, '102.490', 1, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos_categorias`
--

CREATE TABLE `productos_categorias` (
  `pc_clave` int(11) NOT NULL,
  `pc_pro_clave` int(11) DEFAULT NULL,
  `pc_cat_clave` int(11) DEFAULT NULL,
  `pc_status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `productos_categorias`
--

INSERT INTO `productos_categorias` (`pc_clave`, `pc_pro_clave`, `pc_cat_clave`, `pc_status`) VALUES
(1, 1, 1, 1),
(2, 4, 1, 1),
(3, 5, 2, 1),
(4, 6, 2, 1),
(5, 7, 2, 1),
(6, 8, 2, 1),
(7, 9, 2, 1),
(8, 10, 2, 1),
(9, 11, 2, 1),
(10, 12, 2, 1),
(11, 13, 2, 1),
(12, 14, 2, 1),
(13, 15, 2, 1),
(14, 16, 2, 1),
(15, 17, 2, 1),
(16, 18, 2, 1),
(17, 19, 2, 1),
(18, 20, 2, 1),
(19, 21, 2, 1),
(20, 22, 2, 1),
(21, 23, 2, 1),
(22, 24, 2, 1),
(23, 25, 2, 1),
(24, 26, 2, 1),
(25, 27, 2, 1),
(26, 28, 2, 1),
(27, 29, 2, 1),
(28, 30, 2, 1),
(29, 31, 2, 1),
(30, 32, 2, 1),
(31, 33, 2, 1),
(32, 34, 2, 1),
(33, 35, 2, 1),
(34, 36, 2, 1),
(35, 37, 2, 1),
(36, 38, 2, 1),
(37, 39, 2, 1),
(38, 40, 2, 1),
(39, 41, 2, 1),
(40, 42, 2, 1),
(41, 43, 2, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedores`
--

CREATE TABLE `proveedores` (
  `prov_clave` int(11) NOT NULL,
  `prov_nombre` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `prov_appaterno` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `prov_apmaterno` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `prov_nombrecompleto` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `prov_correo` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `prov_telefono` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `prov_status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `proveedores`
--

INSERT INTO `proveedores` (`prov_clave`, `prov_nombre`, `prov_appaterno`, `prov_apmaterno`, `prov_nombrecompleto`, `prov_correo`, `prov_telefono`, `prov_status`) VALUES
(1, 'Ismael', 'Carranza', 'Tronco', 'Ismael Carranza Tronco', 'null', 'null', 1),
(2, 'Pruebas', 'Pruebas', 'Pruebas', 'Pruebas Pruebas Pruebas', '', '', 0),
(3, 'Daniel', 'Nsiau', 'Jdbcibna', 'Daniel Nsiau Jdbcibna', '', '', 1),
(4, 'A', 'K', 'Lkzdfkmval', 'A K Lkzdfkmval', 'null@gmail.com', 'null', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedores_productos`
--

CREATE TABLE `proveedores_productos` (
  `pp_clave` int(11) NOT NULL,
  `pp_prov_clave` int(11) DEFAULT NULL,
  `pp_pro_clave` int(11) DEFAULT NULL,
  `pp_status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `puestos`
--

CREATE TABLE `puestos` (
  `pue_clave` int(11) NOT NULL,
  `pue_nombre` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `pue_status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `puestos`
--

INSERT INTO `puestos` (`pue_clave`, `pue_nombre`, `pue_status`) VALUES
(1, 'Cajero', 1),
(2, 'Vendedor', 1),
(3, 'Almacenista', 1),
(4, 'Repartidor', 1),
(5, 'Gerente', 1),
(6, 'Vigilante', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sistemaunidades`
--

CREATE TABLE `sistemaunidades` (
  `su_clave` int(11) NOT NULL,
  `su_nombre` varchar(255) DEFAULT NULL,
  `su_status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sistemaunidades`
--

INSERT INTO `sistemaunidades` (`su_clave`, `su_nombre`, `su_status`) VALUES
(1, 'sin unidad', 1),
(2, 'kg', 1),
(3, 'g', 1),
(4, 'l', 1),
(5, 'ml', 1),
(6, 'm', 1),
(7, 'm2', 1),
(8, 'pza', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `stocks`
--

CREATE TABLE `stocks` (
  `sto_clave` int(11) NOT NULL,
  `sto_cantidad` float DEFAULT NULL,
  `sto_pro_clave` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sucursales`
--

CREATE TABLE `sucursales` (
  `suc_clave` int(11) NOT NULL,
  `suc_direccion` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `suc_status` int(11) DEFAULT NULL,
  `suc_empr_clave` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `usu_clave` int(11) NOT NULL,
  `usu_nombre` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `usu_password` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `usu_tipo` int(11) DEFAULT NULL,
  `usu_nivel` int(11) DEFAULT NULL,
  `usu_status` int(11) DEFAULT NULL,
  `usu_emp_clave` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`usu_clave`, `usu_nombre`, `usu_password`, `usu_tipo`, `usu_nivel`, `usu_status`, `usu_emp_clave`) VALUES
(1, 'admin', '$2y$05$6PCxmhYX3UagVZKfPLkFsuCPFbmu50XCVCD88iC3KTmC4WU.hCu8O', 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ventas`
--

CREATE TABLE `ventas` (
  `ven_clave` int(11) NOT NULL,
  `ven_cot_clave` int(11) DEFAULT NULL,
  `ven_ccj_clave` int(11) NOT NULL,
  `ven_cli_clave` int(11) NOT NULL,
  `ven_emp_clave` int(11) NOT NULL,
  `ven_fecha` datetime DEFAULT NULL,
  `ven_tipo` int(11) DEFAULT NULL,
  `ven_folio` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `ven_subtotal` decimal(11,3) DEFAULT NULL,
  `ven_iva` decimal(11,3) DEFAULT NULL,
  `ven_total` decimal(11,3) DEFAULT NULL,
  `ven_cobrado` decimal(11,3) DEFAULT NULL,
  `ven_status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `ventas`
--

INSERT INTO `ventas` (`ven_clave`, `ven_cot_clave`, `ven_ccj_clave`, `ven_cli_clave`, `ven_emp_clave`, `ven_fecha`, `ven_tipo`, `ven_folio`, `ven_subtotal`, `ven_iva`, `ven_total`, `ven_cobrado`, `ven_status`) VALUES
(1, 1, 1, 1, 1, '2021-09-30 06:51:15', 2, '0001', '137.200', '21.950', '159.150', '1249.630', 2),
(2, 2, 1, 1, 1, '2021-09-30 06:52:56', 2, '0002', '1699.310', '271.890', '1971.200', '1971.200', 2),
(3, 3, 1, 1, 1, '2021-09-30 07:32:50', 2, '0003', '1077.270', '172.360', '1249.630', '1249.630', 2);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `bitacora`
--
ALTER TABLE `bitacora`
  ADD PRIMARY KEY (`bit_clave`);

--
-- Indices de la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`cat_clave`);

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`cli_clave`);

--
-- Indices de la tabla `cobros`
--
ALTER TABLE `cobros`
  ADD PRIMARY KEY (`cob_clave`),
  ADD KEY `fk_cobros_formasCobroPago1_idx` (`cob_fcp_clave`),
  ADD KEY `fk_cobros_ventas1_idx` (`cob_ven_clave`),
  ADD KEY `fk_cobros_corteCaja1_idx` (`cob_ccj_clave`),
  ADD KEY `fk_cobros_empleados1_idx` (`cob_emp_clave`),
  ADD KEY `fk_cobros_clientes1_idx` (`cob_cli_clave`);

--
-- Indices de la tabla `cortecaja`
--
ALTER TABLE `cortecaja`
  ADD PRIMARY KEY (`ccj_clave`),
  ADD KEY `fk_corteCaja_empleados1_idx` (`ccj_emp_abrio`);

--
-- Indices de la tabla `cotizaciones`
--
ALTER TABLE `cotizaciones`
  ADD PRIMARY KEY (`cot_clave`),
  ADD KEY `fk_cotizaciones_cortecaja1_idx` (`cot_ccj_clave`);

--
-- Indices de la tabla `detallecotizacion`
--
ALTER TABLE `detallecotizacion`
  ADD PRIMARY KEY (`dc_clave`),
  ADD KEY `fk_detalleCotizacion_cotizaciones1_idx` (`dc_cot_clave`);

--
-- Indices de la tabla `detalleinventarios`
--
ALTER TABLE `detalleinventarios`
  ADD PRIMARY KEY (`di_clave`),
  ADD KEY `fk_detalleinventarios_inventarios1_idx` (`di_inv_clave`),
  ADD KEY `fk_detalleinventarios_productos1_idx` (`di_pro_clave`);

--
-- Indices de la tabla `detalleventas`
--
ALTER TABLE `detalleventas`
  ADD PRIMARY KEY (`dv_clave`),
  ADD KEY `fk_detalleVentas_ventas1_idx` (`dv_ven_clave`),
  ADD KEY `fk_detalleVentas_productos1_idx` (`dv_pro_clave`),
  ADD KEY `fk_detalleventas_cortecaja1_idx` (`dv_ccj_clave`);

--
-- Indices de la tabla `empleados`
--
ALTER TABLE `empleados`
  ADD PRIMARY KEY (`emp_clave`),
  ADD KEY `fk_empleados_puestos_idx` (`emp_pue_clave`);

--
-- Indices de la tabla `empresas`
--
ALTER TABLE `empresas`
  ADD PRIMARY KEY (`empr_clave`);

--
-- Indices de la tabla `formascobropago`
--
ALTER TABLE `formascobropago`
  ADD PRIMARY KEY (`fp_clave`);

--
-- Indices de la tabla `inventarios`
--
ALTER TABLE `inventarios`
  ADD PRIMARY KEY (`inv_clave`);

--
-- Indices de la tabla `movcaja`
--
ALTER TABLE `movcaja`
  ADD PRIMARY KEY (`mc_clave`),
  ADD KEY `fk_movCaja_formasCobroPago1_idx` (`mc_fp_clave`),
  ADD KEY `fk_movCaja_ventas1_idx` (`mc_ven_clave`),
  ADD KEY `fk_movCaja_cobros1_idx` (`mc_cob_clave`),
  ADD KEY `fk_movcaja_cortecaja1_idx` (`mc_ccj_clave`);

--
-- Indices de la tabla `presentaciones`
--
ALTER TABLE `presentaciones`
  ADD PRIMARY KEY (`pre_clave`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`pro_clave`);

--
-- Indices de la tabla `productos_categorias`
--
ALTER TABLE `productos_categorias`
  ADD PRIMARY KEY (`pc_clave`),
  ADD KEY `fk_productos_has_categorias_categorias1_idx` (`pc_cat_clave`),
  ADD KEY `fk_productos_has_categorias_productos1_idx` (`pc_pro_clave`);

--
-- Indices de la tabla `proveedores`
--
ALTER TABLE `proveedores`
  ADD PRIMARY KEY (`prov_clave`);

--
-- Indices de la tabla `proveedores_productos`
--
ALTER TABLE `proveedores_productos`
  ADD PRIMARY KEY (`pp_clave`),
  ADD KEY `fk_proveedores_has_productos_productos1_idx` (`pp_pro_clave`),
  ADD KEY `fk_proveedores_has_productos_proveedores1_idx` (`pp_prov_clave`);

--
-- Indices de la tabla `puestos`
--
ALTER TABLE `puestos`
  ADD PRIMARY KEY (`pue_clave`);

--
-- Indices de la tabla `sistemaunidades`
--
ALTER TABLE `sistemaunidades`
  ADD PRIMARY KEY (`su_clave`);

--
-- Indices de la tabla `stocks`
--
ALTER TABLE `stocks`
  ADD PRIMARY KEY (`sto_clave`),
  ADD KEY `fk_stock_productos1_idx` (`sto_pro_clave`);

--
-- Indices de la tabla `sucursales`
--
ALTER TABLE `sucursales`
  ADD PRIMARY KEY (`suc_clave`),
  ADD KEY `fk_sucursales_empresas1_idx` (`suc_empr_clave`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`usu_clave`),
  ADD KEY `fk_usuarios_empleados1_idx` (`usu_emp_clave`);

--
-- Indices de la tabla `ventas`
--
ALTER TABLE `ventas`
  ADD PRIMARY KEY (`ven_clave`),
  ADD KEY `fk_ventas_corteCaja1_idx` (`ven_ccj_clave`),
  ADD KEY `fk_ventas_clientes1_idx` (`ven_cli_clave`),
  ADD KEY `fk_ventas_empleados1_idx` (`ven_emp_clave`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `bitacora`
--
ALTER TABLE `bitacora`
  MODIFY `bit_clave` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=306;

--
-- AUTO_INCREMENT de la tabla `categorias`
--
ALTER TABLE `categorias`
  MODIFY `cat_clave` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
  MODIFY `cli_clave` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `cobros`
--
ALTER TABLE `cobros`
  MODIFY `cob_clave` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `cortecaja`
--
ALTER TABLE `cortecaja`
  MODIFY `ccj_clave` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `cotizaciones`
--
ALTER TABLE `cotizaciones`
  MODIFY `cot_clave` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `detallecotizacion`
--
ALTER TABLE `detallecotizacion`
  MODIFY `dc_clave` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `detalleinventarios`
--
ALTER TABLE `detalleinventarios`
  MODIFY `di_clave` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `detalleventas`
--
ALTER TABLE `detalleventas`
  MODIFY `dv_clave` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `empleados`
--
ALTER TABLE `empleados`
  MODIFY `emp_clave` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `empresas`
--
ALTER TABLE `empresas`
  MODIFY `empr_clave` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `formascobropago`
--
ALTER TABLE `formascobropago`
  MODIFY `fp_clave` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `inventarios`
--
ALTER TABLE `inventarios`
  MODIFY `inv_clave` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `movcaja`
--
ALTER TABLE `movcaja`
  MODIFY `mc_clave` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `presentaciones`
--
ALTER TABLE `presentaciones`
  MODIFY `pre_clave` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `pro_clave` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT de la tabla `productos_categorias`
--
ALTER TABLE `productos_categorias`
  MODIFY `pc_clave` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT de la tabla `proveedores`
--
ALTER TABLE `proveedores`
  MODIFY `prov_clave` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `proveedores_productos`
--
ALTER TABLE `proveedores_productos`
  MODIFY `pp_clave` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `puestos`
--
ALTER TABLE `puestos`
  MODIFY `pue_clave` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `sistemaunidades`
--
ALTER TABLE `sistemaunidades`
  MODIFY `su_clave` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `stocks`
--
ALTER TABLE `stocks`
  MODIFY `sto_clave` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sucursales`
--
ALTER TABLE `sucursales`
  MODIFY `suc_clave` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `usu_clave` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `ventas`
--
ALTER TABLE `ventas`
  MODIFY `ven_clave` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cobros`
--
ALTER TABLE `cobros`
  ADD CONSTRAINT `fk_cobros_clientes1` FOREIGN KEY (`cob_cli_clave`) REFERENCES `clientes` (`cli_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_cobros_corteCaja1` FOREIGN KEY (`cob_ccj_clave`) REFERENCES `cortecaja` (`ccj_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_cobros_empleados1` FOREIGN KEY (`cob_emp_clave`) REFERENCES `empleados` (`emp_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_cobros_formasCobroPago1` FOREIGN KEY (`cob_fcp_clave`) REFERENCES `formascobropago` (`fp_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_cobros_ventas1` FOREIGN KEY (`cob_ven_clave`) REFERENCES `ventas` (`ven_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `cortecaja`
--
ALTER TABLE `cortecaja`
  ADD CONSTRAINT `fk_corteCaja_empleados1` FOREIGN KEY (`ccj_emp_abrio`) REFERENCES `empleados` (`emp_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `cotizaciones`
--
ALTER TABLE `cotizaciones`
  ADD CONSTRAINT `fk_cotizaciones_cortecaja1` FOREIGN KEY (`cot_ccj_clave`) REFERENCES `cortecaja` (`ccj_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `detallecotizacion`
--
ALTER TABLE `detallecotizacion`
  ADD CONSTRAINT `fk_detalleCotizacion_cotizaciones1` FOREIGN KEY (`dc_cot_clave`) REFERENCES `cotizaciones` (`cot_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `detalleinventarios`
--
ALTER TABLE `detalleinventarios`
  ADD CONSTRAINT `fk_detalleinventarios_inventarios1` FOREIGN KEY (`di_inv_clave`) REFERENCES `inventarios` (`inv_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_detalleinventarios_productos1` FOREIGN KEY (`di_pro_clave`) REFERENCES `productos` (`pro_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `detalleventas`
--
ALTER TABLE `detalleventas`
  ADD CONSTRAINT `fk_detalleVentas_productos1` FOREIGN KEY (`dv_pro_clave`) REFERENCES `productos` (`pro_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_detalleVentas_ventas1` FOREIGN KEY (`dv_ven_clave`) REFERENCES `ventas` (`ven_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_detalleventas_cortecaja1` FOREIGN KEY (`dv_ccj_clave`) REFERENCES `cortecaja` (`ccj_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `empleados`
--
ALTER TABLE `empleados`
  ADD CONSTRAINT `fk_empleados_puestos` FOREIGN KEY (`emp_pue_clave`) REFERENCES `puestos` (`pue_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `movcaja`
--
ALTER TABLE `movcaja`
  ADD CONSTRAINT `fk_movCaja_cobros1` FOREIGN KEY (`mc_cob_clave`) REFERENCES `cobros` (`cob_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_movCaja_formasCobroPago1` FOREIGN KEY (`mc_fp_clave`) REFERENCES `formascobropago` (`fp_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_movCaja_ventas1` FOREIGN KEY (`mc_ven_clave`) REFERENCES `ventas` (`ven_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_movcaja_cortecaja1` FOREIGN KEY (`mc_ccj_clave`) REFERENCES `cortecaja` (`ccj_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `productos_categorias`
--
ALTER TABLE `productos_categorias`
  ADD CONSTRAINT `fk_productos_has_categorias_categorias1` FOREIGN KEY (`pc_cat_clave`) REFERENCES `categorias` (`cat_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_productos_has_categorias_productos1` FOREIGN KEY (`pc_pro_clave`) REFERENCES `productos` (`pro_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `proveedores_productos`
--
ALTER TABLE `proveedores_productos`
  ADD CONSTRAINT `fk_proveedores_has_productos_productos1` FOREIGN KEY (`pp_pro_clave`) REFERENCES `productos` (`pro_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_proveedores_has_productos_proveedores1` FOREIGN KEY (`pp_prov_clave`) REFERENCES `proveedores` (`prov_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `stocks`
--
ALTER TABLE `stocks`
  ADD CONSTRAINT `fk_stock_productos1` FOREIGN KEY (`sto_pro_clave`) REFERENCES `productos` (`pro_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `sucursales`
--
ALTER TABLE `sucursales`
  ADD CONSTRAINT `fk_sucursales_empresas1` FOREIGN KEY (`suc_empr_clave`) REFERENCES `empresas` (`empr_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD CONSTRAINT `fk_usuarios_empleados1` FOREIGN KEY (`usu_emp_clave`) REFERENCES `empleados` (`emp_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `ventas`
--
ALTER TABLE `ventas`
  ADD CONSTRAINT `fk_ventas_clientes1` FOREIGN KEY (`ven_cli_clave`) REFERENCES `clientes` (`cli_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_ventas_corteCaja1` FOREIGN KEY (`ven_ccj_clave`) REFERENCES `cortecaja` (`ccj_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_ventas_empleados1` FOREIGN KEY (`ven_emp_clave`) REFERENCES `empleados` (`emp_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

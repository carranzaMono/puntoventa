use puntoventa;

truncate table cortecaja;
truncate table movcaja;
truncate table cotizaciones;
truncate table detallecotizacion;
truncate table ventas;
truncate table clientes;
truncate table detalleventas;
truncate table productos_categorias;
truncate table cobros;
SET FOREIGN_KEY_CHECKS = 0;
SET FOREIGN_KEY_CHECKS = 1;

select * from bitacora;
select * from productos;
select * from categorias;
select * from empleados;
select * from usuarios;
select * from cortecaja;
select * from movcaja;

select * from productos_categorias;
select * from inventarios;

select * from cortecaja;
select * from movcaja;
select * from formascobropago;
select * from cotizaciones;
select * from detallecotizacion;


select * from clientes;

select * from presentaciones;
select * from sistemaunidades;
select * from productos;

select * from detalleventas;
select * from formascobropago;
select cast(ifnull(sum(dv_iva), 0)  + ifnull(sum(dv_total), 0) as decimal(11,2)) from detalleventas where dv_status = 1 and dv_ven_clave = 1;

select * from cortecaja;
select * from ventas;
select * from detalleventas;
select * from cobros;
select * from movcaja;

select * from cortecaja;
select * from formascobropago;
      
select fp_nombre, 
	   (select concat('$ ', cast(ifnull(sum(mc_monto), 0) as decimal(11, 2))) from movcaja where mc_fp_clave = fp_clave and mc_clasificacion = 1 and mc_tipo = 2) as total 
	from formascobropago 
		where fp_status = 1;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: puntoventa
-- ------------------------------------------------------
-- Server version	5.5.5-10.4.17-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `bitacora`
--

DROP TABLE IF EXISTS `bitacora`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bitacora` (
  `bit_clave` int(11) NOT NULL AUTO_INCREMENT,
  `bit_modulo` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `bit_accion` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `bit_tipo` int(11) DEFAULT NULL,
  `bit_emp_clave` int(11) DEFAULT NULL,
  `bit_descripcion` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `bit_claves` int(11) DEFAULT NULL,
  PRIMARY KEY (`bit_clave`)
) ENGINE=InnoDB AUTO_INCREMENT=311 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `categorias`
--

DROP TABLE IF EXISTS `categorias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categorias` (
  `cat_clave` int(11) NOT NULL AUTO_INCREMENT,
  `cat_nombre` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `cat_status` int(11) DEFAULT NULL,
  PRIMARY KEY (`cat_clave`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `clientes`
--

DROP TABLE IF EXISTS `clientes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clientes` (
  `cli_clave` int(11) NOT NULL AUTO_INCREMENT,
  `cli_nombre` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `cli_telefono` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `cli_correo` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `cli_direccion` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `cli_status` int(11) DEFAULT NULL,
  PRIMARY KEY (`cli_clave`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cobros`
--

DROP TABLE IF EXISTS `cobros`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cobros` (
  `cob_clave` int(11) NOT NULL AUTO_INCREMENT,
  `cob_fecha` datetime DEFAULT NULL,
  `cob_fcp_clave` int(11) NOT NULL,
  `cob_ven_clave` int(11) NOT NULL,
  `cob_ccj_clave` int(11) NOT NULL,
  `cob_pagaroncon` decimal(11,3) DEFAULT NULL,
  `cob_cambio` decimal(11,3) DEFAULT NULL,
  `cob_monto` decimal(11,3) DEFAULT NULL,
  `cob_descuento` decimal(11,3) DEFAULT NULL,
  `cob_emp_clave` int(11) NOT NULL,
  `cob_status` int(11) DEFAULT NULL,
  `cob_cli_clave` int(11) NOT NULL,
  PRIMARY KEY (`cob_clave`),
  KEY `fk_cobros_formasCobroPago1_idx` (`cob_fcp_clave`),
  KEY `fk_cobros_ventas1_idx` (`cob_ven_clave`),
  KEY `fk_cobros_corteCaja1_idx` (`cob_ccj_clave`),
  KEY `fk_cobros_empleados1_idx` (`cob_emp_clave`),
  KEY `fk_cobros_clientes1_idx` (`cob_cli_clave`),
  CONSTRAINT `fk_cobros_clientes1` FOREIGN KEY (`cob_cli_clave`) REFERENCES `clientes` (`cli_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_cobros_corteCaja1` FOREIGN KEY (`cob_ccj_clave`) REFERENCES `cortecaja` (`ccj_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_cobros_empleados1` FOREIGN KEY (`cob_emp_clave`) REFERENCES `empleados` (`emp_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_cobros_formasCobroPago1` FOREIGN KEY (`cob_fcp_clave`) REFERENCES `formascobropago` (`fp_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_cobros_ventas1` FOREIGN KEY (`cob_ven_clave`) REFERENCES `ventas` (`ven_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cortecaja`
--

DROP TABLE IF EXISTS `cortecaja`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cortecaja` (
  `ccj_clave` int(11) NOT NULL AUTO_INCREMENT,
  `ccj_folio` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `ccj_inicio` datetime DEFAULT NULL,
  `ccj_emp_abrio` int(11) DEFAULT NULL,
  `ccj_fondo` decimal(11,3) DEFAULT NULL,
  `ccj_fin` datetime DEFAULT NULL,
  `ccj_emp_cerro` int(11) DEFAULT NULL,
  `ccj_ventatotal` decimal(11,3) DEFAULT NULL,
  `ccj_entregaron` decimal(11,3) DEFAULT NULL,
  `ccj_entradas` decimal(11,3) DEFAULT NULL,
  `ccj_salidas` decimal(11,3) DEFAULT NULL,
  `ccj_diferiencia` decimal(11,3) DEFAULT NULL,
  `ccj_status` int(11) DEFAULT NULL,
  PRIMARY KEY (`ccj_clave`),
  KEY `fk_corteCaja_empleados1_idx` (`ccj_emp_abrio`),
  CONSTRAINT `fk_corteCaja_empleados1` FOREIGN KEY (`ccj_emp_abrio`) REFERENCES `empleados` (`emp_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cotizaciones`
--

DROP TABLE IF EXISTS `cotizaciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cotizaciones` (
  `cot_clave` int(11) NOT NULL AUTO_INCREMENT,
  `cot_tipo` int(11) DEFAULT NULL,
  `cot_fecha` datetime DEFAULT NULL,
  `cot_folio` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `cot_subtotal` decimal(11,3) DEFAULT NULL,
  `cot_iva` decimal(11,3) DEFAULT NULL,
  `cot_total` decimal(11,3) DEFAULT NULL,
  `cot_ccj_clave` int(11) DEFAULT NULL,
  `cot_status` int(11) DEFAULT NULL,
  `cot_cli_clave` int(11) DEFAULT NULL,
  PRIMARY KEY (`cot_clave`),
  KEY `fk_cotizaciones_cortecaja1_idx` (`cot_ccj_clave`),
  CONSTRAINT `fk_cotizaciones_cortecaja1` FOREIGN KEY (`cot_ccj_clave`) REFERENCES `cortecaja` (`ccj_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `detallecotizacion`
--

DROP TABLE IF EXISTS `detallecotizacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detallecotizacion` (
  `dc_clave` int(11) NOT NULL AUTO_INCREMENT,
  `dc_cot_clave` int(11) NOT NULL,
  `dc_precioUnitario` decimal(11,3) DEFAULT NULL,
  `dc_cantidad` float DEFAULT NULL,
  `dc_subtotal` decimal(11,2) DEFAULT NULL,
  `dc_iva` decimal(11,3) DEFAULT NULL,
  `dc_total` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `dc_stauts` int(11) DEFAULT NULL,
  `dc_pro_clave` int(11) DEFAULT NULL,
  PRIMARY KEY (`dc_clave`),
  KEY `fk_detalleCotizacion_cotizaciones1_idx` (`dc_cot_clave`),
  CONSTRAINT `fk_detalleCotizacion_cotizaciones1` FOREIGN KEY (`dc_cot_clave`) REFERENCES `cotizaciones` (`cot_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `detalleinventarios`
--

DROP TABLE IF EXISTS `detalleinventarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detalleinventarios` (
  `di_clave` int(11) NOT NULL AUTO_INCREMENT,
  `di_inv_clave` int(11) DEFAULT NULL,
  `di_pro_clave` int(11) DEFAULT NULL,
  `di_cantidad` float DEFAULT NULL,
  `di_tipomov` int(11) DEFAULT NULL,
  `di_totalCompra` decimal(11,3) DEFAULT NULL,
  `di_totalSalida` decimal(11,3) DEFAULT NULL,
  `di_status` int(11) DEFAULT NULL,
  PRIMARY KEY (`di_clave`),
  KEY `fk_detalleinventarios_inventarios1_idx` (`di_inv_clave`),
  KEY `fk_detalleinventarios_productos1_idx` (`di_pro_clave`),
  CONSTRAINT `fk_detalleinventarios_inventarios1` FOREIGN KEY (`di_inv_clave`) REFERENCES `inventarios` (`inv_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_detalleinventarios_productos1` FOREIGN KEY (`di_pro_clave`) REFERENCES `productos` (`pro_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `detalleventas`
--

DROP TABLE IF EXISTS `detalleventas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detalleventas` (
  `dv_clave` int(11) NOT NULL AUTO_INCREMENT,
  `dv_fecha` datetime DEFAULT NULL,
  `dv_ccj_clave` int(11) DEFAULT NULL,
  `dv_ven_clave` int(11) NOT NULL,
  `dv_pro_clave` int(11) DEFAULT NULL,
  `dv_precioUnitario` decimal(11,3) DEFAULT NULL,
  `dv_cantidad` float DEFAULT NULL,
  `dv_subtotal` decimal(11,3) DEFAULT NULL,
  `dv_iva` decimal(11,3) DEFAULT NULL,
  `dv_total` decimal(11,3) DEFAULT NULL,
  `dv_status` int(11) DEFAULT NULL,
  `dv_tipo` int(11) DEFAULT NULL,
  PRIMARY KEY (`dv_clave`),
  KEY `fk_detalleVentas_ventas1_idx` (`dv_ven_clave`),
  KEY `fk_detalleVentas_productos1_idx` (`dv_pro_clave`),
  KEY `fk_detalleventas_cortecaja1_idx` (`dv_ccj_clave`),
  CONSTRAINT `fk_detalleVentas_productos1` FOREIGN KEY (`dv_pro_clave`) REFERENCES `productos` (`pro_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_detalleVentas_ventas1` FOREIGN KEY (`dv_ven_clave`) REFERENCES `ventas` (`ven_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_detalleventas_cortecaja1` FOREIGN KEY (`dv_ccj_clave`) REFERENCES `cortecaja` (`ccj_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `empleados`
--

DROP TABLE IF EXISTS `empleados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `empleados` (
  `emp_clave` int(11) NOT NULL AUTO_INCREMENT,
  `emp_nombre` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `emp_apPaterno` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `emp_apMaterno` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `emp_direccion` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `emp_telefono` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `emp_curp` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `emp_rfc` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `emp_correo` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `emp_status` int(11) DEFAULT NULL,
  `emp_pue_clave` int(11) NOT NULL,
  `emp_nombreCompleto` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`emp_clave`),
  KEY `fk_empleados_puestos_idx` (`emp_pue_clave`),
  CONSTRAINT `fk_empleados_puestos` FOREIGN KEY (`emp_pue_clave`) REFERENCES `puestos` (`pue_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `empresas`
--

DROP TABLE IF EXISTS `empresas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `empresas` (
  `empr_clave` int(11) NOT NULL AUTO_INCREMENT,
  `empr_nombre` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `empr_direccion` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `empr_logo` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `empr_status` int(11) DEFAULT NULL,
  PRIMARY KEY (`empr_clave`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `formascobropago`
--

DROP TABLE IF EXISTS `formascobropago`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `formascobropago` (
  `fp_clave` int(11) NOT NULL AUTO_INCREMENT,
  `fp_nombre` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `fp_tipo` int(11) DEFAULT NULL,
  `fp_status` int(11) DEFAULT NULL,
  PRIMARY KEY (`fp_clave`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `inventarios`
--

DROP TABLE IF EXISTS `inventarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inventarios` (
  `inv_clave` int(11) NOT NULL AUTO_INCREMENT,
  `inv_fechaInicio` datetime DEFAULT NULL,
  `inv_folio` int(11) DEFAULT NULL,
  `inv_empInicio` int(11) DEFAULT NULL,
  `inv_totalEntrada` decimal(11,3) DEFAULT NULL,
  `inv_totoalSalidas` decimal(11,3) DEFAULT NULL,
  `inv_fechaCierre` datetime DEFAULT NULL,
  `inv_emplCerro` int(11) DEFAULT NULL,
  `inv_productos` int(11) DEFAULT NULL,
  `inv_totalCompra` decimal(11,3) DEFAULT NULL,
  `inv_status` int(11) DEFAULT NULL,
  PRIMARY KEY (`inv_clave`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `movcaja`
--

DROP TABLE IF EXISTS `movcaja`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `movcaja` (
  `mc_clave` int(11) NOT NULL AUTO_INCREMENT,
  `mc_fecha` datetime DEFAULT NULL,
  `mc_ccj_clave` int(11) DEFAULT NULL,
  `mc_clasificacion` int(11) DEFAULT NULL,
  `mc_tipo` int(11) DEFAULT NULL,
  `mc_fp_clave` int(11) DEFAULT NULL,
  `mc_monto` decimal(11,3) DEFAULT NULL,
  `mc_iva` float DEFAULT NULL,
  `mc_retiva` decimal(11,3) DEFAULT NULL,
  `mc_ven_clave` int(11) DEFAULT NULL,
  `mc_cob_clave` int(11) DEFAULT NULL,
  `mc_observacion` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `mc_status` int(11) DEFAULT NULL,
  PRIMARY KEY (`mc_clave`),
  KEY `fk_movCaja_formasCobroPago1_idx` (`mc_fp_clave`),
  KEY `fk_movCaja_ventas1_idx` (`mc_ven_clave`),
  KEY `fk_movCaja_cobros1_idx` (`mc_cob_clave`),
  KEY `fk_movcaja_cortecaja1_idx` (`mc_ccj_clave`),
  CONSTRAINT `fk_movCaja_cobros1` FOREIGN KEY (`mc_cob_clave`) REFERENCES `cobros` (`cob_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_movCaja_formasCobroPago1` FOREIGN KEY (`mc_fp_clave`) REFERENCES `formascobropago` (`fp_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_movCaja_ventas1` FOREIGN KEY (`mc_ven_clave`) REFERENCES `ventas` (`ven_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_movcaja_cortecaja1` FOREIGN KEY (`mc_ccj_clave`) REFERENCES `cortecaja` (`ccj_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `presentaciones`
--

DROP TABLE IF EXISTS `presentaciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `presentaciones` (
  `pre_clave` int(11) NOT NULL AUTO_INCREMENT,
  `pre_nombre` varchar(255) DEFAULT NULL,
  `pre_status` int(11) DEFAULT NULL,
  PRIMARY KEY (`pre_clave`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `productos`
--

DROP TABLE IF EXISTS `productos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productos` (
  `pro_clave` int(11) NOT NULL AUTO_INCREMENT,
  `pro_nombre` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `pro_descripcion` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `pro_inventariable` int(11) DEFAULT NULL,
  `pro_presentacion` int(11) DEFAULT NULL,
  `pro_unidad` int(11) DEFAULT NULL,
  `pro_medida` float DEFAULT NULL,
  `pro_cantidadMinima` int(11) DEFAULT NULL,
  `pro_precioMayoreo` decimal(11,3) DEFAULT NULL,
  `pro_precio` decimal(11,3) DEFAULT NULL,
  `pro_status` int(11) DEFAULT NULL,
  `pro_tienecodigos` int(11) DEFAULT NULL,
  PRIMARY KEY (`pro_clave`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `productos_categorias`
--

DROP TABLE IF EXISTS `productos_categorias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productos_categorias` (
  `pc_clave` int(11) NOT NULL AUTO_INCREMENT,
  `pc_pro_clave` int(11) DEFAULT NULL,
  `pc_cat_clave` int(11) DEFAULT NULL,
  `pc_status` int(11) DEFAULT NULL,
  PRIMARY KEY (`pc_clave`),
  KEY `fk_productos_has_categorias_categorias1_idx` (`pc_cat_clave`),
  KEY `fk_productos_has_categorias_productos1_idx` (`pc_pro_clave`),
  CONSTRAINT `fk_productos_has_categorias_categorias1` FOREIGN KEY (`pc_cat_clave`) REFERENCES `categorias` (`cat_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_productos_has_categorias_productos1` FOREIGN KEY (`pc_pro_clave`) REFERENCES `productos` (`pro_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `proveedores`
--

DROP TABLE IF EXISTS `proveedores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proveedores` (
  `prov_clave` int(11) NOT NULL AUTO_INCREMENT,
  `prov_nombre` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `prov_appaterno` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `prov_apmaterno` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `prov_nombrecompleto` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `prov_correo` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `prov_telefono` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `prov_status` int(11) DEFAULT NULL,
  PRIMARY KEY (`prov_clave`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `proveedores_productos`
--

DROP TABLE IF EXISTS `proveedores_productos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proveedores_productos` (
  `pp_clave` int(11) NOT NULL AUTO_INCREMENT,
  `pp_prov_clave` int(11) DEFAULT NULL,
  `pp_pro_clave` int(11) DEFAULT NULL,
  `pp_status` int(11) DEFAULT NULL,
  PRIMARY KEY (`pp_clave`),
  KEY `fk_proveedores_has_productos_productos1_idx` (`pp_pro_clave`),
  KEY `fk_proveedores_has_productos_proveedores1_idx` (`pp_prov_clave`),
  CONSTRAINT `fk_proveedores_has_productos_productos1` FOREIGN KEY (`pp_pro_clave`) REFERENCES `productos` (`pro_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_proveedores_has_productos_proveedores1` FOREIGN KEY (`pp_prov_clave`) REFERENCES `proveedores` (`prov_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `puestos`
--

DROP TABLE IF EXISTS `puestos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `puestos` (
  `pue_clave` int(11) NOT NULL AUTO_INCREMENT,
  `pue_nombre` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `pue_status` int(11) DEFAULT NULL,
  PRIMARY KEY (`pue_clave`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sistemaunidades`
--

DROP TABLE IF EXISTS `sistemaunidades`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sistemaunidades` (
  `su_clave` int(11) NOT NULL AUTO_INCREMENT,
  `su_nombre` varchar(255) DEFAULT NULL,
  `su_status` int(11) DEFAULT NULL,
  PRIMARY KEY (`su_clave`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `stocks`
--

DROP TABLE IF EXISTS `stocks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stocks` (
  `sto_clave` int(11) NOT NULL AUTO_INCREMENT,
  `sto_cantidad` float DEFAULT NULL,
  `sto_pro_clave` int(11) NOT NULL,
  PRIMARY KEY (`sto_clave`),
  KEY `fk_stock_productos1_idx` (`sto_pro_clave`),
  CONSTRAINT `fk_stock_productos1` FOREIGN KEY (`sto_pro_clave`) REFERENCES `productos` (`pro_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sucursales`
--

DROP TABLE IF EXISTS `sucursales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sucursales` (
  `suc_clave` int(11) NOT NULL AUTO_INCREMENT,
  `suc_direccion` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `suc_status` int(11) DEFAULT NULL,
  `suc_empr_clave` int(11) DEFAULT NULL,
  PRIMARY KEY (`suc_clave`),
  KEY `fk_sucursales_empresas1_idx` (`suc_empr_clave`),
  CONSTRAINT `fk_sucursales_empresas1` FOREIGN KEY (`suc_empr_clave`) REFERENCES `empresas` (`empr_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `usu_clave` int(11) NOT NULL AUTO_INCREMENT,
  `usu_nombre` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `usu_password` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `usu_tipo` int(11) DEFAULT NULL,
  `usu_nivel` int(11) DEFAULT NULL,
  `usu_status` int(11) DEFAULT NULL,
  `usu_emp_clave` int(11) NOT NULL,
  PRIMARY KEY (`usu_clave`),
  KEY `fk_usuarios_empleados1_idx` (`usu_emp_clave`),
  CONSTRAINT `fk_usuarios_empleados1` FOREIGN KEY (`usu_emp_clave`) REFERENCES `empleados` (`emp_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ventas`
--

DROP TABLE IF EXISTS `ventas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ventas` (
  `ven_clave` int(11) NOT NULL AUTO_INCREMENT,
  `ven_cot_clave` int(11) DEFAULT NULL,
  `ven_ccj_clave` int(11) NOT NULL,
  `ven_cli_clave` int(11) NOT NULL,
  `ven_emp_clave` int(11) NOT NULL,
  `ven_fecha` datetime DEFAULT NULL,
  `ven_tipo` int(11) DEFAULT NULL,
  `ven_folio` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `ven_subtotal` decimal(11,3) DEFAULT NULL,
  `ven_iva` decimal(11,3) DEFAULT NULL,
  `ven_total` decimal(11,3) DEFAULT NULL,
  `ven_cobrado` decimal(11,3) DEFAULT NULL,
  `ven_status` int(11) DEFAULT NULL,
  PRIMARY KEY (`ven_clave`),
  KEY `fk_ventas_corteCaja1_idx` (`ven_ccj_clave`),
  KEY `fk_ventas_clientes1_idx` (`ven_cli_clave`),
  KEY `fk_ventas_empleados1_idx` (`ven_emp_clave`),
  CONSTRAINT `fk_ventas_clientes1` FOREIGN KEY (`ven_cli_clave`) REFERENCES `clientes` (`cli_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ventas_corteCaja1` FOREIGN KEY (`ven_ccj_clave`) REFERENCES `cortecaja` (`ccj_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ventas_empleados1` FOREIGN KEY (`ven_emp_clave`) REFERENCES `empleados` (`emp_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping events for database 'puntoventa'
--

--
-- Dumping routines for database 'puntoventa'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-12-07  6:24:37

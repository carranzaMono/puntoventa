-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: puntoventa
-- ------------------------------------------------------
-- Server version	5.5.5-10.4.17-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `bitacora`
--

DROP TABLE IF EXISTS `bitacora`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bitacora` (
  `bit_clave` int(11) NOT NULL AUTO_INCREMENT,
  `bit_modulo` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `bit_accion` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `bit_tipo` int(11) DEFAULT NULL,
  `bit_emp_clave` int(11) DEFAULT NULL,
  `bit_descripcion` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `bit_claves` int(11) DEFAULT NULL,
  PRIMARY KEY (`bit_clave`)
) ENGINE=InnoDB AUTO_INCREMENT=275 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bitacora`
--

LOCK TABLES `bitacora` WRITE;
/*!40000 ALTER TABLE `bitacora` DISABLE KEYS */;
INSERT INTO `bitacora` VALUES (1,'Catálogo','Actualización',1,1,'Se modifico el puesto Almacenista por AlmacenistaX',0),(2,'Catálogo','Actualización',1,1,'Se modifico el puesto AlmacenistaX por Almacenista',0),(3,'Catálogo','Eliminar',1,1,'Se elimino el puesto Almacenista',0),(4,'Catálogo','Eliminar',1,1,'Se elimino el puesto Almacenista',0),(5,'Catálogo','Eliminar',1,1,'Se elimino el puesto Almacenista',0),(6,'Catálogo','Eliminar',1,1,'Se elimino el puesto Almacenista',0),(7,'Catálogo','Registro',1,1,'Se registro el puesto Almacenista',0),(8,'Catálogo','Registro',1,1,'Se registro el empleado Ismael Carranza Tronco',0),(9,'Catálogo','Eliminar',1,1,'Se elimino el empleado Ismael Carranza Tronco',0),(10,'Catálogo','Eliminar',1,1,'Se elimino el empleado Ismael Carranza Tronco',0),(11,'Catálogo','Registro',1,1,'Se registro el empleado Ismael Carranza Tronco',0),(12,'Catálogo','Actualización',1,1,'Se modifico información del empleado Ismael Carranza Tronco',0),(13,'Catálogo','Actualización',1,1,'Se modifico información del empleado Ismael Carranza Tronco',0),(14,'Catálogo','Actualización',1,1,'Se modifico información del empleado Ismael Carranza Tronco',0),(15,'Catálogo','Actualización',1,1,'Se modifico información del empleado Ismael Carranza Tronco',0),(16,'Catálogo','Actualización',1,1,'Se modifico información del empleado Ismael Carranza Tronco',0),(17,'Catálogo','Actualización',1,1,'Se modifico información del empleado Ismael Carranza Tronco',0),(18,'Catálogo','Actualización',1,1,'Se modifico información del empleado Ismael Carranza Tronco',0),(19,'Catálogo','Actualización',1,1,'Se modifico información del empleado Ismael Carranza Tronco',0),(20,'Catálogo','Eliminar',1,1,'Se elimino al usuario ',0),(21,'Catálogo','Eliminar',1,1,'Se suspendio al usuario 1',0),(22,'Catálogo','Actualización',1,1,'Se modifico el password del usuario 1',0),(23,'Catálogo','Actualización',1,1,'Se modifico información del usuario 1',0),(24,'Catálogo','Actualización',1,1,'Se modifico el password del usuario 1',0),(25,'Catálogo','Actualización',1,1,'Se modifico el password del usuario admin',0),(26,'Catálogo','Actualización',1,1,'Se modifico el password del usuario admin',0),(27,'Catálogo','Actualización',1,1,'Se modifico el password del usuario admin',0),(28,'Catálogo','Registro',1,1,'Se registro la categoria Dulces',0),(29,'Catálogo','Registro',1,1,'Se registro la categoria Galletas',0),(30,'Catálogo','Eliminar',1,1,'Se elimino la categoria Galletas',0),(31,'Catálogo','Actualización',1,1,'Se modifico la categoria  por Cervezas',0),(32,'Catálogo','Actualización',1,1,'Se modifico la categoria  por Refrescos',0),(33,'Catálogo','Actualización',1,1,'Se modifico la categoria Refrescos por Dulces',0),(34,'Catálogo','Registro',1,1,'Se registro la categoria Galletas',0),(35,'Catálogo','Registro',1,1,'Se registro la marca Coca cola',0),(36,'Catálogo','Registro',1,1,'Se registro la marca Coca cola',0),(37,'Catálogo','Registro',1,1,'Se registro la marca Coca cola',0),(38,'Catálogo','Registro',1,1,'Se registro la marca Coca cola',0),(39,'Catálogo','Registro',1,1,'Se registro la marca Coca cola',0),(40,'Catálogo','Registro',1,1,'Se registro la marca Coca cola',0),(41,'Catálogo','Registro',1,1,'Se registro la marca Pepsi',0),(42,'Catálogo','Eliminar',1,1,'Se elimino al usuario ',0),(43,'Catálogo','Eliminar',1,1,'Se elimino al usuario e',0),(44,'Catálogo','Eliminar',1,1,'Se elimino al usuario Pepsi',0),(45,'Catálogo','Registro',1,1,'Se registro la marca Pepsi',0),(46,'Catálogo','Actualización',1,1,'Se modifico la marca Pepsi por Pepsi x',0),(47,'Catálogo','Actualización',1,1,'Se modifico la marca Pepsi x por Pepsi',0),(48,'Catálogo','Registro',1,1,'Se registro el proveedor Ismael Carranza Tronco',0),(49,'Catálogo','Registro',1,1,'Se registro el proveedor Pruebas Pruebas Pruebas',0),(50,'Catálogo','Registro',1,1,'Se registro el proveedor Daniel Nsiau Jdbcibna',0),(51,'Catálogo','Registro',1,1,'Se registro el proveedor Adjsak Ksdnaovñ Lkzdfkmval',0),(52,'Catálogo','Eliminar',1,1,'Se elimino el proveedor Pruebas Pruebas Pruebas',0),(53,'Catálogo','Eliminar',1,1,'Se elimino el proveedor Pruebas Pruebas Pruebas',0),(54,'Catálogo','Registro',1,1,'Se registro la marca Lala',0),(55,'Catálogo','Actualización',1,1,'Se modifico información del proveedor ',0),(56,'Catálogo','Actualización',1,1,'Se modifico información del proveedor ',0),(57,'Catálogo','Actualización',1,1,'Se modifico información del proveedor A Ksdnaovñ Lkzdfkmval',0),(58,'Catálogo','Actualización',1,1,'Se modifico información del proveedor A Ksdnaovñ Lkzdfkmval',0),(59,'Catálogo','Registro',1,1,'Se registro el producto Agua Oxigenada - Balmen',0),(60,'Catálogo','Registro',1,1,'Se registro el producto Agua Oxigenada - Balmen',0),(61,'Catálogo','Registro',1,1,'Se registro el producto Agua Oxigenada - Balmen',0),(62,'Catálogo','Registro',1,1,'Se registro la categoria Antisépticos',0),(63,'Catálogo','Registro',1,1,'Se asigno la categoria Antisépticos al producto Botella 112 ml - Agua Oxigenada - Balmen',0),(64,'Catálogo','Eliminar',1,1,'Se elimino la categoria Antisépticos del producto Botella 112 ml - Agua Oxigenada - Balmen',0),(65,'Catálogo','Eliminar',1,1,'Se elimino el producto Botella 112 ml - Agua Oxigenada - Balmen',0),(66,'Catálogo','Eliminar',1,1,'Se elimino el producto Botella 112 ml - Agua Oxigenada - Balmen',0),(67,'Catálogo','Eliminar',1,1,'Se elimino el producto Botella 112 ml - Agua Oxigenada - Balmen',0),(68,'Catálogo','Actualización',1,1,'Se modifico información del producto Agua Oxigenada - Balmen',0),(69,'Catálogo','Actualización',1,1,'Se modifico información del producto Agua Oxigenada - Balmen',0),(70,'Catálogo','Eliminar',1,1,'Se elimino al usuario admin',0),(71,'Catálogo','Actualización',1,1,'Se modifico el password del usuario admin',0),(72,'Sesión','Incio de sesión',0,1,'Ismael Carranza inicio sesión',0),(73,'Sesión','Incio de sesión',0,1,'Ismael Carranza inicio sesión',0),(74,'Sesión','Incio de sesión',0,1,'Ismael Carranza inicio sesión',0),(75,'Sesión','Incio de sesión',0,1,'Ismael Carranza inicio sesión',0),(76,'Sesión','Incio de sesión',0,1,'Ismael Carranza inicio sesión',0),(77,'Sesión','Incio de sesión',0,1,'Ismael Carranza inicio sesión',0),(78,'Sesión','Fin de sesión',0,1,'Ismael Carranza cerro sesión',0),(79,'Sesión','Fin de sesión',0,1,'Ismael Carranza cerro sesión',0),(80,'Sesión','Incio de sesión',0,1,'Ismael Carranza inicio sesión',0),(81,'Sesión','Fin de sesión',0,1,'Ismael Carranza cerro sesión',0),(82,'Sesión','Incio de sesión',0,1,'Ismael Carranza inicio sesión',0),(83,'Sesión','Fin de sesión',0,1,'Ismael Carranza cerro sesión',0),(84,'Sesión','Fin de sesión',0,0,'team cerro sesión',0),(85,'Sesión','Fin de sesión',0,0,'teamMono cerro sesión',0),(86,'Sesión','Fin de sesión',0,0,'teamMono cerro sesión',0),(87,'Sesión','Incio de sesión',0,1,'Ismael Carranza inicio sesión',0),(88,'Sesión','Incio de sesión',0,1,'Ismael Carranza inicio sesión',0),(89,'Sesión','Fin de sesión',0,1,'Ismael Carranza cerro sesión',0),(90,'Sesión','Incio de sesión',0,1,'Ismael Carranza inicio sesión',0),(91,'Sesión','Incio de sesión',0,1,'Ismael Carranza inicio sesión',0),(92,'Sesión','Incio de sesión',0,1,'Ismael Carranza inicio sesión',0),(93,'Sesión','Incio de sesión',0,1,'Ismael Carranza inicio sesión',0),(94,'Sesión','Incio de sesión',0,1,'Ismael Carranza inicio sesión',0),(95,'Sesión','Incio de sesión',0,1,'Ismael Carranza inicio sesión',0),(96,'Sesión','Fin de sesión',0,131,' cerro sesión',0),(97,'Sesión','Incio de sesión',0,1,'Ismael Carranza inicio sesión',0),(98,'Sesión','Fin de sesión',0,131,' cerro sesión',0),(99,'Sesión','Incio de sesión',0,1,'Ismael Carranza inicio sesión',0),(100,'Caja','Registro',2,1,'Se abrio el corte de caja con folio 0001',0),(101,'Caja','Registro',2,1,'Se abrio el corte de caja con folio 0001',0),(102,'Sesión','Incio de sesión',0,1,'Ismael Carranza inicio sesión',0),(103,'Sesión','Incio de sesión',0,1,'Ismael Carranza inicio sesión',0),(104,'Caja','Salida',2,1,'Se registro una salida con monto 100 para la caja0001',0),(105,'Caja','Salida',2,1,'Se registro una salida con monto 5 para la caja0001',0),(106,'Caja','Salida',2,1,'Se registro una salida con monto 50 para la caja0001',0),(107,'Sesión','Incio de sesión',0,1,'Ismael Carranza inicio sesión',0),(108,'Cotización','Resgistro de producto',3,1,'Se agrego el producto: Agua Oxigenada - Balmen a la cotización 0010',0),(109,'Cotización','Resgistro de producto',3,1,'Se agrego el producto: Agua Oxigenada - Balmen a la cotización 0010',0),(110,'Catálogo','Registro',1,1,'Se registro el cliente P??blico en general',0),(111,'Cotización','Nueva cotización',1,1,'Se creo cotización con folio 0001 para el cliente Público en general',0),(112,'Caja','Registro',2,1,'Se abrio el corte de caja con folio 0001',0),(113,'Cotización','Nueva cotización',1,1,'Se creo cotización con folio 0001 para el cliente Público en general',0),(114,'Cotización','Nueva cotización',1,1,'Se creo cotización con folio 0001 para el cliente Público en general',0),(115,'Cotización','Nueva cotización',1,1,'Se creo cotización con folio 0001 para el cliente Público en general',0),(116,'Cotización','Resgistro de producto',3,1,'Se agrego el producto: Agua Oxigenada - Balmen a la cotización 0001',0),(117,'Cotización','Resgistro de producto',3,1,'Se agrego el producto: Agua Oxigenada - Balmen a la cotización 0001',0),(118,'Catálogo','Registro',1,1,'Se registro el producto 3D white - Oral B',0),(119,'Catálogo','Registro',1,1,'Se registro la categoria Pasta dental',0),(120,'Catálogo','Registro',1,1,'Se asigno la categoria Pasta dental al producto Paquete 180 g - 3D white - Oral B',0),(121,'Catálogo','Registro',1,1,'Se registro el producto Alcohol Etílico - Atlanta',0),(122,'Catálogo','Registro',1,1,'Se asigno la categoria Antisépticos al producto Botella 250 ml - Alcohol Etílico - Atlanta',0),(123,'Catálogo','Registro',1,1,'Se registro la categoria Suspensión',0),(124,'Catálogo','Registro',1,1,'Se registro el producto Dexametasona - Alin Depot',0),(125,'Catálogo','Registro',1,1,'Se asigno la categoria Suspensión al producto Caja 2 ml - Dexametasona - Alin Depot',0),(126,'Cotización','Cotización a Venta',3,1,'Se convirtio la cotizacion 0001a la venta ',0),(127,'Cotización','Cotización a Venta',3,1,'Se convirtio la cotizacion 0001a la venta 0001',0),(128,'Catálogo','Actualización',1,1,'Se modifico información del producto 3D white - Oral B',0),(129,'Catálogo','Eliminar',1,1,'Se elimino el producto Paquete 2 pza. - 3D white - Oral B',0),(130,'Catálogo','Eliminar',1,1,'Se elimino el producto Paquete 2 pza. - 3D white - Oral B',0),(131,'Catálogo','Eliminar',1,1,'Se elimino el producto Paquete 2 pza. - 3D white - Oral B',0),(132,'Catálogo','Eliminar',1,1,'Se elimino el producto Paquete 2 pza. - 3D white - Oral B',0),(133,'Catálogo','Actualización',1,1,'Se modifico información del producto 3D white - Oral B',0),(134,'Catálogo','Registro',1,1,'Se registro la categoria  Antibióticos',0),(135,'Catálogo','Registro',1,1,'Se asigno la categoria  Antibióticos al producto Caja 20 pza. - Treda 129mg/280mg/30mg',0),(136,'Catálogo','Actualización',1,1,'Se modifico información del producto Treda 129mg/280mg/30mg',0),(137,'Catálogo','Actualización',1,1,'Se modifico información del producto Treda 129mg/280mg/30mg',0),(138,'Catálogo','Registro',1,1,'Se asigno la categoria  Antibióticos al producto Botella 75 ml - Treda Suspensión',0),(139,'Catálogo','Registro',1,1,'Se asigno la categoria  Antibióticos al producto Caja 20 pza. - Treda 129mg/280mg/30mg Tabletas',0),(140,'Catálogo','Registro',1,1,'Se asigno la categoria  Antibióticos al producto Botella 75 ml - Treda Suspensión',0),(141,'Catálogo','Registro',1,1,'Se registro la categoria Accesorios Médicos',0),(142,'Catálogo','Actualización',1,1,'Se modifico información del producto Treda 129mg/280mg/30mg',0),(143,'Catálogo','Registro',1,1,'Se asigno la categoria Accesorios Médicos al producto Caja 1 pza. - Collarín Le Roy Cervical Grande',0),(144,'Catálogo','Registro',1,1,'Se asigno la categoria Accesorios Médicos al producto Caja 20 pza. - Treda 129mg/280mg/30mg',0),(145,'Catálogo','Actualización',1,1,'Se modifico información del producto Treda 129mg/280mg/30mg',0),(146,'Catálogo','Actualización',1,1,'Se modifico información del producto Collarín Le Roy Cervical Grande',0),(147,'Catálogo','Actualización',1,1,'Se modifico información del producto Termómetro Citizen Digital CTA 303',0),(148,'Catálogo','Actualización',1,1,'Se modifico información del producto Treda 129mg/280mg/30mg Tabletas',0),(149,'Catálogo','Actualización',1,1,'Se modifico información del producto Treda 129mg/280mg/30mg Tabletas',0),(150,'Catálogo','Actualización',1,1,'Se modifico información del producto Treda Suspensión',0),(151,'Catálogo','Registro',1,1,'Se registro el producto Termómetro Digital de Frente Omron MC-720',0),(152,'Catálogo','Registro',1,1,'Se asigno la categoria Accesorios Médicos al producto Pieza 1 pza. - Termómetro Digital de Frente Omron MC-720',0),(153,'Catálogo','Registro',1,1,'Se registro el producto Termómetro Digital Sunshine con Punta Flexible',0),(154,'Catálogo','Registro',1,1,'Se asigno la categoria Accesorios Médicos al producto Pieza 1 pza. - Termómetro Digital Sunshine con Punta Flexible',0),(155,'Catálogo','Registro',1,1,'Se registro el producto Oxímetro Digital de Pulso LMT-01',0),(156,'Catálogo','Registro',1,1,'Se asigno la categoria Accesorios Médicos al producto Pieza 1 pza. - Oxímetro Digital de Pulso LMT-01',0),(157,'Catálogo','Registro',1,1,'Se registro el producto Termómetro Digital de Pluma',0),(158,'Catálogo','Registro',1,1,'Se asigno la categoria Accesorios Médicos al producto Pieza 1 pza. - Termómetro Digital de Pluma',0),(159,'Catálogo','Registro',1,1,'Se registro el producto Termómetro Infrarrojo Hi8us HG01 de Frente',0),(160,'Catálogo','Registro',1,1,'Se asigno la categoria Accesorios Médicos al producto Pieza 1 pza. - Termómetro Infrarrojo Hi8us HG01 de Frente',0),(161,'Catálogo','Registro',1,1,'Se registro el producto Oxímetro Digital de Pulso Xignal para Adulto',0),(162,'Catálogo','Registro',1,1,'Se asigno la categoria Accesorios Médicos al producto Pieza 1 pza. - Oxímetro Digital de Pulso Xignal para Adulto',0),(163,'Catálogo','Registro',1,1,'Se registro el producto Oxímetro de Pulso Levy and Lulu Digital',0),(164,'Catálogo','Registro',1,1,'Se asigno la categoria Accesorios Médicos al producto Pieza 1 pza. - Oxímetro de Pulso Levy and Lulu Digital',0),(165,'Catálogo','Registro',1,1,'Se registro el producto Monitor Omron Presión Arterial Automático Brazo',0),(166,'Catálogo','Registro',1,1,'Se asigno la categoria Accesorios Médicos al producto Pieza 1 pza. - Monitor Omron Presión Arterial Automático Brazo',0),(167,'Catálogo','Registro',1,1,'Se registro el producto Nebulizador de Compresor Omron Ne-C801LA',0),(168,'Catálogo','Registro',1,1,'Se asigno la categoria Accesorios Médicos al producto Pieza 1 pza. - Nebulizador de Compresor Omron Ne-C801LA',0),(169,'Catálogo','Registro',1,1,'Se registro el producto Nebulizador Omron de Compresor Mod NE-C101',0),(170,'Catálogo','Registro',1,1,'Se asigno la categoria Accesorios Médicos al producto Pieza 1 pza. - Nebulizador Omron de Compresor Mod NE-C101',0),(171,'Catálogo','Registro',1,1,'Se registro el producto Baumanómetro Citizen Ch-617',0),(172,'Catálogo','Registro',1,1,'Se asigno la categoria Accesorios Médicos al producto Pieza 1 pza. - Baumanómetro Citizen Ch-617',0),(173,'Catálogo','Registro',1,1,'Se registro el producto Nebulizador Nebucor P-103',0),(174,'Catálogo','Registro',1,1,'Se asigno la categoria Accesorios Médicos al producto Pieza 1 pza. - Nebulizador Nebucor P-103',0),(175,'Catálogo','Registro',1,1,'Se registro el producto Baumanómetro Citizen Ch-452 Brazo',0),(176,'Catálogo','Registro',1,1,'Se asigno la categoria Accesorios Médicos al producto Pieza 1 pza. - Baumanómetro Citizen Ch-452 Brazo',0),(177,'Catálogo','Registro',1,1,'Se registro el producto Nebulizador Nebucor De Compresor P102',0),(178,'Catálogo','Registro',1,1,'Se asigno la categoria Accesorios Médicos al producto Pieza 1 pza. - Nebulizador Nebucor De Compresor P102',0),(179,'Catálogo','Registro',1,1,'Se registro el producto Termómetro Infrarrojo Levy and Lulu a Distancia',0),(180,'Catálogo','Registro',1,1,'Se asigno la categoria Accesorios Médicos al producto Pieza 1 pza. - Termómetro Infrarrojo Levy and Lulu a Distancia',0),(181,'Catálogo','Registro',1,1,'Se registro el producto Kit Para Nebulizador Sun Shine 6pzas',0),(182,'Catálogo','Registro',1,1,'Se asigno la categoria Accesorios Médicos al producto Bolsa 1 pza. - Kit Para Nebulizador Sun Shine 6pzas',0),(183,'Catálogo','Registro',1,1,'Se registro el producto Monitor Neutek de Presión Sanguínea Brazo Digital',0),(184,'Catálogo','Registro',1,1,'Se asigno la categoria Accesorios Médicos al producto Pieza 1 pza. - Monitor Neutek de Presión Sanguínea Brazo Digital',0),(185,'Catálogo','Registro',1,1,'Se registro el producto Termómetro Omron Digital MC-246',0),(186,'Catálogo','Registro',1,1,'Se asigno la categoria Accesorios Médicos al producto Pieza 1 pza. - Termómetro Omron Digital MC-246',0),(187,'Catálogo','Registro',1,1,'Se registro el producto Monitor Omron de Presión Arterial Automático Muñeca con 30 Memorias',0),(188,'Catálogo','Registro',1,1,'Se asigno la categoria Accesorios Médicos al producto Pieza 1 pza. - Monitor Omron de Presión Arterial Automático Muñeca con 30 Memorias',0),(189,'Catálogo','Registro',1,1,'Se registro el producto Termómetro Digital Citizen de Pluma',0),(190,'Catálogo','Registro',1,1,'Se asigno la categoria Accesorios Médicos al producto Pieza 1 pza. - Termómetro Digital Citizen de Pluma',0),(191,'Catálogo','Registro',1,1,'Se registro el producto Termómetro Digital Contra el Agua MT 201C',0),(192,'Catálogo','Registro',1,1,'Se asigno la categoria Accesorios Médicos al producto Pieza 1 pza. - Termómetro Digital Contra el Agua MT 201C',0),(193,'Catálogo','Registro',1,1,'Se registro el producto Baumanómetro de Muñeca Nebucor HL168F',0),(194,'Catálogo','Registro',1,1,'Se asigno la categoria Accesorios Médicos al producto Pieza 1 pza. - Baumanómetro de Muñeca Nebucor HL168F',0),(195,'Catálogo','Registro',1,1,'Se registro el producto Férula Flents Insty Splint Para Dedos 2 Piezas Mediano/Grande',0),(196,'Catálogo','Registro',1,1,'Se asigno la categoria Accesorios Médicos al producto Paquete 1 pza. - Férula Flents Insty Splint Para Dedos 2 Piezas Mediano/Grande',0),(197,'Catálogo','Registro',1,1,'Se registro el producto Monitor Neutek De Presión Sanguínea Para Muñeca Automático',0),(198,'Catálogo','Registro',1,1,'Se asigno la categoria Accesorios Médicos al producto Pieza 1 pza. - Monitor Neutek De Presión Sanguínea Para Muñeca Automático',0),(199,'Catálogo','Registro',1,1,'Se registro el producto Monitor Omron de Presión Arterial Automático Muñeca',0),(200,'Catálogo','Registro',1,1,'Se asigno la categoria Accesorios Médicos al producto Pieza 1 pza. - Monitor Omron de Presión Arterial Automático Muñeca',0),(201,'Catálogo','Registro',1,1,'Se registro el producto Baumanómetro Nebucor HL888JA Brazo',0),(202,'Catálogo','Registro',1,1,'Se asigno la categoria Accesorios Médicos al producto Pieza 1 pza. - Baumanómetro Nebucor HL888JA Brazo',0),(203,'Catálogo','Registro',1,1,'Se registro el producto Termómetro Infrarrojo HG01',0),(204,'Catálogo','Registro',1,1,'Se asigno la categoria Accesorios Médicos al producto Pieza 1 pza. - Termómetro Infrarrojo HG01',0),(205,'Catálogo','Registro',1,1,'Se registro el producto Termómetro Omron Digital Flex MC 343F',0),(206,'Catálogo','Registro',1,1,'Se asigno la categoria Accesorios Médicos al producto Pieza 1 pza. - Termómetro Omron Digital Flex MC 343F',0),(207,'Catálogo','Registro',1,1,'Se registro el producto Nebulizador Sun Shine Flujo Continuo',0),(208,'Catálogo','Registro',1,1,'Se asigno la categoria Accesorios Médicos al producto Pieza 1 pza. - Nebulizador Sun Shine Flujo Continuo',0),(209,'Catálogo','Registro',1,1,'Se registro el producto Nebulizador Neutek Hi-Neb de Pistón',0),(210,'Catálogo','Registro',1,1,'Se asigno la categoria Accesorios Médicos al producto Pieza 1 pza. - Nebulizador Neutek Hi-Neb de Pistón',0),(211,'Catálogo','Registro',1,1,'Se registro el producto Monitor Omron de Presión Arterial de Muñeca Automático Mod HEM-6230',0),(212,'Catálogo','Registro',1,1,'Se asigno la categoria Accesorios Médicos al producto Pieza 1 pza. - Monitor Omron de Presión Arterial de Muñeca Automático Mod HEM-6230',0),(213,'Catálogo','Registro',1,1,'Se registro el producto Muñequera Para Pulgar Barrere Mediano',0),(214,'Catálogo','Registro',1,1,'Se asigno la categoria Accesorios Médicos al producto Pieza 1 pza. - Muñequera Para Pulgar Barrere Mediano',0),(215,'Catálogo','Registro',1,1,'Se registro el producto Nebulizador Sun Shine con Pistón',0),(216,'Catálogo','Registro',1,1,'Se asigno la categoria Accesorios Médicos al producto Pieza 1 pza. - Nebulizador Sun Shine con Pistón',0),(217,'Catálogo','Registro',1,1,'Se registro el producto Rodillera Elástica Rekord Le Roy Mediana',0),(218,'Catálogo','Registro',1,1,'Se asigno la categoria Accesorios Médicos al producto Pieza 1 pza. - Rodillera Elástica Rekord Le Roy Mediana',0),(219,'Catálogo','Registro',1,1,'Se registro el producto Tobillera Elástica Rekord Le Roy Mediana',0),(220,'Catálogo','Registro',1,1,'Se asigno la categoria Accesorios Médicos al producto Pieza 1 pza. - Tobillera Elástica Rekord Le Roy Mediana',0),(221,'Catálogo','Registro',1,1,'Se registro el producto Rodillera Elástica Rekord Le Roy Grande',0),(222,'Catálogo','Registro',1,1,'Se asigno la categoria Accesorios Médicos al producto Pieza 1 pza. - Rodillera Elástica Rekord Le Roy Grande',0),(223,'Catálogo','Registro',1,1,'Se registro el producto Collarín Barrere Blando Beige Grande',0),(224,'Catálogo','Registro',1,1,'Se asigno la categoria Accesorios Médicos al producto Pieza 1 pza. - Collarín Barrere Blando Beige Grande',0),(225,'Sesión','Incio de sesión',0,1,'Ismael Carranza inicio sesión',0),(226,'Sesión','Incio de sesión',0,1,'Ismael Carranza inicio sesión',0),(227,'Cotización','Nueva cotización',3,1,'Se creo la cotización 0001 para el cliente Público en general',0),(228,'Cotización','Cotización a Venta',3,1,'Se convirtio la cotizacion 0004a la venta 0001',0),(229,'Cotización','Resgistro de producto',3,1,'Se agrego el producto: Baumanómetro Citizen Ch-452 Brazo a la cotización 0003',0),(230,'Cotización','Cotización a Venta',3,1,'Se convirtio la cotizacion 0003a la venta error',0),(231,'Cotización','Cotización a Venta',3,1,'Se convirtio la cotizacion 0003a la venta error',0),(232,'Cotización','Cotización a Venta',3,1,'Se convirtio la cotizacion 0003a la venta ',0),(233,'Cotización','Cotización a Venta',3,1,'Se convirtio la cotizacion 0003a la venta ',0),(234,'Cotización','Cotización a Venta',3,1,'Se convirtio la cotizacion 0003a la venta ',0),(235,'Cotización','Cotización a Venta',3,1,'Se convirtio la cotizacion 0003a la venta ',0),(236,'Cotización','Cotización a Venta',3,1,'Se convirtio la cotizacion 0003a la venta ',0),(237,'Cotización','Cotización a Venta',3,1,'Se convirtio la cotizacion 0003a la venta ',0),(238,'Cotización','Cotización a Venta',3,1,'Se convirtio la cotizacion 0003a la venta ',0),(239,'Cotización','Cotización a Venta',3,1,'Se convirtio la cotizacion 0003a la venta 1',0),(240,'Cotización','Cotización a Venta',3,1,'Se convirtio la cotizacion 0003a la venta ',0),(241,'Cotización','Cotización a Venta',3,1,'Se convirtio la cotizacion 0003a la venta ',0),(242,'Cotización','Cotización a Venta',3,1,'Se convirtio la cotizacion 0003a la venta 0001',0),(243,'Cotización','Cotización a Venta',3,1,'Se convirtio la cotizacion 0003a la venta 0001',0),(244,'Sesión','Incio de sesión',0,1,'Ismael Carranza inicio sesión',0),(245,'Cotización','Nueva cotización',1,1,'Se creo cotización con folio  para el cliente Juan López',0),(246,'Cotización','Nueva cotización',1,1,'Se creo cotización con folio  para el cliente Juan López',0),(247,'Cotización','Nueva cotización',1,1,'Se creo cotización con folio  para el cliente Juan López',0),(248,'Cotización','Nueva cotización',1,1,'Se creo cotización con folio  para el cliente Juan López',0),(249,'Cotización','Nueva cotización',1,1,'Se creo cotización con folio 0001 para el cliente Juan López',0),(250,'Cotización','Nueva cotización',1,1,'Se creo cotización con folio 0001 para el cliente Público en general',0),(251,'Cotización','Nueva cotización',1,1,'Se creo cotización con folio 0001 para el cliente Público en general',0),(252,'Cotización','Resgistro de producto',3,1,'Se agrego el producto: Baumanómetro Citizen Ch-452 Brazo a la cotización 0002',0),(253,'Cotización','Resgistro de producto',3,1,'Se agrego el producto: Rodillera Elástica Rekord Le Roy Mediana a la cotización 0002',0),(254,'Cotización','Cotización a Venta',3,1,'Se convirtio la cotizacion 0002a la venta 0001',0),(255,'Cotización','Nueva cotización',3,1,'Se creo la cotización 0003',0),(256,'Sesión','Incio de sesión',0,1,'Ismael Carranza inicio sesión',0),(257,'Sesión','Fin de sesión',0,131,'Ismael Carranza cerro sesión',0),(258,'Sesión','Incio de sesión',0,1,'Ismael Carranza inicio sesión',0),(259,'Sesión','Incio de sesión',0,1,'Ismael Carranza inicio sesión',0),(260,'Sesión','Incio de sesión',0,1,'Ismael Carranza inicio sesión',0),(261,'Cotización','Resgistro de producto',3,1,'Se agrego el producto: Baumanómetro Citizen Ch-452 Brazo a la cotización 0003',0),(262,'Cotización','Resgistro de producto',3,1,'Se agrego el producto: Treda 129mg/280mg/30mg 20 Tabletas a la cotización 0003',0),(263,'Cotización','Resgistro de producto',3,1,'Se agrego el producto: Rodillera Elástica Rekord Le Roy Mediana a la cotización 0003',0),(264,'Cotización','Nueva cotización',3,1,'Se creo la cotización 0001',0),(265,'Cotización','Resgistro de producto',3,1,'Se agrego el producto: Kit Para Nebulizador Sun Shine 6pzas a la cotización 0001',0),(266,'Cotización','Resgistro de producto',3,1,'Se agrego el producto: Oxímetro de Pulso Levy and Lulu Digital a la cotización 0001',0),(267,'Cotización','Resgistro de producto',3,1,'Se agrego el producto: Treda 129mg/280mg/30mg 20 Tabletas a la cotización 0001',0),(268,'Cotización','Resgistro de producto',3,1,'Se agrego el producto: Collarín Le Roy Cervical Grande a la cotización 0001',0),(269,'Cotización','Cotización a Venta',3,1,'Se convirtio la cotizacion 0001a la venta 0001',0),(270,'Cotización','Nueva cotización',3,1,'Se creo la cotización 0002',0),(271,'Venta','Resgistro de producto',3,1,'Se agrego el producto: Monitor Omron Presión Arterial Automático Brazo a la venta 0001',0),(272,'Venta','Resgistro de producto',3,1,'Se agrego el producto: Collarín Barrere Blando Beige Grande a la venta 0001',0),(273,'Sesión','Incio de sesión',0,1,'Ismael Carranza inicio sesión',0),(274,'Sesión','Incio de sesión',0,1,'Ismael Carranza inicio sesión',0);
/*!40000 ALTER TABLE `bitacora` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categorias`
--

DROP TABLE IF EXISTS `categorias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categorias` (
  `cat_clave` int(11) NOT NULL AUTO_INCREMENT,
  `cat_nombre` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `cat_status` int(11) DEFAULT NULL,
  PRIMARY KEY (`cat_clave`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categorias`
--

LOCK TABLES `categorias` WRITE;
/*!40000 ALTER TABLE `categorias` DISABLE KEYS */;
INSERT INTO `categorias` VALUES (1,' Antibióticos',1),(2,'Accesorios Médicos',1);
/*!40000 ALTER TABLE `categorias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clientes`
--

DROP TABLE IF EXISTS `clientes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clientes` (
  `cli_clave` int(11) NOT NULL AUTO_INCREMENT,
  `cli_nombre` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `cli_telefono` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `cli_correo` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `cli_direccion` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `cli_status` int(11) DEFAULT NULL,
  PRIMARY KEY (`cli_clave`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clientes`
--

LOCK TABLES `clientes` WRITE;
/*!40000 ALTER TABLE `clientes` DISABLE KEYS */;
INSERT INTO `clientes` VALUES (1,'Público en general','Sin teléfono','sincorreo@correo.com','Sin dirección',1);
/*!40000 ALTER TABLE `clientes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cobros`
--

DROP TABLE IF EXISTS `cobros`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cobros` (
  `cob_clave` int(11) NOT NULL AUTO_INCREMENT,
  `cob_fecha` datetime DEFAULT NULL,
  `cob_fcp_clave` int(11) NOT NULL,
  `cob_ven_clave` int(11) NOT NULL,
  `cob_ccj_clave` int(11) NOT NULL,
  `cob_pagaroncon` decimal(11,3) DEFAULT NULL,
  `cob_cambio` decimal(11,3) DEFAULT NULL,
  `cob_monto` decimal(11,3) DEFAULT NULL,
  `cob_descuento` decimal(11,3) DEFAULT NULL,
  `cob_emp_clave` int(11) NOT NULL,
  `cob_status` int(11) DEFAULT NULL,
  `cob_cli_clave` int(11) NOT NULL,
  PRIMARY KEY (`cob_clave`),
  KEY `fk_cobros_formasCobroPago1_idx` (`cob_fcp_clave`),
  KEY `fk_cobros_ventas1_idx` (`cob_ven_clave`),
  KEY `fk_cobros_corteCaja1_idx` (`cob_ccj_clave`),
  KEY `fk_cobros_empleados1_idx` (`cob_emp_clave`),
  KEY `fk_cobros_clientes1_idx` (`cob_cli_clave`),
  CONSTRAINT `fk_cobros_clientes1` FOREIGN KEY (`cob_cli_clave`) REFERENCES `clientes` (`cli_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_cobros_corteCaja1` FOREIGN KEY (`cob_ccj_clave`) REFERENCES `cortecaja` (`ccj_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_cobros_empleados1` FOREIGN KEY (`cob_emp_clave`) REFERENCES `empleados` (`emp_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_cobros_formasCobroPago1` FOREIGN KEY (`cob_fcp_clave`) REFERENCES `formascobropago` (`fp_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_cobros_ventas1` FOREIGN KEY (`cob_ven_clave`) REFERENCES `ventas` (`ven_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cobros`
--

LOCK TABLES `cobros` WRITE;
/*!40000 ALTER TABLE `cobros` DISABLE KEYS */;
INSERT INTO `cobros` VALUES (1,'2021-09-27 04:01:58',1,1,1,1000.000,0.000,1000.000,NULL,1,1,1);
/*!40000 ALTER TABLE `cobros` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cortecaja`
--

DROP TABLE IF EXISTS `cortecaja`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cortecaja` (
  `ccj_clave` int(11) NOT NULL AUTO_INCREMENT,
  `ccj_folio` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `ccj_inicio` datetime DEFAULT NULL,
  `ccj_fin` datetime DEFAULT NULL,
  `ccj_diferiencia` decimal(11,3) DEFAULT NULL,
  `ccj_total` decimal(11,3) DEFAULT NULL,
  `ccj_emp_abrio` int(11) DEFAULT NULL,
  `ccj_status` int(11) DEFAULT NULL,
  `ccj_emp_cerro` int(11) DEFAULT NULL,
  PRIMARY KEY (`ccj_clave`),
  KEY `fk_corteCaja_empleados1_idx` (`ccj_emp_abrio`),
  CONSTRAINT `fk_corteCaja_empleados1` FOREIGN KEY (`ccj_emp_abrio`) REFERENCES `empleados` (`emp_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cortecaja`
--

LOCK TABLES `cortecaja` WRITE;
/*!40000 ALTER TABLE `cortecaja` DISABLE KEYS */;
INSERT INTO `cortecaja` VALUES (1,'0001','2021-09-13 16:21:50',NULL,NULL,NULL,1,0,NULL);
/*!40000 ALTER TABLE `cortecaja` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cotizaciones`
--

DROP TABLE IF EXISTS `cotizaciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cotizaciones` (
  `cot_clave` int(11) NOT NULL AUTO_INCREMENT,
  `cot_tipo` int(11) DEFAULT NULL,
  `cot_fecha` datetime DEFAULT NULL,
  `cot_folio` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `cot_subtotal` decimal(11,3) DEFAULT NULL,
  `cot_iva` decimal(11,3) DEFAULT NULL,
  `cot_total` decimal(11,3) DEFAULT NULL,
  `cot_ccj_clave` int(11) DEFAULT NULL,
  `cot_status` int(11) DEFAULT NULL,
  `cot_cli_clave` int(11) DEFAULT NULL,
  PRIMARY KEY (`cot_clave`),
  KEY `fk_cotizaciones_cortecaja1_idx` (`cot_ccj_clave`),
  CONSTRAINT `fk_cotizaciones_cortecaja1` FOREIGN KEY (`cot_ccj_clave`) REFERENCES `cortecaja` (`ccj_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cotizaciones`
--

LOCK TABLES `cotizaciones` WRITE;
/*!40000 ALTER TABLE `cotizaciones` DISABLE KEYS */;
INSERT INTO `cotizaciones` VALUES (1,1,'2021-09-24 13:02:14','0001',1109.900,153.089,1262.989,1,1,1),(2,1,'2021-09-26 10:53:57','0002',NULL,NULL,NULL,1,1,1);
/*!40000 ALTER TABLE `cotizaciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detallecotizacion`
--

DROP TABLE IF EXISTS `detallecotizacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detallecotizacion` (
  `dc_clave` int(11) NOT NULL AUTO_INCREMENT,
  `dc_cot_clave` int(11) NOT NULL,
  `dc_precioUnitario` decimal(11,3) DEFAULT NULL,
  `dc_cantidad` float DEFAULT NULL,
  `dc_subtotal` decimal(11,2) DEFAULT NULL,
  `dc_iva` decimal(11,3) DEFAULT NULL,
  `dc_total` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `dc_stauts` int(11) DEFAULT NULL,
  `dc_pro_clave` int(11) DEFAULT NULL,
  PRIMARY KEY (`dc_clave`),
  KEY `fk_detalleCotizacion_cotizaciones1_idx` (`dc_cot_clave`),
  CONSTRAINT `fk_detalleCotizacion_cotizaciones1` FOREIGN KEY (`dc_cot_clave`) REFERENCES `cotizaciones` (`cot_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detallecotizacion`
--

LOCK TABLES `detallecotizacion` WRITE;
/*!40000 ALTER TABLE `detallecotizacion` DISABLE KEYS */;
INSERT INTO `detallecotizacion` VALUES (1,1,438.890,1,438.89,70.222,'509.1124',1,13),(2,1,117.500,3,352.50,56.400,'408.9',1,1),(3,1,165.420,1,165.42,26.467,'191.8872',1,5);
/*!40000 ALTER TABLE `detallecotizacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detalleinventarios`
--

DROP TABLE IF EXISTS `detalleinventarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detalleinventarios` (
  `di_clave` int(11) NOT NULL AUTO_INCREMENT,
  `di_inv_clave` int(11) DEFAULT NULL,
  `di_pro_clave` int(11) DEFAULT NULL,
  `di_cantidad` float DEFAULT NULL,
  `di_tipomov` int(11) DEFAULT NULL,
  `di_totalCompra` decimal(11,3) DEFAULT NULL,
  `di_totalSalida` decimal(11,3) DEFAULT NULL,
  `di_status` int(11) DEFAULT NULL,
  PRIMARY KEY (`di_clave`),
  KEY `fk_detalleinventarios_inventarios1_idx` (`di_inv_clave`),
  KEY `fk_detalleinventarios_productos1_idx` (`di_pro_clave`),
  CONSTRAINT `fk_detalleinventarios_inventarios1` FOREIGN KEY (`di_inv_clave`) REFERENCES `inventarios` (`inv_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_detalleinventarios_productos1` FOREIGN KEY (`di_pro_clave`) REFERENCES `productos` (`pro_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detalleinventarios`
--

LOCK TABLES `detalleinventarios` WRITE;
/*!40000 ALTER TABLE `detalleinventarios` DISABLE KEYS */;
/*!40000 ALTER TABLE `detalleinventarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detalleventas`
--

DROP TABLE IF EXISTS `detalleventas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detalleventas` (
  `dv_clave` int(11) NOT NULL AUTO_INCREMENT,
  `dv_fecha` datetime DEFAULT NULL,
  `dv_ccj_clave` int(11) DEFAULT NULL,
  `dv_ven_clave` int(11) NOT NULL,
  `dv_pro_clave` int(11) DEFAULT NULL,
  `dv_precioUnitario` decimal(11,3) DEFAULT NULL,
  `dv_cantidad` float DEFAULT NULL,
  `dv_subtotal` decimal(11,3) DEFAULT NULL,
  `dv_iva` decimal(11,3) DEFAULT NULL,
  `dv_total` decimal(11,3) DEFAULT NULL,
  `dv_status` int(11) DEFAULT NULL,
  `dv_tipo` int(11) DEFAULT NULL,
  PRIMARY KEY (`dv_clave`),
  KEY `fk_detalleVentas_ventas1_idx` (`dv_ven_clave`),
  KEY `fk_detalleVentas_productos1_idx` (`dv_pro_clave`),
  KEY `fk_detalleventas_cortecaja1_idx` (`dv_ccj_clave`),
  CONSTRAINT `fk_detalleVentas_productos1` FOREIGN KEY (`dv_pro_clave`) REFERENCES `productos` (`pro_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_detalleVentas_ventas1` FOREIGN KEY (`dv_ven_clave`) REFERENCES `ventas` (`ven_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_detalleventas_cortecaja1` FOREIGN KEY (`dv_ccj_clave`) REFERENCES `cortecaja` (`ccj_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detalleventas`
--

LOCK TABLES `detalleventas` WRITE;
/*!40000 ALTER TABLE `detalleventas` DISABLE KEYS */;
INSERT INTO `detalleventas` VALUES (1,'2021-09-26 02:38:33',1,1,13,438.890,1,438.890,70.222,509.112,1,2),(2,'2021-09-26 02:38:33',1,1,1,117.500,3,352.500,56.400,408.900,1,2),(3,'2021-09-26 02:38:33',1,1,5,165.420,1,165.420,26.467,191.887,1,2),(25,'2021-09-27 03:46:30',1,1,43,102.490,1,102.490,16.398,118.888,1,1);
/*!40000 ALTER TABLE `detalleventas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `empleados`
--

DROP TABLE IF EXISTS `empleados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `empleados` (
  `emp_clave` int(11) NOT NULL AUTO_INCREMENT,
  `emp_nombre` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `emp_apPaterno` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `emp_apMaterno` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `emp_direccion` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `emp_telefono` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `emp_curp` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `emp_rfc` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `emp_correo` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `emp_status` int(11) DEFAULT NULL,
  `emp_pue_clave` int(11) NOT NULL,
  `emp_nombreCompleto` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`emp_clave`),
  KEY `fk_empleados_puestos_idx` (`emp_pue_clave`),
  CONSTRAINT `fk_empleados_puestos` FOREIGN KEY (`emp_pue_clave`) REFERENCES `puestos` (`pue_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empleados`
--

LOCK TABLES `empleados` WRITE;
/*!40000 ALTER TABLE `empleados` DISABLE KEYS */;
INSERT INTO `empleados` VALUES (1,'Ismael','Carranza','Tronco','546546fzkvaom','kmfzolmzÑ','54646464','564646','carranzatronco@gmail.com',1,3,'Ismael Carranza Tronco');
/*!40000 ALTER TABLE `empleados` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `empresas`
--

DROP TABLE IF EXISTS `empresas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `empresas` (
  `empr_clave` int(11) NOT NULL AUTO_INCREMENT,
  `empr_nombre` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `empr_direccion` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `empr_logo` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `empr_status` int(11) DEFAULT NULL,
  PRIMARY KEY (`empr_clave`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empresas`
--

LOCK TABLES `empresas` WRITE;
/*!40000 ALTER TABLE `empresas` DISABLE KEYS */;
/*!40000 ALTER TABLE `empresas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `formascobropago`
--

DROP TABLE IF EXISTS `formascobropago`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `formascobropago` (
  `fp_clave` int(11) NOT NULL AUTO_INCREMENT,
  `fp_nombre` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `fp_tipo` int(11) DEFAULT NULL,
  `fp_status` int(11) DEFAULT NULL,
  PRIMARY KEY (`fp_clave`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `formascobropago`
--

LOCK TABLES `formascobropago` WRITE;
/*!40000 ALTER TABLE `formascobropago` DISABLE KEYS */;
INSERT INTO `formascobropago` VALUES (1,'Efectivo',1,1),(2,'Tarjeta de credito',0,1),(3,'Tarjeta de debito',0,1),(4,'Vale de despensas',0,1),(5,'Cheque',0,1);
/*!40000 ALTER TABLE `formascobropago` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inventarios`
--

DROP TABLE IF EXISTS `inventarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inventarios` (
  `inv_clave` int(11) NOT NULL AUTO_INCREMENT,
  `inv_fechaInicio` datetime DEFAULT NULL,
  `inv_folio` int(11) DEFAULT NULL,
  `inv_empInicio` int(11) DEFAULT NULL,
  `inv_totalEntrada` decimal(11,3) DEFAULT NULL,
  `inv_totoalSalidas` decimal(11,3) DEFAULT NULL,
  `inv_fechaCierre` datetime DEFAULT NULL,
  `inv_emplCerro` int(11) DEFAULT NULL,
  `inv_productos` int(11) DEFAULT NULL,
  `inv_totalCompra` decimal(11,3) DEFAULT NULL,
  `inv_status` int(11) DEFAULT NULL,
  PRIMARY KEY (`inv_clave`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inventarios`
--

LOCK TABLES `inventarios` WRITE;
/*!40000 ALTER TABLE `inventarios` DISABLE KEYS */;
/*!40000 ALTER TABLE `inventarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `movcaja`
--

DROP TABLE IF EXISTS `movcaja`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `movcaja` (
  `mc_clave` int(11) NOT NULL AUTO_INCREMENT,
  `mc_fecha` datetime DEFAULT NULL,
  `mc_ccj_clave` int(11) DEFAULT NULL,
  `mc_clasificacion` int(11) DEFAULT NULL,
  `mc_tipo` int(11) DEFAULT NULL,
  `mc_fp_clave` int(11) DEFAULT NULL,
  `mc_monto` decimal(11,3) DEFAULT NULL,
  `mc_iva` float DEFAULT NULL,
  `mc_retiva` decimal(11,3) DEFAULT NULL,
  `mc_ven_clave` int(11) DEFAULT NULL,
  `mc_cob_clave` int(11) DEFAULT NULL,
  `mc_observacion` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `mc_status` int(11) DEFAULT NULL,
  PRIMARY KEY (`mc_clave`),
  KEY `fk_movCaja_formasCobroPago1_idx` (`mc_fp_clave`),
  KEY `fk_movCaja_ventas1_idx` (`mc_ven_clave`),
  KEY `fk_movCaja_cobros1_idx` (`mc_cob_clave`),
  KEY `fk_movcaja_cortecaja1_idx` (`mc_ccj_clave`),
  CONSTRAINT `fk_movCaja_cobros1` FOREIGN KEY (`mc_cob_clave`) REFERENCES `cobros` (`cob_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_movCaja_formasCobroPago1` FOREIGN KEY (`mc_fp_clave`) REFERENCES `formascobropago` (`fp_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_movCaja_ventas1` FOREIGN KEY (`mc_ven_clave`) REFERENCES `ventas` (`ven_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_movcaja_cortecaja1` FOREIGN KEY (`mc_ccj_clave`) REFERENCES `cortecaja` (`ccj_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movcaja`
--

LOCK TABLES `movcaja` WRITE;
/*!40000 ALTER TABLE `movcaja` DISABLE KEYS */;
INSERT INTO `movcaja` VALUES (1,'2021-09-13 16:21:50',1,1,1,1,200.000,NULL,NULL,NULL,NULL,'fondo de caja',1),(18,'2021-09-27 04:01:58',1,1,2,1,1000.000,0.16,160.000,1,1,'Cobro de 0001',1);
/*!40000 ALTER TABLE `movcaja` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `presentaciones`
--

DROP TABLE IF EXISTS `presentaciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `presentaciones` (
  `pre_clave` int(11) NOT NULL AUTO_INCREMENT,
  `pre_nombre` varchar(255) DEFAULT NULL,
  `pre_status` int(11) DEFAULT NULL,
  PRIMARY KEY (`pre_clave`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `presentaciones`
--

LOCK TABLES `presentaciones` WRITE;
/*!40000 ALTER TABLE `presentaciones` DISABLE KEYS */;
INSERT INTO `presentaciones` VALUES (1,'Bolsa',1),(2,'Botella',1),(3,'Caja',1),(4,'Frasco',1),(5,'Sobre',1),(6,'Pieza',1),(7,'Paquete',1),(8,'Rollo',1);
/*!40000 ALTER TABLE `presentaciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `productos`
--

DROP TABLE IF EXISTS `productos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productos` (
  `pro_clave` int(11) NOT NULL AUTO_INCREMENT,
  `pro_nombre` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `pro_descripcion` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `pro_inventariable` int(11) DEFAULT NULL,
  `pro_presentacion` int(11) DEFAULT NULL,
  `pro_unidad` int(11) DEFAULT NULL,
  `pro_medida` float DEFAULT NULL,
  `pro_cantidadMinima` int(11) DEFAULT NULL,
  `pro_precioMayoreo` decimal(11,3) DEFAULT NULL,
  `pro_precio` decimal(11,3) DEFAULT NULL,
  `pro_status` int(11) DEFAULT NULL,
  `pro_tienecodigos` int(11) DEFAULT NULL,
  PRIMARY KEY (`pro_clave`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `productos`
--

LOCK TABLES `productos` WRITE;
/*!40000 ALTER TABLE `productos` DISABLE KEYS */;
INSERT INTO `productos` VALUES (1,'Treda 129mg/280mg/30mg 20 Tabletas',NULL,1,3,8,1,NULL,NULL,117.500,1,1),(4,'Treda Suspensión 75 ml',NULL,1,2,8,1,NULL,NULL,122.510,1,1),(5,'Collarín Le Roy Cervical Grande',NULL,1,6,8,1,NULL,NULL,165.420,1,1),(6,'Termómetro Citizen Digital CTA 303',NULL,1,6,8,1,NULL,NULL,87.900,1,1),(7,'Termómetro Digital de Frente Omron MC-720',NULL,1,6,8,1,NULL,NULL,810.760,1,NULL),(8,'Termómetro Digital Sunshine con Punta Flexible',NULL,1,6,8,1,NULL,NULL,49.000,1,NULL),(9,'Oxímetro Digital de Pulso LMT-01',NULL,1,6,8,1,NULL,NULL,699.900,1,NULL),(10,'Termómetro Digital de Pluma',NULL,1,6,8,1,NULL,NULL,77.400,1,NULL),(11,'Termómetro Infrarrojo Hi8us HG01 de Frente',NULL,1,6,8,1,NULL,NULL,812.000,1,NULL),(12,'Oxímetro Digital de Pulso Xignal para Adulto',NULL,1,6,8,1,NULL,NULL,437.490,1,NULL),(13,'Oxímetro de Pulso Levy and Lulu Digital',NULL,1,6,8,1,NULL,NULL,438.890,1,NULL),(14,'Monitor Omron Presión Arterial Automático Brazo',NULL,1,6,8,1,NULL,NULL,965.860,1,NULL),(15,'Nebulizador de Compresor Omron Ne-C801LA',NULL,1,6,8,1,NULL,NULL,1420.970,1,NULL),(16,'Nebulizador Omron de Compresor Mod NE-C101',NULL,1,6,8,1,NULL,NULL,1217.030,1,NULL),(17,'Baumanómetro Citizen Ch-617',NULL,1,6,8,1,NULL,NULL,539.180,1,NULL),(18,'Nebulizador Nebucor P-103',NULL,1,6,8,1,NULL,NULL,733.450,1,NULL),(19,'Baumanómetro Citizen Ch-452 Brazo',NULL,1,6,8,1,NULL,NULL,665.140,1,NULL),(20,'Nebulizador Nebucor De Compresor P102',NULL,1,6,8,1,NULL,NULL,762.550,1,NULL),(21,'Termómetro Infrarrojo Levy and Lulu a Distancia',NULL,1,6,8,1,NULL,NULL,760.700,1,NULL),(22,'Kit Para Nebulizador Sun Shine 6pzas',NULL,1,1,8,1,NULL,NULL,59.800,1,NULL),(23,'Monitor Neutek de Presión Sanguínea Brazo Digital',NULL,1,6,8,1,NULL,NULL,512.920,1,NULL),(24,'Termómetro Omron Digital MC-246',NULL,1,6,8,1,NULL,NULL,101.020,1,NULL),(25,'Monitor Omron de Presión Arterial Automático Muñeca con 30 Memorias',NULL,1,6,8,1,NULL,NULL,908.740,1,NULL),(26,'Termómetro Digital Citizen de Pluma',NULL,1,6,8,1,NULL,NULL,80.600,1,NULL),(27,'Termómetro Digital Contra el Agua MT 201C',NULL,1,6,8,1,NULL,NULL,55.100,1,NULL),(28,'Baumanómetro de Muñeca Nebucor HL168F',NULL,1,6,8,1,NULL,NULL,489.510,1,NULL),(29,'Férula Flents Insty Splint Para Dedos 2 Piezas Mediano/Grande',NULL,1,7,8,1,NULL,NULL,109.050,1,NULL),(30,'Monitor Neutek De Presión Sanguínea Para Muñeca Automático',NULL,1,6,8,1,NULL,NULL,424.150,1,NULL),(31,'Monitor Omron de Presión Arterial Automático Muñeca',NULL,1,6,8,1,NULL,NULL,796.470,1,NULL),(32,'Baumanómetro Nebucor HL888JA Brazo',NULL,1,6,8,1,NULL,NULL,613.510,1,NULL),(33,'Termómetro Infrarrojo HG01',NULL,1,6,8,1,NULL,NULL,661.300,1,NULL),(34,'Termómetro Omron Digital Flex MC 343F',NULL,1,6,8,1,NULL,NULL,126.290,1,NULL),(35,'Nebulizador Sun Shine Flujo Continuo',NULL,1,6,8,1,NULL,NULL,703.700,1,NULL),(36,'Nebulizador Neutek Hi-Neb de Pistón',NULL,1,6,8,1,NULL,NULL,665.140,1,NULL),(37,'Monitor Omron de Presión Arterial de Muñeca Automático Mod HEM-6230',NULL,1,6,8,1,NULL,NULL,1265.710,1,NULL),(38,'Muñequera Para Pulgar Barrere Mediano',NULL,1,6,8,1,NULL,NULL,84.600,1,NULL),(39,'Nebulizador Sun Shine con Pistón',NULL,1,6,8,1,NULL,NULL,802.800,1,NULL),(40,'Rodillera Elástica Rekord Le Roy Mediana',NULL,1,6,8,1,NULL,NULL,64.930,1,NULL),(41,'Tobillera Elástica Rekord Le Roy Mediana',NULL,1,6,8,1,NULL,NULL,69.810,1,NULL),(42,'Rodillera Elástica Rekord Le Roy Grande',NULL,1,6,8,1,NULL,NULL,64.930,1,NULL),(43,'Collarín Barrere Blando Beige Grande',NULL,1,6,8,1,NULL,NULL,102.490,1,NULL);
/*!40000 ALTER TABLE `productos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `productos_categorias`
--

DROP TABLE IF EXISTS `productos_categorias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productos_categorias` (
  `pc_clave` int(11) NOT NULL AUTO_INCREMENT,
  `pc_pro_clave` int(11) DEFAULT NULL,
  `pc_cat_clave` int(11) DEFAULT NULL,
  `pc_status` int(11) DEFAULT NULL,
  PRIMARY KEY (`pc_clave`),
  KEY `fk_productos_has_categorias_categorias1_idx` (`pc_cat_clave`),
  KEY `fk_productos_has_categorias_productos1_idx` (`pc_pro_clave`),
  CONSTRAINT `fk_productos_has_categorias_categorias1` FOREIGN KEY (`pc_cat_clave`) REFERENCES `categorias` (`cat_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_productos_has_categorias_productos1` FOREIGN KEY (`pc_pro_clave`) REFERENCES `productos` (`pro_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `productos_categorias`
--

LOCK TABLES `productos_categorias` WRITE;
/*!40000 ALTER TABLE `productos_categorias` DISABLE KEYS */;
INSERT INTO `productos_categorias` VALUES (1,1,1,1),(2,4,1,1),(3,5,2,1),(4,6,2,1),(5,7,2,1),(6,8,2,1),(7,9,2,1),(8,10,2,1),(9,11,2,1),(10,12,2,1),(11,13,2,1),(12,14,2,1),(13,15,2,1),(14,16,2,1),(15,17,2,1),(16,18,2,1),(17,19,2,1),(18,20,2,1),(19,21,2,1),(20,22,2,1),(21,23,2,1),(22,24,2,1),(23,25,2,1),(24,26,2,1),(25,27,2,1),(26,28,2,1),(27,29,2,1),(28,30,2,1),(29,31,2,1),(30,32,2,1),(31,33,2,1),(32,34,2,1),(33,35,2,1),(34,36,2,1),(35,37,2,1),(36,38,2,1),(37,39,2,1),(38,40,2,1),(39,41,2,1),(40,42,2,1),(41,43,2,1);
/*!40000 ALTER TABLE `productos_categorias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proveedores`
--

DROP TABLE IF EXISTS `proveedores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proveedores` (
  `prov_clave` int(11) NOT NULL AUTO_INCREMENT,
  `prov_nombre` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `prov_appaterno` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `prov_apmaterno` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `prov_nombrecompleto` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `prov_correo` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `prov_telefono` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `prov_status` int(11) DEFAULT NULL,
  PRIMARY KEY (`prov_clave`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proveedores`
--

LOCK TABLES `proveedores` WRITE;
/*!40000 ALTER TABLE `proveedores` DISABLE KEYS */;
INSERT INTO `proveedores` VALUES (1,'Ismael','Carranza','Tronco','Ismael Carranza Tronco','null','null',1),(2,'Pruebas','Pruebas','Pruebas','Pruebas Pruebas Pruebas','','',0),(3,'Daniel','Nsiau','Jdbcibna','Daniel Nsiau Jdbcibna','','',1),(4,'A','K','Lkzdfkmval','A K Lkzdfkmval','null@gmail.com','null',1);
/*!40000 ALTER TABLE `proveedores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proveedores_productos`
--

DROP TABLE IF EXISTS `proveedores_productos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proveedores_productos` (
  `pp_clave` int(11) NOT NULL AUTO_INCREMENT,
  `pp_prov_clave` int(11) DEFAULT NULL,
  `pp_pro_clave` int(11) DEFAULT NULL,
  `pp_status` int(11) DEFAULT NULL,
  PRIMARY KEY (`pp_clave`),
  KEY `fk_proveedores_has_productos_productos1_idx` (`pp_pro_clave`),
  KEY `fk_proveedores_has_productos_proveedores1_idx` (`pp_prov_clave`),
  CONSTRAINT `fk_proveedores_has_productos_productos1` FOREIGN KEY (`pp_pro_clave`) REFERENCES `productos` (`pro_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_proveedores_has_productos_proveedores1` FOREIGN KEY (`pp_prov_clave`) REFERENCES `proveedores` (`prov_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proveedores_productos`
--

LOCK TABLES `proveedores_productos` WRITE;
/*!40000 ALTER TABLE `proveedores_productos` DISABLE KEYS */;
/*!40000 ALTER TABLE `proveedores_productos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `puestos`
--

DROP TABLE IF EXISTS `puestos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `puestos` (
  `pue_clave` int(11) NOT NULL AUTO_INCREMENT,
  `pue_nombre` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `pue_status` int(11) DEFAULT NULL,
  PRIMARY KEY (`pue_clave`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `puestos`
--

LOCK TABLES `puestos` WRITE;
/*!40000 ALTER TABLE `puestos` DISABLE KEYS */;
INSERT INTO `puestos` VALUES (1,'Cajero',1),(2,'Vendedor',1),(3,'Almacenista',1),(4,'Repartidor',1),(5,'Gerente',1),(6,'Vigilante',1);
/*!40000 ALTER TABLE `puestos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sistemaunidades`
--

DROP TABLE IF EXISTS `sistemaunidades`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sistemaunidades` (
  `su_clave` int(11) NOT NULL AUTO_INCREMENT,
  `su_nombre` varchar(255) DEFAULT NULL,
  `su_status` int(11) DEFAULT NULL,
  PRIMARY KEY (`su_clave`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sistemaunidades`
--

LOCK TABLES `sistemaunidades` WRITE;
/*!40000 ALTER TABLE `sistemaunidades` DISABLE KEYS */;
INSERT INTO `sistemaunidades` VALUES (1,'sin unidad',1),(2,'kg',1),(3,'g',1),(4,'l',1),(5,'ml',1),(6,'m',1),(7,'m2',1),(8,'pza',1);
/*!40000 ALTER TABLE `sistemaunidades` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stocks`
--

DROP TABLE IF EXISTS `stocks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stocks` (
  `sto_clave` int(11) NOT NULL AUTO_INCREMENT,
  `sto_cantidad` float DEFAULT NULL,
  `sto_pro_clave` int(11) NOT NULL,
  PRIMARY KEY (`sto_clave`),
  KEY `fk_stock_productos1_idx` (`sto_pro_clave`),
  CONSTRAINT `fk_stock_productos1` FOREIGN KEY (`sto_pro_clave`) REFERENCES `productos` (`pro_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stocks`
--

LOCK TABLES `stocks` WRITE;
/*!40000 ALTER TABLE `stocks` DISABLE KEYS */;
/*!40000 ALTER TABLE `stocks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sucursales`
--

DROP TABLE IF EXISTS `sucursales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sucursales` (
  `suc_clave` int(11) NOT NULL AUTO_INCREMENT,
  `suc_direccion` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `suc_status` int(11) DEFAULT NULL,
  `suc_empr_clave` int(11) DEFAULT NULL,
  PRIMARY KEY (`suc_clave`),
  KEY `fk_sucursales_empresas1_idx` (`suc_empr_clave`),
  CONSTRAINT `fk_sucursales_empresas1` FOREIGN KEY (`suc_empr_clave`) REFERENCES `empresas` (`empr_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sucursales`
--

LOCK TABLES `sucursales` WRITE;
/*!40000 ALTER TABLE `sucursales` DISABLE KEYS */;
/*!40000 ALTER TABLE `sucursales` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `usu_clave` int(11) NOT NULL AUTO_INCREMENT,
  `usu_nombre` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `usu_password` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `usu_tipo` int(11) DEFAULT NULL,
  `usu_nivel` int(11) DEFAULT NULL,
  `usu_status` int(11) DEFAULT NULL,
  `usu_emp_clave` int(11) NOT NULL,
  PRIMARY KEY (`usu_clave`),
  KEY `fk_usuarios_empleados1_idx` (`usu_emp_clave`),
  CONSTRAINT `fk_usuarios_empleados1` FOREIGN KEY (`usu_emp_clave`) REFERENCES `empleados` (`emp_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (1,'admin','$2y$05$6PCxmhYX3UagVZKfPLkFsuCPFbmu50XCVCD88iC3KTmC4WU.hCu8O',1,1,1,1);
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ventas`
--

DROP TABLE IF EXISTS `ventas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ventas` (
  `ven_clave` int(11) NOT NULL AUTO_INCREMENT,
  `ven_cot_clave` int(11) DEFAULT NULL,
  `ven_ccj_clave` int(11) NOT NULL,
  `ven_cli_clave` int(11) NOT NULL,
  `ven_emp_clave` int(11) NOT NULL,
  `ven_fecha` datetime DEFAULT NULL,
  `ven_tipo` int(11) DEFAULT NULL,
  `ven_folio` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `ven_subtotal` decimal(11,3) DEFAULT NULL,
  `ven_iva` decimal(11,3) DEFAULT NULL,
  `ven_total` decimal(11,3) DEFAULT NULL,
  `ven_cobrado` decimal(11,3) DEFAULT NULL,
  `ven_status` int(11) DEFAULT NULL,
  PRIMARY KEY (`ven_clave`),
  KEY `fk_ventas_corteCaja1_idx` (`ven_ccj_clave`),
  KEY `fk_ventas_clientes1_idx` (`ven_cli_clave`),
  KEY `fk_ventas_empleados1_idx` (`ven_emp_clave`),
  CONSTRAINT `fk_ventas_clientes1` FOREIGN KEY (`ven_cli_clave`) REFERENCES `clientes` (`cli_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ventas_corteCaja1` FOREIGN KEY (`ven_ccj_clave`) REFERENCES `cortecaja` (`ccj_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ventas_empleados1` FOREIGN KEY (`ven_emp_clave`) REFERENCES `empleados` (`emp_clave`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ventas`
--

LOCK TABLES `ventas` WRITE;
/*!40000 ALTER TABLE `ventas` DISABLE KEYS */;
INSERT INTO `ventas` VALUES (1,1,1,1,1,'2021-09-26 02:38:33',2,'0001',1059.300,169.487,1228.787,1000.000,1);
/*!40000 ALTER TABLE `ventas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'puntoventa'
--

--
-- Dumping routines for database 'puntoventa'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-09-28 16:33:52

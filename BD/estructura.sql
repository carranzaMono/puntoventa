-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema puntoventa
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema puntoventa
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `puntoventa` DEFAULT CHARACTER SET utf8 ;
USE `puntoventa` ;

-- -----------------------------------------------------
-- Table `puntoventa`.`bitacora`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `puntoventa`.`bitacora` (
  `bit_clave` INT(11) NOT NULL AUTO_INCREMENT,
  `bit_modulo` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NULL DEFAULT NULL,
  `bit_accion` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NULL DEFAULT NULL,
  `bit_tipo` INT(11) NULL DEFAULT NULL,
  `bit_emp_clave` INT(11) NULL DEFAULT NULL,
  `bit_descripcion` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NULL DEFAULT NULL,
  `bit_claves` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`bit_clave`))
ENGINE = InnoDB
AUTO_INCREMENT = 283
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish_ci;


-- -----------------------------------------------------
-- Table `puntoventa`.`categorias`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `puntoventa`.`categorias` (
  `cat_clave` INT(11) NOT NULL AUTO_INCREMENT,
  `cat_nombre` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NULL DEFAULT NULL,
  `cat_status` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`cat_clave`))
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish_ci;


-- -----------------------------------------------------
-- Table `puntoventa`.`clientes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `puntoventa`.`clientes` (
  `cli_clave` INT(11) NOT NULL AUTO_INCREMENT,
  `cli_nombre` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NULL DEFAULT NULL,
  `cli_telefono` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NULL DEFAULT NULL,
  `cli_correo` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NULL DEFAULT NULL,
  `cli_direccion` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NULL DEFAULT NULL,
  `cli_status` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`cli_clave`))
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish_ci;


-- -----------------------------------------------------
-- Table `puntoventa`.`puestos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `puntoventa`.`puestos` (
  `pue_clave` INT(11) NOT NULL AUTO_INCREMENT,
  `pue_nombre` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NULL DEFAULT NULL,
  `pue_status` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`pue_clave`))
ENGINE = InnoDB
AUTO_INCREMENT = 7
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish_ci;


-- -----------------------------------------------------
-- Table `puntoventa`.`empleados`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `puntoventa`.`empleados` (
  `emp_clave` INT(11) NOT NULL AUTO_INCREMENT,
  `emp_nombre` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NULL DEFAULT NULL,
  `emp_apPaterno` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NULL DEFAULT NULL,
  `emp_apMaterno` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NULL DEFAULT NULL,
  `emp_direccion` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NULL DEFAULT NULL,
  `emp_telefono` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NULL DEFAULT NULL,
  `emp_curp` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NULL DEFAULT NULL,
  `emp_rfc` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NULL DEFAULT NULL,
  `emp_correo` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NULL DEFAULT NULL,
  `emp_status` INT(11) NULL DEFAULT NULL,
  `emp_pue_clave` INT(11) NOT NULL,
  `emp_nombreCompleto` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NULL DEFAULT NULL,
  PRIMARY KEY (`emp_clave`),
  INDEX `fk_empleados_puestos_idx` (`emp_pue_clave` ASC),
  CONSTRAINT `fk_empleados_puestos`
    FOREIGN KEY (`emp_pue_clave`)
    REFERENCES `puntoventa`.`puestos` (`pue_clave`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish_ci;


-- -----------------------------------------------------
-- Table `puntoventa`.`cortecaja`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `puntoventa`.`cortecaja` (
  `ccj_clave` INT(11) NOT NULL AUTO_INCREMENT,
  `ccj_folio` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NULL DEFAULT NULL,
  `ccj_inicio` DATETIME NULL DEFAULT NULL,
  `ccj_emp_abrio` INT(11) NULL DEFAULT NULL,
  `ccj_fondo` DECIMAL(11,3) NULL,
  `ccj_fin` DATETIME NULL DEFAULT NULL,
  `ccj_emp_cerro` INT(11) NULL DEFAULT NULL,
  `ccj_ventatotal` DECIMAL(11,3) NULL,
  `ccj_entregaron` DECIMAL(11,3) NULL DEFAULT NULL,
  `ccj_entradas` DECIMAL(11,3) NULL,
  `ccj_salidas` DECIMAL(11,3) NULL,
  `ccj_diferiencia` DECIMAL(11,3) NULL DEFAULT NULL,
  `ccj_status` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`ccj_clave`),
  INDEX `fk_corteCaja_empleados1_idx` (`ccj_emp_abrio` ASC),
  CONSTRAINT `fk_corteCaja_empleados1`
    FOREIGN KEY (`ccj_emp_abrio`)
    REFERENCES `puntoventa`.`empleados` (`emp_clave`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish_ci;


-- -----------------------------------------------------
-- Table `puntoventa`.`formascobropago`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `puntoventa`.`formascobropago` (
  `fp_clave` INT(11) NOT NULL AUTO_INCREMENT,
  `fp_nombre` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NULL DEFAULT NULL,
  `fp_tipo` INT(11) NULL DEFAULT NULL,
  `fp_status` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`fp_clave`))
ENGINE = InnoDB
AUTO_INCREMENT = 6
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish_ci;


-- -----------------------------------------------------
-- Table `puntoventa`.`ventas`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `puntoventa`.`ventas` (
  `ven_clave` INT(11) NOT NULL AUTO_INCREMENT,
  `ven_cot_clave` INT(11) NULL DEFAULT NULL,
  `ven_ccj_clave` INT(11) NOT NULL,
  `ven_cli_clave` INT(11) NOT NULL,
  `ven_emp_clave` INT(11) NOT NULL,
  `ven_fecha` DATETIME NULL DEFAULT NULL,
  `ven_tipo` INT(11) NULL DEFAULT NULL,
  `ven_folio` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NULL DEFAULT NULL,
  `ven_subtotal` DECIMAL(11,3) NULL DEFAULT NULL,
  `ven_iva` DECIMAL(11,3) NULL DEFAULT NULL,
  `ven_total` DECIMAL(11,3) NULL DEFAULT NULL,
  `ven_cobrado` DECIMAL(11,3) NULL DEFAULT NULL,
  `ven_status` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`ven_clave`),
  INDEX `fk_ventas_corteCaja1_idx` (`ven_ccj_clave` ASC),
  INDEX `fk_ventas_clientes1_idx` (`ven_cli_clave` ASC),
  INDEX `fk_ventas_empleados1_idx` (`ven_emp_clave` ASC),
  CONSTRAINT `fk_ventas_clientes1`
    FOREIGN KEY (`ven_cli_clave`)
    REFERENCES `puntoventa`.`clientes` (`cli_clave`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ventas_corteCaja1`
    FOREIGN KEY (`ven_ccj_clave`)
    REFERENCES `puntoventa`.`cortecaja` (`ccj_clave`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ventas_empleados1`
    FOREIGN KEY (`ven_emp_clave`)
    REFERENCES `puntoventa`.`empleados` (`emp_clave`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish_ci;


-- -----------------------------------------------------
-- Table `puntoventa`.`cobros`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `puntoventa`.`cobros` (
  `cob_clave` INT(11) NOT NULL AUTO_INCREMENT,
  `cob_fecha` DATETIME NULL DEFAULT NULL,
  `cob_fcp_clave` INT(11) NOT NULL,
  `cob_ven_clave` INT(11) NOT NULL,
  `cob_ccj_clave` INT(11) NOT NULL,
  `cob_pagaroncon` DECIMAL(11,3) NULL DEFAULT NULL,
  `cob_cambio` DECIMAL(11,3) NULL DEFAULT NULL,
  `cob_monto` DECIMAL(11,3) NULL DEFAULT NULL,
  `cob_descuento` DECIMAL(11,3) NULL DEFAULT NULL,
  `cob_emp_clave` INT(11) NOT NULL,
  `cob_status` INT(11) NULL DEFAULT NULL,
  `cob_cli_clave` INT(11) NOT NULL,
  PRIMARY KEY (`cob_clave`),
  INDEX `fk_cobros_formasCobroPago1_idx` (`cob_fcp_clave` ASC),
  INDEX `fk_cobros_ventas1_idx` (`cob_ven_clave` ASC),
  INDEX `fk_cobros_corteCaja1_idx` (`cob_ccj_clave` ASC),
  INDEX `fk_cobros_empleados1_idx` (`cob_emp_clave` ASC),
  INDEX `fk_cobros_clientes1_idx` (`cob_cli_clave` ASC),
  CONSTRAINT `fk_cobros_clientes1`
    FOREIGN KEY (`cob_cli_clave`)
    REFERENCES `puntoventa`.`clientes` (`cli_clave`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cobros_corteCaja1`
    FOREIGN KEY (`cob_ccj_clave`)
    REFERENCES `puntoventa`.`cortecaja` (`ccj_clave`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cobros_empleados1`
    FOREIGN KEY (`cob_emp_clave`)
    REFERENCES `puntoventa`.`empleados` (`emp_clave`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cobros_formasCobroPago1`
    FOREIGN KEY (`cob_fcp_clave`)
    REFERENCES `puntoventa`.`formascobropago` (`fp_clave`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cobros_ventas1`
    FOREIGN KEY (`cob_ven_clave`)
    REFERENCES `puntoventa`.`ventas` (`ven_clave`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish_ci;


-- -----------------------------------------------------
-- Table `puntoventa`.`cotizaciones`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `puntoventa`.`cotizaciones` (
  `cot_clave` INT(11) NOT NULL AUTO_INCREMENT,
  `cot_tipo` INT(11) NULL DEFAULT NULL,
  `cot_fecha` DATETIME NULL DEFAULT NULL,
  `cot_folio` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NULL DEFAULT NULL,
  `cot_subtotal` DECIMAL(11,3) NULL DEFAULT NULL,
  `cot_iva` DECIMAL(11,3) NULL DEFAULT NULL,
  `cot_total` DECIMAL(11,3) NULL DEFAULT NULL,
  `cot_ccj_clave` INT(11) NULL DEFAULT NULL,
  `cot_status` INT(11) NULL DEFAULT NULL,
  `cot_cli_clave` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`cot_clave`),
  INDEX `fk_cotizaciones_cortecaja1_idx` (`cot_ccj_clave` ASC),
  CONSTRAINT `fk_cotizaciones_cortecaja1`
    FOREIGN KEY (`cot_ccj_clave`)
    REFERENCES `puntoventa`.`cortecaja` (`ccj_clave`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish_ci;


-- -----------------------------------------------------
-- Table `puntoventa`.`detallecotizacion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `puntoventa`.`detallecotizacion` (
  `dc_clave` INT(11) NOT NULL AUTO_INCREMENT,
  `dc_cot_clave` INT(11) NOT NULL,
  `dc_precioUnitario` DECIMAL(11,3) NULL DEFAULT NULL,
  `dc_cantidad` FLOAT NULL DEFAULT NULL,
  `dc_subtotal` DECIMAL(11,2) NULL DEFAULT NULL,
  `dc_iva` DECIMAL(11,3) NULL DEFAULT NULL,
  `dc_total` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NULL DEFAULT NULL,
  `dc_stauts` INT(11) NULL DEFAULT NULL,
  `dc_pro_clave` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`dc_clave`),
  INDEX `fk_detalleCotizacion_cotizaciones1_idx` (`dc_cot_clave` ASC),
  CONSTRAINT `fk_detalleCotizacion_cotizaciones1`
    FOREIGN KEY (`dc_cot_clave`)
    REFERENCES `puntoventa`.`cotizaciones` (`cot_clave`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish_ci;


-- -----------------------------------------------------
-- Table `puntoventa`.`inventarios`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `puntoventa`.`inventarios` (
  `inv_clave` INT(11) NOT NULL AUTO_INCREMENT,
  `inv_fechaInicio` DATETIME NULL DEFAULT NULL,
  `inv_folio` INT(11) NULL DEFAULT NULL,
  `inv_empInicio` INT(11) NULL DEFAULT NULL,
  `inv_totalEntrada` DECIMAL(11,3) NULL DEFAULT NULL,
  `inv_totoalSalidas` DECIMAL(11,3) NULL DEFAULT NULL,
  `inv_fechaCierre` DATETIME NULL DEFAULT NULL,
  `inv_emplCerro` INT(11) NULL DEFAULT NULL,
  `inv_productos` INT(11) NULL DEFAULT NULL,
  `inv_totalCompra` DECIMAL(11,3) NULL DEFAULT NULL,
  `inv_status` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`inv_clave`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `puntoventa`.`productos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `puntoventa`.`productos` (
  `pro_clave` INT(11) NOT NULL AUTO_INCREMENT,
  `pro_nombre` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NULL DEFAULT NULL,
  `pro_descripcion` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NULL DEFAULT NULL,
  `pro_inventariable` INT(11) NULL DEFAULT NULL,
  `pro_presentacion` INT(11) NULL DEFAULT NULL,
  `pro_unidad` INT(11) NULL DEFAULT NULL,
  `pro_medida` FLOAT NULL DEFAULT NULL,
  `pro_cantidadMinima` INT(11) NULL DEFAULT NULL,
  `pro_precioMayoreo` DECIMAL(11,3) NULL DEFAULT NULL,
  `pro_precio` DECIMAL(11,3) NULL DEFAULT NULL,
  `pro_status` INT(11) NULL DEFAULT NULL,
  `pro_tienecodigos` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`pro_clave`))
ENGINE = InnoDB
AUTO_INCREMENT = 44
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish_ci;


-- -----------------------------------------------------
-- Table `puntoventa`.`detalleinventarios`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `puntoventa`.`detalleinventarios` (
  `di_clave` INT(11) NOT NULL AUTO_INCREMENT,
  `di_inv_clave` INT(11) NULL DEFAULT NULL,
  `di_pro_clave` INT(11) NULL DEFAULT NULL,
  `di_cantidad` FLOAT NULL DEFAULT NULL,
  `di_tipomov` INT(11) NULL DEFAULT NULL,
  `di_totalCompra` DECIMAL(11,3) NULL DEFAULT NULL,
  `di_totalSalida` DECIMAL(11,3) NULL DEFAULT NULL,
  `di_status` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`di_clave`),
  INDEX `fk_detalleinventarios_inventarios1_idx` (`di_inv_clave` ASC),
  INDEX `fk_detalleinventarios_productos1_idx` (`di_pro_clave` ASC),
  CONSTRAINT `fk_detalleinventarios_inventarios1`
    FOREIGN KEY (`di_inv_clave`)
    REFERENCES `puntoventa`.`inventarios` (`inv_clave`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_detalleinventarios_productos1`
    FOREIGN KEY (`di_pro_clave`)
    REFERENCES `puntoventa`.`productos` (`pro_clave`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `puntoventa`.`detalleventas`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `puntoventa`.`detalleventas` (
  `dv_clave` INT(11) NOT NULL AUTO_INCREMENT,
  `dv_fecha` DATETIME NULL DEFAULT NULL,
  `dv_ccj_clave` INT(11) NULL DEFAULT NULL,
  `dv_ven_clave` INT(11) NOT NULL,
  `dv_pro_clave` INT(11) NULL DEFAULT NULL,
  `dv_precioUnitario` DECIMAL(11,3) NULL DEFAULT NULL,
  `dv_cantidad` FLOAT NULL DEFAULT NULL,
  `dv_subtotal` DECIMAL(11,3) NULL DEFAULT NULL,
  `dv_iva` DECIMAL(11,3) NULL DEFAULT NULL,
  `dv_total` DECIMAL(11,3) NULL DEFAULT NULL,
  `dv_status` INT(11) NULL DEFAULT NULL,
  `dv_tipo` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`dv_clave`),
  INDEX `fk_detalleVentas_ventas1_idx` (`dv_ven_clave` ASC),
  INDEX `fk_detalleVentas_productos1_idx` (`dv_pro_clave` ASC),
  INDEX `fk_detalleventas_cortecaja1_idx` (`dv_ccj_clave` ASC),
  CONSTRAINT `fk_detalleVentas_productos1`
    FOREIGN KEY (`dv_pro_clave`)
    REFERENCES `puntoventa`.`productos` (`pro_clave`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_detalleVentas_ventas1`
    FOREIGN KEY (`dv_ven_clave`)
    REFERENCES `puntoventa`.`ventas` (`ven_clave`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_detalleventas_cortecaja1`
    FOREIGN KEY (`dv_ccj_clave`)
    REFERENCES `puntoventa`.`cortecaja` (`ccj_clave`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish_ci;


-- -----------------------------------------------------
-- Table `puntoventa`.`empresas`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `puntoventa`.`empresas` (
  `empr_clave` INT(11) NOT NULL AUTO_INCREMENT,
  `empr_nombre` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NULL DEFAULT NULL,
  `empr_direccion` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NULL DEFAULT NULL,
  `empr_logo` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NULL DEFAULT NULL,
  `empr_status` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`empr_clave`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish_ci;


-- -----------------------------------------------------
-- Table `puntoventa`.`movcaja`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `puntoventa`.`movcaja` (
  `mc_clave` INT(11) NOT NULL AUTO_INCREMENT,
  `mc_fecha` DATETIME NULL DEFAULT NULL,
  `mc_ccj_clave` INT(11) NULL DEFAULT NULL,
  `mc_clasificacion` INT(11) NULL DEFAULT NULL,
  `mc_tipo` INT(11) NULL DEFAULT NULL,
  `mc_fp_clave` INT(11) NULL DEFAULT NULL,
  `mc_monto` DECIMAL(11,3) NULL DEFAULT NULL,
  `mc_iva` FLOAT NULL DEFAULT NULL,
  `mc_retiva` DECIMAL(11,3) NULL DEFAULT NULL,
  `mc_ven_clave` INT(11) NULL DEFAULT NULL,
  `mc_cob_clave` INT(11) NULL DEFAULT NULL,
  `mc_observacion` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NULL DEFAULT NULL,
  `mc_status` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`mc_clave`),
  INDEX `fk_movCaja_formasCobroPago1_idx` (`mc_fp_clave` ASC),
  INDEX `fk_movCaja_ventas1_idx` (`mc_ven_clave` ASC),
  INDEX `fk_movCaja_cobros1_idx` (`mc_cob_clave` ASC),
  INDEX `fk_movcaja_cortecaja1_idx` (`mc_ccj_clave` ASC),
  CONSTRAINT `fk_movCaja_cobros1`
    FOREIGN KEY (`mc_cob_clave`)
    REFERENCES `puntoventa`.`cobros` (`cob_clave`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_movCaja_formasCobroPago1`
    FOREIGN KEY (`mc_fp_clave`)
    REFERENCES `puntoventa`.`formascobropago` (`fp_clave`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_movCaja_ventas1`
    FOREIGN KEY (`mc_ven_clave`)
    REFERENCES `puntoventa`.`ventas` (`ven_clave`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_movcaja_cortecaja1`
    FOREIGN KEY (`mc_ccj_clave`)
    REFERENCES `puntoventa`.`cortecaja` (`ccj_clave`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish_ci;


-- -----------------------------------------------------
-- Table `puntoventa`.`presentaciones`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `puntoventa`.`presentaciones` (
  `pre_clave` INT(11) NOT NULL AUTO_INCREMENT,
  `pre_nombre` VARCHAR(255) NULL DEFAULT NULL,
  `pre_status` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`pre_clave`))
ENGINE = InnoDB
AUTO_INCREMENT = 9
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `puntoventa`.`productos_categorias`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `puntoventa`.`productos_categorias` (
  `pc_clave` INT(11) NOT NULL AUTO_INCREMENT,
  `pc_pro_clave` INT(11) NULL DEFAULT NULL,
  `pc_cat_clave` INT(11) NULL DEFAULT NULL,
  `pc_status` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`pc_clave`),
  INDEX `fk_productos_has_categorias_categorias1_idx` (`pc_cat_clave` ASC),
  INDEX `fk_productos_has_categorias_productos1_idx` (`pc_pro_clave` ASC),
  CONSTRAINT `fk_productos_has_categorias_categorias1`
    FOREIGN KEY (`pc_cat_clave`)
    REFERENCES `puntoventa`.`categorias` (`cat_clave`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_productos_has_categorias_productos1`
    FOREIGN KEY (`pc_pro_clave`)
    REFERENCES `puntoventa`.`productos` (`pro_clave`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 42
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish_ci;


-- -----------------------------------------------------
-- Table `puntoventa`.`proveedores`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `puntoventa`.`proveedores` (
  `prov_clave` INT(11) NOT NULL AUTO_INCREMENT,
  `prov_nombre` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NULL DEFAULT NULL,
  `prov_appaterno` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NULL DEFAULT NULL,
  `prov_apmaterno` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NULL DEFAULT NULL,
  `prov_nombrecompleto` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NULL DEFAULT NULL,
  `prov_correo` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NULL DEFAULT NULL,
  `prov_telefono` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NULL DEFAULT NULL,
  `prov_status` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`prov_clave`))
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish_ci;


-- -----------------------------------------------------
-- Table `puntoventa`.`proveedores_productos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `puntoventa`.`proveedores_productos` (
  `pp_clave` INT(11) NOT NULL AUTO_INCREMENT,
  `pp_prov_clave` INT(11) NULL DEFAULT NULL,
  `pp_pro_clave` INT(11) NULL DEFAULT NULL,
  `pp_status` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`pp_clave`),
  INDEX `fk_proveedores_has_productos_productos1_idx` (`pp_pro_clave` ASC),
  INDEX `fk_proveedores_has_productos_proveedores1_idx` (`pp_prov_clave` ASC),
  CONSTRAINT `fk_proveedores_has_productos_productos1`
    FOREIGN KEY (`pp_pro_clave`)
    REFERENCES `puntoventa`.`productos` (`pro_clave`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_proveedores_has_productos_proveedores1`
    FOREIGN KEY (`pp_prov_clave`)
    REFERENCES `puntoventa`.`proveedores` (`prov_clave`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish_ci;


-- -----------------------------------------------------
-- Table `puntoventa`.`sistemaunidades`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `puntoventa`.`sistemaunidades` (
  `su_clave` INT(11) NOT NULL AUTO_INCREMENT,
  `su_nombre` VARCHAR(255) NULL DEFAULT NULL,
  `su_status` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`su_clave`))
ENGINE = InnoDB
AUTO_INCREMENT = 9
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `puntoventa`.`stocks`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `puntoventa`.`stocks` (
  `sto_clave` INT(11) NOT NULL AUTO_INCREMENT,
  `sto_cantidad` FLOAT NULL DEFAULT NULL,
  `sto_pro_clave` INT(11) NOT NULL,
  PRIMARY KEY (`sto_clave`),
  INDEX `fk_stock_productos1_idx` (`sto_pro_clave` ASC),
  CONSTRAINT `fk_stock_productos1`
    FOREIGN KEY (`sto_pro_clave`)
    REFERENCES `puntoventa`.`productos` (`pro_clave`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish_ci;


-- -----------------------------------------------------
-- Table `puntoventa`.`sucursales`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `puntoventa`.`sucursales` (
  `suc_clave` INT(11) NOT NULL AUTO_INCREMENT,
  `suc_direccion` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NULL DEFAULT NULL,
  `suc_status` INT(11) NULL DEFAULT NULL,
  `suc_empr_clave` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`suc_clave`),
  INDEX `fk_sucursales_empresas1_idx` (`suc_empr_clave` ASC),
  CONSTRAINT `fk_sucursales_empresas1`
    FOREIGN KEY (`suc_empr_clave`)
    REFERENCES `puntoventa`.`empresas` (`empr_clave`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish_ci;


-- -----------------------------------------------------
-- Table `puntoventa`.`usuarios`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `puntoventa`.`usuarios` (
  `usu_clave` INT(11) NOT NULL AUTO_INCREMENT,
  `usu_nombre` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NULL DEFAULT NULL,
  `usu_password` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_spanish_ci' NULL DEFAULT NULL,
  `usu_tipo` INT(11) NULL DEFAULT NULL,
  `usu_nivel` INT(11) NULL DEFAULT NULL,
  `usu_status` INT(11) NULL DEFAULT NULL,
  `usu_emp_clave` INT(11) NOT NULL,
  PRIMARY KEY (`usu_clave`),
  INDEX `fk_usuarios_empleados1_idx` (`usu_emp_clave` ASC),
  CONSTRAINT `fk_usuarios_empleados1`
    FOREIGN KEY (`usu_emp_clave`)
    REFERENCES `puntoventa`.`empleados` (`emp_clave`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_spanish_ci;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
